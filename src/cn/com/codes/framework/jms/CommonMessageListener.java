package cn.com.codes.framework.jms;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.NetworkInterface;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

import org.apache.log4j.Logger;
import org.eclipse.birt.report.listener.ViewerServletContextListener;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import cn.com.codes.common.SecurityContextHolderHelp;
import cn.com.codes.framework.app.blh.BusinessBlh;
import cn.com.codes.framework.app.services.BaseServiceImpl;
import cn.com.codes.framework.common.ResourceUtils;
import cn.com.codes.framework.common.config.PropertiesBean;
import cn.com.codes.framework.common.util.Context;
import cn.com.codes.framework.common.util.MypmBean;
import cn.com.codes.framework.hibernate.HibernateGenericController;
import cn.com.codes.framework.security.ButtonContainerService;
import cn.com.codes.framework.security.SysLog;
import cn.com.codes.msgManager.dto.MailBean;
import cn.com.codes.userManager.blh.PasswordTool;

/**
 * @author liuyg
 * 
 */
public class CommonMessageListener {

	private static Logger logger = Logger.getLogger(HibernateGenericController.class);
	
	HibernateGenericController hc = null;
	
	JavaMailSender mailSender;
	private static long reptViewCount=0L;
	private static long reptLimitCount =750+250;
	private static int sendInfo =0;
	private static String info= null;
	
	public CommonMessageListener(){
		//reptInit();
	}
	
	public void listener(Object obj) {
		if(obj instanceof SysLog ){
		}else{
			sendMail((MailBean)obj);
		}
	}



	private void sendMail(MailBean mailBean){
		if((mailBean.getAttachPhName()!=null&&mailBean.getAttachPhName().length>0)||mailBean.isMimeMail()){
			sendMimeMail(mailBean);
			return;
		}
		sendSimpleMail(mailBean);
	}
	
	private void sendSimpleMail(MailBean mailBean){
		if(mailBean.getRecip()==null||"".equals(mailBean.getRecip().trim())){
			return;
		}
		SimpleMailMessage mail = new SimpleMailMessage();  
		JavaMailSenderImpl sendImp = (JavaMailSenderImpl)mailSender;
		if(sendImp.getHost()==null||"".equals(sendImp.getHost())||sendImp.getUsername()==null||"".equals(sendImp.getUsername())||sendImp.getPassword()==null||"".equals(sendImp.getPassword())) {
			sendImp.setHost("smtp.163.com");
			sendImp.setPort(25);
			sendImp.setUsername("itestmailproxy@163.com");
			sendImp.setPassword("Itest9186");
		}
		mail.setFrom(sendImp.getUsername());  
		mail.setTo(mailBean.getRecip().split(";"));  
		if("smtp.163.com".equals(sendImp.getHost())) {
			mail.setBcc(sendImp.getUsername());
		}
		if("smtp.163.com".equals(sendImp.getHost())&&25==sendImp.getPort()) {
			if(sendImp.getJavaMailProperties().getProperty("mail.smtp.socketFactory.class")!=null) {
				sendImp.getJavaMailProperties().remove("mail.smtp.socketFactory.class");
			}
			
		}
		mail.setSubject(mailBean.getSubject());  
		mail.setText(mailBean.getMsg());  
		mailSender.send(mail);
		
		
	}
	private void sendMimeMail(MailBean mailBean){
		if(mailBean.getRecip()==null||"".equals(mailBean.getRecip().trim())){
			return;
		}
		MimeMessage mm  =mailSender.createMimeMessage();
		MimeMessageHelper mmh = null;
		JavaMailSenderImpl sendImp = (JavaMailSenderImpl)mailSender;
		if(sendImp.getHost()==null||"".equals(sendImp.getHost())||sendImp.getUsername()==null||"".equals(sendImp.getUsername())||sendImp.getPassword()==null||"".equals(sendImp.getPassword())) {
			sendImp.setHost("smtp.163.com");
			sendImp.setPort(25);
			sendImp.setUsername("itestmailproxy@163.com");
			sendImp.setPassword("Itest9186");
		}
		
		try {
			mmh = new MimeMessageHelper(mm,true,"utf-8");
			mmh.setSubject(mailBean.getSubject()) ;   
			mmh.setText("<html><head></head><body>"+mailBean.getMsg()+"</body></html>",true);  
			mmh.setFrom(sendImp.getUsername());
			mmh.setTo(mailBean.getRecip().split(";"));
			if("smtp.163.com".equals(sendImp.getHost())) {
				mmh.setBcc(sendImp.getUsername());
			}
			if("smtp.163.com".equals(sendImp.getHost())&&25==sendImp.getPort()) {
				if(sendImp.getJavaMailProperties().getProperty("mail.smtp.socketFactory.class")!=null) {
					sendImp.getJavaMailProperties().remove("mail.smtp.socketFactory.class");
				}
				
			}
			if(mailBean.getAttachPhName()!=null){
				String upDirectory = SecurityContextHolderHelp.getUpDirectory();
				for(String phName :mailBean.getAttachPhName()){
					URL url = ResourceUtils.getFileURL(upDirectory+File.separator+phName);
					File file = new File(url.getFile());
					if(phName.indexOf("/")==phName.lastIndexOf("/")){
						String flieName =MimeUtility.encodeWord(phName);
						mmh.addAttachment(flieName,file); 					
					}else{
						String flieName =MimeUtility.encodeWord(phName.substring(phName.lastIndexOf("/")));
						mmh.addAttachment(flieName,file); 	
					}			
				}
			}
			mailSender.send(mm);
			
		} catch (UnsupportedEncodingException e) {
			logger.error(e);
			//e.printStackTrace();
		} catch (MessagingException e) {
			logger.error(e);
			//e.printStackTrace();
		} 
	}	
	public void setHc(HibernateGenericController hc) {
		this.hc = hc;
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}
	public static long getReptViewCount() {
		return reptViewCount;
	}
	public static long getReptLimitCount() {
		return reptLimitCount;
	}


	public static void setReptViewCount(long reptViewCount) {
		CommonMessageListener.reptViewCount = reptViewCount;
	}


	public static void setReptLimitCount(long reptLimitCount) {
		CommonMessageListener.reptLimitCount = reptLimitCount;
	}
	

}