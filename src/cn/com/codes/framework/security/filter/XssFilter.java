package cn.com.codes.framework.security.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;


public class XssFilter implements Filter {

	FilterConfig filterConfig = null;
	private static boolean enableXss = false;
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
		if("true".equals(filterConfig.getInitParameter("enableXss"))){
			enableXss = true;
		}
	}
   
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		if (enableXss) {			
			chain.doFilter(new XssHttpServletRequestWrapper((HttpServletRequest)request), response);		
		}else {
			chain.doFilter(request, response);
		}
		
	}

	@Override
	public void destroy() {
		this.filterConfig = null;		
	}

}
