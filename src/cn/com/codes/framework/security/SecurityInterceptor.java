package cn.com.codes.framework.security;

import javax.servlet.ServletContext;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.log4j.Logger;
import org.springframework.web.context.ServletContextAware;

import cn.com.codes.framework.hibernate.HibernateGenericController;
import cn.com.codes.framework.security.filter.SecurityContextHolder;



public class SecurityInterceptor implements MethodInterceptor,
		ServletContextAware {

	private SecurityPrivilege securityPrivilege;
	private SecurityLog securityLog ;
	private ServletContext sc;
	private boolean isAuthentication = true;

	private boolean recordTime = false;
	
	public HibernateGenericController hibernateGenericController;
	
	private static Logger logger = Logger.getLogger(SecurityInterceptor.class);


	public Object invoke(MethodInvocation invoke) throws Throwable {

		Object result = null;
		return null;

	}

	private VisitUser getUser() {
		return SecurityContextHolder.getContext().getUserInfo();
	}


	public void setServletContext(ServletContext sc) {
		this.sc = sc;
		if ("false".equalsIgnoreCase(sc.getInitParameter("isAuthentication"))) {
			this.isAuthentication = false;
		}
		if ("true".equalsIgnoreCase(sc.getInitParameter("recordTime"))) {
			recordTime = true;
		}
	}

	public SecurityPrivilege getSecurityPrivilege() {
		return securityPrivilege;
	}

	public void setSecurityPrivilege(SecurityPrivilege securityPrivilege) {
		this.securityPrivilege = securityPrivilege;
	}

	public HibernateGenericController getHibernateGenericController() {
		return hibernateGenericController;
	}

	public void setHibernateGenericController(
			HibernateGenericController hibernateGenericController) {
		this.hibernateGenericController = hibernateGenericController;
	}

	public void setSecurityLog(SecurityLog securityLog) {
		this.securityLog = securityLog;
	}

}