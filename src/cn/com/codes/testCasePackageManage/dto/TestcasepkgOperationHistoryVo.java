/**
 * 
 */
package cn.com.codes.testCasePackageManage.dto;

import java.util.Date;

import cn.com.codes.framework.transmission.JsonInterface;


public class TestcasepkgOperationHistoryVo implements JsonInterface{
	
	private static final long serialVersionUID = 7666219762321844175L;
	
	private String id;
	private String taskId;
	private String proName;
	private String packageId;
	private Long testCaseId;
	private String operationPersonId; 
	private String operationType;
    private Date operationTime;
    private String testcaseDesc;
    private String operationPersonName;
    private String operationHistory;
    
    
	/**
	 * 
	 */
	public TestcasepkgOperationHistoryVo() {
		super();
		// TODO Auto-generated constructor stub
	}


	/**
	 * @param id
	 * @param taskId
	 * @param proName
	 * @param packageId
	 * @param testCaseId
	 * @param operationPersonId
	 * @param operationType
	 * @param operationTime
	 * @param testcaseDesc
	 * @param operationPersonName
	 * @param operationHistory
	 */
	public TestcasepkgOperationHistoryVo(String id, String taskId,
			String proName, String packageId, Long testCaseId,
			String operationPersonId, String operationType, Date operationTime,
			String testcaseDesc, String operationPersonName,
			String operationHistory) {
		super();
		this.id = id;
		this.taskId = taskId;
		this.proName = proName;
		this.packageId = packageId;
		this.testCaseId = testCaseId;
		this.operationPersonId = operationPersonId;
		this.operationType = operationType;
		this.operationTime = operationTime;
		this.testcaseDesc = testcaseDesc;
		this.operationPersonName = operationPersonName;
		this.operationHistory = operationHistory;
	}


	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}


	/**
	 * @return the taskId
	 */
	public String getTaskId() {
		return taskId;
	}


	/**
	 * @param taskId the taskId to set
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	/**
	 * @return the proName
	 */
	public String getProName() {
		return proName;
	}


	/**
	 * @param proName the proName to set
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}


	/**
	 * @return the packageId
	 */
	public String getPackageId() {
		return packageId;
	}


	/**
	 * @param packageId the packageId to set
	 */
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}


	/**
	 * @return the testCaseId
	 */
	public Long getTestCaseId() {
		return testCaseId;
	}


	/**
	 * @param testCaseId the testCaseId to set
	 */
	public void setTestCaseId(Long testCaseId) {
		this.testCaseId = testCaseId;
	}


	/**
	 * @return the operationPersonId
	 */
	public String getOperationPersonId() {
		return operationPersonId;
	}


	/**
	 * @param operationPersonId the operationPersonId to set
	 */
	public void setOperationPersonId(String operationPersonId) {
		this.operationPersonId = operationPersonId;
	}


	/**
	 * @return the operationType
	 */
	public String getOperationType() {
		return operationType;
	}


	/**
	 * @param operationType the operationType to set
	 */
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}


	/**
	 * @return the operationTime
	 */
	public Date getOperationTime() {
		return operationTime;
	}


	/**
	 * @param operationTime the operationTime to set
	 */
	public void setOperationTime(Date operationTime) {
		this.operationTime = operationTime;
	}


	/**
	 * @return the testcaseDesc
	 */
	public String getTestcaseDesc() {
		return testcaseDesc;
	}


	/**
	 * @param testcaseDesc the testcaseDesc to set
	 */
	public void setTestcaseDesc(String testcaseDesc) {
		this.testcaseDesc = testcaseDesc;
	}


	/**
	 * @return the operationPersonName
	 */
	public String getOperationPersonName() {
		return operationPersonName;
	}


	/**
	 * @param operationPersonName the operationPersonName to set
	 */
	public void setOperationPersonName(String operationPersonName) {
		this.operationPersonName = operationPersonName;
	}


	/**
	 * @return the operationHistory
	 */
	public String getOperationHistory() {
		return operationHistory;
	}


	/**
	 * @param operationHistory the operationHistory to set
	 */
	public void setOperationHistory(String operationHistory) {
		this.operationHistory = operationHistory;
	}


	@Override
	public String toStrUpdateInit() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String toStrList() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String toStrUpdateRest() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void toString(StringBuffer bf) {
		// TODO Auto-generated method stub
		
	}
    
    
  
}
