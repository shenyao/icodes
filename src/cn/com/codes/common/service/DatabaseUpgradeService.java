package cn.com.codes.common.service;

import cn.com.codes.framework.app.services.BaseService;

public interface DatabaseUpgradeService  extends BaseService {

	public int  exeUpgrade();
	
	public int  exeSql();
	
	public int  exeSql(String sql );
	public int  exeSqlNoPringLog(String sql ) ;
	public int deleteDemoData() ;
}
