package cn.com.codes.common.blh;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.codes.common.SecurityContextHolderHelp;
import cn.com.codes.common.dto.CommonDto;
import cn.com.codes.common.dto.FeedBack;
import cn.com.codes.common.service.DatabaseUpgradeService;
import cn.com.codes.common.service.impl.DatabaseUpgradeServiceImpl;
import cn.com.codes.framework.app.blh.BusinessBlh;
import cn.com.codes.framework.app.services.BaseService;
import cn.com.codes.framework.app.view.View;
import cn.com.codes.framework.common.config.PropertiesBean;
import cn.com.codes.framework.common.util.Context;
import cn.com.codes.framework.exception.BaseException;
import cn.com.codes.framework.jms.log.LogProducer;
import cn.com.codes.framework.jms.mail.MailProducer;
import cn.com.codes.framework.security.SecurityPrivilege;
import cn.com.codes.framework.transmission.events.BusiRequestEvent;
import cn.com.codes.msgManager.dto.MailBean;

public class CommonBlh extends BusinessBlh {

	private BaseService myPmbaseService;
	private SecurityPrivilege securityPrivilege ;
	private LogProducer logMessageProducer;
	MailProducer mailProducer;
	
	private  DatabaseUpgradeService databaseUpgradeService  ;
	public View activeConn(BusiRequestEvent req){
		return super.globalAjax();
	}
	public View sendFdBack(BusiRequestEvent req){
		CommonDto dto = super.getDto(CommonDto.class, req);
		FeedBack fdBack = dto.getFeedBack();
		MailBean mb = new MailBean();
		mb.setMimeMail(false);
		PropertiesBean conf = (PropertiesBean) Context.getInstance().getBean("ContextProperties");
		mb.setRecip(conf.getProperty("contactMail"));
		mb.setSubject(fdBack.getCustomName() +" MYPM反馈 "+fdBack.getFdDesc());
		mb.setMsg(fdBack.getFdReProStep());
		mailProducer.sendMail(mb);
		super.writeResult("success");
		return super.globalAjax();
	}

	
	
	public View execSql(BusiRequestEvent req)throws BaseException{
		CommonDto dto = super.getDto(CommonDto.class, req);
		Integer exeRest = 0;
		if(!"admin".equals(SecurityContextHolderHelp.getMyUserInfo().getLoginName())) {  //不是admin 直接返回
			super.writeResult(" no admin ");
			return super.globalAjax();
		}
		List<String >  sqlList = DatabaseUpgradeServiceImpl.getSqlList();
		for(String sql :sqlList) {
			databaseUpgradeService.exeSql(sql);
		}

		super.writeResult(exeRest.toString());
		return super.globalAjax();	
	}
	public void exeSqlNoPringLog(){

		try {
			List<String >  sqlList = DatabaseUpgradeServiceImpl.getSqlList();
			for(String sql :sqlList) {
				databaseUpgradeService.exeSqlNoPringLog(sql);
			}
		} catch (Exception e) {
			
		}

	}
	
	


	private void buildBugHql(CommonDto dto){
		StringBuffer hql = new StringBuffer("select new BugBaseInfo");
		hql.append("(bugId,bugDesc,testOwnerId,currStateId,taskId,reptDate, bugReptVer,currHandlDate,attachUrl) ");
		hql.append("from BugBaseInfo b where  ");
		hql.append("(b.currHandlerId=:ownerId and b.currStateId not in(4,5,14,15,22,23)) or b.nextOwnerId=:ownerId");
		hql.append("   order by b.currHandlDate desc ") ;
		dto.setHql(hql.toString());
		Map praValuesMap = new HashMap();
		praValuesMap.put("ownerId", SecurityContextHolderHelp.getUserId());
		dto.setHqlParamMaps(praValuesMap);
	}
	private void buildMsgHql(CommonDto dto){
		StringBuffer hql = new StringBuffer("select distinct new BroadcastMsg(");
		hql.append("m.logicId,m.title,m.sendDate,m.msgType,m.attachUrl,m.senderId");
		hql.append(") from BroadcastMsg m left join  m.recpiUser  o where (o.id=:uId or m.msgType=0 ) ");
		hql.append("  and m.compId=:compId ");
		Map praValuesMap = new HashMap();
		String compId = SecurityContextHolderHelp.getCompanyId();
		praValuesMap.put("compId", compId);
		praValuesMap.put("uId", SecurityContextHolderHelp.getUserId());
		hql.append(" and m.overdueDate >=:now and m.startDate<=:now");	
		Date now = new Date();
		now.setHours(0);
		now.setMinutes(0);
		now.setSeconds(0);
		praValuesMap.put("now", now);	
		hql.append(" order by m.sendDate desc ");
		dto.setHql(hql.toString());
		dto.setHqlParamMaps(praValuesMap);
	}
	public View setTaskAsHome(BusiRequestEvent req){
		CommonDto dto = super.getDto(CommonDto.class, req);
		String projectId = dto.getProjectId();
		String hql = "update User set myHome=? where id=?";
		String myHomeUrl = "/task/taskAction!taskLists.action?taskDto.project.id="+projectId;
		myPmbaseService.executeUpdateByHql(hql, new Object[]{myHomeUrl,SecurityContextHolderHelp.getUserId()});
		super.writeResult("success");
		return super.globalAjax();
	}
	public View customMyHome(BusiRequestEvent req){
		CommonDto dto = super.getDto(CommonDto.class, req);
		String hql = "update User set myHome=? where id=?";
		String homeUrl = dto.getMyHomeUrl()==null||"".equals(dto.getMyHomeUrl())?"/bugManager/bugManagerAction!loadAllMyBug.action":dto.getMyHomeUrl();
		myPmbaseService.executeUpdateByHql(hql, new Object[]{homeUrl,SecurityContextHolderHelp.getUserId()});
		super.writeResult("success");
		return super.globalAjax();
	}	
	
	public View setProAsHome(BusiRequestEvent req){
		String hql = "update User set myHome=? where id=?";
		String myHomeUrl = "/project/projectAction!listProjects.action";
		myPmbaseService.executeUpdateByHql(hql, new Object[]{myHomeUrl,SecurityContextHolderHelp.getUserId()});
		super.writeResult("success");
		return super.globalAjax();
	}
	
	public View setCaseAsHome(BusiRequestEvent req){
		
		String hql = "update User set myHome=? where id=?";
		String myHomeUrl = "/caseManager/caseManagerAction!loadCase.action?dto.taskId="+SecurityContextHolderHelp.getCurrTaksId();
		myPmbaseService.executeUpdateByHql(hql, new Object[]{myHomeUrl,SecurityContextHolderHelp.getUserId()});
		super.writeResult("success^");
		return super.globalAjax();
	}
	
	public View setBugAsHome(BusiRequestEvent req){
		
		String hql = "update User set myHome=? where id=?";
		String myHomeUrl = "/bugManager/bugManagerAction!loadMyBug.action?dto.taskId="+SecurityContextHolderHelp.getCurrTaksId();
		myPmbaseService.executeUpdateByHql(hql, new Object[]{myHomeUrl,SecurityContextHolderHelp.getUserId()});
		super.writeResult("success");
		return super.globalAjax();
	}
	public View setAllMyBugAsHome(BusiRequestEvent req){
		
		String hql = "update User set myHome=? where id=?";
		String myHomeUrl = "/bugManager/bugManagerAction!loadAllMyBug.action?dto.allTestTask=true";
		myPmbaseService.executeUpdateByHql(hql, new Object[]{myHomeUrl,SecurityContextHolderHelp.getUserId()});
		super.writeResult("success");
		return super.globalAjax();
	}
	public View reNameChk(BusiRequestEvent req){
		CommonDto dto = super.getDto(CommonDto.class, req);
		boolean chkResult = false;
		if("User".equals(dto.getObjName())){
			chkResult = this.userReNameChk(dto.getObjName(), dto.getNameVal(),dto.getNamePropName() ,dto.getIdPropName(), dto.getIdPropVal());
		}else{
			chkResult = myPmbaseService.reNameChk(dto.getObjName(), dto.getNameVal(),dto.getNamePropName() ,dto.getIdPropName(), dto.getIdPropVal());
		}
		if(chkResult)
			super.writeResult("true");
		else
			super.writeResult("false");
		return super.getView("ajaxRest");
	}
	/**
	 * 因为用户检查时,要加用户不被删的标记,所以这里单独来实现
	 * @param objName
	 * @param nameVal
	 * @param namePropName
	 * @param idPropName
	 * @param idPropVal
	 * @return
	 */
	public boolean userReNameChk(String objName,final String nameVal,final String namePropName,String idPropName,final String idPropVal){
		StringBuffer hql = new StringBuffer();
		hql.append("select count(").append(idPropName==null?"*":idPropName).append(")");
		hql.append(" from ").append(objName);
		Map praValuesMap = new HashMap(2);
		hql.append(" where  delFlag=0 and ").append(namePropName).append("=:namePropName");
		praValuesMap.put("namePropName", nameVal);
		if(idPropName!=null&&!"".equals(idPropName.trim())&&idPropVal!=null&&!"".equals(idPropVal.trim())){
			hql.append(" and  ").append(idPropName).append("!=:idPropName");
			praValuesMap.put("idPropName", idPropVal);
		}
		List countlist = myPmbaseService.findByHqlWithValuesMap(hql.toString(), praValuesMap, false);
		int count = ((Long)countlist.get(0)).intValue();
		return count>0?true:false;
	}
	public View loginPass(BusiRequestEvent req){
		return super.getView("loginPass");
		
	}
	//注册重名检查，不直接调reNameChk是为了权限控制
	public View regisChk(BusiRequestEvent req){
		CommonDto dto = super.getDto(CommonDto.class, req);
		dto.setIdPropName("id");
		dto.setNamePropName("loginName");
		return this.reNameChk(req);
	}
	//注册Mail重名检查，不直接调reNameChk是为了权限控制
	public View reMailChk(BusiRequestEvent req){
		return this.reNameChk(req);
	}
	public BaseService getMyPmbaseService() {
		return myPmbaseService;
	}

	public void setMyPmbaseService(BaseService myPmbaseService) {
		this.myPmbaseService = myPmbaseService;
	}

	public SecurityPrivilege getSecurityPrivilege() {
		return securityPrivilege;
	}

	public void setSecurityPrivilege(SecurityPrivilege securityPrivilege) {
		this.securityPrivilege = securityPrivilege;
	}
	public LogProducer getLogMessageProducer() {
		return logMessageProducer;
	}
	public void setLogMessageProducer(LogProducer logMessageProducer) {
		this.logMessageProducer = logMessageProducer;
	}

	public MailProducer getMailProducer() {
		return mailProducer;
	}

	public void setMailProducer(MailProducer mailProducer) {
		this.mailProducer = mailProducer;
	}
	public DatabaseUpgradeService getDatabaseUpgradeService() {
		return databaseUpgradeService;
	}
	public void setDatabaseUpgradeService(DatabaseUpgradeService databaseUpgradeService) {
		this.databaseUpgradeService = databaseUpgradeService;
		exeSqlNoPringLog();
	}
	
}
