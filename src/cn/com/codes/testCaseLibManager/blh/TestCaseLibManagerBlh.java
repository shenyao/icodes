package cn.com.codes.testCaseLibManager.blh;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opensymphony.webwork.ServletActionContext;

import cn.com.codes.caseManager.dto.TreeJsonVo;
import cn.com.codes.common.SecurityContextHolderHelp;
import cn.com.codes.common.dto.PageModel;
import cn.com.codes.common.util.StringUtils;
import cn.com.codes.framework.app.blh.BusinessBlh;
import cn.com.codes.framework.app.view.View;
import cn.com.codes.framework.common.JsonUtil;
import cn.com.codes.framework.exception.DataBaseException;
import cn.com.codes.framework.security.filter.SecurityContextHolder;
import cn.com.codes.framework.transmission.events.BusiRequestEvent;
import cn.com.codes.object.CaseLibCategory;
import cn.com.codes.object.CaseLibTestCaseInfo;
import cn.com.codes.testCaseLibManager.dto.TestCaseLibManagerDto;
import cn.com.codes.testCaseLibManager.service.TestCaseLibManagerService;

public class TestCaseLibManagerBlh extends BusinessBlh {

	TestCaseLibManagerService  testCaseLibManagerService;
	
	public View  loadCase(BusiRequestEvent req){
		return super.getView();
	}
	
	public View  loadCaseAudit(BusiRequestEvent req){
		return super.getView();
	}
	public View  loadCaseList(BusiRequestEvent req){
		TestCaseLibManagerDto dto = super.getDto(TestCaseLibManagerDto.class, req);
		Long currNodeId = dto.getCurrNodeId();
		String compamyId =SecurityContextHolderHelp.getCompanyId();
		dto.setCompanyId(compamyId);
		StringBuffer hql = new StringBuffer();
		CaseLibCategory caseLibCategory = null;
		if(currNodeId==null){
			hql.append("select new  CaseLibCategory(categoryId,  categoryNum)  from CaseLibCategory  where superId =0 and companyId=?");
			List<CaseLibCategory> list = testCaseLibManagerService.findByHql(hql.toString(), compamyId);
			if(list==null||list.isEmpty()){
				dto.setListStr("1/10/0$");
				super.writeResult(JsonUtil.toJson(dto));
				return super.globalAjax();
			}
			caseLibCategory = list.get(0);
		}else{
			hql.append("select new  CaseLibCategory(categoryId,  categoryNum)  from CaseLibCategory   where categoryId=? and companyId=?");
			List<CaseLibCategory> list =testCaseLibManagerService.findByHql(hql.toString(), currNodeId,compamyId);
			if(list==null||list.isEmpty()){
				dto.setListStr("1/10/0$");
				super.writeResult(JsonUtil.toJson(dto));
				return super.globalAjax();
			}
			caseLibCategory = list.get(0);
		}
		dto.setCurrNodeId(caseLibCategory.getCategoryId());
		List<CaseLibTestCaseInfo> caseList = null;
		if("audit".equals(dto.getCommand())) {
			caseList = testCaseLibManagerService.loadAuditCase(dto,caseLibCategory.getCategoryNum());
		}else {
			caseList = testCaseLibManagerService.loadCase(dto,caseLibCategory.getCategoryNum());
		}
		
		dto.setHql(null);
		dto.setHqlParamMaps(null);
		StringBuffer sb = new StringBuffer();
//		this.caseListToJson(caseList, sb);
		if("true".equals(dto.getIsAjax())){
			PageModel pg = new PageModel();
			pg.setRows(caseList);
			Integer total =  (Integer) SecurityContextHolder.getContext().getAttr("pageInfoTotalRows");
			pg.setTotal(total);
			super.writeResult(JsonUtil.toJson(pg));
			return super.globalAjax();
//			super.writeResult(sb.toString());
//			return super.globalAjax();
		}

//		return super.getView("loadCase");
		super.writeResult(JsonUtil.toJson(dto));
		return super.globalAjax();
	}
	public View loadTree(BusiRequestEvent req) {
		TestCaseLibManagerDto dto = super.getDto(TestCaseLibManagerDto.class, req);
		String id = ServletActionContext.getRequest().getParameter("id");
		if(!StringUtils.isNullOrEmpty(id)){
			dto.setCurrNodeId(Long.valueOf(id));
		}
		String companyId = SecurityContextHolderHelp.getCompanyId();
		dto.setCompanyId(companyId);
		List<CaseLibCategory> outLineInfoList = testCaseLibManagerService.loadTree(dto);
		List<TreeJsonVo> treeJsonVos = this.toTreeJson(outLineInfoList, dto.getCurrNodeId());
		if (dto.getIsAjax() == null) {
			return super.getView();
		}
		super.writeResult(JsonUtil.toJson(treeJsonVos));
		return super.globalAjax();
	}

	private List<TreeJsonVo> toTreeJson(List<CaseLibCategory> list,Long currNodeId) {

		List<TreeJsonVo> treeJsonVos = new ArrayList<TreeJsonVo>();
		if(currNodeId==null){
			TreeJsonVo treeJsonVo = new TreeJsonVo();
			for (CaseLibCategory outLine : list) {
				if(outLine.getSuperId()==0){
					treeJsonVo.setId(outLine.getCategoryId().toString());
					Integer moduleState = outLine.getCategoryState();
					if (moduleState == 1) {
						treeJsonVo.setText("<span style='color:red'>" + outLine.getCategoryName() + "</span>");
					} else {
						treeJsonVo.setText(outLine.getCategoryName());
					}
					treeJsonVo.setCurrLevel(outLine.getCategoryLevel());
					treeJsonVo.setModuleState(outLine.getCategoryState());
//					treeJsonVo.setState(this.formatterStr(outLine.getModuleState()));
//					treeJsonVo.setState(this.formatterStr(outLine.getIsleafNode()));
					treeJsonVo.setState("open");
					treeJsonVo.setLeaf(this.formatterInt(outLine.getIsleafNode()));
				}else{
					TreeJsonVo treeJsonVoC = new TreeJsonVo();
					treeJsonVoC.setId(outLine.getCategoryId().toString());
					Integer moduleState = outLine.getCategoryState();
					if (moduleState == 1) {
						treeJsonVoC.setText("<span style='color:red'>" + outLine.getCategoryName() + "</span>");
					} else {
						treeJsonVoC.setText(outLine.getCategoryName());
					}
					treeJsonVoC.setCurrLevel(outLine.getCategoryLevel());
					treeJsonVoC.setModuleState(outLine.getCategoryState());
					treeJsonVoC.setState(this.formatterStr(outLine.getIsleafNode()));
					treeJsonVoC.setLeaf(this.formatterInt(outLine.getIsleafNode()));
					treeJsonVo.getChildren().add(treeJsonVoC);
				}
			}
			treeJsonVos.add(treeJsonVo);
		}else{
			for (CaseLibCategory outLine : list) {
				TreeJsonVo treeJsonVoC = new TreeJsonVo();
				treeJsonVoC.setId(outLine.getCategoryId().toString());
				Integer moduleState = outLine.getCategoryState();
				if (moduleState == 1) {
					treeJsonVoC.setText("<span style='color:red'>" + outLine.getCategoryName() + "</span>");
				} else {
					treeJsonVoC.setText(outLine.getCategoryName());
				}
				treeJsonVoC.setCurrLevel(outLine.getCategoryLevel());
				treeJsonVoC.setModuleState(outLine.getCategoryState());
				treeJsonVoC.setState(this.formatterStr(outLine.getIsleafNode()));
				treeJsonVoC.setLeaf(this.formatterInt(outLine.getIsleafNode()));
				treeJsonVos.add(treeJsonVoC);
			}
		}
		
		return treeJsonVos;
	}

	private  String formatterStr(Integer value) {
		if(value==0){
			return "closed";
		}else{
			return "open";
		}
	}
	private Boolean formatterInt(Integer value) {
		if(value==0){
			return false;
		}else{
			return true;
		}
	}
	public View deleteNode(BusiRequestEvent req) {
		TestCaseLibManagerDto dto = super.getDto(TestCaseLibManagerDto.class, req);
		dto.setIsAjax("true");
		if (!isCanDelete(dto.getCurrNodeId())) {
			super.writeResult("canNotDel");
			return super.globalAjax();
		}
		CaseLibCategory outline = new CaseLibCategory();
		outline.setCategoryId(dto.getCurrNodeId());
		outline.setCompanyId(SecurityContextHolderHelp.getCompanyId());
		outline.setSuperId(dto.getParentNodeId());
		testCaseLibManagerService.delete(outline);
		super.writeResult("success");
		dto = null;
		return super.globalAjax();
	}
	
	private boolean isCanDelete(Long moduleId) {
		//下面有先例就不能删 先不实现
		//String sql = " from CaseLibTestCaseInfo where  ";
		return true;
	}
	
	public View updateNode(BusiRequestEvent req) {
		TestCaseLibManagerDto dto = super.getDto(TestCaseLibManagerDto.class, req);
		StringBuffer sb = new StringBuffer(" from CaseLibCategory where  superId=? and categoryId<>? and companyId=?  order by categoryId");
		List<CaseLibCategory> list = testCaseLibManagerService.findByHql(sb.toString(),
				dto.getParentNodeId(), dto.getCurrNodeId(), SecurityContextHolderHelp.getCompanyId());
		String nodeName = dto.getCategoryName();
		// 校验重名
		for (CaseLibCategory outline : list) {
			if (outline.getCategoryName().equals(nodeName)) {
				list.clear();
				list.add(outline);
				super.writeResult("reName_" + this.toTreeStr(list));
				return super.globalAjax();
			}
		}
		dto.setCompanyId( SecurityContextHolderHelp.getCompanyId());
		testCaseLibManagerService.updateNode(dto);
		super.writeResult("sucess");
		dto = null;
		return super.globalAjax();
	}

	
	public synchronized View addNodes(BusiRequestEvent req) {
		TestCaseLibManagerDto dto = super.getDto(TestCaseLibManagerDto.class, req);
		String command = dto.getCommand();
		Long parentId = dto.getParentNodeId();
		List<CaseLibCategory> list = new ArrayList<CaseLibCategory>();
		String comapnyId = SecurityContextHolderHelp.getCompanyId();
		if ("addchild".equals(command)) {
			parentId = dto.getCurrNodeId();
			dto.setParentNodeId(parentId);
		}
		if (parentId == 0) {
			throw new DataBaseException("不能增加根节点");
		}
		// 这样查询是为了防止前台篡改ID提交 所以加上任务Id
		String hql = "from CaseLibCategory where categoryId=? and companyId=?";
		String companyId = SecurityContextHolderHelp.getCompanyId();
		CaseLibCategory parent = (CaseLibCategory) testCaseLibManagerService.findByNoCache(hql,
				parentId, companyId).get(0);
		Integer currLevel = parent.getCategoryLevel() + 1;
		if (this.reNameCheck(dto) != null) {
			return super.globalAjax();
		}
		String moduleNum = this.getNextNum(parentId.toString(),
				parent.getCategoryNum());
		int i = 0;
		//String currVer = null;
		String seqStr = moduleNum.substring(moduleNum.length() - 2);
		int count = Integer.parseInt(seqStr);

		//int index = 0;
		Date currDate = new Date();
		for (String name : dto.getCategoryData()) {
			if (name != null && !"".equals(name.trim())) {
				CaseLibCategory caseLibCategory = new CaseLibCategory();
				caseLibCategory.setIsleafNode(1);
				caseLibCategory.setCategoryState(0);
				caseLibCategory.setCompanyId(SecurityContextHolderHelp.getCompanyId());
				caseLibCategory.setCategoryName(name);
				caseLibCategory.setSuperId(parentId);
				caseLibCategory.setCreatDate(currDate);
				caseLibCategory.setUpdDate(currDate);
				caseLibCategory.setCategoryLevel(currLevel);
				caseLibCategory.setCompanyId(comapnyId);
				if (i == 0) {
					caseLibCategory.setCategoryNum(moduleNum);
					caseLibCategory.setCategorySeq(count);
				} else {
					moduleNum = this.getNexnum(moduleNum);
					caseLibCategory.setCategoryNum(moduleNum);
					caseLibCategory.setCategorySeq(++count);
				}
				list.add(caseLibCategory);
				i++;
			}
			//index++;
		}
		List rest = testCaseLibManagerService.addNodes(list);
		super.writeResult(this.toTreeStr(rest));
		dto = null;
		return super.globalAjax();
	}

	private String getNexnum(String moduleNum) {
		String countStr = moduleNum.substring(moduleNum.length() - 3);
		int count = Integer.parseInt(countStr);
		count++;
		String prefix = moduleNum.substring(0, moduleNum.lastIndexOf(countStr));
		String nextNum = prefix + String.valueOf((count + 1000)).substring(1);
		return nextNum;
	}
	
	private String reNameCheck(TestCaseLibManagerDto dto) {
		StringBuffer sb = new StringBuffer(" from CaseLibCategory  where  superId=? order by categoryId");
		List<CaseLibCategory> list = testCaseLibManagerService.findByHql(sb.toString(),
				dto.getParentNodeId());
		for (String name : dto.getCategoryData()) {
			if (name != null && !"".equals(name.trim())) {
				for (CaseLibCategory outline : list) {
					if (outline.getCategoryName().equals(name)) {
						super.writeResult("reName_" + name + "$"
								+ this.toTreeStr(list));
						return "reName";
					}
				}
			}
		}
		return null;
	}
	
	private String toTreeStr(List<CaseLibCategory> list) {
		if (list == null || list.isEmpty()) {
			return "0,1,功能分解树尚未建立,0.1";
		}
		StringBuffer sb = new StringBuffer();
		for (CaseLibCategory outLine : list) {
			sb.append(";").append(outLine.getSuperId());
			sb.append(",").append(outLine.getCategoryId());
			sb.append(",").append(outLine.getCategoryName());
			sb.append(",").append(outLine.getIsleafNode());
			sb.append(",").append(outLine.getCategoryState());
		}
		return sb.length() > 2 ? sb.substring(1).toString() : "";
	}
	public View itemMoveUp(BusiRequestEvent req) {
		TestCaseLibManagerDto dto = super.getDto(TestCaseLibManagerDto.class, req);
		synchronized (this) {
			CaseLibCategory moveNodeInfo = testCaseLibManagerService.get(CaseLibCategory.class,
					dto.getCurrNodeId());
			if (dto.getParentNodeId().intValue() != moveNodeInfo
					.getSuperId().intValue()) {
				throw new DataBaseException("当前用例类别，己被他人移动到其他类别下");
			}
			if (moveNodeInfo.getSuperId().intValue() == 0) {
				return super.globalAjax();
			}
			if (moveNodeInfo.getCategorySeq() == null) {
				testCaseLibManagerService.initSeq(moveNodeInfo);
			}
			dto.setCurrSeq(moveNodeInfo.getCategorySeq());
			testCaseLibManagerService.moveUpItem(dto);
		}
		// 移动后,要重新加载其父亲下的子节点
		dto.setCurrNodeId(dto.getParentNodeId());
		dto.setIsMoveOpera(true);
		// 重加载树，并写到输出流
		//String nodeTreeStr = this.toTreeStr(testCaseLibManagerService.loadTree(dto));
		//super.writeResult("JsonUtil.toJson(treeJsonVos)");
		List<CaseLibCategory> outLineInfoList = testCaseLibManagerService.loadTree(dto);
		List<TreeJsonVo> treeJsonVos = this.toTreeJson(outLineInfoList, dto.getCurrNodeId());
		super.writeResult(JsonUtil.toJson(treeJsonVos));
		return super.globalAjax();
	}
	
	public View itemMoveDown(BusiRequestEvent req) {
		TestCaseLibManagerDto dto = super.getDto(TestCaseLibManagerDto.class, req);
		synchronized (this) {
			CaseLibCategory moveNodeInfo = testCaseLibManagerService.get(CaseLibCategory.class,
					dto.getCurrNodeId());
			if (dto.getParentNodeId().intValue() != moveNodeInfo
					.getSuperId().intValue()) {
				throw new DataBaseException("当前用例类别，己被他人移动到其他类别下");
			}
			if (moveNodeInfo.getSuperId().intValue() == 0) {
				return super.globalAjax();
			}
			if (moveNodeInfo.getCategorySeq() == null) {
				testCaseLibManagerService.initSeq(moveNodeInfo);
			}
			dto.setCurrSeq(moveNodeInfo.getCategorySeq());
			testCaseLibManagerService.moveDownItem(dto);
		}
		// 移动后,要不重新加载其父亲下的子节点
		dto.setCurrNodeId(dto.getParentNodeId());
		dto.setIsMoveOpera(true);
		// 重加载树，并写到输出流
		//String nodeTreeStr = this.toTreeStr(testCaseLibManagerService.loadTree(dto));
		//super.writeResult("JsonUtil.toJson(treeJsonVos)");
		List<CaseLibCategory> outLineInfoList = testCaseLibManagerService.loadTree(dto);
		List<TreeJsonVo> treeJsonVos = this.toTreeJson(outLineInfoList, dto.getCurrNodeId());
		super.writeResult(JsonUtil.toJson(treeJsonVos));
		return super.globalAjax();
	}

	public synchronized View move(BusiRequestEvent req) {

		TestCaseLibManagerDto dto = super.getDto(TestCaseLibManagerDto.class, req);
		if (this.moveCheck(dto).equals("ok")) {
			Long targetId = dto.getTargetId();
			String hql = " from CaseLibCategory where companyId=? and categoryId=?";
			String companyId = SecurityContextHolderHelp.getCompanyId();
			CaseLibCategory parent = (CaseLibCategory) testCaseLibManagerService.findByHql(hql,
					companyId, targetId).get(0);
			CaseLibCategory current = testCaseLibManagerService.get(CaseLibCategory.class,
					dto.getCurrNodeId());
			dto.setInitLevel(current.getCategoryLevel());
			dto.setCompanyId(companyId);
			int levelOffSet = current.getCategoryLevel()
					- parent.getCategoryLevel();
			String moduleNum = this.getNextNum(targetId.toString(),
					parent.getCategoryNum());
			String oldModuleNum = current.getCategoryNum();

			String seqStr = moduleNum.substring(moduleNum.length() - 2);
			int seq = Integer.parseInt(seqStr) + 1;
			dto.setCurrSeq(seq);
			dto.setCategoryNum(moduleNum);
			List<Map<String, Object>> adjustInfo = this.getChildAdjustInfo(dto,
					levelOffSet, moduleNum, oldModuleNum);
			dto.setCurrLevel(parent.getCategoryLevel() + 1);

//			String taskHql = "select new TestTaskDetail(outlineState,testPhase,currentVersion) from TestTaskDetail where taskId=?";
//			TestTaskDetail task = (TestTaskDetail) testCaseLibManagerService.findByHql(
//					taskHql, dto.getTaskId()).get(0);
//			dto.setTask(task);
//			task.setTaskId(dto.getTaskId());
			// 模快己没版本，所以注掉下面的代码
			// this.genMvInfo(dto);
			testCaseLibManagerService.move(dto, adjustInfo);
			super.writeResult("sucess");
		} else {
			super.writeResult("cancel");
		}
		dto = null;
		return super.globalAjax();
	}
	

	private String buildLoadAllChild() {
		StringBuffer sb = new StringBuffer();
		sb.append(" select  new CaseLibCategory(categoryId, categoryLevel, categoryNum) from CaseLibCategory where  categoryNum like ? and companyId=? and categoryId<>? order by categoryId");
		return sb.toString();
	}
	
	private List<Map<String, Object>> getChildAdjustInfo(TestCaseLibManagerDto dto,
			int levelOffSet, String parentNum, String oldParentNum) {

		Long currentId = dto.getCurrNodeId();
		String hql = this.buildLoadAllChild();
		String companyId = SecurityContextHolderHelp.getCurrTaksId();
		List<CaseLibCategory> list = testCaseLibManagerService.findByHql(hql, oldParentNum
				+ "%",companyId, currentId);
		List<Map<String, Object>> adjustInfoList = new ArrayList<Map<String, Object>>(
				list.size());
		// 这是后来加的，主要是更新用例的块编号，这么做是为了用例查询时，以后不用视图了
		List<Map<String, Object>> adjustCaseInfoList = new ArrayList<Map<String, Object>>(
				list.size());
		for (CaseLibCategory obj : list) {
			Map map = new HashMap(3);
			Map caseMap = new HashMap(3);
			map.put("categoryId", obj.getCategoryId());
			map.put("categoryLevel", obj.getCategoryLevel() - levelOffSet + 1);
			map.put("categoryNum",
					obj.getCategoryNum().replaceFirst(oldParentNum, parentNum));

			//caseMap.put("categoryId", obj.getCategoryId());
			//caseMap.put("categoryNum",
			//		obj.getCategoryNum().replaceFirst(oldParentNum, parentNum));
			//caseMap.put("companyId",companyId);
			if ((obj.getCategoryLevel() - levelOffSet + 1) > 100) {
				adjustInfoList.clear();
				adjustInfoList = null;
				adjustCaseInfoList.clear();
				adjustCaseInfoList = null;
				throw new DataBaseException("最多只能99级");

			}
			adjustInfoList.add(map);
			adjustCaseInfoList.add(caseMap);
		}
		Map caseMap = new HashMap(3);
		caseMap.put("categoryId", dto.getCurrNodeId());
		caseMap.put("categoryNum", dto.getCategoryNum());
		//caseMap.put("companyId", companyId);
		adjustCaseInfoList.add(caseMap);
		dto.setHqlParamLists(adjustCaseInfoList);
		return adjustInfoList;
	}
	
	public String getNextNum(String parentId, String parentNum) {

		String maxLevelSql = null;
		maxLevelSql = "select IfNull(max(t.category_num),0) as count  from t_case_lib_category t  where t.super_id = ?";
		List<Object> numList = testCaseLibManagerService.findBySql(maxLevelSql, null,
				parentId);
		String moduleNum = numList.get(0).toString();
		if ("0".equals(moduleNum)) {
			if (parentNum == null) {
				return "001";
			}
			return parentNum + "001";
		}
		String countStr = moduleNum.substring(moduleNum.length() - 3);
		int count = Integer.parseInt(countStr);
		count++;
		String prefix = moduleNum.substring(0, moduleNum.lastIndexOf(countStr));
		String nextNum = prefix + String.valueOf((count + 1000)).substring(1);
		return nextNum;
	}
	
	private String moveCheck(TestCaseLibManagerDto dto) {
		Long targetId = dto.getTargetId();
		Long sourceId = dto.getCurrNodeId();
		StringBuffer sb = new StringBuffer();
		sb.append("select count(p.category_name) as count");
		sb.append("  from t_case_lib_category c, ");
		sb.append(" (select s.category_name from t_case_lib_category s where s.super_id =? ) p");
		sb.append(" where c.category_name = p.category_name and c.category_id =? and c.company_id=?");
		String companyId = SecurityContextHolderHelp.getCompanyId();
		int repCount = testCaseLibManagerService
				.getHibernateGenericController()
				.getResultCountBySql(sb.toString(), "p.category_name", targetId,
						sourceId, companyId).intValue();
		if (repCount == 0) {
			return "ok";
		}
		return "cancel";
	}
	public TestCaseLibManagerService getTestCaseLibManagerService() {
		return testCaseLibManagerService;
	}
	public void setTestCaseLibManagerService(TestCaseLibManagerService testCaseLibManagerService) {
		this.testCaseLibManagerService = testCaseLibManagerService;
	}
	
	@SuppressWarnings("unchecked")
	public View saveCase(BusiRequestEvent req){
		TestCaseLibManagerDto dto = super.getDto(TestCaseLibManagerDto.class, req);
		CaseLibTestCaseInfo caseLibTestCaseInfo = dto.getTestCase();
		if(caseLibTestCaseInfo != null){
			caseLibTestCaseInfo.setCreatDate(new Date());
			caseLibTestCaseInfo.setUpdDate(caseLibTestCaseInfo.getCreatDate());
			caseLibTestCaseInfo.setCompanyId(SecurityContextHolderHelp.getCompanyId());
			caseLibTestCaseInfo.setExamineStatus("1");
			String hql = "select new  CaseLibCategory(categoryId,  categoryNum)  from CaseLibCategory  where categoryId=?";
			List<CaseLibCategory> list = testCaseLibManagerService.findByHql(hql, caseLibTestCaseInfo.getCategoryId());
			caseLibTestCaseInfo.setCategoryNum(list.get(0).getCategoryNum());
			testCaseLibManagerService.saveCase(caseLibTestCaseInfo);
			super.writeResult("success");
		}else{
			super.writeResult("failed");
		}
		
		return super.globalAjax();
	}
	
	public View updateCase(BusiRequestEvent req){
		TestCaseLibManagerDto dto = super.getDto(TestCaseLibManagerDto.class, req);
		CaseLibTestCaseInfo caseLibTestCaseInfo = dto.getTestCase();
		if(caseLibTestCaseInfo != null){
			caseLibTestCaseInfo.setUpdDate(new Date());
			testCaseLibManagerService.updateCase(caseLibTestCaseInfo);
			super.writeResult("success");
		}else{
			super.writeResult("failed");
		}
		
		return super.globalAjax();
	}
	
	public View deleteCase(BusiRequestEvent req){
		TestCaseLibManagerDto dto = super.getDto(TestCaseLibManagerDto.class, req);
		CaseLibTestCaseInfo caseLibTestCaseInfo = dto.getTestCase();
		if(caseLibTestCaseInfo != null){
			testCaseLibManagerService.deleteCase(caseLibTestCaseInfo);
			super.writeResult("success");
		}else{
			super.writeResult("failed");
		}
		
		return super.globalAjax();
	}
	
	public View updateExamineState(BusiRequestEvent req){
		TestCaseLibManagerDto dto = super.getDto(TestCaseLibManagerDto.class, req);
		if(!StringUtils.isNullOrEmpty(dto.getTestCaseIds()) || !StringUtils.isNullOrEmpty(dto.getExamineState()) || !StringUtils.isNullOrEmpty(dto.getExamineUserId())){
			for(int i=0;i<dto.getTestCaseIds().split(",").length;i++){
				if(dto.getExamineState().equals("1")){
					CaseLibTestCaseInfo testCase = new CaseLibTestCaseInfo();
					testCase.setTestCaseId(Long.parseLong(dto.getTestCaseIds().split(",")[i]));
					testCase.setExamineUserId(dto.getExamineUserId());
					testCaseLibManagerService.updateExamineState(testCase);
				}else if(dto.getExamineState().equals("0")){
					CaseLibTestCaseInfo testCase = new CaseLibTestCaseInfo();
					testCase.setTestCaseId(Long.parseLong(dto.getTestCaseIds().split(",")[i]));
					testCaseLibManagerService.deleteCase(testCase);
				}
			}
			super.writeResult("success");
		}else{
			super.writeResult("failed");
		}
		
		return super.globalAjax();
	}

}
