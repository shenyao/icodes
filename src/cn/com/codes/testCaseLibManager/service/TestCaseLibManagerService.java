package cn.com.codes.testCaseLibManager.service;

import java.util.List;
import java.util.Map;

import cn.com.codes.framework.app.services.BaseService;
import cn.com.codes.object.CaseLibCategory;
import cn.com.codes.object.CaseLibTestCaseInfo;
import cn.com.codes.testCaseLibManager.dto.TestCaseLibManagerDto;

public interface TestCaseLibManagerService  extends BaseService  {
	
	public List<CaseLibCategory> loadTree(TestCaseLibManagerDto dto);
	
	public List<CaseLibCategory> loadNormalNode(String companyId,Long currNodeId);
	
	public List addNodes(List<CaseLibCategory> list);
	
	
	public void updateNode(TestCaseLibManagerDto dto);
	
	public void switchState(TestCaseLibManagerDto dto);
	
	public void delete(CaseLibCategory caseLibCategory);
	
	public List loadCase(TestCaseLibManagerDto dto,String categoryNum);
	
	public void moveUpItem(TestCaseLibManagerDto dto);
	
	public void moveDownItem(TestCaseLibManagerDto dto);

	public void initSeq(CaseLibCategory caseLibCategory);
	
	public void move(TestCaseLibManagerDto dto,List<Map<String, Object>> adjustMapInfo);
	
	public List loadAuditCase(TestCaseLibManagerDto dto,String categoryNum);
	
	public void saveCase(CaseLibTestCaseInfo caseLibTestCaseInfo);
	
	public void updateCase(CaseLibTestCaseInfo caseLibTestCaseInfo);
	
	public void deleteCase(CaseLibTestCaseInfo caseLibTestCaseInfo);
	
	public void updateExamineState(CaseLibTestCaseInfo caseLibTestCaseInfo);
	
	public int saveImpotTestCase(List<CaseLibTestCaseInfo> list);
}
