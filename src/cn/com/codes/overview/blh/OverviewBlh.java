package cn.com.codes.overview.blh;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import cn.com.codes.common.SecurityContextHolderHelp;
import cn.com.codes.framework.app.blh.BusinessBlh;
import cn.com.codes.framework.app.view.View;
import cn.com.codes.framework.common.JsonUtil;
import cn.com.codes.framework.transmission.events.BusiRequestEvent;
import cn.com.codes.impExpManager.service.ImpExpManagerService;
import cn.com.codes.object.ConcernOtherMission;
import cn.com.codes.object.OtherMission;
import cn.com.codes.otherMission.service.OtherMissionService;
import cn.com.codes.overview.dto.DataVo;
import cn.com.codes.overview.dto.OverviewDto;

public class OverviewBlh extends BusinessBlh {

	private static Logger logger = Logger.getLogger(OverviewBlh.class);
	
	private ImpExpManagerService impExpManagerService;
	
	private OtherMissionService otherMissionService;
	
	/** 
	* 方法名:          loadInformation
	* 方法功能描述:    跳转到总览页面
	* @param:        
	* @return:        
	* @Author:          
	* @Create Date:   2018年9月14日 上午9:30:22
	*/
	public View loadInformation(BusiRequestEvent req){
		return super.getView();
	}
	
	/** 
	* 方法名:          getDetails
	* 方法功能描述:    得到所有统计信息
	* @param:        
	* @return:        
	* @Author:          
	* @Create Date:   2018年9月14日 上午11:04:06
	*/
	@SuppressWarnings({ "unchecked", "unused" })
	public View getDetails(BusiRequestEvent req){
		OverviewDto dto = super.getDto(OverviewDto.class, req);
		//得到用例统计信息
		DataVo dataVo = impExpManagerService.getCaseCount(dto.getTaskId());
		//得到bug统计信息
		impExpManagerService.getBugCount(dto.getTaskId(),dataVo);
		/*统计我创建的任务*/
		//OverviewDto overviewDto = new OverviewDto();
		this.buildOtherMissionListSql(dto);
		List<Map<String,Long>> countResult = otherMissionService.getJdbcTemplateWrapper().queryAllMatchListWithParaMap(dto.getHql(), null, null);

		dataVo.setMeCreate(countResult.get(0).get("createCount").intValue());
		/*统计我负责的任务*/
		dataVo.setMeCharge(countResult.get(0).get("chargeCount").intValue());
		/*统计我参与的任务*/

		dataVo.setMeJoin(countResult.get(0).get("joinCount").intValue());
		int concernCount  = countResult.get(0).get("concernCount").intValue();
		int all = concernCount+dataVo.getMeCharge()+dataVo.getMeCreate()+dataVo.getMeJoin();
		dataVo.setMeAll(all);
		
		super.writeResult(JsonUtil.toJson(dataVo));
		return super.globalAjax();
	}
	
	private void buildOtherMissionListSql(OverviewDto dto){
		String taskId = dto.getTaskId();
		String currUserId  = SecurityContextHolderHelp.getUserId();
		String currLoginName = SecurityContextHolderHelp.getLoginName();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT");
		sql.append("	*");
		sql.append("FROM");
		sql.append("	(");
		sql.append("		(");
		sql.append("			SELECT");
		sql.append("				count(om.mission_id) AS chargeCount");
		sql.append("			FROM");
		sql.append("				t_other_mission om");
		sql.append("			WHERE");
		sql.append("				om.project_id = '"+taskId+"'");
		sql.append("			AND om.charge_person_id = '"+currUserId+"'");
		sql.append("		) aa,");
		sql.append("		(");
		sql.append("			SELECT");
		sql.append("				count(om.mission_id) AS createCount");
		sql.append("			FROM");
		sql.append("				t_other_mission om");
		sql.append("			WHERE");
		sql.append("				om.project_id = '"+taskId+"'");
		sql.append("			AND om.create_user_id = '"+currLoginName+"'");
		sql.append("			AND om.charge_person_id != '"+currUserId+"'");
		sql.append("		) bb,");
		sql.append("		(");
		sql.append("			SELECT");
		sql.append("				count(*) AS concernCount");
		sql.append("			FROM");
		sql.append("				t_concern_other_mission com");
		sql.append("			JOIN t_other_mission om ON com.mission_id = om.mission_id");
		sql.append("			AND om.charge_person_id != '"+currUserId+"'");
		sql.append("			AND om.create_user_id != '"+currLoginName+"'");
		sql.append("			AND om.mission_id = '"+taskId+"'");
		sql.append("			WHERE");
		sql.append("				com.mission_id = '"+taskId+"'");
		sql.append("			AND com.user_id = '"+currUserId+"'");
		sql.append("		) cc,");
		sql.append("		(");
		sql.append("			SELECT");
		sql.append("				count(*) AS joinCount");
		sql.append("			FROM");
		sql.append("				t_user_other_mission um");
		sql.append("			JOIN t_other_mission om ON um.mission_id = om.mission_id");
		sql.append("			AND om.charge_person_id != '"+currUserId+"'");
		sql.append("			AND om.create_user_id != '"+currLoginName+"'");
		sql.append("			AND om.mission_id = '"+taskId+"'");
		sql.append("			WHERE");
		sql.append("				um.mission_id = '"+taskId+"'");
		sql.append("			AND um.user_id = '"+currUserId+"'");
		sql.append("		) dd");
		sql.append("	)");
		dto.setHql(sql.toString());
		
	}

	
	/** 
	* 方法名:          getIterationDetails
	* 方法功能描述:    得到迭代里的所有信息
	* @param:        
	* @return:        
	* @Author:          
	* @Create Date:   2019年1月8日 上午9:35:59
	*/
	public View getIterationDetails(BusiRequestEvent req){
		OverviewDto dto = super.getDto(OverviewDto.class, req);
		List<List<Map<String,Object>>> iterationDetails = new ArrayList<List<Map<String,Object>>>();
		/*得到用例信息*/
		String sql = "SELECT  count(ifnull(pkg_case.execStatus ,1))as countNum,"+
						" ifnull(pkg_case.execStatus ,1) as status"+
						" FROM t_iteration_testcasepackage_real it_pkg_rel"+
						" join t_testcase_casepkg pkg_case on it_pkg_rel.package_id=pkg_case.packageid"+
						" where   it_pkg_rel.iteration_id='"+dto.getIterationId()+"' "+
						" group by pkg_case.execStatus";
		List<Map<String,Object>> caseLists = otherMissionService.commonfindBySqlByJDBC(sql,false,new HashMap<String,Object>()); 
		if(caseLists != null && caseLists.size() > 0){
			iterationDetails.add(caseLists);
		}else{
			iterationDetails.add(new ArrayList<Map<String,Object>>());
		}
		/*得到bug信息*/
		String sql1 = "select * from ("+
				" (select count(*) as allcount from"+
				" (select bug.bugcardid from t_iteration_list it"+
				" join   t_bugbaseinfo bug on bug.task_id=it.task_id"+
				" and    it.iteration_id='"+dto.getIterationId()+"' "+
				" join  t_iteration_bug_real it_bug_real  on bug.bugcardid= it_bug_real.bug_card_id"+
				" ) as base ) allBug,"+
				" (select count(*) as validCout  from"+
				" (select bug.bugcardid from t_iteration_list it"+
				" join   t_bugbaseinfo bug on bug.task_id=it.task_id"+
				" and    it.iteration_id='"+dto.getIterationId()+"' "+
				" and    bug.CURRENT_STATE not in (2,3,4,5,22)"+
				" join  t_iteration_bug_real it_bug_real  on bug.bugcardid= it_bug_real.bug_card_id)as  base2)validBug"+
				" ,	"+
				" (select count(*)  as closedCount from"+
				" (select bug.bugcardid from t_iteration_list it"+
				" join   t_bugbaseinfo bug on bug.task_id=it.task_id"+
				" and    it.iteration_id='"+dto.getIterationId()+"' "+
				" and    bug.CURRENT_STATE  in (14,15,23)"+
				" join  t_iteration_bug_real it_bug_real  on bug.bugcardid= it_bug_real.bug_card_id) as base3) closeBug"+
				" ,"+
				" (select count(*) as fixCount   from"+
				" (select bug.bugcardid from t_iteration_list it"+
				" join   t_bugbaseinfo bug on bug.task_id=it.task_id"+
				" and    it.iteration_id='"+dto.getIterationId()+"' "+
				" and    (bug.CURRENT_STATE= 13 or bug.CURRENT_STATE= 26)"+
				" join  t_iteration_bug_real it_bug_real  on bug.bugcardid= it_bug_real.bug_card_id)as base4 )fixBug"+
				" ,	"+
				" (select count(*) as noBugCount  from"+
				" (select bug.bugcardid from t_iteration_list it"+
				" join   t_bugbaseinfo bug on bug.task_id=it.task_id"+
				" and    it.iteration_id='"+dto.getIterationId()+"' "+
				" and    bug.CURRENT_STATE=16"+
				" join  t_iteration_bug_real it_bug_real  on bug.bugcardid= it_bug_real.bug_card_id)as base5 ) noBug"+
				" )";
		List<Map<String,Object>> bugLists = otherMissionService.commonfindBySqlByJDBC(sql1,false,new HashMap<String,Object>());
		if(bugLists != null && bugLists.size() > 0){
			iterationDetails.add(bugLists);
		}else{
			iterationDetails.add(new ArrayList<Map<String,Object>>());
		}
		/*得到任务信息*/
		String sql2 = "SELECT count(om.status)as countNum ,om.status"+
				" FROM t_iteration_task_real it"+
				" join  t_other_mission om   on om.mission_id=it.mission_id"+
				" where it.iteration_id='"+dto.getIterationId()+"' "+
				" group by om.status ";
		List<Map<String,Object>> missionsLists = otherMissionService.commonfindBySqlByJDBC(sql2,false,new HashMap<String,Object>()); 
		if(missionsLists != null && missionsLists.size() > 0){
			iterationDetails.add(missionsLists);
		}else{
			iterationDetails.add(new ArrayList<Map<String,Object>>());
		}
		super.writeResult(JsonUtil.toJson(iterationDetails));
		return super.globalAjax();
	}
	
	public ImpExpManagerService getImpExpManagerService() {
		return impExpManagerService;
	}

	public void setImpExpManagerService(ImpExpManagerService impExpManagerService) {
		this.impExpManagerService = impExpManagerService;
	}

	public OtherMissionService getOtherMissionService() {
		return otherMissionService;
	}

	public void setOtherMissionService(OtherMissionService otherMissionService) {
		this.otherMissionService = otherMissionService;
	}
}
