package cn.com.codes.object;

import cn.com.codes.framework.transmission.JsonInterface;

public class Cert implements  JsonInterface {
	
	private static final long serialVersionUID = 1L;
	
	private String id;
	
	private String serverIp;
	
	private String certName;
	
	private String apiCertName;
	
	private String certPwd;
	
	private String uploadDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getServerIp() {
		return serverIp;
	}

	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}

	public String getCertName() {
		return certName;
	}

	public void setCertName(String certName) {
		this.certName = certName;
	}

	public String getApiCertName() {
		return apiCertName;
	}

	public void setApiCertName(String apiCertName) {
		this.apiCertName = apiCertName;
	}

	public String getCertPwd() {
		return certPwd;
	}

	public void setCertPwd(String certPwd) {
		this.certPwd = certPwd;
	}

	public String getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}

	@Override
	public String toStrUpdateInit() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toStrList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toStrUpdateRest() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void toString(StringBuffer bf) {
		// TODO Auto-generated method stub
		
	}
	
	
}
