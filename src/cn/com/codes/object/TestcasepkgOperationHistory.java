package cn.com.codes.object;


import java.io.Serializable;
import java.util.Date;

public class TestcasepkgOperationHistory implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String taskId;
	private String packageId;
	private Long testCaseId;
	private String operationPersonId; 
	private String operationType;
    private Date operationTime;
    private String testcaseDesc;
    private String operationPersonName;
    private String operationHistory;
    private String operationDesc;
	
	/**  
	* <p>Title: </p> 
	* <p>Description: </p>  
	*/
	public TestcasepkgOperationHistory() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	/**
	 * @param id
	 * @param taskId
	 * @param packageId
	 * @param testCaseId
	 * @param operationPersonId
	 * @param operationType
	 * @param operationTime
	 * @param testcaseDesc
	 * @param operationPersonName
	 * @param operationHistory
	 */
	public TestcasepkgOperationHistory(String id, String taskId,
			String packageId, Long testCaseId, String operationPersonId,
			String operationType, Date operationTime, String testcaseDesc,
			String operationPersonName, String operationHistory) {
		super();
		this.id = id;
		this.taskId = taskId;
		this.packageId = packageId;
		this.testCaseId = testCaseId;
		this.operationPersonId = operationPersonId;
		this.operationType = operationType;
		this.operationTime = operationTime;
		this.testcaseDesc = testcaseDesc;
		this.operationPersonName = operationPersonName;
		this.operationHistory = operationHistory;
	}



	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the taskId
	 */
	public String getTaskId() {
		return taskId;
	}

	/**
	 * @param taskId the taskId to set
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	/**
	 * @return the packageId
	 */
	public String getPackageId() {
		return packageId;
	}

	/**
	 * @param packageId the packageId to set
	 */
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	/**
	 * @return the testCaseId
	 */
	public Long getTestCaseId() {
		return testCaseId;
	}

	/**
	 * @param testCaseId the testCaseId to set
	 */
	public void setTestCaseId(Long testCaseId) {
		this.testCaseId = testCaseId;
	}

	/**
	 * @return the operationPersonId
	 */
	public String getOperationPersonId() {
		return operationPersonId;
	}

	/**
	 * @param operationPersonId the operationPersonId to set
	 */
	public void setOperationPersonId(String operationPersonId) {
		this.operationPersonId = operationPersonId;
	}

	/**
	 * @return the operationType
	 */
	public String getOperationType() {
		return operationType;
	}

	/**
	 * @param operationType the operationType to set
	 */
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	/**
	 * @return the operationTime
	 */
	public Date getOperationTime() {
		return operationTime;
	}

	/**
	 * @param operationTime the operationTime to set
	 */
	public void setOperationTime(Date operationTime) {
		this.operationTime = operationTime;
	}

	/**
	 * @return the testcaseDesc
	 */
	public String getTestcaseDesc() {
		return testcaseDesc;
	}

	/**
	 * @param testcaseDesc the testcaseDesc to set
	 */
	public void setTestcaseDesc(String testcaseDesc) {
		this.testcaseDesc = testcaseDesc;
	}

	/**
	 * @return the operationPersonName
	 */
	public String getOperationPersonName() {
		return operationPersonName;
	}

	/**
	 * @param operationPersonName the operationPersonName to set
	 */
	public void setOperationPersonName(String operationPersonName) {
		this.operationPersonName = operationPersonName;
	}

	/**
	 * @return the operationHistory
	 */
	public String getOperationHistory() {
		return operationHistory;
	}

	/**
	 * @param operationHistory the operationHistory to set
	 */
	public void setOperationHistory(String operationHistory) {
		this.operationHistory = operationHistory;
	}



	/**
	 * @return the operationDesc
	 */
	public String getOperationDesc() {
		return operationDesc;
	}



	/**
	 * @param operationDesc the operationDesc to set
	 */
	public void setOperationDesc(String operationDesc) {
		this.operationDesc = operationDesc;
	}
	
	
	
}
