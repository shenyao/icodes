package cn.com.codes.object;

import java.io.Serializable;
import java.util.Date;

public class CaseLibTestCaseInfo implements Serializable {


	private static final long serialVersionUID = 1L;
	private Long testCaseId;
	private Long categoryId;
	private String categoryNum;
	private String createrId;
	private String testCaseDes;
	private String operData;
	private String expResult;
	private Integer weight;
	private Date creatDate;
	private Date  updDate;
	private String remark;
	private String recommendUserId;
	private String recommendReason;
	private String examineStatus;
	private String examineUserId;
	private String companyId;
	private String baselineId;
	private Long caseType;
	private Long casePri;
	
	public CaseLibTestCaseInfo() {
		
	}
	public CaseLibTestCaseInfo(Long testCaseId,String testCaseDes, String operData,String expResult,Integer weight ,String remark,Long caseType,Long casePri) {
		this.testCaseId = testCaseId;
		this.testCaseDes = testCaseDes;
		this.operData = operData;
		this.expResult = expResult;
		this.remark = remark;
		this.caseType = caseType;
		this.casePri = casePri;
	}
	
	public CaseLibTestCaseInfo(Long testCaseId,String testCaseDes, String operData,String expResult,Integer weight ,String remark,String recommendReason,String recommendUserId,Long caseType,Long casePri) {
		this.testCaseId = testCaseId;
		this.testCaseDes = testCaseDes;
		this.operData = operData;
		this.expResult = expResult;
		this.remark = remark;
		this.recommendReason = recommendReason;
		this.recommendUserId = recommendUserId;
		this.caseType = caseType;
		this.casePri = casePri;
	}
		
	public Long getTestCaseId() {
		return testCaseId;
	}
	public void setTestCaseId(Long testCaseId) {
		this.testCaseId = testCaseId;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryNum() {
		return categoryNum;
	}
	public void setCategoryNum(String categoryNum) {
		this.categoryNum = categoryNum;
	}
	public String getCreaterId() {
		return createrId;
	}
	public void setCreaterId(String createrId) {
		this.createrId = createrId;
	}
	public String getTestCaseDes() {
		return testCaseDes;
	}
	public void setTestCaseDes(String testCaseDes) {
		this.testCaseDes = testCaseDes;
	}
	public String getOperData() {
		return operData;
	}
	public void setOperData(String operData) {
		this.operData = operData;
	}
	public String getExpResult() {
		return expResult;
	}
	public void setExpResult(String expResult) {
		this.expResult = expResult;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	public Date getCreatDate() {
		return creatDate;
	}
	public void setCreatDate(Date creatDate) {
		this.creatDate = creatDate;
	}
	public Date getUpdDate() {
		return updDate;
	}
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getRecommendUserId() {
		return recommendUserId;
	}
	public void setRecommendUserId(String recommendUserId) {
		this.recommendUserId = recommendUserId;
	}
	public String getRecommendReason() {
		return recommendReason;
	}
	public void setRecommendReason(String recommendReason) {
		this.recommendReason = recommendReason;
	}
	public String getExamineStatus() {
		return examineStatus;
	}
	public void setExamineStatus(String examineStatus) {
		this.examineStatus = examineStatus;
	}
	public String getExamineUserId() {
		return examineUserId;
	}
	public void setExamineUserId(String examineUserId) {
		this.examineUserId = examineUserId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getBaselineId() {
		return baselineId;
	}
	public void setBaselineId(String baselineId) {
		this.baselineId = baselineId;
	}
	public Long getCaseType() {
		return caseType;
	}
	public void setCaseType(Long caseType) {
		this.caseType = caseType;
	}
	public Long getCasePri() {
		return casePri;
	}
	public void setCasePri(Long casePri) {
		this.casePri = casePri;
	}

}
