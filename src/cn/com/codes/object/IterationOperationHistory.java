package cn.com.codes.object;


import java.util.Date;

import cn.com.codes.framework.transmission.JsonInterface;


public class IterationOperationHistory implements JsonInterface {
	
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String taskId;
	private String missionId;
	private String packageId;
	private String bugId;
	private String userId; 
	private String name;
	private String operationType;
    private Date operationTime;
    private String iterationId;
    private String bugDesc;
    private String operationPersonName;
    private String operationHistory;
	
	/**  
	* <p>Title: </p> 
	* <p>Description: </p>  
	*/
	public IterationOperationHistory() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**  
	* <p>Title: </p> 
	* <p>Description: </p> 
	* @param id
	* @param taskId
	* @param missionId
	* @param packageId
	* @param bugId
	* @param userId
	* @param name
	* @param operationType
	* @param operationTime 
	*/
	public IterationOperationHistory(String id, String taskId,
			String missionId, String packageId, String bugId, String userId,
			String name, String operationType, Date operationTime,String iterationId,
			String bugDesc,String operationPersonName,String operationHistory) {
		super();
		this.id = id;
		this.taskId = taskId;
		this.missionId = missionId;
		this.packageId = packageId;
		this.bugId = bugId;
		this.userId = userId;
		this.name = name;
		this.operationType = operationType;
		this.operationTime = operationTime;
		this.iterationId = iterationId;
		this.bugDesc = bugDesc;
		this.operationPersonName = operationPersonName;
		this.operationHistory = operationHistory;
	}

	/**  
	 * @return id 
	 */
	public String getId() {
		return id;
	}

	/**  
	 * @param id id 
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**  
	 * @return taskId 
	 */
	public String getTaskId() {
		return taskId;
	}

	/**  
	 * @param taskId taskId 
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	/**  
	 * @return missionId 
	 */
	public String getMissionId() {
		return missionId;
	}

	/**  
	 * @param missionId missionId 
	 */
	public void setMissionId(String missionId) {
		this.missionId = missionId;
	}

	/**  
	 * @return packageId 
	 */
	public String getPackageId() {
		return packageId;
	}

	/**  
	 * @param packageId packageId 
	 */
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	/**  
	 * @return bugId 
	 */
	public String getBugId() {
		return bugId;
	}

	/**  
	 * @param bugId bugId 
	 */
	public void setBugId(String bugId) {
		this.bugId = bugId;
	}

	/**  
	 * @return userId 
	 */
	public String getUserId() {
		return userId;
	}

	/**  
	 * @param userId userId 
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**  
	 * @return name 
	 */
	public String getName() {
		return name;
	}

	/**  
	 * @param name name 
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**  
	 * @return operationType 
	 */
	public String getOperationType() {
		return operationType;
	}

	/**  
	 * @param operationType operationType 
	 */
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	/**  
	 * @return operationTime 
	 */
	public Date getOperationTime() {
		return operationTime;
	}

	/**  
	 * @param date operationTime 
	 */
	public void setOperationTime(Date date) {
		this.operationTime = date;
	}



	@Override
	public String toStrUpdateInit() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String toStrList() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String toStrUpdateRest() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void toString(StringBuffer bf) {
		// TODO Auto-generated method stub
		
	}

	/**  
	* @return iterationId 
	*/
	public String getIterationId() {
		return iterationId;
	}

	/**  
	* @param iterationId iterationId 
	*/
	public void setIterationId(String iterationId) {
		this.iterationId = iterationId;
	}

	/**  
	* @return bugDesc 
	*/
	public String getBugDesc() {
		return bugDesc;
	}

	/**  
	* @param bugDesc bugDesc 
	*/
	public void setBugDesc(String bugDesc) {
		this.bugDesc = bugDesc;
	}

	/**  
	* @return operationPersonName 
	*/
	public String getOperationPersonName() {
		return operationPersonName;
	}

	/**  
	* @param operationPersonName operationPersonName 
	*/
	public void setOperationPersonName(String operationPersonName) {
		this.operationPersonName = operationPersonName;
	}

	/**  
	* @return operationHistory 
	*/
	public String getOperationHistory() {
		return operationHistory;
	}

	/**  
	* @param operationHistory operationHistory 
	*/
	public void setOperationHistory(String operationHistory) {
		this.operationHistory = operationHistory;
	}
	
}
