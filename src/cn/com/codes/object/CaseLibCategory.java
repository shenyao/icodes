package cn.com.codes.object;

import java.io.Serializable;
import java.util.Date;

public class CaseLibCategory implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long categoryId;
	private String categoryName;
	private Integer categoryLevel;
	private Integer isleafNode;
	private Integer categoryState ;
	private Long superId;
	private String createrId;
	private String companyId;
	private String categoryNum;
	private Integer categorySeq;
	private Date creatDate;
	private Date updDate;

	// 导入时要用这两属性 
	private String superCategoryName;
	private String superCategoryNum;
	
	public CaseLibCategory() {
		
	}
	public CaseLibCategory(Long categoryId, String categoryNum) {
		this.categoryId = categoryId;
		this.categoryNum = categoryNum;
	}
	
	public CaseLibCategory(Long categoryId, Integer categoryLevel, String categoryNum) {
		this.categoryId = categoryId;
		this.categoryLevel = categoryLevel;
		this.categoryNum = categoryNum;
	}
	public CaseLibCategory(Long categoryId , Long superId,Integer isleafNode, String categoryName,Integer categoryState,Integer categoryLevel,String categoryNum){
		this.categoryId = categoryId;
		this.superId = superId;
		this.isleafNode  = isleafNode;
		this.categoryName =categoryName;
		this.categoryState = categoryState ;
		this.categoryLevel =categoryLevel;
		this.categoryNum  = categoryNum;
		
	}
	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Integer getCategoryLevel() {
		return categoryLevel;
	}

	public void setCategoryLevel(Integer categoryLevel) {
		this.categoryLevel = categoryLevel;
	}

	public Integer getIsleafNode() {
		return isleafNode;
	}

	public void setIsleafNode(Integer isleafNode) {
		this.isleafNode = isleafNode;
	}

	public Long getSuperId() {
		return superId;
	}

	public void setSuperId(Long superId) {
		this.superId = superId;
	}

	public String getCreaterId() {
		return createrId;
	}

	public void setCreaterId(String createrId) {
		this.createrId = createrId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getCategoryNum() {
		return categoryNum;
	}

	public void setCategoryNum(String categoryNum) {
		this.categoryNum = categoryNum;
	}

	public Integer getCategorySeq() {
		return categorySeq;
	}

	public void setCategorySeq(Integer categorySeq) {
		this.categorySeq = categorySeq;
	}

	public Date getCreatDate() {
		return creatDate;
	}

	public void setCreatDate(Date creatDate) {
		this.creatDate = creatDate;
	}

	public Date getUpdDate() {
		return updDate;
	}

	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}

	public Integer getCategoryState() {
		return categoryState;
	}

	public void setCategoryState(Integer categoryState) {
		this.categoryState = categoryState;
	}
	public String getSuperCategoryName() {
		return superCategoryName;
	}
	public void setSuperCategoryName(String superCategoryName) {
		this.superCategoryName = superCategoryName;
	}
	public String getSuperCategoryNum() {
		return superCategoryNum;
	}
	public void setSuperCategoryNum(String superCategoryNum) {
		this.superCategoryNum = superCategoryNum;
	}
}
