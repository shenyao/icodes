package cn.com.codes.bugManager.dto;

import java.util.List;

import cn.com.codes.framework.common.ListObject;

public class ControlInfo {

	private List<ListObject> optnalStateList;
	private String fixFlwName;
	private boolean viewFlag;
	private String staFlwMemStr;

	public List<ListObject> getOptnalStateList() {
		return optnalStateList;
	}

	public void setOptnalStateList(List<ListObject> optnalStateList) {
		this.optnalStateList = optnalStateList;
	}

	public boolean isViewFlag() {
		return viewFlag;
	}

	public void setViewFlag(boolean viewFlag) {
		this.viewFlag = viewFlag;
	}

	public String getStaFlwMemStr() {
		return staFlwMemStr;
	}

	public void setStaFlwMemStr(String staFlwMemStr) {
		this.staFlwMemStr = staFlwMemStr;
	}

	public String getFixFlwName() {
		return fixFlwName;
	}

	public void setFixFlwName(String fixFlwName) {
		this.fixFlwName = fixFlwName;
	}

}
