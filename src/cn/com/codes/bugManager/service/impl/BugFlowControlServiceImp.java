package cn.com.codes.bugManager.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.codes.bugManager.blh.BugFlowConst;
import cn.com.codes.bugManager.dto.BugManagerDto;
import cn.com.codes.bugManager.dto.ControlInfo;
import cn.com.codes.bugManager.service.BugCommonService;
import cn.com.codes.bugManager.service.BugFlowControlService;
import cn.com.codes.common.SecurityContextHolderHelp;
import cn.com.codes.common.service.DrawHtmlListDateService;
import cn.com.codes.framework.app.services.BaseServiceImpl;
import cn.com.codes.framework.common.HtmlListComponent;
import cn.com.codes.framework.common.ListObject;
import cn.com.codes.framework.exception.DataBaseException;
import cn.com.codes.object.BugBaseInfo;
import cn.com.codes.object.TypeDefine;
import cn.com.codes.object.User;
import cn.com.codes.testTaskManager.dto.CurrTaskInfo;
import cn.com.codes.testTaskManager.service.TestTaskDetailService;

public class BugFlowControlServiceImp extends BaseServiceImpl implements BugFlowControlService {

	private BugCommonService bugCommonService;
	private TestTaskDetailService testTaskService ;
	private DrawHtmlListDateService drawHtmlListDateService;

	public void upInitContl(CurrTaskInfo currTaskInfo,BugBaseInfo bug){
		Integer nextFlowCd = bug.getNextFlowCd();
		String hql="select new User(id,name ,loginName) from User where id in (:ids)";
		Map praValues = new HashMap(1);
		String myId = SecurityContextHolderHelp.getUserId();
		List<User> nextFlowUser = null;
		if((nextFlowCd==1||nextFlowCd==8)&&(bug.getCurrHandlerId().equals(myId)
				||currTaskInfo.getRoleInTask().indexOf("8")>=0)){
			praValues.put("ids", this.strArr2List(currTaskInfo.getTestAndLdStr().split(",")));
			nextFlowUser = this.findByHqlWithValuesMap(hql, praValues, false);
			List<ListObject> actorList = drawHtmlListDateService.convertUser2ListObj(nextFlowUser);
			bug.setTestSelStr(HtmlListComponent.toSelectStr(actorList, "$"));
		}else if(nextFlowCd==2&&(bug.getCurrHandlerId().equals(myId)
				||currTaskInfo.getRoleInTask().indexOf("8")>=0)){
			praValues.put("ids", this.strArr2List(currTaskInfo.getTestChkAndLdStr().split(",")));
			nextFlowUser = this.findByHqlWithValuesMap(hql, praValues, false);
			List<ListObject> actorList = drawHtmlListDateService.convertUser2ListObj(nextFlowUser);
			bug.setTestSelStr(HtmlListComponent.toSelectStr(actorList, "$"));
		}else if(nextFlowCd==3&&bug.getCurrHandlerId().equals(myId)){
			praValues.put("ids", this.strArr2List(currTaskInfo.getAnalysIdStr().split(",")));
			nextFlowUser = this.findByHqlWithValuesMap(hql, praValues, false);
			List<ListObject> actorList = drawHtmlListDateService.convertUser2ListObj(nextFlowUser);
			bug.setAnalySelStr(HtmlListComponent.toSelectStr(actorList, "$"));
		}else if(nextFlowCd==4&&bug.getCurrHandlerId().equals(myId)){
			praValues.put("ids", this.strArr2List(currTaskInfo.getAssinIdStr().split(",")));
			nextFlowUser = this.findByHqlWithValuesMap(hql, praValues, false);
			List<ListObject> actorList = drawHtmlListDateService.convertUser2ListObj(nextFlowUser);
			bug.setAssignSelStr(HtmlListComponent.toSelectStr(actorList, "$"));
		}else if(nextFlowCd==5&&bug.getCurrHandlerId().equals(myId)){
			praValues.put("ids", this.strArr2List(currTaskInfo.getDevIdStr().split(",")));
			nextFlowUser = this.findByHqlWithValuesMap(hql, praValues, false);
			List<ListObject> actorList = drawHtmlListDateService.convertUser2ListObj(nextFlowUser);
			bug.setDevStr(HtmlListComponent.toSelectStr(actorList, "$"));
		}else if(nextFlowCd==6&&bug.getCurrHandlerId().equals(myId)){
			praValues.put("ids", this.strArr2List(currTaskInfo.getDevChkIdStr().split(",")));
			nextFlowUser = this.findByHqlWithValuesMap(hql, praValues, false);
			List<ListObject> actorList = drawHtmlListDateService.convertUser2ListObj(nextFlowUser);
			bug.setDevStr(HtmlListComponent.toSelectStr(actorList, "$"));
		}else if(nextFlowCd==7&&bug.getCurrHandlerId().equals(myId)){
			praValues.put("ids", this.strArr2List(currTaskInfo.getIntercsIdStr().split(",")));
			nextFlowUser = this.findByHqlWithValuesMap(hql, praValues, false);
			List<ListObject> actorList = drawHtmlListDateService.convertUser2ListObj(nextFlowUser);
			bug.setInterCesSelStr(HtmlListComponent.toSelectStr(actorList, "$"));
		}
	
	}


	public void bugHand(BugManagerDto dto){
		
	}
	private void loadRelaData(BugManagerDto dto,ControlInfo controlInfo,CurrTaskInfo currTaskInfo){
		bugCommonService.setRelaType(dto.getBug());
		bugCommonService.setActorListData(currTaskInfo, dto.getBug());
		if(dto.getLoadType()==1&&controlInfo!=null&&!controlInfo.isViewFlag()){
			List<TypeDefine> typeList = bugCommonService.getBugListData();
			dto.setTypeList(typeList);		
		}
		bugCommonService.initBugListDate(dto);
		dto.setModuleName(bugCommonService.getMdPathName(dto.getBug().getModuleId(),""));
		this.setCurrOwner(dto);
	}

	private void setCurrOwner(BugManagerDto dto){
		BugBaseInfo bug = dto.getBug();
		if(bug.getCurrStateId()==4||bug.getCurrStateId()==5||bug.getCurrStateId()==14
				||bug.getCurrStateId()==15||bug.getCurrStateId()==22||bug.getCurrStateId()==23
				){
			StringBuffer sb  = new StringBuffer("<font color=\"blue\">处理人</font>:");
			sb.append(bug.getCurrHander().getName()).append("(").append(bug.getCurrHander().getLoginName()).append(")");
			dto.setCurrOwner(sb.toString());
		}
		StringBuffer sb  = new StringBuffer(" 	<font color=\"blue\">等待"); 
		if(bug.getNextFlowCd()==1&&!bug.getCurrHandlerId().equals(bug.getTestOwnerId())){
			dto.setModuleName("处理问题");
			sb.append("</font>: </td>\n");
			sb.append(" <td  class=\"tdtxt\" width=\"160\" style=\"padding:2 0 0 4;\">\n");
			sb.append(bug.getTestOwner().getName()).append("(").append(bug.getTestOwner().getLoginName()).append(")处理");
			dto.setCurrOwner(sb.toString());
		}else if(dto.getBug().getNextFlowCd()==2&&!bug.getCurrHandlerId().equals(bug.getTestOwnerId())){
			dto.setModuleName("测试互验");
			sb.append("</font>:</td>");
			sb.append(" <td  class=\"tdtxt\" width=\"160\" style=\"padding:2 0 0 4;\">\n");
			sb.append(bug.getTestOwner().getName()).append("(").append(bug.getTestOwner().getLoginName()).append(")互验");
			dto.setCurrOwner(sb.toString());
		}else if(dto.getBug().getNextFlowCd()==3&&!bug.getCurrHandlerId().equals(bug.getAnalyseOwnerId())){
			dto.setModuleName("分析问题");
			sb.append("</font>:</td>\n");
			sb.append(" <td  class=\"tdtxt\" width=\"160\" style=\"padding:2 0 0 4;\">\n");
			sb.append(bug.getAnalysOwner().getName()).append("(").append(bug.getAnalysOwner().getLoginName()).append(")分析");
			dto.setCurrOwner(sb.toString());
		}else if(dto.getBug().getNextFlowCd()==4&&!bug.getCurrHandlerId().equals(bug.getAssinOwnerId())){
			dto.setModuleName("分配问题");
			sb.append("</font>:</td>\n");
			sb.append(" <td  class=\"tdtxt\" width=\"160\" style=\"padding:2 0 0 4;\">\n");
			sb.append(bug.getAssinOwner().getName()).append("(").append(bug.getAssinOwner().getLoginName()).append(")分配");
			dto.setCurrOwner(sb.toString());
		}else if(dto.getBug().getNextFlowCd()==5&&!bug.getCurrHandlerId().equals(bug.getDevOwnerId())){
			dto.setModuleName("修改问题");
			sb.append("</font>:</td>\n");
			sb.append(" <td  class=\"tdtxt\" width=\"160\" style=\"padding:2 0 0 4;\">\n");
			sb.append(bug.getDevOwner().getName()).append("(").append(bug.getDevOwner().getLoginName()).append(")处理");
			dto.setCurrOwner(sb.toString());
		}else if(dto.getBug().getNextFlowCd()==6&&!bug.getCurrHandlerId().equals(bug.getDevOwnerId())){
			dto.setModuleName("开发互检");
			sb.append("</font>:</td>\n");
			sb.append(" <td  class=\"tdtxt\" width=\"160\" style=\"padding:2 0 0 4;\">\n");
			sb.append(bug.getDevOwner().getName()).append("(").append(bug.getDevOwner().getLoginName()).append(")处理");
			dto.setCurrOwner(sb.toString());
		}else if(dto.getBug().getNextFlowCd()==7&&!bug.getCurrHandlerId().equals(bug.getIntercessOwnerId())){
			dto.setModuleName("开发负责人仲裁");
			sb.append("</font>:</td>\n");
			sb.append(" <td  class=\"tdtxt\" width=\"160\" style=\"padding:2 0 0 4;\">\n");
			sb.append(bug.getIntecesOwner().getName()).append("(").append(bug.getIntecesOwner().getLoginName()).append(")处理");
			dto.setCurrOwner(sb.toString());
		}else if(dto.getBug().getNextFlowCd()==8&&!bug.getCurrHandlerId().equals(bug.getTestOwnerId())){
			dto.setModuleName("测试确认");
			sb.append("</font>:</td>\n");
			sb.append(" <td  class=\"tdtxt\" width=\"160\" style=\"padding:2 0 0 4;\">\n");
			sb.append(bug.getTestOwner().getName()).append("(").append(bug.getTestOwner().getLoginName()).append(")确认");
			dto.setCurrOwner(sb.toString());
		}		
	}
	//如果下一流程编码小于当前流程编码，处理后，他们互换
	private int getNextFlwCd(BugBaseInfo bug,String[] testFlw){
		
		String myUserId = SecurityContextHolderHelp.getUserId();
		String currHandId = bug.getCurrHandlerId();
		if(bug.getCurrFlowCd()>bug.getNextFlowCd()){//踢皮球发生
			if(currHandId.equals(myUserId)){
				int i=0;
				for(String flw :testFlw){
					if(flw.equals(bug.getCurrFlowCd().toString())){
						break;
					}
					i++;
				}
				return Integer.parseInt(testFlw[i+1]);
			}else{
				return bug.getCurrFlowCd();
			}
		}else{
			if(currHandId.equals(myUserId)){//本人置状态后，再修改状态
				return bug.getNextFlowCd();
			}else{//别人处理，流转到自己名字7的BUG
				int i=0;
				for(String flw :testFlw){
					if(flw.equals(bug.getNextFlowCd().toString())){
						break;
					}
					i++;
				}
				return Integer.parseInt(testFlw[i+1]);
			}			
		}
	}
	

	//处理问题，当前处理人是本人
	
	
	
	public BugCommonService getBugCommonService() {
		return bugCommonService;
	}
	public void setBugCommonService(BugCommonService bugCommonService) {
		this.bugCommonService = bugCommonService;
	}


	public TestTaskDetailService getTestTaskService() {
		return testTaskService;
	}
	public void setTestTaskService(TestTaskDetailService testTaskService) {
		this.testTaskService = testTaskService;
	}

	private List strArr2List(String[] strArr){
		List list = new ArrayList();
		for(String str :strArr){
			if(!"".equals(str)&&!list.contains(str)){
				list.add(str);
			}
		}
		return list;
	}

	public DrawHtmlListDateService getDrawHtmlListDateService() {
		return drawHtmlListDateService;
	}

	public void setDrawHtmlListDateService(
			DrawHtmlListDateService drawHtmlListDateService) {
		this.drawHtmlListDateService = drawHtmlListDateService;
	}

}
