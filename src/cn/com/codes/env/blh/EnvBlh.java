package cn.com.codes.env.blh;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.log4j.Logger;

import com.opensymphony.webwork.ServletActionContext;

import cn.com.codes.common.dto.PageModel;
import cn.com.codes.common.util.StringUtils;
import cn.com.codes.env.dto.DeployApp;
import cn.com.codes.env.dto.EnvDto;
import cn.com.codes.env.service.EnvService;
import cn.com.codes.env.util.EOMSHttpUtil;
import cn.com.codes.env.util.PropertyUtil;
import cn.com.codes.framework.app.blh.BusinessBlh;
import cn.com.codes.framework.app.view.View;
import cn.com.codes.framework.common.JsonUtil;
import cn.com.codes.framework.transmission.events.BusiRequestEvent;
import cn.com.codes.object.Cert;
import cn.com.codes.object.Image;
import net.sf.json.JSONObject;

public class EnvBlh extends BusinessBlh {
	
	private static Logger logger = Logger.getLogger(EnvBlh.class);
	
	private Map<String, EOMSHttpUtil> certMap = new HashMap<String, EOMSHttpUtil>();
	
	private EnvService envService;

	
	public View envList(BusiRequestEvent req){
		return super.getView();
	}
	
	/**
	 * 查询镜像列表
	 * @param req
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public View getImageList(BusiRequestEvent req) {
		EnvDto envDto = super.getDto(EnvDto.class, req);
		envDto.setHql("from Image where 1=1 order by createDate desc");
		List<Image> imageList = envService.findByHqlWithValuesMap(envDto);
		PageModel pg = new PageModel();
		pg.setRows(imageList);
		pg.setTotal(envDto.getTotal());
		
		super.writeResult(JsonUtil.toJson(pg));
		return super.globalAjax();
		
	}
	
	/**
	 * 查询证书列表
	 * @param req
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public View getCertList(BusiRequestEvent req) {
		EnvDto envDto = super.getDto(EnvDto.class, req);
		envDto.setHql("from Cert where 1=1 order by uploadDate desc");
		List<Cert> certList = envService.findByHqlWithValuesMap(envDto);
		PageModel pg = new PageModel();
		pg.setRows(certList);
		pg.setTotal(envDto.getTotal());
		
		super.writeResult(JsonUtil.toJson(pg));
		return super.globalAjax();
	}
	
	/**
	 * 通过tar镜像构建镜像
	 * @param req
	 * @return
	 */
	public View buildTarImage(BusiRequestEvent req) {
		//System.setProperty("javax.net.debug", "all");
		logger.info("=================buildTarImage============");
		EnvDto envDto = super.getDto(EnvDto.class, req);
		String filePath = ServletActionContext.getServletContext().getRealPath("/") + "itest" + File.separator + "env" + File.separator + "image" + File.separator + envDto.getImage().getImageFileName();
		String imageName = envDto.getImage().getImageName();
		String serverIp = envDto.getImage().getServerIp();
		//InputStream ins = this.getClass().getClassLoader().getResourceAsStream(filePath);
		File file = new File(filePath);
		InputStream ins = null;
		try {
			ins = new FileInputStream(file);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		InputStreamEntity entity = new InputStreamEntity(ins);
		
		String url = PropertyUtil.getProperties("API_SERVER_IMAGE_LOAD_URL");
		url = url.replace("serverIp", serverIp);
		String line = null;
		String value = null;
		JSONObject returnData = new JSONObject();
		EOMSHttpUtil eomsHttpUtil = certMap.get(serverIp);
		if (eomsHttpUtil == null) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("oprateResult", "noCert");
			map.put("project", envDto.getImage());
			super.writeResult(JsonUtil.toJson(map));
			return super.globalAjax();
		}
		//EOMSHttpUtil eomsHttpUtil = EOMSHttpUtil.getInstance(serverIp);
		HttpEntity responseEntity = eomsHttpUtil.httpPostAtStream(url,entity);
		logger.info("================building end==================");
		try {
			InputStream is = responseEntity.getContent();
			BufferedReader br =  new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				logger.info("building: " + line);
				value = line;
			}
			if (value.indexOf("Loaded") != -1) {
				logger.info("================start save image==================");
				returnData.put("message", "1");
				JSONObject streamJsonObject = JSONObject.fromObject(value);
				String stream = streamJsonObject.getString("stream");
				System.out.println(stream);
				String loadImage = stream.substring(stream.indexOf("Loaded image:") + 14, stream.length());
				String[] loadImageSp = loadImage.split(":");
				String inspectListUrl = PropertyUtil.getProperties("API_SERVER_IMAGES_INSPECT_LIST_URL");
				inspectListUrl = inspectListUrl.replace("serverIp", serverIp);
				String imageTag = loadImageSp[1];
				if (!StringUtils.isNullOrEmpty(imageTag)) {
					Pattern p = Pattern.compile("\\s*|\t|\r|\n");
					Matcher m = p.matcher(imageTag);
					imageTag = m.replaceAll("");
				}
				inspectListUrl = inspectListUrl.replace("name", loadImageSp[0] + ":" + imageTag);
				Object data = eomsHttpUtil.httpGet(inspectListUrl, null);
				JSONObject imageJsonObject = JSONObject.fromObject(data.toString());
				envDto.getImage().setImageId(String.valueOf(imageJsonObject.get("Id")));
				envDto.getImage().setTag(imageTag);

				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				envDto.getImage().setCreateDate(df.format(new Date()));
				envService.saveBuildImageInfo(envDto);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("oprateResult", "success");
				map.put("project", envDto.getImage());
				super.writeResult(JsonUtil.toJson(map));
			} else {
				logger.info("================build images faild==================");
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("oprateResult", "failed");
				super.writeResult(JsonUtil.toJson(map));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("oprateResult", "failed");
			super.writeResult(JsonUtil.toJson(map));
			e.printStackTrace();
		}
		
		return super.globalAjax();
	}
	
	/**
	 * 通过dockerfile构建镜像
	 * @param req
	 * @return
	 */
	public View buildDockerfileImage(BusiRequestEvent req) {
		EnvDto envDto = super.getDto(EnvDto.class, req);
		String filePath = ServletActionContext.getServletContext().getRealPath("/") + "itest" + File.separator + "env" + File.separator + "image" + File.separator + envDto.getImage().getImageFileName();
		String filePathName = envDto.getImage().getFilePath();
		String fileType = envDto.getImage().getFileType();
		String imageName = envDto.getImage().getImageName();
		String url = PropertyUtil.getProperties("API_SERVER_IMAGE_DOCKERFILE_BUILD_FILE");
		File file = new File(filePath);
		//String name = file.getName();
		String fileName = filePathName.substring(0, filePathName.lastIndexOf(".tar.gz"));
		String[] fileNameSplit = fileName.split("_");
		if (StringUtils.isNullOrEmpty(imageName)) {
			imageName = fileNameSplit[0];
		}
		String imageTag = envDto.getImage().getTag();
		if (StringUtils.isNullOrEmpty(imageTag)) {
			imageTag = fileNameSplit[1];
		}
		//String[] nameSplit = name.split("_");
		url = new StringBuffer(url).append("?t=").append(imageName).append(":").append(imageTag).toString();
		String serverIp = envDto.getImage().getServerIp();
		url = url.replace("serverIp", serverIp);
		InputStream ins = null;
		try {
			ins = new FileInputStream(file);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}//this.getClass().getClassLoader().getResourceAsStream(filePath);
		InputStreamEntity entity = new InputStreamEntity(ins);
		
		String line = null;
		String value = null;
		//EOMSHttpUtil eomsHttpUtil = EOMSHttpUtil.getInstance(serverIp);
		EOMSHttpUtil eomsHttpUtil = certMap.get(serverIp);
		if (eomsHttpUtil == null) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("oprateResult", "noCert");
			map.put("project", envDto.getImage());
			super.writeResult(JsonUtil.toJson(map));
			return super.globalAjax();
		}
		HttpEntity responseEntity = eomsHttpUtil.httpPostAtStream(url,entity);
		try {
			InputStream is = responseEntity.getContent();
			BufferedReader br =  new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				value = line;
			}
			if (value.indexOf("Successfully") != -1) {
				//查询构建的镜像并保存到数据库
				String inspectListUrl = PropertyUtil.getProperties("API_SERVER_IMAGES_INSPECT_LIST_URL");
				inspectListUrl = inspectListUrl.replace("serverIp", serverIp);
				inspectListUrl = inspectListUrl.replace("name", imageName + ":" + imageTag);
				Object data = eomsHttpUtil.httpGet(inspectListUrl, null);
				JSONObject jsonObject = JSONObject.fromObject(data.toString());
				envDto.getImage().setImageId(String.valueOf(jsonObject.get("Id")));
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				envDto.getImage().setCreateDate(df.format(new Date()));
				envService.saveBuildImageInfo(envDto);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("oprateResult", "success");
				map.put("project", envDto.getImage());
				super.writeResult(JsonUtil.toJson(map));
			} else {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("oprateResult", "failed");
				super.writeResult(JsonUtil.toJson(map));
			}
		} catch (IOException e) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("oprateResult", "failed");
			super.writeResult(JsonUtil.toJson(map));
			e.printStackTrace();
		}
		
		return super.globalAjax();
	}
	
	/**
	 * 保存上传的镜像
	 * @param req
	 * @return
	 */
	public View saveImage(BusiRequestEvent req) {
		EnvDto envDto = super.getDto(EnvDto.class, req);

		String fileType = envDto.getImage().getFileType();
		if ("2".equals(fileType)) {
			String filePathName = envDto.getImage().getFilePath();
			String fileName = filePathName.substring(0, filePathName.lastIndexOf(".tar.gz"));
			String[] fileNameSplit = fileName.split("_");
			envDto.getImage().setTag(fileNameSplit[1]);
		}
		Boolean status = envService.saveImage(envDto);
		if (status) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("oprateResult", "success");
			map.put("project", envDto.getImage());
			super.writeResult(JsonUtil.toJson(map));
		} else {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("oprateResult", "failed");
			super.writeResult(JsonUtil.toJson(map));
		}
		return super.globalAjax();
	}
	
	/**
	 * 部署应用
	 * @param req
	 * @return
	 */
	public View deployApp(BusiRequestEvent req) {
		EnvDto envDto = super.getDto(EnvDto.class, req);
		DeployApp deployApp = envDto.getDeployApp();
		String imageName = deployApp.getImageName();
		String appName = deployApp.getAppName();
		String hostIp = deployApp.getHostIp();
		String hostPort = deployApp.getHostPort();
		String containersPort = deployApp.getContainersPort();
		String envVar = deployApp.getEnvVar();
		String serverIp = deployApp.getServerIp();
		String link = deployApp.getLink();
		
		JSONObject app = new JSONObject();
		app.put("Image", imageName);
		ArrayList<HashMap> list = new ArrayList<HashMap>();
		
		HashMap<String,Object> portProperties = new HashMap<String,Object>();
		if(!StringUtils.isNullOrEmpty(hostIp)) {
			portProperties.put("HostIp", hostIp);
		}
		if(!StringUtils.isNullOrEmpty(hostPort)) {
			portProperties.put("HostPort", hostPort);
		}
		if (portProperties.size() > 0) {
			list.add(portProperties);
		}
		HashMap<String,Object> port = new HashMap<String,Object>();
		if (list.size() > 0) {
			port.put(containersPort + "/tcp", list);
		}
		HashMap<String,Object> hostConfigMap = new HashMap<String,Object>();
		if (port.size() > 0) {
			hostConfigMap.put("PortBindings", port);
		}
		
//		HashMap<String,Object> privileged = new HashMap<String,Object>();
//		privileged.put("Privileged", true);
		hostConfigMap.put("Privileged", true);
		//app.put("HostConfig", privileged);
		if (!StringUtils.isNullOrEmpty(link) && envVar != "null") {
			ArrayList<String> listLink = new ArrayList<String>();
			listLink.add(link);
			hostConfigMap.put("Links", listLink);
		}
		
		if (hostConfigMap.size() > 0) {
			app.put("HostConfig", hostConfigMap);
		}
		
		if(!StringUtils.isNullOrEmpty(envVar) && envVar != "null") {
			List<String> env = new ArrayList<String>();
			String [] envVarArr = envVar.split(";");
			if(envVarArr.length > 0) {
				for(int i = 0; i < envVarArr.length; i++) {
					env.add(envVarArr[i]);
				}
			} else {
				env.add(envVar);
			}		
			app.put("Env", env);
		}
		logger.info("=================params:" + app.toString());
		String url = PropertyUtil.getProperties("API_SERVER_CONTAINERS_CREATE_URL");
		url = url.replace("serverIp", serverIp);
		StringBuffer sb = new StringBuffer(url);
		sb.append("?name=").append(appName);
		//EOMSHttpUtil eomsHttpUtil = EOMSHttpUtil.getInstance(serverIp);
		EOMSHttpUtil eomsHttpUtil = certMap.get(serverIp);
		if (eomsHttpUtil == null) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("oprateResult", "noCert");
			map.put("project", envDto.getImage());
			super.writeResult(JsonUtil.toJson(map));
			return super.globalAjax();
		}
		Object returnData = eomsHttpUtil.httpPost(sb.toString(), app);
		JSONObject jsonObject = JSONObject.fromObject(returnData);
		String returnId = String.valueOf(jsonObject.get("Id"));
		logger.info("=================id:" + returnId);
		String returnWarnings = String.valueOf(jsonObject.get("Warnings"));
		logger.info("=================returnWarnings:" + returnWarnings);
		if (jsonObject.containsKey("Id") && jsonObject.containsKey("Warnings") && null != returnId && null != returnWarnings) {
			logger.info("true");
			//先通过docker api接口查询新部署的实例基本信息
			String appInspectListUrl = PropertyUtil.getProperties("API_SERVER_CONTAINERS_INSPECT_LIST_URL");
			appInspectListUrl = appInspectListUrl.replace("serverIp", serverIp);
			appInspectListUrl = appInspectListUrl.replace("name", appName);
			Object appData = eomsHttpUtil.httpGet(appInspectListUrl, null);
			JSONObject appDataObject = JSONObject.fromObject(appData.toString());
			String appId = appDataObject.getString("Id");
			envDto.getImage().setAppId(appId);
			Map stateMap = (Map) appDataObject.get("State");
			String status = (String) stateMap.get("Status");
			envDto.getImage().setAppStatus(status);
			envDto.getImage().setAppName(appName);
			envDto.getImage().setAppAccessIp(hostIp);
			envDto.getImage().setAppPort(hostPort);
			envDto.getImage().setContainersPort(containersPort);
			envDto.getImage().setAppEnvVar(envVar);
			envDto.getImage().setAppLinkContainer(link);
			envService.saveAppInfo(envDto);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("oprateResult", "success");
			map.put("project", envDto.getDeployApp());
			super.writeResult(JsonUtil.toJson(map));
		} else {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("oprateResult", "failed");
			super.writeResult(JsonUtil.toJson(map));
		}
		return super.globalAjax();
	}
	
	/**
	 * 启动应用
	 * @param req
	 * @return
	 */
	public View startApp(BusiRequestEvent req) {
		EnvDto envDto = super.getDto(EnvDto.class, req);
		String url = PropertyUtil.getProperties("API_SERVER_CONTAINERS_START_URL");
		url = url.replace("id", envDto.getImage().getAppId());
		url = url.replace("serverIp", envDto.getImage().getServerIp());
		EOMSHttpUtil eomsHttpUtil = certMap.get(envDto.getImage().getServerIp());
		if (eomsHttpUtil == null) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("oprateResult", "noCert");
			map.put("project", envDto.getImage());
			super.writeResult(JsonUtil.toJson(map));
			return super.globalAjax();
		}
		//EOMSHttpUtil eomsHttpUtil = EOMSHttpUtil.getInstance(envDto.getImage().getServerIp());
		Object data = eomsHttpUtil.httpPost(url, null);
		JSONObject jsonObject = JSONObject.fromObject(data);
		if ("1".equals(jsonObject.get("message"))) {
			envDto.getImage().setAppStatus("running");
			envService.updateAppStatus(envDto);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("oprateResult", "success");
			map.put("project", envDto.getImage());
			super.writeResult(JsonUtil.toJson(map));
		} else {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("oprateResult", "failed");
			super.writeResult(JsonUtil.toJson(map));
		}
		
		return super.globalAjax();
	}
	
	/**
	 * 停止应用
	 * @param req
	 * @return
	 */
	public View stopApp(BusiRequestEvent req) {
		EnvDto envDto = super.getDto(EnvDto.class, req);
		String url = PropertyUtil.getProperties("API_SERVER_CONTAINERS_STOP_URL");
		url = url.replace("id", envDto.getImage().getAppId());
		url = url.replace("serverIp", envDto.getImage().getServerIp());
		EOMSHttpUtil eomsHttpUtil = certMap.get(envDto.getImage().getServerIp());
		if (eomsHttpUtil == null) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("oprateResult", "noCert");
			map.put("project", envDto.getImage());
			super.writeResult(JsonUtil.toJson(map));
			return super.globalAjax();
		}
		//EOMSHttpUtil eomsHttpUtil = EOMSHttpUtil.getInstance(envDto.getImage().getServerIp());
		Object data = eomsHttpUtil.httpPost(url, null);
		JSONObject jsonObject = JSONObject.fromObject(data);
		if ("1".equals(jsonObject.get("message"))) {
			envDto.getImage().setAppStatus("exited");
			envService.updateAppStatus(envDto);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("oprateResult", "success");
			map.put("project", envDto.getImage());
			super.writeResult(JsonUtil.toJson(map));
		} else {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("oprateResult", "failed");
			super.writeResult(JsonUtil.toJson(map));
		}
		
		return super.globalAjax();
	}
	
	/**
	 * 保存证书
	 * @param req
	 * @return
	 */
	public View saveCert(BusiRequestEvent req) {
		EnvDto envDto = super.getDto(EnvDto.class, req);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		envDto.getCert().setUploadDate(df.format(new Date()));
		Boolean status = envService.saveCert(envDto);
		if (status) {
			certMap.put(envDto.getCert().getServerIp(), new EOMSHttpUtil(envDto.getCert()));
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("oprateResult", "success");
			map.put("project", envDto.getCert());
			super.writeResult(JsonUtil.toJson(map));
		} else {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("oprateResult", "failed");
			super.writeResult(JsonUtil.toJson(map));
		}
		return super.globalAjax();
	}
	
	/**
	 * 删除证书
	 * @param req
	 * @return
	 */
	public View deleteCert(BusiRequestEvent req) {
		EnvDto envDto = super.getDto(EnvDto.class, req);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
//			URL resourcePath = EnvBlh.class.getClassLoader().getResource("/resource/cert");
//     		String filePath1 = URLDecoder.decode(resourcePath.getPath(), "UTF-8");
			String filePath = ServletActionContext.getServletContext().getRealPath("/") +  "itest"+File.separator+"env"+File.separator+"cert";
     		File file = new File(filePath + File.separator + envDto.getCert().getApiCertName());
     		envService.delete(envDto.getCert());
     		String serverIp = envDto.getCert().getServerIp();
     		if (file.exists()) {
     			certMap.remove(serverIp);
     			if (file.delete()) {
     				map.put("oprateResult", "success");
     				map.put("project", envDto.getCert());
     				super.writeResult(JsonUtil.toJson(map));
     			} else {
     				map.put("oprateResult", "failed");
     				super.writeResult(JsonUtil.toJson(map));
     			}
     		}
			
		} catch (Exception e) {
			e.printStackTrace();
			map.put("oprateResult", "failed");
			super.writeResult(JsonUtil.toJson(map));
		}
		return super.globalAjax();
	}
	
	public View deleteImage(BusiRequestEvent req) {
		EnvDto envDto = super.getDto(EnvDto.class, req);
		String filePath = ServletActionContext.getServletContext().getRealPath("/") +  "itest" + File.separator + "env" + File.separator + "image";
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			File file = new File(filePath + File.separator + envDto.getImage().getImageFileName());
			if (file.exists()) {
				if (file.delete()) {
					envService.deleteImage(envDto);
					map.put("oprateResult", "success");
					map.put("project", envDto.getCert());
					super.writeResult(JsonUtil.toJson(map));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("oprateResult", "failed");
			super.writeResult(JsonUtil.toJson(map));
		}
		return super.globalAjax();
		
	}

	public EnvService getEnvService() {
		return envService;
	}

	public void setEnvService(EnvService envService) {
		this.envService = envService;
		List<Cert> certList = this.envService.getCertList();
		logger.info("=============初始化API证书MAP开始============");
		for (Cert cert: certList) {
			certMap.put(cert.getServerIp(), new EOMSHttpUtil(cert));
		}
		logger.info("=============初始化API证书MAP结束============");
	}
	
	

}
