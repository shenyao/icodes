package cn.com.codes.env.util;


import java.io.PrintWriter;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cn.com.codes.framework.common.JsonUtil;

import java.io.File;

import net.sf.json.JSONObject;

@WebServlet("/uploadImage")
public class UploadImageServlet extends HttpServlet {
	
	private static Log logger = LogFactory.getLog(UploadImageServlet.class);
	
	@SuppressWarnings("unchecked")
	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/plain;charset=utf-8");
        try {
			PrintWriter pw = response.getWriter();
			DiskFileItemFactory diskFactory = new DiskFileItemFactory();
            // threshold 极限、临界值，即硬盘缓存 1M
            //diskFactory.setSizeThreshold(4 * 1024);
            // repository 贮藏室，即临时文件目录
            //diskFactory.setRepository(new File(filePath));
            ServletFileUpload upload = new ServletFileUpload(diskFactory);
            // 设置允许上传的最大文件大小 4M
            upload.setSizeMax(2000 * 1024 * 1024);
            // 解析HTTP请求消息头
            List<FileItem> fileItems = upload.parseRequest(request);
            Iterator<FileItem> iter = fileItems.iterator();
            while (iter.hasNext()) {
                FileItem item = (FileItem) iter.next();
                if (!item.isFormField()) {
                	uploadImage(item, pw);
                }
            }
            pw.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void uploadImage(FileItem item, PrintWriter pw) {
		String filePath = this.getServletContext().getRealPath("/") +  "itest"+File.separator+"env"+File.separator+"image";
		Map<String, Object> map = new HashMap<String, Object>();
		// 此时的文件名包含了完整的路径，得注意加工一下
        String filename = item.getName();
        //System.out.println("完整的文件名：" + filename);
        int index = filename.lastIndexOf("\\");
        filename = filename.substring(index + 1, filename.length());
        long fileSize = item.getSize();
        if ("".equals(filename) && fileSize == 0) {
            return;
        }
        String fileType = filename.substring(filename.lastIndexOf("."), filename.length());
		Date date = new Date();
		long dateTime = date.getTime();
        File uploadFile = new File(filePath + File.separator + dateTime + fileType);
        File uploadFile2 = new File(filePath);
        if(!uploadFile2.exists()){
            uploadFile2.mkdirs();
        }
        //String path = uploadFile.getPath().replaceAll("\\\\","/");
        try {
			item.write(uploadFile);
			map.put("oprateResult", "success");
			map.put("filePath", filename);//path.replace(dateTime + fileType, filename)
			map.put("imageFileName", dateTime + fileType);
			pw.write(JsonUtil.toJson(map).toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			map.put("oprateResult", "failed");
			pw.write(JsonUtil.toJson(map).toString());
		}
	}
}
