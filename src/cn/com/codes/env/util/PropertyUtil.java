package cn.com.codes.env.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyUtil {
	
	public static Properties properties = null;
	
	static {
		if(properties == null){
			properties = new Properties();
			InputStream in = null;
			try {
				in =  PropertyUtil.class.getClassLoader().getResourceAsStream("/resource/spring/eoms.properties");
				properties.load(in);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
	}
	
	public static String getProperties(String key) {
		return properties.getProperty(key);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String value = PropertyUtil.getProperties("SSL_KEY_PASSWORD");
		System.out.println(value);
	}

}
