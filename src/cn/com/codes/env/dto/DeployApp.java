package cn.com.codes.env.dto;

public class DeployApp {
	
	private String imageName;
	
	private String appName;
	
	private String hostIp;
	
	private String containersPort;
	
	private String hostPort;
	
	private String envVar;
	
	private String serverIp;
	
	private String link;

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getHostIp() {
		return hostIp;
	}

	public void setHostIp(String hostIp) {
		this.hostIp = hostIp;
	}

	public String getContainersPort() {
		return containersPort;
	}

	public void setContainersPort(String containersPort) {
		this.containersPort = containersPort;
	}

	public String getHostPort() {
		return hostPort;
	}

	public void setHostPort(String hostPort) {
		this.hostPort = hostPort;
	}

	public String getEnvVar() {
		return envVar;
	}

	public void setEnvVar(String envVar) {
		this.envVar = envVar;
	}

	public String getServerIp() {
		return serverIp;
	}

	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
	
	
}
