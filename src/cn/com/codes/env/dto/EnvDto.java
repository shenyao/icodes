package cn.com.codes.env.dto;

import cn.com.codes.framework.transmission.dto.BaseDto;
import cn.com.codes.object.Cert;
import cn.com.codes.object.Image;

public class EnvDto extends BaseDto {
	
	private static final long serialVersionUID = 1L;
	
	private Image image;
	
	private DeployApp deployApp;
	
	private Cert cert;

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public DeployApp getDeployApp() {
		return deployApp;
	}

	public void setDeployApp(DeployApp deployApp) {
		this.deployApp = deployApp;
	}

	public Cert getCert() {
		return cert;
	}

	public void setCert(Cert cert) {
		this.cert = cert;
	}
	
	

}
