package cn.com.codes.fileInfo.service;

import java.util.List;

import cn.com.codes.fileInfo.dto.FileInfoDto;
import cn.com.codes.framework.app.services.BaseService;
import cn.com.codes.object.FileInfo;



public interface FileInfoService extends BaseService{


	public List<FileInfo> getFileInfoByTypeId(FileInfoDto dto);

	/** 
	* 方法名:          deteById
	* 方法功能描述:    
	* @param:         
	* @return:        
	* @Author:          
	* @Create Date:   2018年10月12日 上午10:11:11
	*/
	public void deleteById(FileInfoDto dto);

	/** 
	* 方法名:          addFileInfo
	* 方法功能描述:    
	* @param:         
	* @return:        
	* @Author:          
	* @Create Date:   2018年10月12日 上午11:25:29
	*/
	public void addFileInfo(FileInfoDto dto);

}
