// dom缓存，提升页面访问性能
var windowHeight;
var objs = {
	$body: $("body"),
	$switchBtn: $("#switch_btn"),
	$loginForm: $("#loginForm"),
	$loginName: $("#loginlName"),
	$loginPwd: $("#loginPwd"),
	$remember: $("#remember")
};
/*存放登录按钮是否是disabled*/
var disableButton = false;
var logOut = false;
$(function() {
	//var height = $(".login-form").height();
	//console.log(height);
	objs.$switchBtn.on('click', function() {
		$(this).toggleClass("collapse");
		objs.$body.toggleClass("collapse");
	});
	if (GetQueryString("logOut") != null) {
		logOut = GetQueryString("logOut");
	}
	if (GetQueryString("mailBugId") != null) {
		localStorage.mailBugId = GetQueryString("mailBugId");
	}
	if (GetQueryString("taskId") != null) {
		localStorage.taskId = GetQueryString("taskId");
	}
	if (GetQueryString("isMyself") != null) {
		localStorage.isMyself = GetQueryString("isMyself");
	}
	// 记住我
	rememberMe();

	// 单击回车键登录
	$(document).keypress(function(e) {
		if (e.keyCode == 13) {
			if (!disableButton) {
				login();
			}
		}
	});
	//判断浏览器是否为火狐或者chrome
	if (navigator.userAgent.indexOf("Firefox") == -1 && navigator.userAgent.indexOf("Chrome") == -1) {
		alert("请使用火狐浏览器或者chrome浏览器登录");
	}
	windowHeight = window.screen.height;
	if (windowHeight > 899) {
		$(".login").css('padding-top', windowHeight * 0.13 + 'px');
	}

});

function GetQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
	var r = window.location.search.substr(1).match(reg);
	if (r != null) return (r[2]);
	return null;
}



// 记住用户名和密码
function rememberMe() {
	if ("undefined" != typeof localStorage.checked && "true" == localStorage.checked) {
		objs.$loginName.on('input propertychange', function() {
			localStorage.loginName = objs.$loginName.val();
		});

		objs.$loginPwd.on('input propertychange', function() {
			localStorage.password = objs.$loginPwd.val();
		});
		if ("undefined" != typeof localStorage.loginName) {
			objs.$loginName.val(localStorage.loginName);
		}
		//密码解密
		try {
			var pword = Security.enc.AES.cbcDecrypt(localStorage.password, "670b14728ad9902aecba32e22fa4f6bd");
		} catch (err) {
			localStorage.password = Security.enc.AES.cbcEncrypt(localStorage.password, "670b14728ad9902aecba32e22fa4f6bd");
			var pword = Security.enc.AES.cbcDecrypt(localStorage.password, "670b14728ad9902aecba32e22fa4f6bd");
			//objs.$loginPwd.val(pword);
		}

		objs.$loginPwd.val(pword);
		objs.$remember.attr('checked', 'checked');
		//判断是否需要自动登录
		if (!logOut) {
			login();
		} else {
			if ("undefined" != typeof localStorage.loginName && localStorage.loginName != "") {
				$("#holder01").text("");
			}
			if ("undefined" != typeof localStorage.password && localStorage.password != "") {
				$("#holder02").text("");
			}
		}

	} else {
		sessionStorage.removeItem("loginName");
		sessionStorage.removeItem("password");
	}

	/*objs.$remember.on('click', function() {
		if (objs.$remember.is(':checked')) {
			localStorage.checked = 'true';
			localStorage.loginName = objs.$loginName.val();
			//密码加密
			localStorage.password = Security.enc.AES.cbcEncrypt(objs.$loginPwd.val(), "670b14728ad9902aecba32e22fa4f6bd");
			//localStorage.password = objs.$loginPwd.val();
		} else {
			localStorage.checked = 'false';
			sessionStorage.removeItem("loginName");
			sessionStorage.removeItem("password");
		}
	});*/
}

// 登录
function login() {
	/*将登陆按钮设置成disabled*/
	$("#loginBu").attr("disabled", "disabled");
	disableButton = true;
	$.ajax({
		type: 'POST',
		url: baseUrl + '/userManager/userManagerAction!login.action',
		data: /*objs.$loginForm.serialize()*/ {
			'dto.user.loginName': $("#loginlName").val().trim(),
			'dto.user.password': $("#loginPwd").val().trim()
		},
		dataType: 'json'
	}).done(function(data) {

		if ("success" == data.loginItest) {
			if (objs.$remember.is(':checked')) {
				localStorage.checked = 'true';
				//加密密码
				localStorage.password = Security.enc.AES.cbcEncrypt(objs.$loginPwd.val(), "670b14728ad9902aecba32e22fa4f6bd");
				localStorage.loginName = objs.$loginName.val();
			} else {
				localStorage.checked = 'false';
				sessionStorage.removeItem("loginName");
				sessionStorage.removeItem("password");
			}
			objs.$loginForm.find('button').text('登录中...');
			//window.location.href = baseUrl + "/" + data.HomeUrl;
			window.location.href = baseUrl + "/main.htm";

		} else {
			$("#login-error").show();
			//$.xnotify("用户名或密码错误！", {type:'warning'});
			$("#loginBu").removeAttr("disabled");
			disableButton = false;
		}
	}).fail(function(xhr, textStatus, errorThrown) {
		$.xnotify("登录失败！", {
			type: 'danger'
		});
		$("#loginBu").removeAttr("disabled");
		disableButton = false;
	});

	/*$.post(
		baseUrl + '/userManager/userManagerAction!login.action',
		objs.$loginForm.serialize(),
		function(data) {
			if ("success" == data.loginMYPM) {
				window.location.href = baseUrl + "/" + data.HomeUrl;
			} else {
				console.log("登录失败！");
			}
		},
		'json'
	);*/
}