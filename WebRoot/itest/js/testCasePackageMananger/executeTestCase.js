var fileInfos =  new Array();
var executeTaskId = "";
var exe_testpkgId="",exe_testpkgName='',pageSource="";
var currPageNumber =null,selectedInd=null;
var executeVersion = null;

//获取页面url参数
function getQueryParam(name) {
       var obj = $('#executeTestCaseDlg').dialog('options');
       var queryParams = obj["queryParams"]; 
       return queryParams[name];
}

var packageId = "";
$(function(){
	 packageId = getQueryParam('testCasePackageId');
	 exe_testpkgId = packageId;
	 exe_testpkgName = getQueryParam('testCasePackageName');
	 pageSource = getQueryParam('pageSource');
	 var execVersion = getQueryParam('execVersion');
	 if(execVersion){
		 executeVersion = execVersion;
	 }
	
	 $('#exeStatus').val('-1')
	//初始化加载测试用例列表
	loadTestCase(packageId);
	getTestCaseResult(packageId);
	
});

function loadTestCase(packageId){
	
	$("#executeTestCaseList").xdatagrid({
		url: baseUrl + '/testCasePkgManager/testCasePackageAction!getPaginationForSelTestCasesByPkgId.action?dto.testCasePackage.packageId=' + packageId ,
		method: 'post',
		singleSelect:true,
		height: mainObjs.tableHeight-140,
		fitColumns:true,
		pagination: true,
		pageNumber: 1,
		pageSize: 10,
		pageList: [10,30,50,100],
		columns:[[  
		            {field:'pkgCaseId',hidden:true},
		            {field:'cp',title:'选择',checkbox:true},
		            {field: 'taskId',hidden:true},
					{field:'testCaseId',title:'编号',width:'5%',align:'center',formatter:addBugFun},
					{field:'taskName',title:'项目名称',width:'10%',align:'center',halign:'center'},
					{field:'testCaseDes',title:'用例描述',width:'30%',align:'left',halign:'center',formatter:caseDetail},
					{field:'testStatus',title:'执行状态',width:'10%',align:'center',formatter:testStatusFormat},
					{field:'typeName',title:'类别',width:'5%',align:'left',halign:'center'},
					{field:'priName',title:'优先级',width:'5%',align:'center'},
					{field:'auditerNmae',title:'最近处理人',width:'12%',align:'center'},
					{field:'authorName',title:'编写人',width:'10%',align:'center',formatter:caseHistory},
					{field:'weight',title:'成本',width:'5%',align:'center'},
					{field:'creatdate',title:'编写日期',width:'12%',align:'left'},
					{field:'operDataRichText',title:'过程',hidden:true},
					{field:'expResult',title:'执行结果',hidden:true}
				]],
			onLoadSuccess : function (data) {	
				if(!executeVersion){//因为不用用包列表过来时，版本属性放这这里
					 executeVersion = data.countProName;
				 }
				if (data.total==0) {
					$('#executeTestCaseList').parent().find(".datagrid-view2 .datagrid-body").append('<div style="font-size:16px; text-align: center;">暂无数据</div>');
				}
			}
	         });
}


//用例状态
function testStatusFormat(value,row,index) {
	switch (value) {
	case 0:
		return "<a style='cursor: pointer;' title='执行用例' onclick=\"executeCaseByStatus(\'"+ index +"\')\">待审核</a>";
	case 1:
		return "<a style='cursor: pointer;' title='执行用例' onclick=\"executeCaseByStatus(\'"+ index +"\')\">未测试</a>";
	case 2:
		return "<a style='cursor: pointer;' title='执行用例' onclick=\"executeCaseByStatus(\'"+ index +"\')\">通过</a>";
	case 3:
		return "<a style='cursor: pointer;' title='执行用例' onclick=\"executeCaseByStatus(\'"+ index +"\')\">未通过</a>";
	case 4:
		return "<a style='cursor: pointer;' title='执行用例' onclick=\"executeCaseByStatus(\'"+ index +"\')\">不适用</a>";
	case 5:
		return "<a style='cursor: pointer;' title='执行用例' onclick=\"executeCaseByStatus(\'"+ index +"\')\">阻塞</a>";
	case 6:
		return "<a style='cursor: pointer;' title='执行用例' onclick=\"executeCaseByStatus(\'"+ index +"\')\">待修正</a>";
	default:
		return "<a style='cursor: pointer;' title='执行用例' onclick=\"executeCaseByStatus(\'"+ index +"\')\">未测试</a>";
	}
}
//executeCaseWin(caseIdPra ,taskId)

//增加bug
function addBugFun(value,row,index){
	return "<a style='cursor: pointer;' title='转BUG' onclick=\"addBugFunWindow(\'" + row.testCaseId + "\',\'"+row.taskId+"\',\'"+row.moduleId+"\',\'"+row.packageId+"\')\">" + value + "</a>";

}

//用例详情
function caseDetail(value,row,index) {
	return "<span style='cursor:default' title='用例详情：--"+ value + "' href='javascript:;'>" + value + "</span>";
}
//用例历史
function caseHistory(value,row,index) {
	if(value=="" || value=="null" ||value==null){
		value=="--";
	}
	return "<span style=\"cursor: default\" title=\"" + value + "\" href=\"javascript:;\" >" + value + "</span>";
}

function addBugFunWindow(caseId,tId,mId,testCasePkId){
	$("<div></div>").xwindow({
    	id:'bugAddOrEditWindown',
    	title:"新增软件问题",
    	width : 960,
        height : 600,
    	modal:true,
    	footer:'#bugAddOrEditFoot',
    	collapsible:false,
		minimizable:false,
		maximizable:false,
    	href:baseUrl + '/bugManager/bugManagerAction!bugAddNew.action',
    	queryParams: {
    		'tId':tId,
    		'mId':mId,
    		"caseId":caseId,
    		"testCasePkId":testCasePkId
    	},
        onClose : function() {
        	$("#executeTestCaseList").xdatagrid('reload');
            $(this).xwindow('destroy');
        }
    });
	
}

function executeCaseByStatus(index){
	
	$("#executeTestCaseList").xdatagrid('selectRow',index);
	executeCaseWin();
}

//执行用例
function executeCaseWin(){
	var row = $("#executeTestCaseList").xdatagrid('getSelected');

	if (!row) {
		$.xalert({title:'消息提示',msg:'请选择要执行的一条记录'});
		return;
	}
	var caseId = row.testCaseId;
	 executeTaskId = row.taskId;
	 
	 //获取将要执行用例的执行状态
	 var status = "";
	 var exeStatus = row.testStatus;
	 //当用例未执行时  设置标识符标识需要重新计算执行用例数
	 if(exeStatus == "" || exeStatus == 1 || exeStatus == null){
		 status = "computeExeCount";
	 }
	 
	 $("<div></div>").xwindow({
	    	id:'editCaseWindown',
	    	title:"执行用例",
	    	width : 900,
	        height : 650,
	    	modal:true,
	    	footer:'#addOrEditFoot',
	    	collapsible:false,
			minimizable:false,
			maximizable:false,
	    	href:baseUrl + '/caseManager/caseManagerAction!caseEdit.action',
	    	queryParams: {
	    			'caseId':caseId,
	    			'operaType':'exec',
	    			'flag':'testCase',
	    			'testcasepkgId':exe_testpkgId,
	    			'testcasepkgName':exe_testpkgName,
	    			'computeExeCount':status,
	    			'executeTaskId':executeTaskId
	    		},
	        onClose : function() {
	        	if(pageSource.includes('iteration')){
	        		objs.$proTestcase.xdatagrid('reload');
	        	}
	        	$('#executeTestCaseList').xdatagrid('reload');
	        	$("#executeTestCaseList").xdatagrid('gotoPage',currPageNumber);
	        	$("#executeTestCaseList").xdatagrid('selectRow',selectedInd);
	        	
	        	getTestCaseResult(packageId);
	        	
	            $(this).xwindow('destroy');
	        }
	    });
}

//加载用例类别列表
function loadCaseCategory(){
	$.post(
			baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action",
			{
				"page": 1,
				"rows": 80,
				"dto.subName": "用例类型"
			},
			function(dat) {
				if (dat != null) {
					$(".caseTypeId").xcombobox({
						data:dat.rows
					});
				} else {
					/*$.xnotify("系统错误！", {type:'warning'});*/
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//加载优先级列表
function loadCaseYXJ(){
	$.post(
			baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action",
			{
				"page": 1,
				"rows": 80,
				"dto.subName": "用例优先级"
			},
			function(dat) {
				if (dat != null) {
					$(".priId").xcombobox({
						data:dat.rows
					});
				} else {
					/*$.xnotify("系统错误！", {type:'warning'});*/
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}


//执行用例--执行历史（列表初始化）
//操作转换
function operaTypeFormat(value,row,index){
	switch (value) {
	case 1:
		return '增加用例';
	case 2:
		return '修改用例';
	case 3:
		return '审核用例';
	case 4:
		return '执行用例';
	case 5:
		return '修正用例';
	default:
		return '-';
	}
}

//执行用例--执行历史（列表初始化）
//状态转换
function convCase(value,row,index){
	switch (value) {
	case 0:
		return '待审核';
	case 1:
		return '未测试';
	case 2:
		return '通过';
	case 3:
		return '未通过';
	case 4:
		return '不适用';
	case 5:
		return '阻塞';
	case 6:
		return '待修正';
	default:
		return '-';
	}
}

//执行用例---执行返回
function cancleCaseWin(){
	$("#executeTestCaseList").xdatagrid('reload');
	$("#editCaseForm").xform('clear');
	$("#editCaseWindown").xwindow('close');
}

function closeExecWin(){
	
	if(pageSource.includes('iteration')){
		objs.$proTestcase.xdatagrid('reload');
	}else if(pageSource.includes('task')){//
		loadOtherMission();//fresh 看板
	}else{
		casePkgObjs.$testCasePkgTb.xdatagrid('reload');
	}
	 $('#executeTestCaseDlg').dialog('destroy');
}

//执行用例---移除用例
function delSelectedTestCase(){
	var row = $("#executeTestCaseList").xdatagrid('getSelected');

	if (!row) {
		$.xalert({title:'消息提示',msg:'请选择需要移除的一条未执行记录'});
		return;
	}else if(row.testStatus !== 1 && row.testStatus !== null){
		$.xalert({title:'消息提示',msg:'已执行的测试用例不能移除'});
		return;
	}
	
	
	$.xconfirm({
		msg:'您确定移除选择的记录吗?',
		okFn: function() {
			$.post(
					baseUrl + '/testCasePkgManager/testCasePackageAction!delTestCase_CasePkgByPkgCaseId.action',
					   {'dto.pkgCaseId': row.pkgCaseId,
						'dto.packageId':packageId,
						'dto.testCaseId':row.testCaseId},
				function(data) {
					if (data.indexOf("success")>=0) {
						$("#executeTestCaseList").xdatagrid('reload');
						$.xalert('删除成功');
					} else {
						$.xalert("删除失败，请稍后再试", {type:'warning'});
					}
				},"text"
			);
		}
	});
}

//查询执行状态
function searchExeStatus(){
	 var data = {
			  "dto.queryParam":$('#exeStatus').val()
	    };
		$("#executeTestCaseList").xdatagrid('load',data);
}

function getTestCaseResult(packageId){
	var urlStr = baseUrl + '/testCasePkgManager/testCasePackageAction!getBugStaticsByPkgId.action';
	$.post(
			urlStr,
			{'dto.testCasePackage.packageId':packageId},
			function(dataObj) {
		        if(dataObj){
		        	dataObj = JSON.parse(dataObj);
		        	//未测试
		        	var noTestCount = dataObj.allCount-dataObj.passCount-dataObj.failedCount-dataObj.blockCount-dataObj.waitAuditCount-dataObj.invalidCount
		        	                  -dataObj.waitModifyCount-dataObj.waitModifyCount;
		        	$("#execAllCount").html(dataObj.allCount);
		        	$("#execPassCount").html(dataObj.passCount);
		        	$("#execFailedCount").html(dataObj.failedCount);
		        	$("#execBlockCount").html(dataObj.blockCount);
		        	$("#execInvalidCount").html(dataObj.invalidCount);
		        	$("#execNoTestCount").html(noTestCount);
		        }
			},"text"
		);
}
//@ sourceURL=executeTestCase.js