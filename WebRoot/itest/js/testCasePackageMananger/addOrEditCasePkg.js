var addOrEditPkgFlag = '';

//获取页面url参数
function getQueryParam(name) {
       var obj = $('#addOrEditCasePackageDlg').dialog('options');
       var queryParams = obj["queryParams"]; 
       return queryParams[name];
}

$(function(){
	$.parser.parse();
	addOrEditPkgFlag = getQueryParam('addOrEditPkgFlag');
	loadExecutor();
    loadExecVersion();
	if(addOrEditPkgFlag == 'edit'){
		$("#exportOtherPkgs").css("display","none");

		//修改用例包--获取用例包数据
		loadTestCasePkg();
	}else{
		$("#exportOtherPkgs").css("display","table-row");
		loadOtherTestPkg();
	}
});

function loadExecutor(){
	$.post(
			baseUrl + "/otherMission/otherMissionAction!getPeopleLists.action",
			null,
			function(dat) {
				if (dat != null) {
					var peopleList = dat.rows;
					$("#executor").xcombobox({
						data:dat.rows,
						multiple:true,
						valueField:'id',
						textField:'name'
					});
				}
			}, "json")
}


function loadOtherTestPkg() {
	var currTaksId = $("#taksIdmain").val();
	$("#testPkgs").xcombobox({
		url:  baseUrl + '/testCasePkgManager/testCasePackageAction!loadTestCasePkgIds.action?dto.testCasePackage.taskId=' + currTaksId ,
		valueField: 'keyObj',
		textField: 'valueObj',
		multiple:true,
	});
}

function loadExecVersion() {
	var currTaksId = $("#taksIdmain").val();
	$("#execVersion").xcombobox({
		url: baseUrl + '/testTaskManager/testTaskManagerAction!loadVerSel.action?dto.taskId=' + currTaksId,
		valueField: 'keyObj',
		textField: 'valueObj',
		onSelect: function(rec) {
		}
	});
}

function loadTestCasePkg(){
	var row = casePkgObjs.$testCasePkgTb.xdatagrid('getSelected');
	var fillData = {};
	fillData["dto.testCasePackage"] = row;
//	casePkgObjs.isAddTestCasePkg = false;
	$.getJSON(
			baseUrl + '/testCasePkgManager/testCasePackageAction!getUserIdsByPackageId.action',
		{'dto.testCasePackage.packageId': row.packageId},
		function(data) {
			$('#addOrEditPkgWin').xform('clear');
			if(null!=data){
				$("#executor").xcombobox("setValues",data);
			}
			if(fillData["dto.testCasePackage.execVersion"]){				
				$("#execVersion").xcombobox("setValues",fillData["dto.testCasePackage.execVersion"]);
			}
			//赋值
//			setForm(row);
		$('#addOrEditPkgWin').xdeserialize(fillData);
		}
	);
}

//提交新增用例包
document.getElementById("submitPkgBtn").addEventListener('click',function(){
	var packageId = $("#packageId").val();
	var urlStr = "";
	var tipStr = "";
	var otherPkgIs = "";
	var packageName = $("#packageName").textbox('getValue');
	var executor =  $("#executor").xcombobox("getValues").toString();
	var expectedStartTime = $("#expectedStartTime").xdatebox('getValue');
	var expectedEndTime = $("#expectedEndTime").xdatebox('getValue');
	var actualStartTime = $("#actualStartTime").xdatebox('getValue');
   
	if(null == packageName || "" == packageName){
		$.xalert("请输入测试包名称");
		return ;
	}
	if(null == executor || "" == executor){
		$.xalert("请选择分配人");
		return ;
	}
	
	if(null == expectedStartTime || "" == expectedStartTime){
		$.xalert("请选择预计开始时间");
		return ;
	}
	
	if(null == expectedEndTime || "" == expectedEndTime){
		$.xalert("请选择预计结束时间");
		return ;
	}
	
	if ($("#execVersion").xcombobox("getValue") == "") {
		$.xalert({
			title: '提示',
			msg: '请选择执行版本！'
		});
		return;
	}
	
	if(null == packageId || "" == packageId){
	   urlStr = baseUrl + '/testCasePkgManager/testCasePackageAction!saveTestCasePackage.action';
	   tipStr = "新增成功";
	   otherPkgIds = $("#testPkgs").xcombobox("getValues").toString();
	   var otherPkgIdArr = otherPkgIds.split(',')
	   if(otherPkgIdArr.length>3){
			$.xalert({
				title: '提示',
				msg: '导入其它测试包用例不能超过3个！'
			});
			return;
	   }
	}else{
	   urlStr = baseUrl + '/testCasePkgManager/testCasePackageAction!updateTestCasePackage.action';
	   tipStr = "修改成功";
	}
	
	var data = $('#addOrEditPkgWin').xserialize();
	data["dto.selectedUserIds"] = $("#executor").xcombobox("getValues").toString();
	data["dto.testCasePackage.executor"] = $("#executor").xcombobox("getText");
	data["dto.testCasePackage.execVersion"] = $("#execVersion").xcombobox("getValues").toString();
	if(null == packageId || "" == packageId){		
		//只有新增测试包时可选择导入其它测试包的用例
		data["dto.otherTestPkgIds"] = otherPkgIds;
	}
	
	$.post(
			urlStr,
			data,
			function(dataObj) {
				if(dataObj.indexOf('reName') >= 0){
					$.xalert("测试包名称'" + packageName + "'已存在,请更换");
				}else{
					 $.xalert(tipStr);
				    $('#executor').combobox('clear');
				    $('#execVersion').combobox('clear');
					$('#addOrEditPkgWin').xform('clear');
					$('#addOrEditCasePackageDlg').xwindow('close');
		    	    casePkgObjs.$testCasePkgTb.xdatagrid('reload');
				}
			},"text"
		);
});


document.getElementById("closePkgWinBtn").addEventListener('click',function(){
	 $('#addOrEditCasePackageDlg').xwindow('close');
});

//@ sourceURL=addOrEditCasePkg.js