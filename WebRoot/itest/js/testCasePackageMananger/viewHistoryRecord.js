
//获取页面url参数
function getQueryParam(name) {
       var obj = $('#viewHistoryRecordDlg').dialog('options');
       var queryParams = obj["queryParams"]; 
       return queryParams[name];
}

var packageIdHistory= '';
$(function(){
	packageIdHistory = getQueryParam('testCasePackageId');
	//初始化包历史记录
	loadPkgHistory();
	
});


//初始化包历史记录
function loadPkgHistory(){
	$("#pkgHistory").xdatagrid({
		url: baseUrl + '/testCasePkgManager/testCasePackageAction!testcasePkgHisRecordList.action?dto.testCasePackage.packageId=' + packageIdHistory + '&dto.queryParam=' + 'package',
		method: 'get',
		height: mainObjs.tableHeight,
		striped:false,
		fitColumns: true,
		rownumbers: true,
		singleSelect: true,
		pagination: true,
		pageList:[10,20,30],
		pageNumber: 1,
		pageSize: 10,
		emptyMsg:"暂无数据",
		layout:['list','first','prev','manual','next','last','refresh','info'],
		columns:[[
		    {field:'packageId',title:'',hidden:true},
			{field:'operationDesc',title:'包操作描述',width:'40%',align:'left',formatter:function(value){
				var contentStr = "-";
				if(value){
					contentStr = (value.length<35)? value: "<span title='" + value + "'>" + value.substring(0,35) + "...</span>";
				}
				return contentStr;
			}},
			{field:'operationPersonName',title:'操作人',width:'20%',align:'center',halign:'center'},
			{field:'operationTime',title:'操作时间',width:'25%',align:'center',halign:'center'},
			{field:'operationType',title:'操作类型',width:'15%',align:'center',halign:'center',formatter:pkgDetail}
		]],
		onLoadSuccess : function (data) {
				$("#iterationHisRecord").xwindow("vcenter");
		}
	});
}

function pkgDetail(value){
		if(value == '2'){
			return '修改'
		}else if(value == '4') {
			return '新增'
		}else{
			return '-'
		}

}

//加载用例历史记录
function loadTestCasePkgHistory(){
	
	$("#testCasePkgHistory").xdatagrid({
		url: baseUrl + '/testCasePkgManager/testCasePackageAction!testcasePkgHisRecordList.action?dto.testCasePackage.packageId=' + packageIdHistory,
		method: 'get',
		height: mainObjs.tableHeight,
		striped:false,
		fitColumns: true,
		rownumbers: true,
		singleSelect: true,
		pagination: true,
		pageList:[10,20,30],
		pageNumber: 1,
		pageSize: 10,
		emptyMsg:"暂无数据",
		layout:['list','first','prev','manual','next','last','refresh','info'],
		columns:[[
		    {field:'testCaseId',title:'编号'},
			{field:'proName',title:'项目名称',width:'12%',align:'left'},
			{field:'testcaseDesc',title:'用例描述描述',width:'28%',align:'center',halign:'center'},
			{field:'operationPersonName',title:'操作人',width:'21.5%',align:'center',halign:'center'},
			{field:'operationTime',title:'操作时间',width:'20%',align:'center',halign:'center'},
			{field:'operationType',title:'操作类型',width:'15%',align:'center',halign:'center',formatter:operationDetail}
		]],
		onLoadSuccess : function (data) {
				$("#iterationHisRecord").xwindow("vcenter");
		}
	});
}

function operationDetail(value){
	if(value == '0'){
		return '移除'
	}else if(value == '1') {
		return '添加'
	}else{
		return '-'
	}

}

//选项卡"用例包"的单击事件
document.getElementById('pkgHistoryTab').addEventListener("click",function(){
	document.getElementById('testCasePkgHistoryTab').setAttribute('class','tab_li_rt');
	document.getElementById('pkgHistoryTab').setAttribute('class','tab_li_lf active');
	document.getElementById('testCasePkgHistoryDiv').style.display="none";
	document.getElementById('pkgHistoryDiv').style.display="block";
	loadPkgHistory();
});

//选项卡"用例"的单击事件
document.getElementById('testCasePkgHistoryTab').addEventListener("click",function(){
	document.getElementById('testCasePkgHistoryTab').setAttribute('class','tab_li_rt active');
	document.getElementById('pkgHistoryTab').setAttribute('class','tab_li_lf ');
	document.getElementById('pkgHistoryDiv').style.display="none";
	document.getElementById('testCasePkgHistoryDiv').style.display="block";
	loadTestCasePkgHistory();
});


function closeHistoryWin(){
	 $('#viewHistoryRecordDlg').dialog('destroy');

}
//@ sourceURL=roleUserManager.js