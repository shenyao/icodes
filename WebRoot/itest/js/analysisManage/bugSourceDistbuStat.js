var names = [];
var seriesData = [];
var pieData=[];
var colorArr = [];
$(function(){
	$.parser.parse();
	//加载ehcarts
	var itemId="";
	itemId = $("#analyitemId").val(); 
	
	var url = baseUrl + "/analysis/analysisAction!getBugSourceDistbuStat.action";
	$.post(
			url,
			{"analysisDto.taskId":itemId},
			function(data){
			   if(data.length>0){
				   //构造echarts数据
				   constrEchartsData(data);
				   //BUG来源分布统计--饼图
				   loadEcharts('BUG来源分布统计','bugSourceStatPie','pie');
				 //BUG来源分布统计--柱状图
				   loadEcharts('BUG来源分布统计','bugSourceStatEcharts','bar');
				   loadTable(data);
			   }else{
				   $("#bugSourceDisbuStat").html("<div style='font-size: 25px;text-align: center;padding-top: 105px;'>暂无此项目报表数据</div>");
				   
			   }
			},'json');
});

//构造echarts图表数据

function constrEchartsData(object){
	for(var i=0;i<object.length;i++){
		var color = "#" + Math.random().toString(16).slice(2, 8)
		names.push(object[i][0]);
		seriesData.push({'value':object[i][1],'itemStyle': {
			'color': color
		}});
		
		var obj = {
				'value':object[i][1],
				'name': object[i][0],
				'itemStyle': {
					'color': color
				}
		}
		pieData.push(obj);
	}
}

function loadEcharts(title,echartsId,typeEchart){
	 var option = {}
	//柱状图
	if(typeEchart.indexOf('bar')){
	    // 指定图表的配置项和数据
	      	option = {
//	    		   title: {
//	   		        text: title,
//	   		        x:'center'
//	   		      },
	   		      tooltip: {
	   		        trigger: 'axis'
	   		      },
	    		   xAxis: {
	    		        type: 'category',
	    		        data: names
	    		    },
	    		    yAxis: {
	    		        type: 'value'
	    		    },
	    		    series: [{
	    		        data:seriesData,
	    		        type: 'bar',
	    		      
	    		    }]
			};
	}else if(typeEchart.indexOf('pie')){ //饼图
	    // 指定图表的配置项和数据
	       	option = {
	    		   title: {
	   		        text: title,
	   		        x:'center'
	   		      },
	   		      tooltip: {
	   		        trigger: 'item',
	   		       formatter: "{a} <br/>{b} : {c} ({d}%)"
	   		      },
	    		  series: [{
	    			    name: '来源',
	    		        data:pieData,
	    		        type: 'pie',
	    		        radius : '65%',
			            center: ['50%', '60%'],
			            label: {
			                normal: {
			                    formatter: ' {b} {c}  {per|{d}%}',
			                    backgroundColor: '#eee',
			                    borderColor: '#aaa',
			                    borderWidth: 1,
			                    borderRadius: 4,
			                    // shadowBlur:3,
			                    // shadowOffsetX: 2,
			                    // shadowOffsetY: 2,
			                    // shadowColor: '#999',
			                    // padding: [0, 7],
			                    rich: {
			                        a: {
			                            color: '#999',
			                            lineHeight: 22,
			                            align: 'center'
			                        },
			                        // abg: {
			                        //     backgroundColor: '#333',
			                        //     width: '100%',
			                        //     align: 'right',
			                        //     height: 22,
			                        //     borderRadius: [4, 4, 0, 0]
			                        // },
			                        hr: {
			                            borderColor: '#aaa',
			                            width: '100%',
			                            borderWidth: 0.5,
			                            height: 0
			                        },
			                        b: {
			                            fontSize: 16,
			                            lineHeight: 33
			                        },
			                        per: {
			                            color: '#eee',
			                            backgroundColor: '#334455',
			                            padding: [2, 4],
			                            borderRadius: 2
			                        }
			                    }
			                }
			            } 
	    		      
	    		    }]
			};
	}
	var myChart = echarts.init(document.getElementById(echartsId));


    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
}

//加载表格
function loadTable(data){
	  var tbody = "";
	  var count = 0;
	  for(var i=0;i<names.length;i++){
		  count += seriesData[i].value;
		tbody += "<tr><td>"+ names[i] + "</td>" +
		         "<td>" + seriesData[i].value + "</td>" + 
		          "</tr>";
	  } 
	  
	  tbody +="<tr style='background: #f0f1b3;'><td>合计</td><td>" + count + "</td></tr>"
	  document.getElementById("bugSourceStatTbody").innerHTML = tbody;
}
//@ sourceURL=bugSourceDisbuStat.js
