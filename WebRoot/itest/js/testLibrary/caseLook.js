var currNodeId = "";
//保存用例的个数
var typeLenth = 0;
var haveUploaded = "0";
$(function() {
	$.parser.parse();
	//加载测试用例类型数据
	initCaseTypeTree();
	
	document.getElementById('caseTypeTools').onkeydown = function(event) {
		var e = event || window.event || arguments.callee.caller.arguments[0];
		if (e && e.keyCode == 13) { //keyCode=13是回车键；数字不同代表监听的按键不同
			initTable(currNodeId);
		}
	};
});

//判断用户权限
function getLoginUserPower() {
	var controlButton = $('button[schkUrl],a[schkUrl]');
	$.each(controlButton, function(i, n) {
		var controlUrl = $(controlButton[i]).attr('schkUrl');
		if (privilegeMap[controlUrl] != "1") { 
			$(controlButton[i]).css('display', 'none');
		} else {
			$(controlButton[i]).css('display', 'inline-block');
		}
	});
	if (privilegeMap['testCaseLibAction!move'] != "1"){
		$('#caseTypeTree').xtree('disableDnd');
	}
}
//加载测试用例类型数据
function initCaseTypeTree(){
	var editBeforeNodeName;
	var moveNodeParentNodeId;
	$('#caseTypeTree').xtree({
		url: baseUrl + '/testCaseLib/testCaseLibAction!loadTree.action',
	    method:'post',
	    animate:true,
		lines:true,
		dnd:true,
		formatter:function(node){
			return node.text;
		},
		onLoadSuccess:function(node,data){
			if(currNodeId==""){
				var nodeDep = $('#caseTypeTree').xtree('find',data[0].id);  
				//alert(JSON.stringify(nodeDep));
				if (null != nodeDep && undefined != nodeDep){  
					$('#caseTypeTree').xtree('select',nodeDep.target);  
				}  
			}else{
				var nodeDep = $('#caseTypeTree').xtree('find',currNodeId);  
				if (null != nodeDep && undefined != nodeDep){  
					$('#caseTypeTree').xtree('select',nodeDep.target);
				}  
			}
			$("#descDetail").val('');
			currNodeId = data[0].id;
			initTable(data[0].id);
			//判断用户权限
			getLoginUserPower();
		}, 
		onClick: function(node){
			currNodeId = node.id;
			$("#descDetail").val('');
			initTable(node.id);
		},
		onDblClick: function(node){
			currNodeId = node.id;
			if (privilegeMap['testCaseLibAction!updateNode'] != "1"){
				//没有修改目录权限
				$.xalert({title:'提示', msg: '您没有修改目录权限'});
			}else{
				var parent = $('#caseTypeTree').tree('getParent', node.target);
				if (parent == null) {
					$.xalert({title:'提示', msg: '根目录不能编辑'});
				} else {
					editBeforeNodeName = node.text;
					$(this).xtree('beginEdit',node.target);
				}
			}
		},
		onContextMenu: function(e,node){
			e.preventDefault();
			$(this).xtree('select', node.target);
		},
		onBeforeEdit: function(node) {
			var parent = $(this).xtree('getParent', node.target);
			//var children = $('#testRequirementTree').tree('getChildren', parent.target);
			if (parent != null) {
				var broNodes = "";
				var children = $(this).xtree('getChildren', parent.target); //$('#testRequirementTree').tree('getChildren', parent.target); 
				for (var i = 0; i < children.length; i++) {
					if (broNodes == "") {
						broNodes = children[i].text;
					} else {
						broNodes += "," + children[i].text;
					}
					
				}
				broNodeNameArr = broNodes.split(",");
			}
		},
		onAfterEdit: function(node) {
			var parent = $('#caseTypeTree').tree('getParent', node.target);
			if (node.text == editBeforeNodeName) {
				return;
			}
			if (!updateNodeName(node, parent.id)){
				//$(this).xtree('cancelEdit',node.target);
				$(this).tree('update', {
					target: node.target,
					text: node.text//editBeforeNodeName
				});
			}
		},
		onStartDrag: function(node) {
			var parent = $('#caseTypeTree').tree('getParent', node.target);
			moveNodeParentNodeId = parent.id
		},
		onDrop: function(target,source,point) {
			var currNodeId = source.id;
			var moveNode = $('#caseTypeTree').tree('find', source.id);
			var parent = $('#caseTypeTree').tree('getParent', moveNode.target);
			var targetNodeId = parent.id;
			moveNodeEvent(currNodeId, targetNodeId, moveNodeParentNodeId);
		}
		
	});
}
//上移
function upType(){
	var node = $('#caseTypeTree').tree('getSelected');
	var parent = $('#caseTypeTree').tree('getParent', node.target); 
	if (parent == null){
		$.xalert({title:'提示', msg: '根目录不能上移'});
		return;
	}
	$.ajax({
		type: "post",
		dataType: "text",
		url: baseUrl + '/testCaseLib/testCaseLibAction!itemMoveUp.action?dto.currNodeId='+node.id+'&dto.parentNodeId='+parent.id,
		success: function(data) {
			$('#caseTypeTree').tree('options').queryParams = {id: parent.id};
			$('#caseTypeTree').tree('reload', parent.target);
		}
	});
}
//下移
function downType(){
	var node = $('#caseTypeTree').tree('getSelected');
	var parent = $('#caseTypeTree').tree('getParent', node.target); 
	if (parent == null){
		$.xalert({title:'提示', msg: '根目录不能下移'});
		return;
	}
	$.ajax({
		type: "post",
		dataType: "text",
		url: baseUrl + '/testCaseLib/testCaseLibAction!itemMoveDown.action?dto.currNodeId='+node.id+'&dto.parentNodeId='+parent.id,
		success: function(data) {
			$('#caseTypeTree').tree('options').queryParams = {id: parent.id};
			$('#caseTypeTree').tree('reload', parent.target);
		}
	});
}
//删除节点
function removeType(){
	var node = $('#caseTypeTree').tree('getSelected');
	var parent = $('#caseTypeTree').tree('getParent', node.target); 
	if (parent == null){
		$.xalert({title:'提示', msg: '根目录不能删除'});
		return;
	}
	if(typeLenth > 0){
		$.xalert({title:'提示', msg: '该目录包含有用例，不能删除'});
		return;
	}
	$.xconfirm({
		msg:'您确定删除选择的目录吗?',
		okFn: function() {
			$.ajax({
				type: "post",
				dataType: "text",
				url: baseUrl + '/testCaseLib/testCaseLibAction!deleteNode.action?dto.currNodeId='+node.id+'&dto.parentNodeId='+parent.id,
				success: function(data) {
					if(data == "success"){
						initCaseTypeTree();
					}else{
						$.xalert({title:'提示', msg: '删除失败'});
					}
				}
			});
		}
	});
}
//拖拽事件
function moveNodeEvent(currNodeId, targetNodeId, moveNodeParentNodeId) {
	var url = baseUrl + "/testCaseLib/testCaseLibAction!move.action?dto.isAjax=true&dto.currNodeId="+currNodeId +"&dto.targetId="+targetNodeId +"&dto.parentNodeId="+moveNodeParentNodeId;
	$.ajax({
		type: "post",
		dataType: "text",
		url: url,
		success: function(data) {
			console.log("data:" + data)
			if (data == "cancel") {
				$.xalert({title:'提示', msg: '与目标目录有重名不能移动！'});
				return false ;
			}
			if (data == "sucess") {
				return true;
			} else {
				$.xalert({title:'提示', msg: '保存数据时发生错误！'});
			}
		}
	});
}
//更新节点名称
function updateNodeName(node, parentNodeId) {
	var nodeName = node.text;
	if(includeSpeChar(nodeName)){
		$.xalert({title:'提示', msg: '用例类别项不能含特殊字符!'});
		return;
	}
	if (broNodeNameArr.length > 0) {
		for(var h = 0; h < broNodeNameArr.length; h++){
			var broName = broNodeNameArr[h];
			if(nodeName == broName){
				$.xalert({title:'提示', msg: '目录项: ' + nodeName +' 同级重名'});
				return false;
			}				
		}
	}

	var url = baseUrl + "/testCaseLib/testCaseLibAction!updateNode.action?dto.isAjax=true";
	$.ajax({
		type: "post",
		dataType: "text",
		url: url,
		data: {
			'dto.currNodeId': node.id,
			'dto.categoryName': nodeName,
			'dto.parentNodeId': parentNodeId
		},
		success: function(data) {
			if (data == "sucess") {
//				$.xalert({title:'提示', msg: '修改需求节点名称成功！'});
//				loadTree(taskId);
			} else {
				$.xalert({title:'提示', msg: '修改目录名称失败！'});
				return false;
			}
		}
	});
	
}
//显示添加用例类别窗口
function addType(){
	var node = $('#caseTypeTree').tree('getSelected');
	var parent = $('#caseTypeTree').tree('getParent', node.target); 
	if (parent == null){
		$("#addBroRd").hide();
		$("#addBroRdLabel").hide();
	}else{
		$("#addBroRd").show();
		$("#addBroRdLabel").show();
	}
	$('#createForm')[0].reset();
	$("#command").val("addchild");
	var selected = $('#caseTypeTree').tree('getSelected');
	$("#addCaseType").parent().css("border","none");
	$("#addCaseType").prev().css({ color: "#ffff", background: "#101010" });
	$("#addCaseType").xwindow('setTitle','增加目录--当前目录项：' + selected.text).xwindow('open');
	};

//添加用例类别节点
function addCaseTypeNode(eventType) {
	var node = $('#caseTypeTree').tree('getSelected');
	$("#currNodeId").val(node.id);
	var command = $("#command").val();
	if (command == 'addBro') {
		var parent = $('#caseTypeTree').tree('getParent', node.target); 
		if (parent == null){
			$.xalert({title:'提示', msg: '不能对根目录添加同级目录'});
			return;
		}
			
		var temp;
	    if (parent != null) {
	        temp = parent;
	        parent = $('#caseTypeTree').tree('getParent', parent.target);//对该节点取父节点
	        $("#parentNodeId").val(temp.id);
	    }
	}
	if (!addCaseTypeNodeCheck()) {
		return;
	}
	$.ajax({
		type: "post",
		dataType: "text",
		url: baseUrl + '/testCaseLib/testCaseLibAction!addNodes.action?dto.isAjax=true',
		data: $("#createForm").serialize(),
		success: function(data) {
			if (eventType == 'closed') {
				$("#addCaseType").xwindow('close');
			}
			$('#createForm')[0].reset();
			initCaseTypeTree();
		}
	});
};
//验证新增类别选项
function addCaseTypeNodeCheck() {
	if (!testRequirementNameBlankCheck()) {
		$.xalert({title:'提示', msg: '致少输入一目录项'});
		return false;
	} else if (!testRequirementNameReCheck()) {
		return false;
	}
	
	return true;
}
//验证新增测试需求时需求项是否为空
function testRequirementNameBlankCheck() {
	var form = $("#createForm").get(0);
	var elements = form.elements;
	var element;
	for (var i = 0; i < elements.length; i++) {
	 	element = elements[i];
	 	if (element.id.indexOf("_") < 0 && element.type == "text" && ((element.value.replace(/(^\s*)|(\s*$)/g, ""))!="")){
			return true;
	 	}
	}
	
	return false;
}

//添加同级需求时，如果是根节点不能添加
function checkIsParent() {
	var node = $('#caseTypeTree').tree('getSelected');
	var parent = $('#caseTypeTree').tree('getParent', node.target);
	if (parent == null) {
		$.xalert({title:'提示', msg: '不能对根目录添加同级目录'});
		return false;
	}
}
//验证新增测试需求时需求项名称是否重复
function testRequirementNameReCheck() {
	var form = $("#createForm").get(0);
	var elements = form.elements;
	var element;
	var valuesStr = "";
	for (var i = 0; i < elements.length; i++) {
	 	element = elements[i];
	 	var value= element.value.replace(/(^\s*)|(\s*$)/g, "");
	 	if (element.id.indexOf("_") < 0 && element.type == "text" && value!=""){
			valuesStr = valuesStr +","+value;
	 	}
	}
	valuesStr = valuesStr.substring(1);
	var valuesArr = valuesStr.split(",");
	var node = $('#caseTypeTree').tree('getSelected');
	var command = $("#command").val();
	var broNodeArr;
	if(command == "addchild"){
		var nodes = "";
		var children = $('#caseTypeTree').tree('getChildren', node.target); 
		for (var i = 0; i < children.length; i++) {
			if (nodes == "") {
				nodes = children[i].text;
			} else {
				nodes += "," + children[i].text;
			}
			
		}
		broNodeArr = nodes.split(",");
	} else if (command == "addBro") {
		var nodes = "";
		var parent = $('#caseTypeTree').tree('getParent', node.target);
		if (parent != null) {
			var children = $('#caseTypeTree').tree('getChildren', parent.target); 
			for (var i = 0; i < children.length; i++) {
				if (nodes == "") {
					nodes = children[i].text;
				} else {
					nodes += "," + children[i].text;
				}
				
			}
			broNodeArr = nodes.split(",");
		}
		
	}
	for(var i=0; i<valuesArr.length; i++ ){
		var nName = valuesArr[i];
		//先校验当前输入的相互有无重名
		for(var l= i+1; l < valuesArr.length; l++){
			if(nName == valuesArr[l]){
				$.xalert({title:'提示', msg: '目录项: ' + nName +' 重名'});
				return false;
			}
		}
		
		//再校验树中兄弟节点有无重名
		var broName = "";
		for(var h = 0; h < broNodeArr.length; h++){
			broName = broNodeArr[h];
			if(nName == broName){
				$.xalert({title:'提示', msg: '目录项: ' + nName +' 同级重名'});
				return false;
			}				
		}
	}
	
	for (var k = 0; k < valuesArr.length; k++) {
		if (node.text == valuesArr[k]) {
			$.xalert({title:'提示', msg: '目录项: ' + node.text +' 与父目录重名'});
			return false;
		}
	}
	
	return true;
}
//显示所有用例
function initTable(nodeId){
	$("#caseList").xdatagrid({
		url: baseUrl + '/testCaseLib/testCaseLibAction!loadCaseList.action',
		method: 'post',
		queryParams: {
			'dto.currNodeId':nodeId,
			'dto.testCase.testCaseDes':$("#descDetail").val().trim()
		},
		/*height: mainObjs.tableHeight,*/
		emptyMsg:"无数据",
		columns:[[
		    {field:'checkId',title:'选择',checkbox:true,align:'center'},
			{field:'testCaseDes',title:'用例描述',width:"98%",align:'left',formatter:testCaseDesFormat},
			/*{field:'recommendReason',title:'推荐理由',width:"16%",align:'left',formatter:recommendReasonFormat},
			{field:'recommendUserId',title:'推荐人',width:"16%",align:'center'},
			{field:'libraryId',title:'推荐类别',width:"16%",align:'center',formatter:libraryIdFormat},
			{field:'examineStatus',title:'审核状态',width:"16%",align:'center',formatter:examineStatusFormat},
			{field:'examineUserId',title:'审核人',width:"16%",align:'center'},*/
		]],
		onLoadSuccess : function (data) {	
			typeLenth = data.total;
			$("#caseLayout").xlayout('resize');
			$("#caseList").xdatagrid('resize');
			for(var i=0;i<$("#caseLayout tbody").find("tr").length;i++){
				$($("#caseLayout tbody").find("tr")[i]).attr("title","点击查看详情");
				$($("#caseLayout tbody").find("tr")[i]).css("cursor","pointer");
			}
		},
		onClickRow:function(index,row){
			openDetailWin(row);
		}
	});
}

function testCaseDesFormat(value,row,index){
	return "<a title='"+value+"'>"+value.substring(0,50)+"</a>";
}


//打开详情弹窗
function openDetailWin(rowData){
	loadCaseCategory();
	loadCaseYXJ();
	$("#lookDetailForm").xform('clear');
	$("#lookDetailWin").xwindow('setTitle','查看详情').xwindow('open');
	var fillData = {};
	fillData["dto.testCase"] = rowData;
	$("#lookDetailForm").xdeserialize(fillData);
}


function includeSpeChar(f) {
	var g = "~ ` ! # $ % ^ & * ( ) [ ] { } ; ' : \" , \uff0c < >";
	var h = g.split(" ");
	if (typeof (f) == "undefined" || isWhitespace(f)) {
		return false;
	}
	for (var e = 0; e < h.length; e++) {
		if (f.indexOf(h[e]) >= 0) {
			return true;
		}
	}
	return false;
}

function isWhitespace(b) {
	return !(/\S/.test(b));
}

//提交保存新增或修改的记录
function submitCase() {
	var node = $('#caseTypeTree').tree('getSelected');
	//获取表单数据
	var objData = $("#addCase").xserialize();

	var saveOrUpdateUrl = "";
	if(objData["dto.testCase.testCaseId"]){
		saveOrUpdateUrl = baseUrl + "/testCaseLib/testCaseLibAction!updateCase.action";
	}else{
		saveOrUpdateUrl = baseUrl + "/testCaseLib/testCaseLibAction!saveCase.action";
		objData["dto.testCase.createrId"] = $("#accountId").text();
		objData["dto.testCase.categoryId"] = node.id;
	}
	
	if(!objData["dto.testCase.caseType"] || !objData["dto.testCase.casePri"] || !objData["dto.testCase.weight"] || !objData["dto.testCase.testCaseDes"] || !objData["dto.testCase.operData"] || !objData["dto.testCase.expResult"]){
		$.xalert({title:'提交失败',msg:'请填写完整所有必填信息！'});
		return;
	}
	
	$.post(
		saveOrUpdateUrl,
		objData,
		function(data) {
			if (data =="success") {
				$("#addOrEditCaseForms").xform('clear');
				$("#addCase").xwindow('close');
				$.xalert({title:'提示',msg:'操作成功！'});
				$("#descDetail").val('');
				initTable(node.id);
			}else {
				$.xalert({title:'提交失败',msg:'系统错误！'});
			}
		}, "text");

}

// 打开新增弹窗
function addYL() {
	var node = $('#caseTypeTree').tree('getSelected');
	var parent = $('#caseTypeTree').tree('getParent', node.target); 
	if (parent == null){
		$.xalert({title:'提示', msg: '根目录不能新增用例'});
		return;
	}
	loadCaseCategory();
	loadCaseYXJ();
	$("#addOrEditCaseForms").xform('clear');
	$("#addOrEditCaseForms input").val("");
	$("#addCase").xwindow('setTitle','新增用例').xwindow('open');
}


// 打开修改弹窗
function editYL() {
	var row = $("#caseList").xdatagrid('getSelected');
	if (!row) {
		$.xalert({title:'提示',msg:'请选择要修改的一条记录！'});
		return;
	}
	loadCaseCategory();
	loadCaseYXJ();
	$("#addOrEditCaseForms").xform('clear');
	$("#addOrEditCaseForms input").val("");
	var fillData = {};
	fillData["dto.testCase"] = row;
	$("#addCase").xdeserialize(fillData);
	$("#addCase").xwindow('setTitle','修改用例').xwindow('open');
}


// 打开删除确认弹窗
function deleteYL() {
	var node = $('#caseTypeTree').tree('getSelected');
	var row = $("#caseList").xdatagrid('getSelected');
	if (!row) {
		$.xalert({title:'提示',msg:'请选择要删除的一条记录！'});
		return;
	}
	
	$.xconfirm({
		msg:'您确定删除选择的记录吗?',
		okFn: function() {
			$.post(
				baseUrl + "/testCaseLib/testCaseLibAction!deleteCase.action",
				{'dto.testCase.testCaseId': row.testCaseId},
				function(data) {
					if (data == "success") {
						$("#descDetail").val('');
						initTable(node.id);
					} else {
						$.xalert({title:'提示',msg:data});
					}
				}, "text");
		}
		
	});
	
}

//取消提交并关闭弹窗
function closeCaseWin() {
	$("#addCase").xwindow('close');
}
//加载用例类型
function loadCaseCategory() {
	$.post(
		baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action", {
			"page": 1,
			"rows": 80,
			"dto.subName": "用例类型",
			"dto.flag": "1"
		},
		function(dat) {
			if (dat != null) {
				$(".caseTypeId").xcombobox({
					data: dat.rows
				});
			} else {
				/*$.xnotify("系统错误！", {type:'warning'});*/
				$.xalert({
					title: '提示',
					msg: '系统错误！'
				});
			}
		}, "json");
}

//加载优先级列表
function loadCaseYXJ() {
	$.post(
		baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action", {
			"page": 1,
			"rows": 80,
			"dto.subName": "用例优先级",
			"dto.flag": "1"
		},
		function(dat) {
			if (dat != null) {
				$(".priId").xcombobox({
					data: dat.rows
				});
			} else {
				/*$.xnotify("系统错误！", {type:'warning'});*/
				$.xalert({
					title: '提示',
					msg: '系统错误！'
				});
			}
		}, "json");
}
//下载导入摸板
function downloadModel(obj){
	var href = baseUrl + "/impExpMgr/caseImpExpAction!downLoadCaseLibCaseTempl.action";
	$(obj).prop('href', href);
}
//导出
function downloadCases(obj){
	var href = baseUrl + '/impExpMgr/caseImpExpAction!caseLibExpCase.action?dto.currNodeId='+currNodeId;
	var objData = $("#queryCondtDiv").xserialize();
	if(objData["testCaseDes"]){
		href = href + '&dto.testCaseInfo.testCaseDes='+objData["testCaseDes"];
	}
	$(obj).prop('href', href);
}

//打开从excel导入测试用例弹窗
function uploadYL() {
	haveUploaded = "0";
	$("#uploadForm input").val("");
	$("#uploadFromExcelWin").xwindow('setTitle', '导入').xwindow('open');
	$("#importFile").fileinput("destroy");
	$("#importFile").fileinput({
		theme: 'fa',
		language: 'zh',
		uploadUrl: baseUrl + '/impExpMgr/caseImpAction!caseLibImpCase.action', // you must set a valid URL here else you will get an error
		allowedFileExtensions: ['xlsx', 'xls'],
		showPreview: false,
		showClose: false,
		overwriteInitial: true,
		uploadAsync: false,
		autoReplace: true,
		showUploadedThumbs: false,
		maxFileSize: 6000,
		maxFileCount: 1,
		imageMaxWidth: 200,
		imageMaxHeight: 100,
		enctype: 'multipart/form-data',
		//msgFilesTooMany :"选择图片超过了最大数量", 
		showRemove: false,
		showClose: false,
		showUpload: false,
		showDownload: false,
		allowedPreviewTypes: ['xlsx', 'xls'],
		dropZoneEnabled: false,
		initialPreviewAsData: false,
		/*slugCallback: function (filename) {
		  return filename.replace('(', '_').replace(']', '_');
		}*/
	}).on("filebatchselected", function(event, files) {
		//选择文件后处理
		//$(this).fileinput("upload");
	}).on('filebatchuploadsuccess', function(event, data, previewId, index) {
		if (haveUploaded == "0") {
			$("#uploadFromExcelWin").xwindow('close');
			closeUploadWin(previewId);
			$("#descDetail").val('');
			initCaseTypeTree();
			haveUploaded = "1";
		}
	}).on('filebatchuploaderror', function(event, data, previewId, index) {
		if (haveUploaded == "0") {
			$("#uploadFromExcelWin").xwindow('close');
			closeUploadWin(previewId);
			$("#descDetail").val('');
			initCaseTypeTree();
			haveUploaded = "1";
		}
		//$.xalert({title:'提示',msg:'导入成功！'});
	}).on('filepreupload', function(event, data, previewId, index) {}).on('fileerror', function(event, data, msg) {
		/*$("#uploadFromExcelWin").xwindow('close');
		closeUploadWin();
		caseTree();
    	$.xalert({title:'提示',msg:'导入成功！'});*/
	}).on("fileuploaded", function(event, data, previewId, index) {
		//上传成功后处理方法
		/*$("#uploadFromExcelWin").xwindow('close');
		closeUploadWin();
		caseTree();
    	$.xalert({title:'提示',msg:'导入成功！'});*/
	});
}

//确认导入（Excel）
function sureToUpload() {
	if (!$(".file-caption-name").val()) {
		$.xalert({
			title: '提示',
			msg: '请选择需要导入的Excel文件！'
		});
		return;
	}
	$("#importFile").fileinput("upload");
}

//关闭从excel导入测试用例弹窗
function closeUploadFromExcelWin() {
	$("#uploadForm input").val("");
	$("#uploadFromExcelWin").xwindow('close');
}

//关闭导入弹窗
function closeUploadWin(data) {
	$("#uploadFromExcelWin").xwindow('close');
	if (data.split("<pre>")[1].split("</pre>")[0] != "success") {
		$.xalert({
			title: '提示',
			msg: data.split("<pre>")[1].split("</pre>")[0]
		});
	} else {
		$.xalert({
			title: '提示',
			msg: "导入成功！"
		});
	}
}
//# sourceURL=caseLook.js