var currNodeId;
$(function() {
	$.parser.parse();
	//加载测试用例类型数据
	initCaseTypeTree();
});
//加载测试用例类型数据
function initCaseTypeTree(){
	$('#caseTypeTree').xtree({
		url: baseUrl + '/testCaseLib/testCaseLibAction!loadTree.action',
	    method:'post',
		animate:true,
		lines:true,
		formatter:function(node){
			return node.text;
		},
		onLoadSuccess:function(node,data){
			if(currNodeId==""){
				var nodeDep = $('#caseTypeTree').xtree('find',data[0].id);  
				//alert(JSON.stringify(nodeDep));
				if (null != nodeDep && undefined != nodeDep){  
					$('#caseTypeTree').xtree('select',nodeDep.target);  
				}  
			}else{
				var nodeDep = $('#caseTypeTree').xtree('find',currNodeId);  
				if (null != nodeDep && undefined != nodeDep){  
					$('#caseTypeTree').xtree('select',nodeDep.target);
				}  
			}
			
			initTable(data[0].id);
		}, 
		onClick: function(node){
			currNodeId = node.id;
			initTable(node.id);
		},
		onDblClick:function(node){
//			$(this).xtree('beginEdit',node.target);
		},
		onContextMenu:function(e,node){
			
		}
	});
}

//显示所有用例
function initTable(id){
	$("#caseList").xdatagrid({
		url: baseUrl + '/testCaseLib/testCaseLibAction!loadCaseList.action',
		method: 'post',
		queryParams: {
			'dto.currNodeId':id,
			'dto.command':'audit'
		},
		emptyMsg:"无数据",
		columns:[[
		    {field:'checkId',title:'选择',checkbox:true,align:'center'},
			{field:'testCaseDes',title:'用例描述',width:"40%",align:'left',formatter:testCaseDesFormat},
			{field:'recommendReason',title:'推荐理由',width:"19%",align:'left',formatter:recommendReasonFormat},
			{field:'recommendUserId',title:'推荐人',width:"19%",align:'center'},
			/*{field:'libraryId',title:'推荐类别',width:"16%",align:'center',formatter:libraryIdFormat},*/
			{field:'examineStatus',title:'审核状态',width:"19%",align:'center',formatter:examineStatusFormat}
			/*{field:'examineUserId',title:'审核人',width:"19%",align:'center'},*/
		]],
		onLoadSuccess : function (data) {		
			$("#caseLayout").xlayout('resize');
			$("#caseList").xdatagrid('resize');
		},
		onClickRow:function(index,row){
			openDetailWin(row);
		}
	});
}
//打开详情弹窗
function openDetailWin(rowData){
	loadCaseCategory();
	loadCaseYXJ();
	$("#lookDetailForm").xform('clear');
	$("#lookDetailWin").xwindow('setTitle','查看详情').xwindow('open');
	var fillData = {};
	fillData["dto.testCase"] = rowData;
	$("#lookDetailForm").xdeserialize(fillData);
}
function testCaseDesFormat(value,row,index){
	return "<span title='"+value+"'>"+value.substring(0,10)+"....</span>";
}

function recommendReasonFormat(value,row,index){
	return "<span title='"+value+"'>"+value.substring(0,10)+"....</span>";
}

function examineStatusFormat(value,row,index){
	if(value == "0"){
		return "未审核";
	}else if(value == "1"){
		return "通过";
	}else{
		return "";
	}
}
//通过审核
function agreeLibrary(){
	var rows = $("#caseList").xdatagrid('getSelections');
	if (rows.length == 0) {
		$.xalert({title:'提示',msg:'请选择审核通过的用例！'});
		return;
	}
	var testCaseIds = [];
	for(var i=0;i<rows.length;i++){
		testCaseIds.push(rows[i].testCaseId);
	}
	$.post(baseUrl + '/testCaseLib/testCaseLibAction!updateExamineState.action',{
		'dto.testCaseIds':testCaseIds.toString(),
		'dto.examineUserId':$("#loginNam").text(),
		'dto.examineState':'1'
	},function(data){
		if(data == "success"){
			$("#caseList").xdatagrid("reload");
			$.xalert({title:'提示',msg:'审核成功！'});
		}
	},'text');
}
//不通过审核
function disagreeLibrary(){
	var rows = $("#caseList").xdatagrid('getSelections');
	if (rows.length == 0) {
		$.xalert({title:'提示',msg:'请选择审核不通过的用例！'});
		return;
	}
	var testCaseIds = [];
	for(var i=0;i<rows.length;i++){
		testCaseIds.push(rows[i].testCaseId);
	}
	$.post(baseUrl + '/testCaseLib/testCaseLibAction!updateExamineState.action',{
		'dto.testCaseIds':testCaseIds.toString(),
		'dto.examineUserId':$("#loginNam").text(),
		'dto.examineState':'0'
	},function(data){
		if(data == "success"){
			$("#caseList").xdatagrid("reload");
			$.xalert({title:'提示',msg:'审核成功！'});
		}
	},'text');
}

function loadCaseCategory() {
	$.post(
		baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action", {
			"page": 1,
			"rows": 80,
			"dto.subName": "用例类型",
			"dto.flag": "1"
		},
		function(dat) {
			if (dat != null) {
				$(".caseTypeId").xcombobox({
					data: dat.rows
				});
			} else {
				/*$.xnotify("系统错误！", {type:'warning'});*/
				$.xalert({
					title: '提示',
					msg: '系统错误！'
				});
			}
		}, "json");
}

//加载优先级列表
function loadCaseYXJ() {
	$.post(
		baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action", {
			"page": 1,
			"rows": 80,
			"dto.subName": "用例优先级",
			"dto.flag": "1"
		},
		function(dat) {
			if (dat != null) {
				$(".priId").xcombobox({
					data: dat.rows
				});
			} else {
				/*$.xnotify("系统错误！", {type:'warning'});*/
				$.xalert({
					title: '提示',
					msg: '系统错误！'
				});
			}
		}, "json");
}
//# sourceURL=caseExamine.js