var objs = {
	$missionDg: $("#missionDg"),
	$addOrEditWin: $("#addOrEditWin"),
	$addOrEditForm: $("#addOrEditForm"),
};
//存放所有可执行任务的人员
var peopleList = [];
//存放被选中的参与者
var selectedPeople = [];
//存放被选中的关注者
var selectedConcerns = [];
//存放所有项目
var projectList = [];
//存放选中的项目
var proJ = "";
//标志位，判定是参与者打开的多人弹窗（0）还是关注者打开的多人弹窗（1）
var flagPt = "";
//存放未被使用的项目
var projectsNotUse = [];

var onselectF = false;
var selectIndex=null;
var oncheckF  = false;
var checkId=null;
var checkRow=null;
$(function() {
	$.parser.parse();
	//加载所属项目下拉菜单
	loadProjectList();
	//加载可执行任务员工列表和所有可关注任务者列表
	loadPeopleLists();

	//加载其他任务列表数据
	loadOtherMission();
	$(".searchLogo").hover(function(){
		$(this).attr("src",baseUrl+"/itest/images/mSearch1.png");
	},function(){
		$(this).attr("src",baseUrl+"/itest/images/mSearch.png");
	});
});

//查询任务信息
function searchOtherMission(){
	objs.$missionDg.xdatagrid({
		url: baseUrl + '/otherMission/otherMissionAction!otherMissionListLoad.action',
		method: 'post',
		queryParams: {
			"dto.otherMission.missionName":$("#missionName").val(),
			"dto.otherMission.createUserId":$("#loginName").text(),
			"dto.otherMission.status":$("#statu").val(),
			"dto.otherMission.projectId":$("#projectNa").val()
		},
		height: mainObjs.tableHeight,
		emptyMsg:"无数据",
		checkOnSelect:true,
		selectOnCheck:true,
		columns:[[
		    {field:'checkId',title:'选择',checkbox:true,align:'center'},
			{field:'missionName',title:'任务名称',width:"10%",align:'center',formatter:missionNameFormat},
			/*{field:'missionCategory',title:'任务类别',align:'center',halign:'center',formatter:missionCategoryFormat},
			{field:'missionType',title:'任务类型',align:'center',halign:'center',formatter:missionTypeFormat},*/
			{field:'projectId',title:'所属项目',width:"10%",align:'center',formatter:projectFormat},
			{field:'missionJoiners',title:'任务参与者',width:"10%",align:'center',formatter:missionPersonFormat},
			{field:'chargePersonId',title:'任务负责人',width:"10%",align:'center',halign:'center',formatter:chargePersonFormat},
			/*{field:'emergencyDegree',title:'紧急程度',align:'center',formatter:emergencyDegreeFormat},
			{field:'difficultyDegree',title:'难易程度',align:'center',formatter:difficultyDegreeFormat},
			{field:'standardWorkload',title:'标准工作量',align:'center'},*/
			{field:'actualWorkload',title:'实际工作量(小时)',width:"12%",align:'center'},
			{field:'description',title:'任务描述',width:"10%",align:'center',formatter:descriptionFormat},
			{field:'completionDegree',title:'任务进度(%)',width:"10%",align:'center'},
			{field:'status',title:'状态',width:"8%",align:'center',formatter:statusFormat},
			//{field:'createUserId',title:'发起人',align:'center'},
			{field:'predictStartTime',title:'预计开始时间',width:"10%",align:'center',formatter:predictStartTimeFormat},
			/*{field:'predictEndTime',title:'预计完成时间',width:"9.8%",align:'center',formatter:predictEndTimeFormat},*/
			{field:'realStartTime',title:'实际开始时间',width:"9.8%",align:'center',formatter:predictEndTimeFormat},
			/*{field:'createTime',title:'创建时间',align:'center'}*/
		]],
		onSelect:function(index,data){
			var selectedRow = objs.$missionDg.xdatagrid('getSelected');
			if(onselectF==true && selectedRow!=null  && selectIndex == index){
				objs.$missionDg.xdatagrid('unselectAll');
				onselectF = false;
				selectIndex=null;
			}else{
				selectIndex = objs.$missionDg.xdatagrid('getRowIndex',selectedRow);
				onselectF = true;
			}
		},
		 onCheck:function(index,row){
		 	if(checkRow!=null && oncheckF==true && checkId == row.missionId){
		 		objs.$missionDg.xdatagrid("uncheckAll");
				oncheckF = false;
				checkId = null;
			}else{
				oncheckF = true;
				checkRow = objs.$missionDg.xdatagrid('getChecked');
				checkId = checkRow[0].missionId;
			}
		},
		onLoadSuccess : function (data) {								
			$(".datagrid-header-check").html("");
			$("input[name='checkId']").on('click',function(){
            	var ischecked = $(this).is(":checked");
                if (ischecked) {
                    $(this).attr("checked", true);
                } else {
                    $(this).attr("checked", false);
                }
            });
		}
	});
}



//其他任务数据
function loadOtherMission(){
	objs.$missionDg.xdatagrid({
		url: baseUrl + '/otherMission/otherMissionAction!otherMissionListLoad.action',
		method: 'post',
		queryParams: {
			"dto.otherMission.createUserId":$("#loginName").text()
		},
		height: mainObjs.tableHeight,
		emptyMsg:"无数据",
		checkOnSelect:true,
		selectOnCheck:true,
		columns:[[
		    {field:'checkId',title:'选择',checkbox:true,align:'center'},
			{field:'missionName',title:'任务名称',width:"10%",align:'center',formatter:missionNameFormat},
			/*{field:'missionCategory',title:'任务类别',align:'center',halign:'center',formatter:missionCategoryFormat},
			{field:'missionType',title:'任务类型',align:'center',halign:'center',formatter:missionTypeFormat},*/
			{field:'projectId',title:'所属项目',width:"10%",align:'center',formatter:projectFormat},
			{field:'missionJoiners',title:'任务参与者',width:"10%",align:'center',formatter:missionPersonFormat},
			{field:'chargePersonId',title:'任务负责人',width:"10%",align:'center',halign:'center',formatter:chargePersonFormat},
			/*{field:'emergencyDegree',title:'紧急程度',align:'center',formatter:emergencyDegreeFormat},
			{field:'difficultyDegree',title:'难易程度',align:'center',formatter:difficultyDegreeFormat},
			{field:'standardWorkload',title:'标准工作量',align:'center'},*/
			{field:'actualWorkload',title:'实际工作量(小时)',width:"12%",align:'center'},
			{field:'description',title:'任务描述',width:"10%",align:'center',formatter:descriptionFormat},
			{field:'completionDegree',title:'任务进度(%)',width:"10%",align:'center'},
			{field:'status',title:'状态',width:"8%",align:'center',formatter:statusFormat},
			//{field:'createUserId',title:'发起人',align:'center'},
			{field:'predictStartTime',title:'预计开始时间',width:"10%",align:'center',formatter:predictStartTimeFormat},
			/*{field:'predictEndTime',title:'预计完成时间',width:"9.8%",align:'center',formatter:predictEndTimeFormat},*/
			{field:'realStartTime',title:'实际开始时间',width:"9.8%",align:'center',formatter:predictEndTimeFormat},
			/*{field:'createTime',title:'创建时间',align:'center'}*/
		]],
		onSelect:function(index,data){
			var selectedRow = objs.$missionDg.xdatagrid('getSelected');
			if(onselectF==true && selectedRow!=null  && selectIndex == index){
				objs.$missionDg.xdatagrid('unselectAll');
				onselectF = false;
				selectIndex=null;
			}else{
				selectIndex = objs.$missionDg.xdatagrid('getRowIndex',selectedRow);
				onselectF = true;
			}
		},
		 onCheck:function(index,row){
		 	if(checkRow!=null && oncheckF==true && checkId == row.missionId){
		 		objs.$missionDg.xdatagrid("uncheckAll");
				oncheckF = false;
				checkId = null;
			}else{
				oncheckF = true;
				checkRow = objs.$missionDg.xdatagrid('getChecked');
				checkId = checkRow[0].missionId;
			}
		},
		onLoadSuccess : function (data) {								
			$(".datagrid-header-check").html("");
			$("input[name='checkId']").on('click',function(){
            	var ischecked = $(this).is(":checked");
                if (ischecked) {
                    $(this).attr("checked", true);
                } else {
                    $(this).attr("checked", false);
                }
            });
		}
	});
}

function missionNameFormat(value,row,index){
	if(value.length > 8){
		return "<span title='"+value+"'>"+value.substring(0,8)+"...</span>";
	}else{
		return "<span title='"+value+"'>"+value+"</span>";
	}
}
function missionPersonFormat(value,row,index){
	return "<span title='"+value+"'>"+value.substring(0,5)+"....</span>";
}

function predictStartTimeFormat(value,row,index){
	if(value){
		return value.substring(0,10);
	}
	return "";
}
function predictEndTimeFormat(value,row,index){
	if(value){
		return value.substring(0,10);
	}
	return "";
}
function statusFormat(value,row,index){
	if(value == "0"){
		return "未开始";
	}else if(value == "1"){
		return "进行中";
	}else if(value == "2"){
		return "完成";
	}else if(value == "3"){
		return "终止";
	}else if(value == "4"){
		return "暂停";
	}
	return "";
}
function projectFormat(value,row,index){
	if(value){
		for(var i=0;i<projectList.length;i++){
			if(value == projectList[i].projectId){
				if(projectList[i].projectName.length > 8){
					return "<span title='"+projectList[i].projectName+"'>"+projectList[i].projectName.substring(0,8)+"...</span>";
				}else{
					return "<span title='"+projectList[i].projectName+"'>"+projectList[i].projectName+"</span>";
				}
				
			}
		}
	}
	return "<span></span>";
}

function chargePersonFormat(value,row,index){
	for(var i=0;i<peopleList.length;i++){
		if(value == peopleList[i].id){
			return peopleList[i].name;
		}
	}
	return "暂无";
}
function descriptionFormat(value,row,index){
	return "<span title='"+value+"'>"+value.substring(0,5)+"....</span>";
}
// 提交保存新增或修改的记录
function submit() {
	//获取表单数据
	var objData = objs.$addOrEditWin.xserialize();

	//新增时，将状态设为0
	if(!objData["dto.otherMission.status"]){
		objData["dto.otherMission.status"] = "0";
	}
	//确定projectType，传到后台
	if(proJ){
		for(var i=0;i<projectList.length;i++){
			if(proJ == projectList[i].projectId){
				objData["dto.otherMission.projectType"] = projectList[i].projectType;
				break;
			}
		}
	}
	var saveOrUpdateUrl = "";
	if(objData["dto.otherMission.missionId"]){
		saveOrUpdateUrl = baseUrl + "/otherMission/otherMissionAction!update.action";
	}else{
		saveOrUpdateUrl = baseUrl + "/otherMission/otherMissionAction!add.action";
		objData["dto.otherMission.createUserId"] = $("#loginName").text();
	}
	
	//将选中的项目执行人拼接到表单数据，传到后台
	if(selectedPeople.length > 0){
		objData["dto.userIds"] = selectedPeople.toString();
	}
	//将选中的任务关注者拼接到表单数据，传到后台
	if(selectedConcerns.length > 0){
		objData["dto.concernIds"] = selectedConcerns.toString();
	}
	
	if(!objData["dto.otherMission.missionName"] || !objData["dto.otherMission.description"] || !objData["dto.otherMission.chargePersonId"] || selectedPeople.length == 0){
		$.xalert({title:'提交失败',msg:'请填写完整所有必填信息！'});
		return;
	}else{
		//判断是否勾选了同时创建测试项目
		if($('.checkPro').is(':checked')){
			if(!objData["dto.otherMission.predictStartTime"] || !objData["dto.otherMission.predictEndTime"] || !objData["dto.otherMission.projectId"]){
				$.xalert({title:'提交失败',msg:'勾选同时创建测试项目，需选择所属项目，预计开始时间，预计结束时间！'});
				return;
			}
		}
	}
	var t1 = /^\d+$/;
	if(objData["dto.otherMission.standardWorkload"] && !t1.test(objData["dto.otherMission.standardWorkload"])){
		$.xalert({title:'提交失败',msg:'请正确填写完整所有必填项！'});
		return;
	}
	//判断预计开始时间和预计结束时间前后关系
	if(objData["dto.otherMission.predictStartTime"] && objData["dto.otherMission.predictEndTime"]){
		if(objData["dto.otherMission.predictStartTime"] > objData["dto.otherMission.predictEndTime"]){
			$.xalert({title:'提交失败',msg:'预计结束时间必须在预计开始时间之后！'});
			return;
		}
	}
	//dto.otherMission.missionName 
	//dto.otherMission.description
	if(objData["dto.otherMission.missionName"] &&objData["dto.otherMission.missionName"].length>32){
		$.xalert({title:'提示',msg:'任务名称长度不能超过32！'});
		return;
	}
	if(objData["dto.otherMission.description"] &&objData["dto.otherMission.description"].length>50){
		$.xalert({title:'提示',msg:'任务描述长度不能超过50！'});
		return;
	}
	$.post(
		saveOrUpdateUrl,
		objData,
		function(data) {
			if (data =="success") {
				//判断是否勾选了同时创建测试项目
				if($('.checkPro').is(':checked')){
					if(projectsNotUse.length == 0){
						objs.$addOrEditWin.xform('clear');
						objs.$addOrEditWin.xwindow('close');
						loadOtherMission();
						loadProjectList();
						$.xalert({title:'提示',msg:'操作成功！'});
						loadProjectsNotUse();
					}else{
						for(var dd=0;dd<projectsNotUse.length;dd++){
							if(projectsNotUse[dd].projectId == objData["dto.otherMission.projectId"]){
								//保存测试项目
								$.post(baseUrl + "/singleTestTask/singleTestTaskAction!add.action",{
									"dto.singleTest.taskId":"",
									"dto.singleTest.filterFlag":"1",
									"dto.singleTest.proName":projectsNotUse[dd].projectName,
									"dto.singleTest.psmId":objData["dto.otherMission.chargePersonId"],
									"dto.singleTest.status":"",
									"dto.singleTest.taskProjectId":projectsNotUse[dd].projectId,
									"dto.singleTest.proNum":objData["dto.otherMission.missionName"],
									"dto.singleTest.devDept":"研发部",
									"dto.singleTest.planEndDate":objData["dto.otherMission.predictEndTime"],
									"dto.singleTest.planStartDate":objData["dto.otherMission.predictStartTime"],
									"dto.singleTest.psmName":$(".inchargePeople").xcombobox("getText")
								},function(dataa){
									if (dataa !=null) {
										objs.$addOrEditWin.xform('clear');
										objs.$addOrEditWin.xwindow('close');
										loadOtherMission();
										loadProjectList();
										$.xalert({title:'提示',msg:'操作成功！'});
										loadProjectsNotUse();
									}
								},'text');
							}
						}
					}
				}else{
					objs.$addOrEditWin.xform('clear');
					objs.$addOrEditWin.xwindow('close');
					loadOtherMission();
					loadProjectList();
					$.xalert({title:'提示',msg:'操作成功！'});
					loadProjectsNotUse();
				}
			} else if(data =="existed"){
				$.xalert({title:'提交失败',msg:'该任务名称已存在，请勿重复添加！'});
			}else {
				$.xalert({title:'提交失败',msg:'系统错误！'});
			}
		}, "text");

}

// 打开新增弹窗
function showAddWin() {
	//加载任务类别列表
	loadMissionCategory();
	//加载任务紧急程度列表
	loadEmergencyDegree();
	//加载任务难易程度列表
	loadDifficultyDegree();
	$(".inchargePeople").xcombobox({
		data:[]
	});
	objs.$addOrEditForm.xform('clear');
	$("#addOrEditForm input").val("");
	objs.$addOrEditWin.parent().css("border","none");
	objs.$addOrEditWin.prev().css({ color: "#ffff", background: "#101010" });
	$(".missionNa").next().children("input").css({"width":"475px"});
	$(".missionDe").next().children("input").css({"width":"475px","height":"74px"});
	objs.$addOrEditWin.xwindow('setTitle','创建任务').xwindow('open');
	loadProjectsAddMission();
}


// 打开修改弹窗
function showEditWin() {
	//加载任务类别列表
	loadMissionCategory();
	//加载任务紧急程度列表
	loadEmergencyDegree();
	//加载任务难易程度列表
	loadDifficultyDegree();
	loadProjectsAddMissionEdit();
	var row = objs.$missionDg.xdatagrid('getSelected');
	if (!row) {
		/*$.xnotify('请选择要修改的一条记录', {type:'warning'});*/
		$.xalert({title:'提示',msg:'请选择要修改的一条记录！'});
		return;
	}
	objs.$addOrEditForm.xform('clear');
	$("#addOrEditForm input").val("");
	var fillData = {};
	fillData["dto.otherMission"] = row;
	$.post(
			baseUrl + "/otherMission/otherMissionAction!getUsers.action",
			{'dto.otherMission.missionId': row.missionId},
			function(data) {
				if (data !=null) {
					//回填表单数据
					objs.$addOrEditWin.xdeserialize(fillData);
					$('.projectIds').val(row.projectId);
					$('.projectIds').searchableSelect();
					//回填选中的执行人员和负责人
					$(".peopleList").xcombobox("setValues",data.split(","));
					//加载负责人下拉菜单
					var chargePeopleLists = [];
					if(data.split(",").length > 0 && peopleList.length > 0){
						for(var i=0;i<data.split(",").length;i++){
							for(var j=0;j<peopleList.length;j++){
								if(data.split(",")[i] == peopleList[j].id){
									chargePeopleLists.push(peopleList[j]);
								}
							}
						}
						$(".inchargePeople").xcombobox({
							data:chargePeopleLists
						}).xcombobox("setValue",row.chargePersonId);
					}
					//回填任务关注者
					$.post(baseUrl + "/otherMission/otherMissionAction!getConcerns.action",
					{'dto.otherMission.missionId': row.missionId},
					function(da) {
						if (da !=null && da !="failed") {
							//回填任务关注者
							$(".concernList").xcombobox("setValues",da.split(","));
						}else {
							/*$.xnotify("系统错误！", {type:'warning'});*/
							$.xalert({title:'提示',msg:'系统错误！'});
						}
					},'text');
					objs.$addOrEditWin.parent().css("border","none");
					objs.$addOrEditWin.prev().css({ color: "#ffff", background: "#101010" });
					$(".missionNa").next().children("input").css({"width":"475px"});
					$(".missionDe").next().children("input").css({"width":"475px","height":"74px"});
					objs.$addOrEditWin.xwindow('setTitle','修改任务').xwindow('open');
				} else {
					/*$.xnotify("系统错误！", {type:'warning'});*/
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "text");
}


// 打开删除确认弹窗
function showdelConfirm() {
	var row = objs.$missionDg.xdatagrid('getSelected');
	if (!row) {
		/*$.xnotify('请选择要删除的一条记录', {type:'warning'});*/
		$.xalert({title:'提示',msg:'请选择要删除的一条记录！'});
		return;
	}
	if(row.status != "0"){
		/*$.xnotify('只能删除状态为分配的任务！', {type:'warning'});*/
		$.xalert({title:'提示',msg:'只能删除状态为分配的任务！'});
		return;
	}
	$.xconfirm({
		msg:'您确定删除选择的记录吗?',
		okFn: function() {
			$.post(
				baseUrl + "/otherMission/otherMissionAction!delete.action",
				{'dto.otherMission.missionId': row.missionId},
				function(data) {
					if (data == "success") {
						loadOtherMission();
					} else {
						/*$.xnotify(data, {type:'warning'});*/
						$.xalert({title:'提示',msg:data});
					}
				}, "text");
		}
		
	});
	
}
//取消提交并关闭弹窗
function closeWin() {
	objs.$addOrEditWin.xwindow('close');
}
//新增项目
function addProject(){
	$("#addProjectForm").xform('clear');
	$("#addProject").parent().css("border","none");
	$("#addProject").prev().css({ color: "#ffff", background: "#101010" });
	$("#addProject").xwindow('setTitle','新增项目').xwindow('open');
	//获取相关人员
	relaUser();
}
//提交新增项目
function submitProject(){
	var urls = baseUrl + "/singleTestTask/singleTestTaskAction!add.action";
	var addOrSelectVal = $('input[name="dto.singleTest.proName"][value!=""]').prev().val();
	//检测必填数据
	var flag = requiredTestData();
	if(flag!=true){
		return;
	}
	//保存
	//先保存project表
	$.post(baseUrl + "/otherMission/otherMissionAction!addProject.action",{
		"dto.project.projectType":"1",
		"dto.project.createId":$("#accountId").text(),
		"dto.project.projectName":addOrSelectVal
	},function(data){
		if(data.oprateResult == "success"){
			//保存singletask表
			$.post(
					urls,
					{
					"dto.singleTest.taskId":"",
					"dto.singleTest.filterFlag":"1",
					"dto.singleTest.proName":addOrSelectVal,
					"dto.singleTest.psmId":$(".psmId").val(),
					"dto.singleTest.status":$(".editSta").xcombobox('getValue'),
					"dto.singleTest.taskProjectId":data.project.projectId,
					"dto.singleTest.proNum":$('input[name="dto.singleTest.proNum"]').prev().val(),
					"dto.singleTest.devDept":$('input[name="dto.singleTest.devDept"]').prev().val(),
					"dto.singleTest.planEndDate":$('input[name="dto.singleTest.planEndDate"]').prev().val(),
					"dto.singleTest.planStartDate":$('input[name="dto.singleTest.planStartDate"]').prev().val()
					/*"dto.singleTest.psmName":$('input[name="dto.singleTest.psmName"]').prev().find($('input[id^="_exui_textbox_input"]')).val(),*/
					},
				function(da) {
					if (da !=null) {
						$("#addProjectForm").xform('clear');
						$("#addProject").xwindow('close');
						//加载所属项目下拉菜单
						loadProjectsAddMission();
						//加载输入框搜索项目下拉菜单
						loadProjectList();
						loadProjectsNotUse();
						$.xalert({title:'提示',msg:'操作成功！'});
					} else {
						$.xalert({title:'提交失败',msg:'该项目已存在！'});
					}
				}, "json");
		}else{
			$.xalert({title:'提交失败',msg:'该项目已存在！'});
		}
	}, "json");
}
// 取消提交并关闭新增项目弹窗
function closeProjectWin(){
	$("#addProject").xwindow('close');
}
//加载所属项目下拉菜单
function loadProjectList(){
	if($("#isAdmin").text() == "2" || $("#isAdmin").text() == "1"){
		$.post(
				baseUrl + "/otherMission/otherMissionAction!getProjectLists.action",
				null,
				function(dat) {
					if (dat != null) {
						projectList = dat.rows;
						//加载下拉菜单选项(为管理员时)
						var opti = '<option value="">-请选择项目-</option>';
						if(dat.rows.length > 0){
							for(var i=0;i<dat.rows.length;i++){
								opti = opti + '<option value="'+dat.rows[i].projectId+'">'+dat.rows[i].projectName+'</option>';
							}
						}
						$("#projectNa").next("div.searchable-select").remove();
						$("#projectNa").html(opti);
						$('#projectNa').searchableSelect();
					} else {
						$.xalert({title:'提示',msg:'系统错误！'});
					}
				}, "json");
	}else{
		//加载下拉菜单选项(非管理员时)
		var opti = '<option value="">-请选择项目-</option>';
		$.post(baseUrl + "/otherMission/otherMissionAction!getProjectListsRelated.action?dto.related=create",null,function(datt) {
			projectList = datt.rows;
			if(datt.rows.length > 0){
				for(var p=0;p<datt.rows.length;p++){
					opti = opti + '<option value="'+datt.rows[p].projectId+'">'+datt.rows[p].projectName+'</option>';
				}
			}
			$("#projectNa").next("div.searchable-select").remove();
			$("#projectNa").html(opti);
			$('#projectNa').searchableSelect();
		},'json');
	}

}
//加载新增任务时项目下拉菜单
function loadProjectsAddMission(){
	if($("#isAdmin").text() == "2" || $("#isAdmin").text() == "1"){
		$.post(
				baseUrl + "/otherMission/otherMissionAction!getProjectLists.action",
				null,
				function(dat) {
					if (dat != null) {
						projectList = dat.rows;
						//加载下拉菜单选项(为管理员时)
						var opti = '<option value="">-请选择项目-</option>';
						if(dat.rows.length > 0){
							for(var i=0;i<dat.rows.length;i++){
								opti = opti + '<option value="'+dat.rows[i].projectId+'">'+dat.rows[i].projectName+'</option>';
							}
						}
						$(".projectIds").next("div.searchable-select").remove();
						$(".projectIds").html(opti);
						$('.projectIds').searchableSelect();
					} else {
						$.xalert({title:'提示',msg:'系统错误！'});
					}
				}, "json");		
	}else{
		//加载下拉菜单选项(非管理员时)
		var opti = '<option value="">-请选择项目-</option>';
		$.post(baseUrl + "/otherMission/otherMissionAction!getProjectListsRelated.action?dto.related=create",null,function(datt) {
			projectList = datt.rows;
			if(datt.rows.length > 0){
				for(var p=0;p<datt.rows.length;p++){
					opti = opti + '<option value="'+datt.rows[p].projectId+'">'+datt.rows[p].projectName+'</option>';
				}
			}
			$(".projectIds").next("div.searchable-select").remove();
			$(".projectIds").html(opti);
			$('.projectIds').searchableSelect();
		},'json');
	}

}
//加载修改任务时项目下拉菜单
function loadProjectsAddMissionEdit(){
	if($("#isAdmin").text() == "2" || $("#isAdmin").text() == "1"){
		$.post(
				baseUrl + "/otherMission/otherMissionAction!getProjectLists.action",
				null,
				function(dat) {
					if (dat != null) {
						projectList = dat.rows;
						//加载下拉菜单选项(为管理员时)
						var opti = '<option value="">-请选择项目-</option>';
						if(dat.rows.length > 0){
							for(var i=0;i<dat.rows.length;i++){
								opti = opti + '<option value="'+dat.rows[i].projectId+'">'+dat.rows[i].projectName+'</option>';
							}
						}
						$(".projectIds").next("div.searchable-select").remove();
						$(".projectIds").html(opti);
						$('.projectIds').searchableSelect();
					}else {
						$.xalert({title:'提示',msg:'系统错误！'});
					}
				}, "json");		
	}else{
		//加载下拉菜单选项(非管理员时)
		var opti = '<option value="">-请选择项目-</option>';
		$.post(baseUrl + "/otherMission/otherMissionAction!getProjectListsRelated.action?dto.related=create",null,function(datt) {
			projectList = datt.rows;
			if(datt.rows.length > 0){
				for(var p=0;p<datt.rows.length;p++){
					opti = opti + '<option value="'+datt.rows[p].projectId+'">'+datt.rows[p].projectName+'</option>';
				}
			}
			$(".projectIds").next("div.searchable-select").remove();
			$(".projectIds").html(opti);
			//$('.projectIds').searchableSelect();
		},'json');
	}

}
//加载可执行任务员工列表
function loadPeopleLists(){
	$.post(
			baseUrl + "/otherMission/otherMissionAction!getPeopleLists.action",
			null,
			function(dat) {
				if (dat != null) {
					peopleList = dat.rows;
					$(".peopleList").xcombobox({
						data:dat.rows,
						onChange:function(newValue,oldValue){
							selectedPeople = newValue;
							var chargePeopleList = [];
							var chargePeopleNameList = [];
							if(newValue.length > 0 && dat.rows.length > 0){
								for(var i=0;i<newValue.length;i++){
									for(var j=0;j<dat.rows.length;j++){
										if(newValue[i] == dat.rows[j].id){
											chargePeopleList.push(dat.rows[j]);
											chargePeopleNameList.push(dat.rows[j].name);
										}
									}
								}
								$(".peopleList").next().children().next().attr("title",chargePeopleNameList.toString());
								$(".inchargePeople").xcombobox({
									data:chargePeopleList
								});
								if(newValue.length == 1){
									$(".inchargePeople").xcombobox("setValue",newValue[0]);
								}
							}else{
								$(".inchargePeople").xcombobox({
									data:[]
								});
							}
						}
					});
					$(".concernList").xcombobox({
						data:dat.rows,
						onChange:function(newValue,oldValue){
							selectedConcerns = newValue;
							var chargePeopleNameList = [];
							if(newValue.length > 0 && dat.rows.length > 0){
								for(var i=0;i<newValue.length;i++){
									for(var j=0;j<dat.rows.length;j++){
										if(newValue[i] == dat.rows[j].id){
											chargePeopleNameList.push(dat.rows[j].name);
										}
									}
								}
								$(".concernList").next().children().next().attr("title",chargePeopleNameList.toString());
							}
						}
					});
				} else {
					/*$.xnotify("系统错误！", {type:'warning'});*/
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//加载任务类别列表
function loadMissionCategory(){
	$.post(
			baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action",
			{
				"page": 1,
				"rows": 30,
				"dto.subName": "任务类别",
				"dto.flag": "1",
			},
			function(dat) {
				if (dat != null) {
					$(".missionCategory").xcombobox({
						data:dat.rows
					});
				} else {
					/*$.xnotify("系统错误！", {type:'warning'});*/
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//加载任务紧急程度列表
function loadEmergencyDegree(){
	$.post(
			baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action",
			{
				"page": 1,
				"rows": 30,
				"dto.subName": "任务紧急程度",
				"dto.flag": "1",
			},
			function(dat) {
				if (dat != null) {
					$(".emergencyDegree").xcombobox({
						data:dat.rows
					});
				} else {
					/*$.xnotify("系统错误！", {type:'warning'});*/
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//加载任务难易程度列表
function loadDifficultyDegree(){
	$.post(
			baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action",
			{
				"page": 1,
				"rows": 30,
				"dto.subName": "任务难易程度",
				"dto.flag": "1",
			},
			function(dat) {
				if (dat != null) {
					$(".difficultyDegree").xcombobox({
						data:dat.rows
					});
				} else {
					/*$.xnotify("系统错误！", {type:'warning'});*/
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//加载未被使用的项目
function loadProjectsNotUse(){
	$.post(
			baseUrl + '/otherMission/otherMissionAction!getProjectLists2.action',
			null,
			function(dat) {
				if (dat != null) {
					projectsNotUse = dat.rows;
				} else {
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//选择项目改变事件
function changeProject(obj){
	proJ = obj.value;
}
function searchPm(){
	var keyId = $(".pm option:selected").val();
	var valueText = $(".pm option:selected").text();
	$(".psmId").val(keyId);
	$(".pmId").val(valueText);
}
//检测必填数据
function requiredTestData(){
	if($('.projectNums').xtextbox('getValue')==""){
		$.xalert({title:'提示',msg:'请填写项目编号!'});
		return false;
	}
	
	if($('.hadProject').xtextbox('getValue')==""){
		$.xalert({title:'提示',msg:'请填写项目名称!'});
		return false;
	}
	
	if($('.developmentDep').xtextbox('getValue')==""){
		$.xalert({title:'提示',msg:'请填写研发部门!'});
		return false;
	}
	
	if($('.proStName').val()==""){
		$.xalert({title:'提示',msg:'请选择PM!'});
		return false;
	}
	
	var planStartDate = $('.startTime').xdatebox('getValue');
	var planEndDate = $('.endTime').xdatebox('getValue');
	
	var startTime = planStartDate.replace(/-/g,"/");//替换字符，变成标准格式
	var endTime = planEndDate.replace(/-/g,"/");//替换字符，变成标准格式
	
	var time1 = new Date(Date.parse(startTime));  
	var time2 = new Date(Date.parse(endTime));
	
	if(time1 > time2){
		$.xalert({title:'提示',msg:'开始日期大于结束日期!'});
		return false;
	}
	
	if(planStartDate==""){
		$.xalert({title:'提示',msg:'请选择开始日期!'});
		return false;
	}
	if(planEndDate==""){
		$.xalert({title:'提示',msg:'请选择结束日期!'});
		return false;
	}
	
	return true;
}
//获取相关人员
function relaUser(){	
	$.post(
			baseUrl +'/userManager/userManagerAction!loadDefaultSelUser.action?dto.getPageNo=1&dto.pageSize=1000',
			null,
			function(data) {
				if (data != null) {
					var opti = '<option value="">-请选择PM-</option>';
					if(data.rows.length > 0){
						for(var i=0;i<data.rows.length;i++){
							opti = opti + '<option value="'+data.rows[i].keyObj+'">'+data.rows[i].valueObj+'</option>';
						}
					}
					$(".pm").next("div.searchable-select").remove();
					$(".pm").html(opti);
					$('.pm').searchableSelect();
				} else {
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}

//# sourceURL=otherMission.js