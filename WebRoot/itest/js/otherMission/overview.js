var objs = {
	$missionDg: $("#missionDg"),
	$addOrEditWin: $("#addOrEditWin"),
	$addOrEditForm: $("#addOrEditForm"),
};
//存放所有可执行任务的人员
var peopleList = [];
//存放所有项目
var projectList = [];
//存放所有任务类别
var missionCategoryList = [];
//存放所有任务紧急程度
var emergencyDegreeList = [];
//存放所有任务难易程度
var difficultyDegreeList = [];
//存放任务id
var missiId = "";
//存放修改进度为终止时的标志
var stopFlag = "0";
//保存选中的一条记录
var missionRow = {};
//存放选中的项目
var proJ = "";
//存放未被使用的项目
var projectsNotUse = [];
//存放被选中的参与者
var selectedPeople = [];
//存放被选中的关注者
var selectedConcerns = [];
//存放是否是执行工厂函数的标志
var initFlg = "0";
var haveLoadPeople=0;
$(function() {
	$.parser.parse();
	//加载所有项目
	loadProjectList();
	//加载可执行任务员工列表
	if($("#isAdmin").text() == "2" || $("#isAdmin").text() == "1"){
		loadPeopleLists();
	}
	
	
	$(".sss").xcombobox({
		onChange:function(newValue,oldValue){
			if(newValue == "3"){
				$(".stopReason").val("");
				$(".stopZone").show();
				$(".zantingReason").val("");
				$(".zantingZone").hide();
			}else if(newValue == "4"){
				$(".stopReason").val("");
				$(".stopZone").hide();
				$(".zantingReason").val("");
				$(".zantingZone").show();
			}else{
				$(".stopReason").val("");
				$(".stopZone").hide();
				$(".zantingReason").val("");
				$(".zantingZone").hide();
			}
		}
	});
	if($("#isAdmin").text() == "2" || $("#isAdmin").text() == "1"){
		$("#peopleNameList").show();
	}else{
		$("#peopleNameList").hide();
	}
	loadOtherMission();
});

//查询任务信息
function searchOtherMission(){

}

function handBugInKanban(row){
	$("<div></div>").xwindow({
    	id:'handBugWindownKanban',
    	title:"处理软件问题",
    	width : 920,
        height : 640,
    	modal:true,
    	collapsible:false,
		minimizable:false,
		maximizable:false,
    	href:baseUrl +"/bugManager/bugManagerAction!bugHandInKanban.action",
    	queryParams: {
			"bId": row.missionId,
			"tId": row.projectId,
			"fromPage":"kanban"
		},
        onClose : function() {
            $(this).xwindow('destroy');
        }
    });
}

//其他任务数据
function loadOtherMission(){
	$(".missionDiv").css("height","");
	if(parseInt($(document.body).height()) > 600){
		var totalHeight = 485 + parseInt($(document.body).height()) - 600;
		$(".panelHeight").css("height",totalHeight);
		//$(".missionDiv").css("height",totalHeight);
		//alert(totalHeight);
	}
	$.post(baseUrl + "/otherMission/otherMissionAction!getOtherMissionList.action",{
		"dto.otherMission.projectId":$("#projectNa").val(),
		"dto.usersId":$("#peopleName").val()
		/*"dto.usersName":$("#peopleName option:selected").text()*/
	},function(data){
		$("#distribution").empty();
		$("#accept").empty();
		$("#finish").empty();
		$("#termination").empty();
		$("#pause").empty();
		var distributionCounts = 0;
		var acceptCounts = 0;
		var finishCounts = 0;
		var terminationCounts = 0;
		var pauseCounts = 0;
		if(data.length > 0){
			for(var i=0;i<data.length;i++){
				var missiName = "";
				var pgkName = "";
				if(data[i].missionName.length > 12){
					missiName = data[i].missionName.substring(0,12)+"...";
				}else {
					missiName = data[i].missionName;
				}
				if(data[i].status == "0"){
					distributionCounts = distributionCounts + 1;
					var charPerson = "";
					if(data[i].dataType=="task"){
						for(var t=0;t<peopleList.length;t++){
							if(data[i].chargePersonId == peopleList[t].id){
								charPerson= peopleList[t].name;
								break;
							}
						}
					}else if(data[i].dataType=="testPkg"){
						charPerson = data[i].executor;
						if(data[i].executor.length> 12){
							charPerson = data[i].executor.substring(0,12)+"...";
						}
					}
					if(data[i].dataType=="task"&&data[i].chargePersonId == $("#accountId").text()){
						if(data[i].missionNum){
							$("#distribution").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
									+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看 "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a><br/>"
									+"<span style='margin-right:5px;'>#"+data[i].missionNum+"</span><span>"+charPerson+"</span><a onclick='showEditWin("+JSON.stringify(data[i])+")' style='cursor:pointer;margin-left: 50px;'>  开始</a>"
									+"</div>");
						}else{
							$("#distribution").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
									+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看 "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a><br/>"
									+"<span>"+charPerson+"</span><a onclick='showEditWin("+JSON.stringify(data[i])+")' style='cursor:pointer;margin-left: 50px;'>  开始</a>"
									+"</div>");
						}
					}else{
						if(data[i].missionNum){
							$("#distribution").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
									+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看  "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a><br/>"
									+"<span style='margin-right:5px;'>#"+data[i].missionNum+"</span><span>"+charPerson+"</span>"
									+"</div>");
						}else{
							if(data[i].dataType=="task"){
								$("#distribution").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
										+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看 "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a><br/>"
										+"<span>"+charPerson+"</span>"
										+"</div>");
							}else{
								if(data[i].dataType=="testPkg"){
									$("#distribution").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
											+"<a onclick='showPkgDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看 "+data[i].missionName+"(测试包)详情及执行情况'>"+missiName+"</a>("+data[i].completionDegree+")<br/>"
											/*+"<span>"+charPerson+"</span>"*/
											+"<a style='cursor:pointer' onclick='jumpToPkgPage("+JSON.stringify(data[i])+")' title='执行用例/查看用例'>"+charPerson+"</a>"
											+"</div>");
								}else if(data[i].dataType=="bug"){
									$("#distribution").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
											+"<font  color='red'>"+missiName+"</font><br/>"
											+"<span style='margin-right:5px;'>#"+data[i].missionId+"</span><span>"+data[i].handlerName+"</span>&nbsp;"
											+"<a onclick='handBugInKanban("+JSON.stringify(data[i])+")' style='cursor:pointer' title='处理 Bug:"+data[i].missionName+"'>"+data[i].bugStatus+"</a><br/>"
											+"</div>");
								}else if(data[i].dataType=="itrt"){
									$("#distribution").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
											+"<a onclick='showIterationDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看 "+data[i].missionName+"(迭代)详情及迭代报告'>"+missiName+"</a><br/>"
											+"<span>(迭代)</span>"
											+"</div>");
								}

							}

						}
						
					}
				}
				
				if(data[i].status == "1"){
					acceptCounts = acceptCounts + 1;
					var charPerson = "";
					if(data[i].dataType=="task"){
						for(var t=0;t<peopleList.length;t++){
							if(data[i].chargePersonId == peopleList[t].id){
								charPerson= peopleList[t].name;
								break;
							}
						}
					}else if(data[i].dataType=="testPkg"){
						charPerson = data[i].executor;
						if(data[i].executor.length> 12){
							charPerson = data[i].executor.substring(0,12)+"...";
						}
					}

					if(data[i].dataType=="task"&&data[i].chargePersonId == $("#accountId").text()){
						if(data[i].missionNum){
							$("#accept").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
									+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer;margin-right:3px' title='查看 "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a>("+data[i].completionDegree+"%)<br/>"
									+"<span style='margin-right:5px;'>#"+data[i].missionNum+"</span><span>"+charPerson+"</span><a onclick='showEditWin("+JSON.stringify(data[i])+")' style='cursor:pointer;margin-left: 50px;'>  填写进度</a>"
									+"</div>");
						}else{
							$("#accept").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
									+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer;margin-right:3px' title='查看 "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a>("+data[i].completionDegree+"%)<br/>"
									+"<span>"+charPerson+"</span><a onclick='showEditWin("+JSON.stringify(data[i])+")' style='cursor:pointer;margin-left: 50px;'>  填写进度</a>"
									+"</div>");
						}
					}else{
						if(data[i].missionNum){
							$("#accept").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
									+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer;margin-right:3px' title='查看 "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a>("+data[i].completionDegree+"%)<br/>"
									+"<span style='margin-right:5px;'>#"+data[i].missionNum+"</span><span>"+charPerson+"</span>"
									+"</div>");
						}else{
							if(data[i].dataType=="task"){
								$("#accept").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
										+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer;margin-right:3px' title='查看 "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a>("+data[i].completionDegree+"%)<br/>"
										+"<span>"+charPerson+"</span>"
										+"</div>");	
								
							}else{
								if(data[i].dataType=="testPkg"){
									$("#accept").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
											+"<a onclick='showPkgDetail("+JSON.stringify(data[i])+")' style='cursor:pointer;margin-right:3px' title='查看"+data[i].missionName+"(测试包)详情及执行情况'>"+missiName+"</a>("+data[i].completionDegree+")<br/>"
											/*+"<span>"+charPerson+"</span>"*/
											+"<a style='cursor:pointer' onclick='jumpToPkgPage("+JSON.stringify(data[i])+")' title='执行用例/查看用例'>"+charPerson+"</a>"
											+"</div>");
								}else if(data[i].dataType=="itrt"){
									$("#accept").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
											+"<a onclick='showIterationDetail("+JSON.stringify(data[i])+")' style='cursor:pointer;margin-right:3px' title='查看"+data[i].missionName+"(迭代)详情及迭代报告'>"+missiName+"</a><br/>"
											+"<span>(迭代)</span>"
											+"</div>");
								}

							}

						}
						
					}
				}
				
				if(data[i].status == "2"){
					finishCounts = finishCounts + 1;
					var charPerson = "";
					if(data[i].dataType=="task"){
						for(var t=0;t<peopleList.length;t++){
							if(data[i].chargePersonId == peopleList[t].id){
								charPerson= peopleList[t].name;
								break;
							}
						}
					}else if(data[i].dataType=="testPkg"){
						charPerson = data[i].executor;
						if(data[i].executor.length> 12){
							charPerson = data[i].executor.substring(0,12)+"...";
						}
					}

					if(data[i].dataType=="task"&&data[i].chargePersonId == $("#accountId").text()){
						if(data[i].missionNum){
							$("#finish").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
									+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看 "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a><br/>"
									+"<span style='margin-right:5px;'>#"+data[i].missionNum+"</span><span>"+charPerson+"</span>"
									+"</div>");
						}else{
							$("#finish").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
									+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看 "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a><br/>"
									+"<span>"+charPerson+"</span>"
									+"</div>");
						}
						
					}else{
						if(data[i].missionNum){
							$("#finish").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
									+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看 "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a><br/>"
									+"<span style='margin-right:5px;'>#"+data[i].missionNum+"</span><span>"+charPerson+"</span>"
									+"</div>");
						}else{
							if(data[i].dataType=="task"){
								$("#finish").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
										+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看 "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a><br/>"
										+"<span>"+charPerson+"</span>"
										+"</div>");
							}else{
								if(data[i].dataType=="testPkg"){
									$("#finish").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
											+"<a onclick='showPkgDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看"+data[i].missionName+"(测试包)详情及执行情况'>"+missiName+"</a>("+data[i].completionDegree+")<br/>"
											/*+"<span>"+charPerson+"</span>"*/
											+"<a style='cursor:pointer' onclick='jumpToPkgPage("+JSON.stringify(data[i])+")' title='执行用例/查看用例'>"+charPerson+"</a>"
											+"</div>");
								}else if(data[i].dataType=="itrt"){
									$("#finish").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
											+"<a onclick='showIterationDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看"+data[i].missionName+"(迭代)详情及迭代报告'>"+missiName+"</a><br/>"
											+"<span>(迭代)</span>"
											+"</div>");
								}

							}

						}
						
					}
				}
				
				if(data[i].status == "3"){
					terminationCounts = terminationCounts + 1;
					var charPerson = "";
					if(data[i].dataType=="task"){
						for(var t=0;t<peopleList.length;t++){
							if(data[i].chargePersonId == peopleList[t].id){
								charPerson= peopleList[t].name;
								break;
							}
						}
					}

					if(data[i].dataType=="task"&&data[i].chargePersonId == $("#accountId").text()){
						if(data[i].missionNum){
							$("#termination").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
									+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看 "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a><br/>"
									+"<span style='margin-right:5px;'>#"+data[i].missionNum+"</span><span>"+charPerson+"</span>"
									+"</div>");
						}else{
							$("#termination").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
									+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看 "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a><br/>"
									+"<span>"+charPerson+"</span>"
									+"</div>");
						}
						
					}else{
						if(data[i].missionNum){
							$("#termination").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
									+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看 "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a><br/>"
									+"<span style='margin-right:5px;'>#"+data[i].missionNum+"</span><span>"+charPerson+"</span>"
									+"</div>");
						}else{
							if(data[i].dataType=="task"){
								$("#termination").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
										+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看 "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a><br/>"
										+"<span>"+charPerson+"</span>"
										+"</div>");
							}else if(data[i].dataType=="itrt"){
								$("#termination").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
										+"<a onclick='showIterationDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看"+data[i].missionName+"(迭代)详情及迭代报告'>"+missiName+"</a><br/>"
										+"<span>(迭代)</span>"
										+"</div>");
							}

						}
						
					}
				}
				
				if(data[i].status == "4"){
					pauseCounts = pauseCounts + 1;
					var charPerson = "";
					if(data[i].dataType=="task"){
						for(var t=0;t<peopleList.length;t++){
							if(data[i].chargePersonId == peopleList[t].id){
								charPerson= peopleList[t].name;
								break;
							}
						}
					}

					if(data[i].dataType=="task"&&data[i].chargePersonId == $("#accountId").text()){
						if(data[i].missionNum){
							$("#pause").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
									+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看 "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a><br/>"
									+"<span style='margin-right:5px;'>#"+data[i].missionNum+"</span><span>"+charPerson+"</span>"
									+"</div>");
						}else{
							$("#pause").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
									+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看 "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a><br/>"
									+"<span>"+charPerson+"</span>"
									+"</div>");
						}
						
					}else{
						if(data[i].missionNum){
							$("#pause").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
									+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看 "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a><br/>"
									+"<span style='margin-right:5px;'>#"+data[i].missionNum+"</span><span>"+charPerson+"</span>"
									+"</div>");
						}else{
							if(data[i].dataType=="task"){
								$("#pause").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
										+"<a onclick='showDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看 "+data[i].missionName+"(任务)详情及日志'>"+missiName+"</a><br/>"
										+"<span>"+charPerson+"</span>"
										+"</div>");
							}else if(data[i].dataType=="itrt"){
								$("#pause").append("<div style='margin-bottom:1em; text-align: left; padding-left: 10px;padding-top:0.5em'>"
										+"<a onclick='showIterationDetail("+JSON.stringify(data[i])+")' style='cursor:pointer' title='查看"+data[i].missionName+"(迭代)详情及迭代报告'>"+missiName+"</a><br/>"
										+"<span>(迭代)</span>"
										+"</div>");
							}

						}
						
					}
				}
			}
		}
		$("#distributionCount").html(distributionCounts);
		$("#acceptCount").html(acceptCounts);
		$("#finishCount").html(finishCounts);
		$("#terminationCount").html(terminationCounts);
		$("#pauseCount").html(pauseCounts);
		initFlg = "1";
		//动态调整每个块的高度
		loadHeightAuto();
	},'json');
}

//动态调整每个块的高度
function loadHeightAuto(){
	var missionDiv = $("body").find(".missionDiv");
	var maxHeight = 0;
	for(var i=0;i<missionDiv.length;i++){
		if(parseInt($(missionDiv[i]).height()) > maxHeight){
			maxHeight = parseInt($(missionDiv[i]).height());
		}
	}
	if(maxHeight < parseInt($(".panelHeight").height())){
		$(".missionDiv").css("height",parseInt($(".panelHeight").height()));
	}else{
		$(".missionDiv").css("height",maxHeight);
	}

}

//提交修改的记录
function submit11(){
	//获取表单数据
	var objData = objs.$addOrEditWin.xserialize();
	if(objData["dto.otherMission.status"] == "3" && stopFlag == "0"){
		$.xconfirm({
			msg:'修改状态为终止后，不能再填写进度，你确定吗？',
			okFn: function() {
				stopFlag = "1";
				submit();
			}
		});
	}else if(objData["dto.otherMission.status"] == "3" && stopFlag == "1"){
		submit();
	}else{
		submit();
	}
}
//提交修改的记录
function submit() {
	var saveOrUpdateUrl = baseUrl + "/otherMission/otherMissionAction!updateStatus.action";
	
	//获取表单数据
	var objData = objs.$addOrEditWin.xserialize();
	/*原始记录为分配时，并且进度不是100，弄成进行中状态*/
	if(missionRow.status == "0"){
		if(objData["dto.otherMission.completionDegree"] != "100"){
			objData["dto.otherMission.status"] = "1";
		}else{
			objData["dto.otherMission.status"] = "2";
		}
	}else{
		if(objData["dto.otherMission.completionDegree"] != "100"){
			if(objData["dto.otherMission.status"] == "3"){
				objData["dto.otherMission.stopReason"] = objData["stopReason"];
			}
			if(objData["dto.otherMission.status"] == "4"){
				objData["dto.otherMission.stopReason"] = objData["zantingReason"];
			}
		}else{
			objData["dto.otherMission.status"] = "2";
		}
	}
	
	if(!objData["dto.otherMission.actualWorkload"] || !objData["dto.otherMission.completionDegree"] || !objData["dto.otherMission.status"] || !objData["dto.otherMission.realStartTime"]){
		$.xalert({title:'提交失败',msg:'请填写完整所有必填项！'});
		return;
	}
	var t1 = /^\d+$/;
	var t2 = /^100$|^(\d|[1-9]\d)(\.\d+)*$/;
	if(!t1.test(objData["dto.otherMission.actualWorkload"]) || !t2.test(objData["dto.otherMission.completionDegree"])){
		$.xalert({title:'提交失败',msg:'请正确填写完整所有必填项！'});
		return;
	}
	if(objData["dto.otherMission.status"] == "3" || objData["dto.otherMission.status"] == "4"){
		if(!objData["dto.otherMission.stopReason"]){
			$.xalert({title:'提交失败',msg:'请填写完整所有必填项！'});
			return;
		}
	}
	$.post(
		saveOrUpdateUrl,
		objData,
		function(data) {
			if (data =="success") {
				objs.$addOrEditWin.xform('clear');
				objs.$addOrEditWin.xwindow('close');
				loadOtherMission();
				$.xalert({title:'提示',msg:'操作成功！'});
			} else {
				/*$.xnotify("系统错误！", {type:'warning'});*/
				$.xalert({title:'提示',msg:'系统错误！'});
			}
		}, "text");
}

// 打开填写进度弹窗
function showEditWin(row) {
	missionRow = row;
	stopFlag = "0";
	var fillData = {};
	$("#addOrEditForm").xform('clear');
	$("#addOrEditForm input").val("");
	fillData["dto.otherMission"] = row;
	objs.$addOrEditWin.xdeserialize(fillData);
	if(row.status == "4"){
		$(".zantingReason").val(row.stopReason);
	}
	if(row.status == "0"){
		$(".sss").xcombobox("readonly",true);
	}else{
		$(".sss").xcombobox("readonly",false);
	}
	objs.$addOrEditWin.parent().css("border","none");
	objs.$addOrEditWin.prev().css({ color: "#ffff", background: "#101010" });
	if(row.status == "0"){
		objs.$addOrEditWin.xwindow('setTitle','开始');
	}else{
		objs.$addOrEditWin.xwindow('setTitle','填写进度');
	}
	objs.$addOrEditWin.xwindow('open');
}
//改变任务状态
function changeMissionStatus(missionValue){
	if(missionRow.status == "0"){
		if(missionValue != "100" && missionValue != ""){
			$(".sss").xcombobox("setValue","1");
		}else if(missionValue == ""){
			$(".sss").xcombobox("setValue","0");
		}else{
			$(".sss").xcombobox("setValue","2");
		}
	}
}
//查看详情(任务)
function showDetail(row){
	//加载任务类别列表
	loadMissionCategory();
	//加载任务紧急程度列表
	loadEmergencyDegree();
	//加载任务难易程度列表
	loadDifficultyDegree();
	$(".nav-lattice").show();
	loadPeopleLists();
	missiId = row.missionId;
	$(".nav-lattice").children().addClass("active");
	$(".nav-lattice").children().next().removeClass("active");
	$("#xiangqing").addClass("in active");
	$("#rizhi").removeClass("in active");
	var uu = 0;
	var ii = 0;
	var pp = 0;
	var tt = 0;
	var qq = 0;
	for(var i=0;i<projectList.length;i++){
		if(row.projectId == projectList[i].projectId){
			row.projectId = projectList[i].projectName;
			break;
		}else{
			tt = tt + 1;
		}
	}
	if(tt == projectList.length){
		row.projectId = "";
	}
	
	for(var t=0;t<peopleList.length;t++){
		if(row.chargePersonId == peopleList[t].id){
			row.chargePersonId = peopleList[t].name;
			break;
		}else{
			qq = qq + 1;
		}
	}
	if(qq == peopleList.length){
		row.chargePersonId = "";
	}
	
	if(missionCategoryList.length > 0){
		for(var p=0;p<missionCategoryList.length;p++){
			if(row.missionCategory == missionCategoryList[p].typeId){
				row.missionCategory = missionCategoryList[p].typeName;
				break;
			}else{
				pp = pp + 1;
			}
		}
		if(pp == missionCategoryList.length){
			row.missionCategory = "";
		}
	}else{
		row.missionCategory = "";
	}
	if(emergencyDegreeList.length > 0){
		for(var q=0;q<emergencyDegreeList.length;q++){
			if(row.emergencyDegree == emergencyDegreeList[q].typeId){
				row.emergencyDegree = emergencyDegreeList[q].typeName;
				break;
			}else{
				uu = uu + 1;
			}
		}
		if(uu == emergencyDegreeList.length){
			row.emergencyDegree = "";
		}
	}else{
		row.emergencyDegree = "";
	}
	
	if(difficultyDegreeList.length > 0){
		for(var h=0;h<difficultyDegreeList.length;h++){
			if(row.difficultyDegree == difficultyDegreeList[h].typeId){
				row.difficultyDegree = difficultyDegreeList[h].typeName;
				break;
			}else{
				ii = ii + 1;
			}
		}
		if(ii == difficultyDegreeList.length){
			row.difficultyDegree = "";
		}
	}else{
		row.difficultyDegree = "";
	}
	
	if(row.status == "0"){
		row.status = "未开始";
	}else if(row.status == "1"){
		row.status = "进行中";
	}else if(row.status == "2"){
		row.status = "完成";
	}else if(row.status == "3"){
		row.status = "终止";
	}else if(row.status == "4"){
		row.status = "暂停";
	}else{
		row.status = "";
	}
	if(row.predictStartTime){
		row.predictStartTime = row.predictStartTime.substring(0,10);
	}else{
		row.predictStartTime = "";
	}
	if(row.predictEndTime){
		row.predictEndTime = row.predictEndTime.substring(0,10);
	}else{
		row.predictEndTime = "";
	}
	if(row.realStartTime){
		row.realStartTime = row.realStartTime.substring(0,10);
	}else{
		row.realStartTime = "";
	}
	if(!row.stopReason){
		row.stopReason = "";
	}
	$.ajax({
		  url: baseUrl + "/otherMission/otherMissionAction!getUserNames.action",
		  cache: false,
		  async: false,
		  type: "POST",
		  data: {
			  'dto.otherMission.missionId':row.missionId
		  },
		  dataType:"text",
		  success: function(data){
			  if (data !=null) {
				  row.peoples = data;
				  row.peopless = "";
				  if(row.peoples.length > 15){
					  row.peopless = row.peoples.substring(0,15) + "...";
				  }else{
					  row.peopless = row.peoples;
				  }
				  $.post(baseUrl + '/otherMission/otherMissionAction!getConcernNames.action',{'dto.otherMission.missionId':row.missionId},function(da){
					  if(da != null){
						  if(da == "failed"){
							  row.concerns = "";
						  }else{
							  row.concerns = da;
							  row.concernss = "";
							  if(row.concerns.length > 15){
								  row.concernss = row.concerns.substring(0,15) + "...";
							  }else{
								  row.concernss = row.concerns;
							  }
						  }
						  $("#detailTable").empty().append("<tr>"
						    		+"<th style='text-align:right'>任务名称：</th>"
						    		+"<td style='width:160px'>"+row.missionName+"</td>"
						    		+"<th style='text-align:right'></th>"
						    		+"<td style='width:160px'></td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>任务描述：</th>"
						    		+"<td style='width:160px;' title='"+row.description+"'>"+row.description.substring(0,10)+"...</td>"
						    		+"<th style='text-align:right'></th>"
						    		+"<td style='width:160px'></td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>任务类别：</th>"
						    		+"<td style='width:160px'>"+row.missionCategory+"</td>"
						    		+"<th style='text-align:right'></th>"
						    		+"<td style='width:160px'></td>"
						    		/*+"<th style='text-align:right'>任务类型：</th>"
						    		+"<td style='width:160px'>其他任务</td>"*/
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>所属项目：</th>"
						    		+"<td style='width:160px'>"+row.projectId+"</td>"
						    		+"<th style='text-align:right'></th>"
						    		+"<td style='width:160px'></td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>任务参与者：</th>"
						    		+"<td style='width:160px' title='"+row.peoples+"'>"+row.peopless+"</td>"
						    		+"<th style='text-align:right'>任务负责人：</th>"
						    		+"<td style='width:160px'>"+row.chargePersonId+"</td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>任务关注者：</th>"
						    		+"<td style='width:160px' title='"+row.concerns+"'>"+row.concernss+"</td>"
						    		+"<th style='text-align:right'></th>"
						    		+"<td style='width:160px'></td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>紧急程度：</th>"
						    		+"<td style='width:160px'>"+row.emergencyDegree+"</td>"
						    		+"<th style='text-align:right'>难易程度：</th>"
						    		+"<td style='width:160px'>"+row.difficultyDegree+"</td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>标准工作量(小时)：</th>"
						    		+"<td style='width:160px'>"+row.standardWorkload+"</td>"
						    		+"<th style='text-align:right'>实际工作量(小时)：</th>"
						    		+"<td style='width:160px'>"+row.actualWorkload+"</td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>任务进度(%)：</th>"
						    		+"<td style='width:160px'>"+row.completionDegree+"</td>"
						    		+"<th style='text-align:right'>任务状态：</th>"
						    		+"<td style='width:160px'>"+row.status+"</td>"
						    		+"</tr>"
						    		+"<tr class='klklkl'>"
						    		+"<th style='text-align:right'>终止原因：</th>"
						    		+"<td style='width:160px;' title='"+row.stopReason+"'>"+row.stopReason.substring(0,10)+"...</td>"
						    		+"<th style='text-align:right'></th>"
						    		+"<td style='width:160px'></td>"
						    		+"</tr>"
						    		+"<tr class='lkl'>"
						    		+"<th style='text-align:right'>暂停原因：</th>"
						    		+"<td style='width:160px;' title='"+row.stopReason+"'>"+row.stopReason.substring(0,10)+"...</td>"
						    		+"<th style='text-align:right'></th>"
						    		+"<td style='width:160px'></td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>预计开始时间：</th>"
						    		+"<td style='width:160px'>"+row.predictStartTime+"</td>"
						    		+"<th style='text-align:right'>预计结束时间：</th>"
						    		+"<td style='width:160px'>"+row.predictEndTime+"</td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>实际开始时间：</th>"
						    		+"<td style='width:160px'>"+row.realStartTime+"</td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>任务发起人：</th>"
						    		+"<td style='width:160px'>"+row.createUserId+"</td>"
						    		+"<th style='text-align:right'>任务创建时间：</th>"
						    		+"<td style='width:160px'>"+row.createTime+"</td>"
						    		+"</tr>");
						  if(row.status == "终止"){
								$(".klklkl").show();
								$(".lkl").hide();
							}else if(row.status == "暂停"){
								$(".klklkl").hide();
								$(".lkl").show();
							}else{
								$(".klklkl").hide();
								$(".lkl").hide();
							}
					  }
				  },'text');
				} else {
					$.xalert({title:'提示',msg:'系统错误！'});
				}
		   }
		});
	$("#detailWin").parent().css("border","none");
	$("#detailWin").prev().css({ color: "#ffff", background: "#101010" });
	/*loadOtherMissionLog(row.missionId);*/
	$("#xiangqing").show();
	$("#xiangqing").next().hide();
	$("#detailWin").xwindow('setTitle','详情').xwindow('open');
	$("#detailWin").xwindow("vcenter");
}
//查看详情(测试包)
function showPkgDetail(row){
	$(".nav-lattice").show();
	$(".nav-lattice").children().addClass("active");
	$(".nav-lattice").children().next().removeClass("active");
	$("#testPkgDetail").addClass("in active");
	$("#testPkgDetailResult").removeClass("in active");
	$("#detailPkgWin").parent().css("border","none");
	$("#detailPkgWin").prev().css({ color: "#ffff", background: "#101010" });
	$("#testPkgDetail").show();
	$("#testPkgDetail").next().hide();
	$("#detailPkgWin").xwindow('setTitle','详情').xwindow('open');
	$("#detailPkgWin").xwindow("vcenter");
	var missionShortName = row.missionName;
	if(row.missionName.length > 8){
		missionShortName = row.missionName.substring(0,8)+"..";
	}
	if(row.expectedStartTime){
		row.expectedStartTime = row.expectedStartTime.substring(0,10);
	}else{
		row.expectedStartTime = "";
	}
	if(row.expectedEndTime){
		row.expectedEndTime = row.expectedEndTime.substring(0,10);
	}else{
		row.expectedEndTime = "";
	}
	if(row.actualStartTime){
		row.actualStartTime = row.actualStartTime.substring(0,10);
	}else{
		row.actualStartTime = "";
	}
	$("#detailPkgTable").empty().append("<tr>"
    		+"<th style='text-align:right'>测试包名称：</th>"
    		+"<td style='width:300px' title='"+row.missionName+"'>"+missionShortName+"</td>"
    		+"<th style='text-align:right'></th>"
    		+"<td style='width:300px'></td>"
    		+"</tr>"
    		+"<tr>"
    		+"<th style='text-align:right'>执行人：</th>"
    		+"<td style='width:300px;' title='"+row.executor+"'>"+row.executor.substring(0,10)+"...</td>"
    		+"<th style='text-align:right'>所属项目：</th>"
    		+"<td style='width:300px'>"+row.projectname+"</td>"
    		+"</tr>"
    		+"<tr>"
    		+"<th style='text-align:right'>预计开始时间：</th>"
    		+"<td style='width:300px'>"+row.expectedStartTime+"</td>"
    		+"<th style='text-align:right'>预计结束时间：</th>"
    		+"<td style='width:300px'>"+row.expectedEndTime+"</td>"
    		+"</tr>"
    		+"<tr>"
    		+"<th style='text-align:right'>实际开始时间：</th>"
    		+"<td style='width:160px'>"+row.actualStartTime+"</td>"
    		+"<th style='text-align:right'></th>"
    		+"<td style='width:160px'></td>"
    		+"</tr>"
    		+"<tr>"
    		+"<th style='text-align:right'>已执行用例：</th>"
    		+"<td style='width:160px'>"+row.exeCount+"</td>"
    		+"<th style='text-align:right'>未执行用例：</th>"
    		+"<td style='width:160px'>"+row.notExeCount+"</td>"
    		+"</tr>"
    		);
	//测试包结果信息
	var packageId = row.missionId;
	var urlStr = baseUrl + '/testCasePkgManager/testCasePackageAction!getBugStaticsByPkgId.action';
	$.post(
			urlStr,
			{'dto.testCasePackage.packageId':packageId},
			function(dataObj) {
		        if(dataObj){
		        	dataObj = JSON.parse(dataObj);
		        	//未测试
		        	var noTestCount = dataObj.allCount-dataObj.passCount-dataObj.failedCount-dataObj.blockCount-dataObj.waitAuditCount-dataObj.invalidCount
		        	                  -dataObj.waitModifyCount-dataObj.waitModifyCount;
		        	$("#allCount").html(dataObj.allCount);
		        	$("#passCount").html(dataObj.passCount);
		        	$("#failedCount").html(dataObj.failedCount);
		        	$("#blockCount").html(dataObj.blockCount);
		        	$("#invalidCount").html(dataObj.invalidCount);
		        	$("#noTestCount").html(noTestCount);
		        }
			},"text"
		);
}
$('a[href="#testPkgDetailResult"]').click(function(){
	$("#testPkgDetail").hide();
	$("#testPkgDetail").next().show();
});
$('a[href="#testPkgDetail"]').click(function(){
	$("#testPkgDetail").show();
	$("#testPkgDetail").next().hide();
});
function closePkgDetailWin(){
	$("#detailPkgWin").xwindow('close');
}
//查看详情(迭代)
function showIterationDetail(row){
	$(".nav-lattice").show();
	$(".nav-lattice").children().addClass("active");
	$(".nav-lattice").children().next().removeClass("active");
	$("#iterationDetail").addClass("in active");
	$("#iterationReport").removeClass("in active");
	$("#detailIterationWin").parent().css("border","none");
	$("#detailIterationWin").prev().css({ color: "#ffff", background: "#101010" });
	$("#iterationDetail").show();
	$("#iterationDetail").next().hide();
	$("#detailIterationWin").xwindow('setTitle','详情').xwindow('open');
	$("#detailIterationWin").xwindow("vcenter");
	var missionShortName = row.missionName;
	if(row.missionName.length > 8){
		missionShortName = row.missionName.substring(0,8)+"..";
	}
	if(row.startTime){
		row.startTime = row.startTime.substring(0,10);
	}else{
		row.startTime = "";
	}
	if(row.endTime){
		row.endTime = row.endTime.substring(0,10);
	}else{
		row.endTime = "";
	}
	
	$("#detailIterationTable").empty().append("<tr>"
    		+"<th style='text-align:right'>迭代名称：</th>"
    		+"<td style='width:245px' title='"+row.missionName+"'>"+missionShortName+"</td>"
    		+"<th style='text-align:right'></th>"
    		+"<td style='width:245px'></td>"
    		+"</tr>"
    		+"<tr>"
    		+"<th style='text-align:right'>关联项目：</th>"
    		+"<td style='width:245px;' title='"+row.projectname+"'>"+row.projectname.substring(0,8)+"...</td>"
    		+"<th style='text-align:right'></th>"
    		+"<td style='width:245px'></td>"
    		+"</tr>"
    		+"<tr>"
    		+"<th style='text-align:right'>开始时间：</th>"
    		+"<td style='width:245px'>"+row.startTime+"</td>"
    		+"<th style='text-align:right'>结束时间：</th>"
    		+"<td style='width:245px'>"+row.endTime+"</td>"
    		+"</tr>");
	//回填迭代报告数据
	//将所有数目都设置为0
	$("#11").html(0);
	$("#22").html(0);
	$("#33").html(0);
	$("#44").html(0);
	$("#55").html(0);
	$("#66").html(0);
	$("#aa").html(0);
	$("#bb").html(0);
	$("#cc").html(0);
	$("#dd").html(0);
	$("#fixCount").html(0);
	$("#noBugCount").html(0);
	$("#attributionMissions").html(0);
	$("#runingMissions").html(0);
	$("#finishMissions").html(0);
	$("#terminationMissions").html(0);
	$("#stopMissions").html(0);
	$("#allMissions").html(0);
	$.post(baseUrl+"/overview/overviewAction!getIterationDetails.action",{
		'dto.iterationId':row.missionId
	},function(data){
		if(data[0].length > 0){
			var allBugCounts = 0;
			for(var i=0;i<data[0].length;i++){
				if(data[0][i].status == 2){
					$("#22").html(data[0][i].countNum);
					allBugCounts = allBugCounts + data[0][i].countNum;
				}
				if(data[0][i].status == 3){
					$("#33").html(data[0][i].countNum);
					allBugCounts = allBugCounts + data[0][i].countNum;
				}
				if(data[0][i].status == 5){
					$("#44").html(data[0][i].countNum);
					allBugCounts = allBugCounts + data[0][i].countNum;
				}
				if(data[0][i].status == 1){
					$("#55").html(data[0][i].countNum);
					allBugCounts = allBugCounts + data[0][i].countNum;
				}
				if(data[0][i].status == 4){
					$("#66").html(data[0][i].countNum);
					allBugCounts = allBugCounts + data[0][i].countNum;
				}
			}
			$("#11").html(allBugCounts);
		}
		if(data[1].length > 0){
			$("#aa").html(data[1][0].allcount);
			$("#bb").html(data[1][0].validCout);
			$("#cc").html(data[1][0].closedCount);
			$("#dd").html(data[1][0].validCout - data[1][0].closedCount);
			$("#fixCount").html(data[1][0].fixCount);
			$("#noBugCount").html(data[1][0].noBugCount);
		}
		if(data[2].length > 0){
			var allCounts = 0;
			for(var i=0;i<data[2].length;i++){
				if(data[2][i].status == "0"){
					$("#attributionMissions").html(data[2][i].countNum);
					allCounts = allCounts + data[2][i].countNum;
				}
				if(data[2][i].status == "1"){
					$("#runingMissions").html(data[2][i].countNum);
					allCounts = allCounts + data[2][i].countNum;
				}
				if(data[2][i].status == "2"){
					$("#finishMissions").html(data[2][i].countNum);
					allCounts = allCounts + data[2][i].countNum;
				}
				if(data[2][i].status == "3"){
					$("#terminationMissions").html(data[2][i].countNum);
					allCounts = allCounts + data[2][i].countNum;
				}
				if(data[2][i].status == "4"){
					$("#stopMissions").html(data[2][i].countNum);
					allCounts = allCounts + data[2][i].countNum;
				}
			}
			$("#allMissions").html(allCounts);
		}
	},'json');
}
$('a[href="#iterationReport"]').click(function(){
	$("#iterationDetail").hide();
	$("#iterationDetail").next().show();
});
$('a[href="#iterationDetail"]').click(function(){
	$("#iterationDetail").show();
	$("#iterationDetail").next().hide();
});

function closeIterationDetailWin(){
	$("#detailIterationWin").xwindow('close');
}
//显示日志datagrid
function loadOtherMissionLog(id){
	$("#rizhi").xdatagrid({
		url: baseUrl + '/otherMission/otherMissionAction!getMissionLog.action',
		method: 'post',
		queryParams: {
			"dto.otherMission.missionId":id
		},
		emptyMsg:"无数据",
		fitColumns: true,
		singleSelect: true,
		pagination: true,
		pageNumber: 1,
		pageSize: 10,
		pageList:[10,30,50],
		layout:['list','first','prev','manual','next','last','refresh','info'],
		columns:[[
			{field:'operateTime',title:'操作时间',width:"25%",align:'center',formatter:operateTimeFormat},
			{field:'operatePerson',title:'操作者',width:"25%",align:'center'},
			{field:'operateType',title:'操作类型',width:"25%",align:'center',formatter:operateTypeFormat},
			{field:'operateDetail',title:'操作详情',align:'center',width:"25%",formatter:operateDetailFormat}
		]],
		onSelect : function(index,row){
			$(".datagrid-row-checked").css({"background":"none","color":"#404040"});
		},
		onLoadSuccess : function (data) {								
			
		}
	});
}
function operateTimeFormat(value,row,index){
	if(value){
		return "<span title='"+value+"'>"+value.substring(0,10)+"</span>";
	}
	return "";
}
function operateTypeFormat(value,row,index){
	if(value == "1"){
		return "新建任务并分配";
	}else if(value == "2"){
		return "修改状态";
	}else if(value == "3"){
		return "填写进度";
	}else if(value == "4"){
		return "修改工作量";
	}
}
function operateDetailFormat(value,row,index){
	return "<span title='"+value+"'>"+value.substring(0,20)+".....</span>";
}
//取消提交并关闭弹窗
function closeWin1() {
	objs.$addOrEditWin.xwindow('close');
}
//加载所属项目下拉菜单
function loadProjectList(){
	if($("#isAdmin").text() == "2" || $("#isAdmin").text() == "1"){
		$.post(
				baseUrl + "/otherMission/otherMissionAction!getProjectLists.action",
				null,
				function(dat) {
					if (dat != null) {
						projectList = dat.rows;
						//加载下拉菜单选项(为管理员时)
						var opti = '<option value="">-请选择项目-</option>';
						if(dat.rows.length > 0){
							for(var i=0;i<dat.rows.length;i++){
								opti = opti + '<option value="'+dat.rows[i].projectId+'">'+dat.rows[i].projectName+'</option>';
							}
						}
						$("#projectNa").next("div.searchable-select").remove();
						$("#projectNa").html(opti);
						$("#projectNa").off().on("change",function(){
							if(initFlg == "1"){
								loadOtherMission();
							}
						});
						$('#projectNa').searchableSelect();
					} else {
						$.xnotify("系统错误！", {type:'warning'});
						$.xalert({title:'提示',msg:'系统错误！'});
					}
				}, "json");		
	}else{
		//加载下拉菜单选项(非管理员时)
		var opti = '<option value="">-请选择项目-</option>';
		$.post(baseUrl + "/otherMission/otherMissionAction!getProjectListsRelated.action?dto.related=charge",null,function(datt) {
			projectList = datt.rows;
			if(datt.rows.length > 0){
				for(var p=0;p<datt.rows.length;p++){
					opti = opti + '<option value="'+datt.rows[p].projectId+'">'+datt.rows[p].projectName+'</option>';
				}
			}
			$("#projectNa").next("div.searchable-select").remove();
			$("#projectNa").html(opti);
			$("#projectNa").off().on("change",function(){
				if(initFlg == "1"){
					loadOtherMission();
				}
			});
			$('#projectNa').searchableSelect();
		},'json');
	}

}
//加载可执行任务员工列表
function loadPeopleLists(){
	if(haveLoadPeople==1){
		return;
	}
	$.post(
			baseUrl + "/otherMission/otherMissionAction!getPeopleLists.action",
			null,
			function(dat) {
				if (dat != null) {
					peopleList = dat.rows;
					$(".peopleList").xcombobox({
						data:dat.rows,
						onChange:function(newValue,oldValue){
							selectedPeople = newValue;
							var chargePeopleList = [];
							var chargePeopleNameList = [];
							if(newValue.length > 0 && dat.rows.length > 0){
								for(var i=0;i<newValue.length;i++){
									for(var j=0;j<dat.rows.length;j++){
										if(newValue[i] == dat.rows[j].id){
											chargePeopleList.push(dat.rows[j]);
											chargePeopleNameList.push(dat.rows[j].name);
										}
									}
								}
								$(".peopleList").next().children().next().attr("title",chargePeopleNameList.toString());
								$(".inchargePeople").xcombobox({
									data:chargePeopleList
								});
								if(newValue.length == 1){
									$(".inchargePeople").xcombobox("setValue",newValue[0]);
								}
							}else{
								$(".inchargePeople").xcombobox({
									data:[]
								});
							}
						}
					});
					$(".concernList").xcombobox({
						data:dat.rows,
						onChange:function(newValue,oldValue){
							selectedConcerns = newValue;
							var chargePeopleNameList = [];
							if(newValue.length > 0 && dat.rows.length > 0){
								for(var i=0;i<newValue.length;i++){
									for(var j=0;j<dat.rows.length;j++){
										if(newValue[i] == dat.rows[j].id){
											chargePeopleNameList.push(dat.rows[j].name);
										}
									}
								}
								$(".concernList").next().children().next().attr("title",chargePeopleNameList.toString());
							}
						}
					});
					var opti = '<option value="">-请选择人员-</option>';
					if(dat.rows.length > 0){
						for(var i=0;i<dat.rows.length;i++){
							opti = opti + '<option value="'+dat.rows[i].id+'">'+dat.rows[i].name+'</option>';
						}
					}
					$("#peopleName").html(opti);
					$("#peopleName").off().on("change",function(){
						if(initFlg == "1"){
							loadOtherMission();
						}
					});
					$('#peopleName').searchableSelect();
					haveLoadPeople= 1;
				} else {
					/*$.xnotify("系统错误！", {type:'warning'});*/
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");		
}
//加载任务类别列表
function loadMissionCategory(){
	$.post(
			baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action",
			{
				"page": 1,
				"rows": 30,
				"dto.subName": "任务类别",
				"dto.flag": "1",
			},
			function(dat) {
				if (dat != null) {
					missionCategoryList = dat.rows;
					$(".missionCategory").xcombobox({
						data:dat.rows
					});
				} else {
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//加载任务紧急程度列表
function loadEmergencyDegree(){
	$.post(
			baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action",
			{
				"page": 1,
				"rows": 30,
				"dto.subName": "任务紧急程度",
				"dto.flag": "1",
			},
			function(dat) {
				if (dat != null) {
					emergencyDegreeList = dat.rows;
					$(".emergencyDegree").xcombobox({
						data:dat.rows
					});
				} else {
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//加载任务难易程度列表
function loadDifficultyDegree(){
	$.post(
			baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action",
			{
				"page": 1,
				"rows": 30,
				"dto.subName": "任务难易程度",
				"dto.flag": "1",
			},
			function(dat) {
				if (dat != null) {
					difficultyDegreeList = dat.rows;
					$(".difficultyDegree").xcombobox({
						data:dat.rows
					});
				} else {
					$.xalert({title:'提示',msg:'系统错误！'});
				}
				//loadOtherMission();
			}, "json");
}
$('a[href="#rizhi"]').click(function(){
	$("#xiangqing").hide();
	loadOtherMissionLog(missiId);
	$("#xiangqing").next().show();
});
$('a[href="#xiangqing"]').click(function(){
	$("#xiangqing").show();
	$("#xiangqing").next().hide();
});
//关闭详情页
function closeDetailWin(){
	$("#detailWin").xwindow('close');
}
//打开新增弹窗
function showAddWin() {
	$(".inchargePeople").xcombobox({
		data:[]
	});
	$("#addOrEditForm2").xform('clear');
	$("#addOrEditForm2 input").val("");
	$("#addOrEditWin2").parent().css("border","none");
	$("#addOrEditWin2").prev().css({ color: "#ffff", background: "#101010" });
	$(".missionNa").next().children("input").css({"width":"475px"});
	$(".missionDe").next().children("input").css({"width":"475px","height":"74px"});
	$("#addOrEditWin2").xwindow('setTitle','创建任务').xwindow('open');
	loadPeopleLists();
	loadProjectsAddMission();
	//加载任务类别列表
	loadMissionCategory();
	//加载任务紧急程度列表
	loadEmergencyDegree();
	//加载任务难易程度列表
	loadDifficultyDegree();
	}
//加载新增任务时项目下拉菜单
function loadProjectsAddMission(){
	$.post(
			baseUrl + "/otherMission/otherMissionAction!getProjectLists.action",
			null,
			function(dat) {
				if (dat != null) {
					projectList = dat.rows;
					//加载下拉菜单选项(为管理员时)
					if($("#isAdmin").text() == "2" || $("#isAdmin").text() == "1"){
						var opti = '<option value="">-请选择项目-</option>';
						if(dat.rows.length > 0){
							for(var i=0;i<dat.rows.length;i++){
								opti = opti + '<option value="'+dat.rows[i].projectId+'">'+dat.rows[i].projectName+'</option>';
							}
						}
						$(".projectIds").next("div.searchable-select").remove();
						$(".projectIds").html(opti);
						$('.projectIds').searchableSelect();
					}else{
						//加载下拉菜单选项(非管理员时)
						var opti = '<option value="">-请选择项目-</option>';
						$.post(baseUrl + "/otherMission/otherMissionAction!getProjectListsRelated.action?dto.related=create",null,function(datt) {
							if(datt.rows.length > 0){
								for(var p=0;p<datt.rows.length;p++){
									opti = opti + '<option value="'+datt.rows[p].projectId+'">'+datt.rows[p].projectName+'</option>';
								}
							}
							$(".projectIds").next("div.searchable-select").remove();
							$(".projectIds").html(opti);
							$('.projectIds').searchableSelect();
						},'json');
					}
				} else {
					/*$.xnotify("系统错误！", {type:'warning'});*/
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//取消提交并关闭弹窗
function closeWin() {
	$("#addOrEditWin2").xwindow('close');
}
//提交保存新增或修改的记录
function submitOtherMission() {
	//获取表单数据
	var objData = $("#addOrEditWin2").xserialize();
	//新增时，将状态设为0
	if(!objData["dto.otherMission.status"]){
		objData["dto.otherMission.status"] = "0";
	}
	//确定projectType，传到后台
	if(proJ){
		for(var i=0;i<projectList.length;i++){
			if(proJ == projectList[i].projectId){
				objData["dto.otherMission.projectType"] = projectList[i].projectType;
				break;
			}
		}
	}
	var saveOrUpdateUrl = "";
	if(objData["dto.otherMission.missionId"]){
		saveOrUpdateUrl = baseUrl + "/otherMission/otherMissionAction!update.action";
	}else{
		saveOrUpdateUrl = baseUrl + "/otherMission/otherMissionAction!add.action";
		objData["dto.otherMission.createUserId"] = $("#loginName").text();
	}
	
	//将选中的项目执行人拼接到表单数据，传到后台
	if(selectedPeople.length > 0){
		objData["dto.userIds"] = selectedPeople.toString();
	}
	//将选中的任务关注者拼接到表单数据，传到后台
	if(selectedConcerns.length > 0){
		objData["dto.concernIds"] = selectedConcerns.toString();
	}
	
//	for(var i=0;i<selectedPeople.length;i++){
//		objData["dto.userOtherMissions["+i+"].userId"] = selectedPeople[i];
//	}
	if(!objData["dto.otherMission.missionName"] || !objData["dto.otherMission.description"] || !objData["dto.otherMission.chargePersonId"] || selectedPeople.length == 0){
		$.xalert({title:'提交失败',msg:'请填写完整所有必填信息！'});
		return;
	}else{
		//判断是否勾选了同时创建测试项目
		if($('.checkPro').is(':checked')){
			if(!objData["dto.otherMission.predictStartTime"] || !objData["dto.otherMission.predictEndTime"] || !objData["dto.otherMission.projectId"]){
				$.xalert({title:'提交失败',msg:'勾选同时创建测试项目，需选择所属项目，预计开始时间，预计结束时间！'});
				return;
			}
		}
	}
	var t1 = /^\d+$/;
	if(objData["dto.otherMission.standardWorkload"] && !t1.test(objData["dto.otherMission.standardWorkload"])){
		$.xalert({title:'提交失败',msg:'请正确填写完整所有必填项！'});
		return;
	}
	//判断预计开始时间和预计结束时间前后关系
	if(objData["dto.otherMission.predictStartTime"] && objData["dto.otherMission.predictEndTime"]){
		if(objData["dto.otherMission.predictStartTime"] > objData["dto.otherMission.predictEndTime"]){
			$.xalert({title:'提交失败',msg:'预计结束时间必须在预计开始时间之后！'});
			return;
		}
	}
	if(objData["dto.otherMission.missionName"] &&objData["dto.otherMission.missionName"].length>32){
		$.xalert({title:'提交失败',msg:'任务名称长度不能超过32！'});
		return;
	}
	if(objData["dto.otherMission.description"] &&objData["dto.otherMission.description"].length>50){
		$.xalert({title:'提交失败',msg:'任务描述长度不能超过50！'});
		return;
	}
	$.post(
		saveOrUpdateUrl,
		objData,
		function(data) {
			if (data =="success") {
				$("#addOrEditWin2").xform('clear');
				$("#addOrEditWin2").xwindow('close');
				loadProjectList();
				$.xalert({title:'提示',msg:'操作成功！'});
					
			} else if(data =="existed"){
				$.xalert({title:'提交失败',msg:'该任务名称已存在，请勿重复添加！'});
			}else {
				$.xalert({title:'提交失败',msg:'系统错误！'});
			}
		}, "text");
}
//加载未被使用的项目
function loadProjectsNotUse(){
	$.post(
			baseUrl + '/otherMission/otherMissionAction!getProjectLists2.action',
			null,
			function(dat) {
				if (dat != null) {
					projectsNotUse = dat.rows;
				} else {
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//选择项目改变事件
function changeProject(obj){
	proJ = obj.value;
}
//新增项目
function addProject(){
	$("#addProjectForm").xform('clear');
	$("#addProject").parent().css("border","none");
	$("#addProject").prev().css({ color: "#ffff", background: "#101010" });
	$("#addProject").xwindow('setTitle','新增项目').xwindow('open');
	//获取相关人员
	relaUser();
}
//提交新增项目
function submitProject(){

	
	var urls = baseUrl + "/singleTestTask/singleTestTaskAction!add.action";
	var addOrSelectVal = $('input[name="dto.singleTest.proName"][value!=""]').prev().val();
	//检测必填数据
	var flag = requiredTestData();
	if(flag!=true){
		return;
	}
	//保存
	//先保存project表
	$.post(baseUrl + "/otherMission/otherMissionAction!addProject.action",{
		"dto.project.projectType":"1",
		"dto.project.createId":$("#accountId").text(),
		"dto.project.projectName":addOrSelectVal
	},function(data){
		if(data.oprateResult == "success"){
			//保存singletask表
			$.post(
					urls,
					{
					"dto.singleTest.taskId":"",
					"dto.singleTest.filterFlag":"1",
					"dto.singleTest.proName":addOrSelectVal,
					"dto.singleTest.psmId":$(".psmId").val(),
					"dto.singleTest.status":$(".editSta").xcombobox('getValue'),
					"dto.singleTest.taskProjectId":data.project.projectId,
					"dto.singleTest.proNum":$('input[name="dto.singleTest.proNum"]').prev().val(),
					"dto.singleTest.devDept":$('input[name="dto.singleTest.devDept"]').prev().val(),
					"dto.singleTest.planEndDate":$('input[name="dto.singleTest.planEndDate"]').prev().val(),
					"dto.singleTest.planStartDate":$('input[name="dto.singleTest.planStartDate"]').prev().val()
					/*"dto.singleTest.psmName":$('input[name="dto.singleTest.psmName"]').prev().find($('input[id^="_exui_textbox_input"]')).val(),*/
					},
				function(da) {
					if (da !=null) {
						$("#addProjectForm").xform('clear');
						$("#addProject").xwindow('close');
						//加载所属项目下拉菜单
						loadProjectsAddMission();
						//加载输入框搜索项目下拉菜单
						loadProjectList();
						//loadProjectsNotUse();
						$.xalert({title:'提示',msg:'操作成功！'});
					} else {
						$.xalert({title:'提交失败',msg:'该项目已存在！'});
					}
				}, "json");
		}else{
			$.xalert({title:'提交失败',msg:'该项目已存在！'});
		}
	}, "json");
}
// 取消提交并关闭新增项目弹窗
function closeProjectWin(){
	$("#addProject").xwindow('close');
}
function searchPm(){
	var keyId = $(".pm option:selected").val();
	var valueText = $(".pm option:selected").text();
	$(".psmId").val(keyId);
	$(".pmId").val(valueText);
}
//检测必填数据
function requiredTestData(){
	if($('.projectNums').xtextbox('getValue')==""){
		$.xalert({title:'提示',msg:'请填写项目编号!'});
		return false;
	}
	
	if($('.hadProject').xtextbox('getValue')==""){
		$.xalert({title:'提示',msg:'请填写项目名称!'});
		return false;
	}
	
	if($('.developmentDep').xtextbox('getValue')==""){
		$.xalert({title:'提示',msg:'请填写研发部门!'});
		return false;
	}
	
	if($('.proStName').val()==""){
		$.xalert({title:'提示',msg:'请选择PM!'});
		return false;
	}
	
	var planStartDate = $('.startTime').xdatebox('getValue');
	var planEndDate = $('.endTime').xdatebox('getValue');
	
	var startTime = planStartDate.replace(/-/g,"/");//替换字符，变成标准格式
	var endTime = planEndDate.replace(/-/g,"/");//替换字符，变成标准格式
	
	var time1 = new Date(Date.parse(startTime));  
	var time2 = new Date(Date.parse(endTime));
	
	if(time1 > time2){
		$.xalert({title:'提示',msg:'开始日期大于结束日期!'});
		return false;
	}
	
	if(planStartDate==""){
		$.xalert({title:'提示',msg:'请选择开始日期!'});
		return false;
	}
	if(planEndDate==""){
		$.xalert({title:'提示',msg:'请选择结束日期!'});
		return false;
	}
	
	return true;
}
//获取相关人员
function relaUser(){	
	$.post(
			baseUrl +'/userManager/userManagerAction!loadDefaultSelUser.action?dto.getPageNo=1&dto.pageSize=1000',
			null,
			function(data) {
				if (data != null) {
					var opti = '<option value="">-请选择PM-</option>';
					if(data.rows.length > 0){
						for(var i=0;i<data.rows.length;i++){
							opti = opti + '<option value="'+data.rows[i].keyObj+'">'+data.rows[i].valueObj+'</option>';
						}
					}
					$(".pm").next("div.searchable-select").remove();
					$(".pm").html(opti);
					$('.pm').searchableSelect();
				} else {
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}

//跳转到执行用例页面或者查看用例页面
function jumpToPkgPage(row){
	$.post(baseUrl + '/testCasePkgManager/testCasePackageAction!isMyTpgk.action',{
		'dto.packageId':row.missionId,
		'dto.taskId':row.projectId
	},function(data){
		if(data == 'exeCase'){
			$("<div></div>").xdialog({
		    	id:'executeTestCaseDlg',
		    	title:row.missionName +"用例包--执行用例",
		    	width : 1300,
		        height : 600,
		    	modal:true,
		    	href:baseUrl + '/testCasePkgManager/testCasePackageAction!executeTestCase.action',
		    	queryParams: { "testCasePackageId": row.missionId,   
		    	            	'pageSource':'task',    //用于在测试包管理里面【执行用例】时判断刷新哪个页面的
		    		},
		        onClose : function() {
		            $(this).dialog('destroy');
		        }
		    });
		}else if(data == 'reviewCase'){
			$("<div></div>").xdialog({
		    	id:'viewTestCaseDlg',
		    	title:row.missionName +"用例包--查看用例",
		    	width : 1300,
		        height : 600,
		    	modal:true,
		    	href:baseUrl + '/testCasePkgManager/testCasePackageAction!viewTestCase.action',
		    	queryParams: { "testCasePackageId": row.missionId},
		        onClose : function() {
		            $(this).dialog('destroy');
		        }
		    });
		}
	},'text');
}
//# sourceURL=overview.js