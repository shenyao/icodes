/*var objs = {
	$missionDg: $("#missionDg"),
	$addOrEditWin: $("#addOrEditWin1"),
	$addOrEditForm: $("#addOrEditForm1")
};*/
//存放所有可执行任务的人员
var peopleList = [];
//存放所有项目
var projectList = [];
//存放所有任务类别
var missionCategoryList = [];
//存放所有任务紧急程度
var emergencyDegreeList = [];
//存放所有任务难易程度
var difficultyDegreeList = [];
//存放任务id
var missiId = "";
//存放选中的项目
var proJ = "";
//存放未被使用的项目
var projectsNotUse = [];
//存放被选中的参与者
var selectedPeople = [];
//存放被选中的关注者
var selectedConcerns = [];
$(function() {
	$.parser.parse();
	//加载所有项目
	loadProjectList();
	//加载所有可执行任务的员工
	loadPeopleLists();
	$(".concerButton").hide();
	//加载其他任务列表数据
	loadOtherMission();
});

//查询任务信息
function searchOtherMission(){
	var missionUrl = "";
	missionUrl = baseUrl + '/otherMission/otherMissionAction!getAllOtherMissionList.action';
	$("#missionDg").xdatagrid({
		/*url: baseUrl + '/otherMission/otherMissionAction!otherMissionListLoad.action',*/
		url: missionUrl,
		method: 'post',
		queryParams: {
			"dto.otherMission.missionName":$("#missionName").val(),
			"dto.otherMission.status":$("#statu").val(),
			"dto.otherMission.projectId":$("#projectNa").val()
			/*"dto.userId":$("#accountId").text()*/
		},
		height: mainObjs.tableHeight,
		emptyMsg:"无数据",
		columns:[[
		    {field:'checkId',title:'选择',checkbox:true,align:'center',hidden:true},
			{field:'missionName',title:'任务名称',width:"12.7%",align:'center',formatter:missionNameFormat},
			/*{field:'missionCategory',title:'任务类别',align:'center',halign:'center',formatter:missionCategoryFormat},
			{field:'missionType',title:'任务类型',align:'center',halign:'center',formatter:missionTypeFormat},*/
			{field:'projectId',title:'所属项目',width:"9%",align:'center',formatter:projectFormat},
			{field:'missionJoiners',title:'任务参与者',width:"9%",align:'center',formatter:missionPersonFormat},
			{field:'chargePersonId',title:'任务负责人',width:"9%",align:'center',halign:'center',formatter:chargePersonFormat},
			/*{field:'emergencyDegree',title:'紧急程度',align:'center',formatter:emergencyDegreeFormat},
			{field:'difficultyDegree',title:'难易程度',align:'center',formatter:difficultyDegreeFormat},
			{field:'standardWorkload',title:'标准工作量',align:'center'},*/
			{field:'actualWorkload',title:'实际工作量(小时)',width:"11%",align:'center'},
			{field:'description',title:'任务描述',width:"8%",align:'center',formatter:descriptionFormat},
			{field:'completionDegree',title:'任务进度(%)',width:"9%",align:'center'},
			{field:'status',title:'状态',width:"9%",align:'center',formatter:statusFormat},
			/*{field:'createUserId',title:'发起人',align:'center'},*/
			{field:'predictStartTime',title:'预计开始时间',width:"9%",align:'center',formatter:predictStartTimeFormat},
			/*{field:'predictEndTime',title:'预计完成时间',width:"9.8%",align:'center',formatter:predictEndTimeFormat},*/
			{field:'realStartTime',title:'实际开始时间',width:"9%",align:'center',formatter:predictEndTimeFormat},
			{field:'ttt',title:'操作',align:'center',width:"9.3%",formatter:operationFormat}
			/*{field:'createTime',title:'创建时间',align:'center'}*/
		]],
		onLoadSuccess : function (data) {								
		}
	});
}



//其他任务数据
function loadOtherMission(){
	var missionUrl = "";
	missionUrl = baseUrl + '/otherMission/otherMissionAction!getAllOtherMissionList.action';
	
	$("#missionDg").xdatagrid({
		url: missionUrl,
		method: 'post',
		height: mainObjs.tableHeight,
		emptyMsg:"无数据",
		columns:[[
		    {field:'checkId',title:'选择',checkbox:true,align:'center',hidden:true},
		    {field:'missionName',title:'任务名称',width:"12.7%",align:'center',formatter:missionNameFormat},
	
			/*{field:'missionCategory',title:'任务类别',align:'center',halign:'center',formatter:missionCategoryFormat},
			{field:'missionType',title:'任务类型',align:'center',halign:'center',formatter:missionTypeFormat},*/
			{field:'projectId',title:'所属项目',width:"9%",align:'center',formatter:projectFormat},
			{field:'missionJoiners',title:'任务参与者',width:"9%",align:'center',formatter:missionPersonFormat},
			{field:'chargePersonId',title:'任务负责人',width:"9%",align:'center',halign:'center',formatter:chargePersonFormat},
			/*{field:'emergencyDegree',title:'紧急程度',align:'center',formatter:emergencyDegreeFormat},
			{field:'difficultyDegree',title:'难易程度',align:'center',formatter:difficultyDegreeFormat},
			{field:'standardWorkload',title:'标准工作量',align:'center'},*/
			{field:'actualWorkload',title:'实际工作量(小时)',width:"11%",align:'center'},
			{field:'description',title:'任务描述',width:"8%",align:'center',formatter:descriptionFormat},
			{field:'completionDegree',title:'任务进度(%)',width:"9%",align:'center'},
			{field:'status',title:'状态',width:"9%",align:'center',formatter:statusFormat},
			/*{field:'createUserId',title:'发起人',align:'center'},*/
			{field:'predictStartTime',title:'预计开始时间',width:"9%",align:'center',formatter:predictStartTimeFormat},
			/*{field:'predictEndTime',title:'预计完成时间',width:"9.8%",align:'center',formatter:predictEndTimeFormat},*/
			{field:'realStartTime',title:'实际开始时间',width:"9%",align:'center',formatter:predictEndTimeFormat},
			{field:'ttt',title:'操作',align:'center',width:"9.3%",formatter:operationFormat}
			/*{field:'createTime',title:'创建时间',align:'center'}*/
		]],

		onLoadSuccess : function (data) {								


		}
	});
}

function missionNameFormat(value,row,index){
	if(value.length > 8){
		return "<span title='"+value+"'>"+value.substring(0,8)+"...</span>";
	}else{
		return "<span title='"+value+"'>"+value+"</span>";
	}
}
function missionPersonFormat(value,row,index){
	return "<span title='"+value+"'>"+value.substring(0,5)+"....</span>";
}
function predictStartTimeFormat(value,row,index){
	if(value){
		return value.substring(0,10);
	}
	return "";
}
function predictEndTimeFormat(value,row,index){
	if(value){
		return value.substring(0,10);
	}
	return "";
}
function statusFormat(value,row,index){
	if(value == "0"){
		return "未开始";
	}else if(value == "1"){
		return "进行中";
	}else if(value == "2"){
		return "完成";
	}else if(value == "3"){
		return "终止";
	}else if(value == "4"){
		return "暂停";
	}
	return "";
}
function projectFormat(value,row,index){
	if(value){
		for(var i=0;i<projectList.length;i++){
			if(value == projectList[i].projectId){
				if(projectList[i].projectName.length > 8){
					return "<span title='"+projectList[i].projectName+"'>"+projectList[i].projectName.substring(0,8)+"...</span>";
				}else{
					return "<span title='"+projectList[i].projectName+"'>"+projectList[i].projectName+"</span>";
				}
				
			}
		}
	}
	return "<span></span>";
}

function chargePersonFormat(value,row,index){
	for(var i=0;i<peopleList.length;i++){
		if(value == peopleList[i].id){
			return peopleList[i].name;
		}
	}
	return "暂无";
}
function descriptionFormat(value,row,index){
	return "<span title='"+value+"'>"+value.substring(0,5)+"....</span>";
}
//操作
function operationFormat(value,row,index){
	var html = "<a style='cursor:pointer;' onclick='showDetail("+JSON.stringify(row)+")'>详情</a>";
	return html;
}

//加载所属项目下拉菜单
function loadProjectList(){
	if($("#isAdmin").text() == "2" || $("#isAdmin").text() == "1"){
		$.post(
				baseUrl + "/otherMission/otherMissionAction!getProjectLists.action",
				null,
				function(dat) {
					if (dat != null) {
						projectList = dat.rows;
						//加载下拉菜单选项(为管理员时)
						var opti = '<option value="">-请选择项目-</option>';
						if(dat.rows.length > 0){
							for(var i=0;i<dat.rows.length;i++){
								opti = opti + '<option value="'+dat.rows[i].projectId+'">'+dat.rows[i].projectName+'</option>';
							}
						}
						$("#projectNa").next("div.searchable-select").remove();
						$("#projectNa").html(opti);
						$('#projectNa').searchableSelect();
					} else {
						/*$.xnotify("系统错误！", {type:'warning'});*/
						$.xalert({title:'提示',msg:'系统错误！'});
					}
				}, "json");
	}else{
		//加载下拉菜单选项(非管理员时)
		var opti = '<option value="">-请选择项目-</option>';
		$.post(baseUrl + "/otherMission/otherMissionAction!getProjectListsRelated.action?dto.related=all",null,function(datt) {
			projectList = datt.rows;
			if(datt.rows.length > 0){
				for(var p=0;p<datt.rows.length;p++){
					opti = opti + '<option value="'+datt.rows[p].projectId+'">'+datt.rows[p].projectName+'</option>';
				}
			}
			$("#projectNa").next("div.searchable-select").remove();
			$("#projectNa").html(opti);
			$('#projectNa').searchableSelect();
		},'json');
	}

}
//加载可执行任务员工列表
function loadPeopleLists(){
	$.post(
			baseUrl + "/otherMission/otherMissionAction!getPeopleLists.action",
			null,
			function(dat) {
				if (dat != null) {
					peopleList = dat.rows;
					$(".peopleList").xcombobox({
						data:dat.rows,
						onChange:function(newValue,oldValue){
							selectedPeople = newValue;
							var chargePeopleList = [];
							var chargePeopleNameList = [];
							if(newValue.length > 0 && dat.rows.length > 0){
								for(var i=0;i<newValue.length;i++){
									for(var j=0;j<dat.rows.length;j++){
										if(newValue[i] == dat.rows[j].id){
											chargePeopleList.push(dat.rows[j]);
											chargePeopleNameList.push(dat.rows[j].name);
										}
									}
								}
								$(".peopleList").next().children().next().attr("title",chargePeopleNameList.toString());
								$(".inchargePeople").xcombobox({
									data:chargePeopleList
								});
								if(newValue.length == 1){
									$(".inchargePeople").xcombobox("setValue",newValue[0]);
								}
							}else{
								$(".inchargePeople").xcombobox({
									data:[]
								});
							}
						}
					});
					$(".concernList").xcombobox({
						data:dat.rows,
						onChange:function(newValue,oldValue){
							selectedConcerns = newValue;
							var chargePeopleNameList = [];
							if(newValue.length > 0 && dat.rows.length > 0){
								for(var i=0;i<newValue.length;i++){
									for(var j=0;j<dat.rows.length;j++){
										if(newValue[i] == dat.rows[j].id){
											chargePeopleNameList.push(dat.rows[j].name);
										}
									}
								}
								$(".concernList").next().children().next().attr("title",chargePeopleNameList.toString());
							}
						}
					});
				} else {
					/*$.xnotify("系统错误！", {type:'warning'});*/
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//查看详情
function showDetail(row){
	//加载任务类别列表
	loadMissionCategory();
	//加载任务紧急程度列表
	loadEmergencyDegree();
	//加载任务难易程度列表
	loadDifficultyDegree();
	$(".nav-lattice").show();
	missiId = row.missionId;
	$(".nav-lattice").children().addClass("active");
	$(".nav-lattice").children().next().removeClass("active");
	$("#xiangqing").addClass("in active");
	$("#rizhi").removeClass("in active");
	var uu = 0;
	var ii = 0;
	var pp = 0;
	var tt = 0;
	var qq = 0;
	for(var i=0;i<projectList.length;i++){
		if(row.projectId == projectList[i].projectId){
			row.projectId = projectList[i].projectName;
			break;
		}else{
			tt = tt + 1;
		}
	}
	if(tt == projectList.length){
		row.projectId = "";
	}
	
	for(var t=0;t<peopleList.length;t++){
		if(row.chargePersonId == peopleList[t].id){
			row.chargePersonId = peopleList[t].name;
			break;
		}else{
			qq = qq + 1;
		}
	}
	if(qq == peopleList.length){
		row.chargePersonId = "";
	}
	
	if(missionCategoryList.length > 0){
		for(var p=0;p<missionCategoryList.length;p++){
			if(row.missionCategory == missionCategoryList[p].typeId){
				row.missionCategory = missionCategoryList[p].typeName;
				break;
			}else{
				pp = pp + 1;
			}
		}
		if(pp == missionCategoryList.length){
			row.missionCategory = "";
		}
	}else{
		row.missionCategory = "";
	}
	if(emergencyDegreeList.length > 0){
		for(var q=0;q<emergencyDegreeList.length;q++){
			if(row.emergencyDegree == emergencyDegreeList[q].typeId){
				row.emergencyDegree = emergencyDegreeList[q].typeName;
				break;
			}else{
				uu = uu + 1;
			}
		}
		if(uu == emergencyDegreeList.length){
			row.emergencyDegree = "";
		}
	}else{
		row.emergencyDegree = "";
	}
	
	if(difficultyDegreeList.length > 0){
		for(var h=0;h<difficultyDegreeList.length;h++){
			if(row.difficultyDegree == difficultyDegreeList[h].typeId){
				row.difficultyDegree = difficultyDegreeList[h].typeName;
				break;
			}else{
				ii = ii + 1;
			}
		}
		if(ii == difficultyDegreeList.length){
			row.difficultyDegree = "";
		}
	}else{
		row.difficultyDegree = "";
	}
	
	if(row.status == "0"){
		row.status = "未开始";
	}else if(row.status == "1"){
		row.status = "进行中";
	}else if(row.status == "2"){
		row.status = "完成";
	}else if(row.status == "3"){
		row.status = "终止";
	}else if(row.status == "4"){
		row.status = "暂停";
	}else{
		row.status = "";
	}
	if(row.predictStartTime){
		row.predictStartTime = row.predictStartTime.substring(0,10);
	}else{
		row.predictStartTime = "";
	}
	if(row.predictEndTime){
		row.predictEndTime = row.predictEndTime.substring(0,10);
	}else{
		row.predictEndTime = "";
	}
	if(row.realStartTime){
		row.realStartTime = row.realStartTime.substring(0,10);
	}else{
		row.realStartTime = "";
	}
	if(!row.stopReason){
		row.stopReason = "";
	}
	$.ajax({
		  url: baseUrl + "/otherMission/otherMissionAction!getUserNames.action",
		  cache: false,
		  async: false,
		  type: "POST",
		  data: {
			  'dto.otherMission.missionId':row.missionId
		  },
		  dataType:"text",
		  success: function(data){
			  if (data !=null) {
				  row.peoples = data;
				  row.peopless = "";
				  if(row.peoples.length > 15){
					  row.peopless = row.peoples.substring(0,15) + "...";
				  }else{
					  row.peopless = row.peoples;
				  }
				  $.post(baseUrl + '/otherMission/otherMissionAction!getConcernNames.action',{'dto.otherMission.missionId':row.missionId},function(da){
					  if(da != null){
						  if(da == "failed"){
							  row.concerns = "";
						  }else{
							  row.concerns = da;
							  row.concernss = "";
							  if(row.concerns.length > 15){
								  row.concernss = row.concerns.substring(0,15) + "...";
							  }else{
								  row.concernss = row.concerns;
							  }
						  }
						  $("#detailTable").empty().append("<tr>"
						    		+"<th style='text-align:right'>任务名称：</th>"
						    		+"<td style='width:160px'>"+row.missionName+"</td>"
						    		+"<th style='text-align:right'></th>"
						    		+"<td style='width:160px'></td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>任务描述：</th>"
						    		+"<td style='width:160px;' title='"+row.description+"'>"+row.description.substring(0,10)+"...</td>"
						    		+"<th style='text-align:right'></th>"
						    		+"<td style='width:160px'></td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>任务类别：</th>"
						    		+"<td style='width:160px'>"+row.missionCategory+"</td>"
						    		+"<th style='text-align:right'></th>"
						    		+"<td style='width:160px'></td>"
						    		/*+"<th style='text-align:right'>任务类型：</th>"
						    		+"<td style='width:160px'>其他任务</td>"*/
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>所属项目：</th>"
						    		+"<td style='width:160px'>"+row.projectId+"</td>"
						    		+"<th style='text-align:right'></th>"
						    		+"<td style='width:160px'></td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>任务参与者：</th>"
						    		+"<td style='width:160px' title='"+row.peoples+"'>"+row.peopless+"</td>"
						    		+"<th style='text-align:right'>任务负责人：</th>"
						    		+"<td style='width:160px'>"+row.chargePersonId+"</td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>任务关注者：</th>"
						    		+"<td style='width:160px' title='"+row.concerns+"'>"+row.concernss+"</td>"
						    		+"<th style='text-align:right'></th>"
						    		+"<td style='width:160px'></td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>紧急程度：</th>"
						    		+"<td style='width:160px'>"+row.emergencyDegree+"</td>"
						    		+"<th style='text-align:right'>难易程度：</th>"
						    		+"<td style='width:160px'>"+row.difficultyDegree+"</td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>标准工作量(小时)：</th>"
						    		+"<td style='width:160px'>"+row.standardWorkload+"</td>"
						    		+"<th style='text-align:right'>实际工作量(小时)：</th>"
						    		+"<td style='width:160px'>"+row.actualWorkload+"</td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>任务进度(%)：</th>"
						    		+"<td style='width:160px'>"+row.completionDegree+"</td>"
						    		+"<th style='text-align:right'>任务状态：</th>"
						    		+"<td style='width:160px'>"+row.status+"</td>"
						    		+"</tr>"
						    		+"<tr class='klklkl'>"
						    		+"<th style='text-align:right'>终止原因：</th>"
						    		+"<td style='width:160px;' title='"+row.stopReason+"'>"+row.stopReason.substring(0,10)+"...</td>"
						    		+"<th style='text-align:right'></th>"
						    		+"<td style='width:160px'></td>"
						    		+"</tr>"
						    		+"<tr class='lkl'>"
						    		+"<th style='text-align:right'>暂停原因：</th>"
						    		+"<td style='width:160px;' title='"+row.stopReason+"'>"+row.stopReason.substring(0,10)+"...</td>"
						    		+"<th style='text-align:right'></th>"
						    		+"<td style='width:160px'></td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>预计开始时间：</th>"
						    		+"<td style='width:160px'>"+row.predictStartTime+"</td>"
						    		+"<th style='text-align:right'>预计结束时间：</th>"
						    		+"<td style='width:160px'>"+row.predictEndTime+"</td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>实际开始时间：</th>"
						    		+"<td style='width:160px'>"+row.realStartTime+"</td>"
						    		+"</tr>"
						    		+"<tr>"
						    		+"<th style='text-align:right'>任务发起人：</th>"
						    		+"<td style='width:160px'>"+row.createUserId+"</td>"
						    		+"<th style='text-align:right'>任务创建时间：</th>"
						    		+"<td style='width:160px'>"+row.createTime+"</td>"
						    		+"</tr>");
						  if(row.status == "终止"){
								$(".klklkl").show();
								$(".lkl").hide();
							}else if(row.status == "暂停"){
								$(".klklkl").hide();
								$(".lkl").show();
							}else{
								$(".klklkl").hide();
								$(".lkl").hide();
							}
					  }
				  },'text');
				} else {
					/*$.xnotify("系统错误！", {type:'warning'});*/
					$.xalert({title:'提示',msg:'系统错误！'});
				}
		   }
		});
	$("#detailWin").parent().css("border","none");
	$("#detailWin").prev().css({ color: "#ffff", background: "#101010" });
	/*loadOtherMissionLog(row.missionId);*/
	$("#xiangqing").show();
	$("#xiangqing").next().hide();
	$("#detailWin").xwindow('setTitle','详情').xwindow('open');
	$("#detailWin").xwindow("vcenter");
}
//显示日志datagrid
function loadOtherMissionLog(id){
	$("#rizhi").xdatagrid({
		url: baseUrl + '/otherMission/otherMissionAction!getMissionLog.action',
		method: 'post',
		queryParams: {
			"dto.otherMission.missionId":id
		},
		emptyMsg:"无数据",
		fitColumns: true,
		singleSelect: true,
		pagination: true,
		pageNumber: 1,
		pageSize: 10,
		pageList:[10,30,50],
		layout:['list','first','prev','manual','next','last','refresh','info'],
		columns:[[
			{field:'operateTime',title:'操作时间',width:"25%",align:'center',formatter:operateTimeFormat},
			{field:'operatePerson',title:'操作者',width:"25%",align:'center'},
			{field:'operateType',title:'操作类型',width:"25%",align:'center',formatter:operateTypeFormat},
			{field:'operateDetail',title:'操作详情',align:'center',width:"25%",formatter:operateDetailFormat}
		]],
		onSelect : function(index,row){
			$(".datagrid-row-checked").css({"background":"none","color":"#404040"});
		},
		onLoadSuccess : function (data) {								
			
		}
	});
}
function operateTimeFormat(value,row,index){
	if(value){
		return "<span title='"+value+"'>"+value.substring(0,10)+"</span>";
	}
	return "";
}
function operateTypeFormat(value,row,index){
	if(value == "1"){
		return "新建任务并分配";
	}else if(value == "2"){
		return "修改状态";
	}else if(value == "3"){
		return "填写进度";
	}else if(value == "4"){
		return "修改工作量";
	}
}
function operateDetailFormat(value,row,index){
	return "<span title='"+value+"'>"+value.substring(0,20)+".....</span>";
}
//关注任务操作
function concernMissions(){
	var rows = $("#missionDg").xdatagrid('getSelections');
	if (rows.length == 0) {
		/*$.xnotify('请选择需要关注的任务', {type:'warning'});*/
		$.xalert({title:'提示',msg:'请选择需要关注的任务！'});
		return;
	}
	var missionIds = [];
	for(var i=0;i<rows.length;i++){
		missionIds.push(rows[i].missionId);
	}
	$.post(baseUrl + '/otherMission/otherMissionAction!concernMissions.action',{
		"dto.missionIds":missionIds.toString(),
		"dto.uId":$("#accountId").text()
	},function(data){
		if(data == "success"){
			/*$.xnotify('关注任务成功！', {type:'success'});*/
			$.xalert({title:'提示',msg:'关注任务成功！'});
			$("#missionDg").xdatagrid('clearSelections');
		}else{
			/*$.xnotify('请选择需要关注的任务', {type:'warning'});*/
			$.xalert({title:'提示',msg:'请选择需要关注的任务！'});
		}
	},'text');
}
//加载任务类别列表
function loadMissionCategory(){
	$.post(
			baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action",
			{
				"page": 1,
				"rows": 30,
				"dto.subName": "任务类别",
				"dto.flag": "1",
			},
			function(dat) {
				if (dat != null) {
					missionCategoryList = dat.rows;
					$(".missionCategory").xcombobox({
						data:dat.rows
					});
				} else {
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//加载任务紧急程度列表
function loadEmergencyDegree(){
	$.post(
			baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action",
			{
				"page": 1,
				"rows": 30,
				"dto.subName": "任务紧急程度",
				"dto.flag": "1",
			},
			function(dat) {
				if (dat != null) {
					emergencyDegreeList = dat.rows;
					$(".emergencyDegree").xcombobox({
						data:dat.rows
					});
				} else {
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//加载任务难易程度列表
function loadDifficultyDegree(){
	$.post(
			baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action",
			{
				"page": 1,
				"rows": 30,
				"dto.subName": "任务难易程度",
				"dto.flag": "1",
			},
			function(dat) {
				if (dat != null) {
					difficultyDegreeList = dat.rows;
					$(".difficultyDegree").xcombobox({
						data:dat.rows
					});
				} else {
					$.xalert({title:'提示',msg:'系统错误！'});
				}
				//loadOtherMission();
			}, "json");
}
$('a[href="#rizhi"]').click(function(){
	$("#xiangqing").hide();
	loadOtherMissionLog(missiId);
	$("#xiangqing").next().show();
});
$('a[href="#xiangqing"]').click(function(){
	$("#xiangqing").show();
	$("#xiangqing").next().hide();
});
//关闭详情页
function closeDetailWin(){
	$("#detailWin").xwindow('close');
}
//打开新增弹窗
function showAddWin1() {
	$(".inchargePeople").xcombobox({
		data:[]
	});
	$("#addOrEditForm1").xform('clear');
	$("#addOrEditForm1 input").val("");
	$("#addOrEditWin1").parent().css("border","none");
	$("#addOrEditWin1").prev().css({ color: "#ffff", background: "#101010" });
	$(".missionNa").next().children("input").css({"width":"475px"});
	$(".missionDe").next().children("input").css({"width":"475px","height":"74px"});
	$("#addOrEditWin1").xwindow('setTitle','创建任务').xwindow('open');
	loadProjectsAddMission();
}
//加载新增任务时项目下拉菜单
function loadProjectsAddMission(){
	$.post(
			baseUrl + "/otherMission/otherMissionAction!getProjectLists.action",
			null,
			function(dat) {
				if (dat != null) {
					projectList = dat.rows;
					//加载下拉菜单选项(为管理员时)
					if($("#isAdmin").text() == "2" || $("#isAdmin").text() == "1"){
						var opti = '<option value="">-请选择项目-</option>';
						if(dat.rows.length > 0){
							for(var i=0;i<dat.rows.length;i++){
								opti = opti + '<option value="'+dat.rows[i].projectId+'">'+dat.rows[i].projectName+'</option>';
							}
						}
						$(".projectIds").next("div.searchable-select").remove();
						$(".projectIds").html(opti);
						$('.projectIds').searchableSelect();
					}else{
						//加载下拉菜单选项(非管理员时)
						var opti = '<option value="">-请选择项目-</option>';
						$.post(baseUrl + "/otherMission/otherMissionAction!getProjectListsRelated.action?dto.related=create",null,function(datt) {
							if(datt.rows.length > 0){
								for(var p=0;p<datt.rows.length;p++){
									opti = opti + '<option value="'+datt.rows[p].projectId+'">'+datt.rows[p].projectName+'</option>';
								}
							}
							$(".projectIds").next("div.searchable-select").remove();
							$(".projectIds").html(opti);
							$('.projectIds').searchableSelect();
						},'json');
					}
				} else {
					/*$.xnotify("系统错误！", {type:'warning'});*/
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//取消提交并关闭弹窗
function closeWin() {
	$("#addOrEditWin1").xwindow('close');
}
//提交保存新增或修改的记录
function submit() {
	//获取表单数据
	var objData = $("#addOrEditWin1").xserialize();
	//新增时，将状态设为0
	if(!objData["dto.otherMission.status"]){
		objData["dto.otherMission.status"] = "0";
	}
	//确定projectType，传到后台
	if(proJ){
		for(var i=0;i<projectList.length;i++){
			if(proJ == projectList[i].projectId){
				objData["dto.otherMission.projectType"] = projectList[i].projectType;
				break;
			}
		}
	}
	var saveOrUpdateUrl = "";
	if(objData["dto.otherMission.missionId"]){
		saveOrUpdateUrl = baseUrl + "/otherMission/otherMissionAction!update.action";
	}else{
		saveOrUpdateUrl = baseUrl + "/otherMission/otherMissionAction!add.action";
		objData["dto.otherMission.createUserId"] = $("#loginName").text();
	}
	
	//将选中的项目执行人拼接到表单数据，传到后台
	if(selectedPeople.length > 0){
		objData["dto.userIds"] = selectedPeople.toString();
	}
	//将选中的任务关注者拼接到表单数据，传到后台
	if(selectedConcerns.length > 0){
		objData["dto.concernIds"] = selectedConcerns.toString();
	}
	
//	for(var i=0;i<selectedPeople.length;i++){
//		objData["dto.userOtherMissions["+i+"].userId"] = selectedPeople[i];
//	}
	if(!objData["dto.otherMission.missionName"] || !objData["dto.otherMission.description"] || !objData["dto.otherMission.chargePersonId"] || selectedPeople.length == 0){
		$.xalert({title:'提交失败',msg:'请填写完整所有必填信息！'});
		return;
	}else{
		//判断是否勾选了同时创建测试项目
		if($('.checkPro').is(':checked')){
			if(!objData["dto.otherMission.predictStartTime"] || !objData["dto.otherMission.predictEndTime"] || !objData["dto.otherMission.projectId"]){
				$.xalert({title:'提交失败',msg:'勾选同时创建测试项目，需选择所属项目，预计开始时间，预计结束时间！'});
				return;
			}
		}
	}
	var t1 = /^\d+$/;
	if(objData["dto.otherMission.standardWorkload"] && !t1.test(objData["dto.otherMission.standardWorkload"])){
		$.xalert({title:'提交失败',msg:'请正确填写完整所有必填项！'});
		return;
	}
	//判断预计开始时间和预计结束时间前后关系
	if(objData["dto.otherMission.predictStartTime"] && objData["dto.otherMission.predictEndTime"]){
		if(objData["dto.otherMission.predictStartTime"] > objData["dto.otherMission.predictEndTime"]){
			$.xalert({title:'提交失败',msg:'预计结束时间必须在预计开始时间之后！'});
			return;
		}
	}
	$.post(
		saveOrUpdateUrl,
		objData,
		function(data) {
			if (data =="success") {
				//判断是否勾选了同时创建测试项目
				if($('.checkPro').is(':checked')){
					if(projectsNotUse.length == 0){
						$("#addOrEditWin1").xform('clear');
						$("#addOrEditWin1").xwindow('close');
						loadOtherMission();
						loadProjectList();
						$.xalert({title:'提示',msg:'操作成功！'});
						loadProjectsNotUse();
					}else{
						for(var dd=0;dd<projectsNotUse.length;dd++){
							if(projectsNotUse[dd].projectId == objData["dto.otherMission.projectId"]){
								//保存测试项目
								$.post(baseUrl + "/singleTestTask/singleTestTaskAction!add.action",{
									"dto.singleTest.taskId":"",
									"dto.singleTest.filterFlag":"1",
									"dto.singleTest.proName":projectsNotUse[dd].projectName,
									"dto.singleTest.psmId":objData["dto.otherMission.chargePersonId"],
									"dto.singleTest.status":"",
									"dto.singleTest.taskProjectId":projectsNotUse[dd].projectId,
									"dto.singleTest.proNum":objData["dto.otherMission.missionName"],
									"dto.singleTest.devDept":"研发部",
									"dto.singleTest.planEndDate":objData["dto.otherMission.predictEndTime"],
									"dto.singleTest.planStartDate":objData["dto.otherMission.predictStartTime"],
									"dto.singleTest.psmName":$(".inchargePeople").xcombobox("getText")
								},function(dataa){
									if (dataa !=null) {
										$("#addOrEditWin1").xform('clear');
										$("#addOrEditWin1").xwindow('close');
										loadOtherMission();
										loadProjectList();
										$.xalert({title:'提示',msg:'操作成功！'});
										loadProjectsNotUse();
									}
								},'text');
							}
						}
					}
				}else{
					$("#addOrEditWin1").xform('clear');
					$("#addOrEditWin1").xwindow('close');
					loadOtherMission();
					loadProjectList();
					$.xalert({title:'提示',msg:'操作成功！'});
					loadProjectsNotUse();
				}
			} else if(data =="existed"){
				$.xalert({title:'提交失败',msg:'该任务名称已存在，请勿重复添加！'});
			}else {
				$.xalert({title:'提交失败',msg:'系统错误！'});
			}
		}, "text");
}
//加载未被使用的项目
function loadProjectsNotUse(){
	$.post(
			baseUrl + '/otherMission/otherMissionAction!getProjectLists2.action',
			null,
			function(dat) {
				if (dat != null) {
					projectsNotUse = dat.rows;
				} else {
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//选择项目改变事件
function changeProject(obj){
	proJ = obj.value;
}
//新增项目
function addProject(){
	$("#addProjectForm").xform('clear');
	$("#addProject").parent().css("border","none");
	$("#addProject").prev().css({ color: "#ffff", background: "#101010" });
	$("#addProject").xwindow('setTitle','新增项目').xwindow('open');
	//获取相关人员
	relaUser();
}
//提交新增项目
function submitProject(){


var urls = baseUrl + "/singleTestTask/singleTestTaskAction!add.action";
var addOrSelectVal = $('input[name="dto.singleTest.proName"][value!=""]').prev().val();
//检测必填数据
var flag = requiredTestData();
if(flag!=true){
	return;
}
//保存
//先保存project表
$.post(baseUrl + "/otherMission/otherMissionAction!addProject.action",{
	"dto.project.projectType":"1",
	"dto.project.createId":$("#accountId").text(),
	"dto.project.projectName":addOrSelectVal
},function(data){
	if(data.oprateResult == "success"){
		//保存singletask表
		$.post(
				urls,
				{
				"dto.singleTest.taskId":"",
				"dto.singleTest.filterFlag":"1",
				"dto.singleTest.proName":addOrSelectVal,
				"dto.singleTest.psmId":$(".psmId").val(),
				"dto.singleTest.status":$(".editSta").xcombobox('getValue'),
				"dto.singleTest.taskProjectId":data.project.projectId,
				"dto.singleTest.proNum":$('input[name="dto.singleTest.proNum"]').prev().val(),
				"dto.singleTest.devDept":$('input[name="dto.singleTest.devDept"]').prev().val(),
				"dto.singleTest.planEndDate":$('input[name="dto.singleTest.planEndDate"]').prev().val(),
				"dto.singleTest.planStartDate":$('input[name="dto.singleTest.planStartDate"]').prev().val()
				/*"dto.singleTest.psmName":$('input[name="dto.singleTest.psmName"]').prev().find($('input[id^="_exui_textbox_input"]')).val(),*/
				},
			function(da) {
				if (da !=null) {
					$("#addProjectForm").xform('clear');
					$("#addProject").xwindow('close');
					//加载所属项目下拉菜单
					loadProjectsAddMission();
					//加载输入框搜索项目下拉菜单
					loadProjectList();
					loadProjectsNotUse();
					$.xalert({title:'提示',msg:'操作成功！'});
				} else {
					$.xalert({title:'提交失败',msg:'该项目已存在！'});
				}
			}, "json");
	}else{
		$.xalert({title:'提交失败',msg:'该项目已存在！'});
	}
}, "json");
}
// 取消提交并关闭新增项目弹窗
function closeProjectWin(){
	$("#addProject").xwindow('close');
}
function searchPm(){
	var keyId = $(".pm option:selected").val();
	var valueText = $(".pm option:selected").text();
	$(".psmId").val(keyId);
	$(".pmId").val(valueText);
}
//检测必填数据
function requiredTestData(){
	if($('.projectNums').xtextbox('getValue')==""){
		$.xalert({title:'提示',msg:'请填写项目编号!'});
		return false;
	}
	
	if($('.hadProject').xtextbox('getValue')==""){
		$.xalert({title:'提示',msg:'请填写项目名称!'});
		return false;
	}
	
	if($('.developmentDep').xtextbox('getValue')==""){
		$.xalert({title:'提示',msg:'请填写研发部门!'});
		return false;
	}
	
	if($('.proStName').val()==""){
		$.xalert({title:'提示',msg:'请选择PM!'});
		return false;
	}
	
	var planStartDate = $('.startTime').xdatebox('getValue');
	var planEndDate = $('.endTime').xdatebox('getValue');
	
	var startTime = planStartDate.replace(/-/g,"/");//替换字符，变成标准格式
	var endTime = planEndDate.replace(/-/g,"/");//替换字符，变成标准格式
	
	var time1 = new Date(Date.parse(startTime));  
	var time2 = new Date(Date.parse(endTime));
	
	if(time1 > time2){
		$.xalert({title:'提示',msg:'开始日期大于结束日期!'});
		return false;
	}
	
	if(planStartDate==""){
		$.xalert({title:'提示',msg:'请选择开始日期!'});
		return false;
	}
	if(planEndDate==""){
		$.xalert({title:'提示',msg:'请选择结束日期!'});
		return false;
	}
	
	return true;
}
//获取相关人员
function relaUser(){	
	$.post(
			baseUrl +'/userManager/userManagerAction!loadDefaultSelUser.action?dto.getPageNo=1&dto.pageSize=1000',
			null,
			function(data) {
				if (data != null) {
					var opti = '<option value="">-请选择PM-</option>';
					if(data.rows.length > 0){
						for(var i=0;i<data.rows.length;i++){
							opti = opti + '<option value="'+data.rows[i].keyObj+'">'+data.rows[i].valueObj+'</option>';
						}
					}
					$(".pm").next("div.searchable-select").remove();
					$(".pm").html(opti);
					$('.pm').searchableSelect();
				} else {
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//# sourceURL=otherMissionAll.js