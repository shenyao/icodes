<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/itest/plug-in/chosen/chosen.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/itest/css/singleTestTaskManager/testTaskMagrList.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/itest/css/outlineManager/testRequirementManager.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/itest/css/testCaseManager/testCaseManager.css"/>
<style type="text/css">
th{
 	font-weight:400;
}
.left_td{
	width:20%;
}
.right_td{
	width:80%;
}
.spandesc {
    background-color: inherit;
    border: medium none;
    color: #0ba917;
    padding: 0;
    text-decoration: none;
}
.panel-default > .panel-heading {
    background-color: white;
    color: #ffffff;
}
</style>
<div id="caseLayout" class="exui-layout" style="height: 530px;">
	<div id="caseTreeDiv" data-options="region:'west'" style="width: 280px;padding:10px;" title="<div><span style='font-size: 14.5px;line-height: inherit;margin: auto;'><button type='button' class='btn btn-default bntcss hoverBu' title='增加类别' onclick='addType()' schkUrl='testCaseLibAction!addNodes' style='display:none'><i class='glyphicon glyphicon-plus'></button><button type='button' class='btn btn-default bntcss hoverBu' title='移除类别' onclick='removeType()' schkUrl='testCaseLibAction!deleteNode' style='display:none'><i class='glyphicon glyphicon-remove'></button><button type='button' class='btn btn-default bntcss hoverBu' title='上移' onclick='upType()' schkUrl='testCaseLibAction!itemMoveUp' style='display:none'><i class='glyphicon glyphicon-arrow-up'></button><button type='button' class='btn btn-default bntcss hoverBu' title='下移' onclick='downType()' schkUrl='testCaseLibAction!itemMoveDown' style='display:none'><i class='glyphicon glyphicon-arrow-down'></button></span><br/><span class='spandesc'>(双击修改拖拽移动)</span></div>">
		<ul id="caseTypeTree"></ul>
	</div>
	<div data-options="region:'center'" style="overflow: scroll;" title="">
		<div class="tools" data-options="" id="caseTypeTools">
			<div class="input-field" style="width: 170px;" id="queryCondtDiv">
				<span style="1E7CFB;color:#1E7CFB;">快速查询：</span>
				<input id="descDetail" class="form-control indent-4-5" style="border: 1px solid #1E7CFB;" name="testCaseDes"  placeholder="描述+回车键" style="text-indent:4em;height:40px">
			</div>
			<button  type="button" schkUrl='testCaseLibAction!saveCase' style="display:none" class="btn btn-default bntcss hoverBu" onclick="addYL();"><i class="glyphicon glyphicon-plus"></i>增加</button>
			<button  type="button" schkUrl='testCaseLibAction!updateCase' style="display:none" class="btn btn-default bntcss hoverBu" onclick="editYL();"><i class="glyphicon glyphicon-pencil"></i>修改</button>
			<button  type="button" schkUrl='testCaseLibAction!deleteCase' style="display:none" class="btn btn-default bntcss hoverBu" onclick="deleteYL();"><i class="glyphicon glyphicon-remove"></i>删除</button>
			<button  type="button" schkUrl='caseImpAction!caseLibImpCase' style="display:none" class="btn btn-default bntcss hoverBu" onclick="uploadYL();"><i class="glyphicon glyphicon-arrow-up"></i>导入</button>
			<a  schkUrl='caseImpAction!caseLibImpCase' style="display:none" type="button" class="btn btn-default bntcss hoverBu" onclick="downloadModel(this);"><i class="glyphicon glyphicon-arrow-down"></i>下载导入模板</a>
			<a  schkUrl='caseImpExpAction!caseLibExpCase' style="display:none" type="button" class="btn btn-default bntcss hoverBu" onclick="downloadCases(this);"><i class="glyphicon glyphicon-arrow-down"></i>导出</a>
		</div><!--/.top tools area-->
		
		<table id="caseList" data-options="
			fitColumns: true,
			singleSelect: true,
			pagination: true,
			pageNumber: 1,
			pageSize: 10,
			pageList:[10,30,50],
			layout:['list','first','prev','manual','next','last','refresh','info']">
		</table>
	</div>
</div>

<!-- 查看详情 -->
<div id="lookDetailWin" class="exui-window" style="display:none;width: 900px" data-options="
	modal:true,
	minimizable:false,
	maximizable:false,
	collapsible:false,
	closed:true">
	<form id="lookDetailForm" method="post">
		<table class="form-table" style="width:100%">
			<tr >
				<td class="left_td"><sup>*</sup>类别:</td>
				<td style="width:130px">
					<input class="exui-combobox caseTypeId" name="dto.testCase.caseType" data-options="
	    			required:true,
	    			valueField:'typeId',
    				textField:'typeName',
	    			validateOnCreate:false,
	    			editable:false,
	    			prompt:'-请选择-'" style="width:120px"/>
				</td>
				<td class="left_td" style="padding-left: 0.5em;"><sup>*</sup>优先级:</td>
				<td style="width:130px">
					<input class="exui-combobox priId" name="dto.testCase.casePri" data-options="
	    			required:true,
	    			validateOnCreate:false,
	    			valueField:'typeId',
    				textField:'typeName',
	    			editable:false,
	    			prompt:'-请选择-'" style="width:120px"/>
				</td>
				<td class="left_td"> &nbsp;&nbsp;<sup>*</sup>成本: </td>
				<td style="width:130px">
					<input id="caseWeight" name="dto.testCase.weight"  class="exui-numberbox" value="2" data-options="min:1,max:10,required:true" style="width: 60px;">
					<font color="orange" style="font-size: 11px;">一单位5分钟</font>
				</td>
			</tr>
			<tr >
				<td class="left_td"><sup>*</sup>用例描述:</td>
				<td class="dataM_left" colspan="8">
					<input id="testCaseDes" name="dto.testCase.testCaseDes" class="exui-textbox" data-options="required:true" style="width: 100%;">
				</td>
			</tr>
			<tr >
				<td class="left_td"><sup>*</sup>过程及数据:</td>
				<td  colspan="8" style="background-color: #f7f7f7;">
   			      <input name="dto.testCase.operData" class="exui-textbox" style="width: 100%;height: 160px;" data-options="multiline:true,required:true,prompt:'请填写过程及数据'" />
  			      	</td>
			</tr>
			<tr >
				<td class="left_td"><sup>*</sup>期望结果:</td>
				<td class="dataM_left"  colspan="8">
					<input name="dto.testCase.expResult" class="exui-textbox" style="width: 100%;height: 120px;margin-top: 6px;" data-options="multiline:true,required:true,prompt:'请填写期望结果'" />
  			      </td>
			</tr>
			<tr id="remarkTr">
				<td class="left_td">备注:</td>
				<td  colspan="8">
					<input name="dto.testCase.remark" class="exui-textbox" style="width: 100%;margin-top: 6px;" data-options="multiline:false,prompt:'备注'" />
  			      </td>
			</tr>
		</table>
	</form>
</div>

<!-- 增加用例库类别 -->
<div id="addCaseType" class="exui-window" style="display: none;" data-options="
	modal:true,
	width: 400,
	minimizable:false,
	maximizable:false,
	closed:true">
	<div class="tab-content" style="height: 480px;">
		<div class="addCaseTypeDiv">
			<span>一次性可批量增加1到10个目录项</span>
		</div>
		<div class="addCaseTypeFormDiv">
			<form action="#" method="post" id="createForm" name="createForm">
				<input type="hidden" id="currNodeId" name="dto.currNodeId"/>
				<input type="hidden" id="command" name="dto.command" value="addchild"/>
				<input type="hidden" id="parentNodeId" name="dto.parentNodeId"/>
				<table class="addCaseTypeNameTab" id="addCaseTypeNameTab" style="width:100%;">
					<tr>
						<td style="width:100%; padding-right: 0px;">
							<input type="text" name="dto.categoryData[0]" id="module1" style="width:100%;margin-top:5px;margin-bottom:5px"/><!--  class="exui-textbox" data-options="required:true" -->
						</td>
					</tr>
					<tr>
						<td style="width:100%;">
							<input type="text" name="dto.categoryData[1]" id="module2" style="width:100%;margin-top:5px;margin-bottom:5px"/>
						</td>
					</tr>
					<tr>
						<td style="width:100%;">
							<input type="text" name="dto.categoryData[2]" id="module3" style="width:100%;margin-top:5px;margin-bottom:5px"/>
						</td>
					</tr>
					<tr>
						<td style="width:100%;">
							<input type="text" name="dto.categoryData[3]" id="module4" style="width:100%;margin-top:5px;margin-bottom:5px"/>
						</td>
					</tr>
					<tr>
						<td style="width:100%;">
							<input type="text" name="dto.categoryData[4]" id="module5" style="width:100%;margin-top:5px;margin-bottom:5px"/>
						</td>
					</tr>
					<tr>
						<td style="width:100%;">
							<input type="text" name="dto.categoryData[5]" id="module6" style="width:100%;margin-top:5px;margin-bottom:5px"/>
						</td>
					</tr>
					<tr>
						<td style="width:100%;">
							<input type="text" name="dto.categoryData[6]" id="module7" style="width:100%;margin-top:5px;margin-bottom:5px"/>
						</td>
					</tr>
					<tr>
						<td style="width:100%;">
							<input type="text" name="dto.categoryData[7]" id="module8" style="width:100%;margin-top:5px;margin-bottom:5px"/>
						</td>
					</tr>
					<tr>
						<td style="width:100%;">
							<input type="text" name="dto.categoryData[8]" id="module9" style="width:100%;margin-top:5px;margin-bottom:5px"/>
						</td>
					</tr>
					<tr>
						<td style="width:100%;">
							<input type="text" name="dto.categoryData[9]" id="module10" style="width:100%;margin-top:5px;margin-bottom:5px"/>
						</td>
					</tr>
				</table>
				<table class="form-table">
					<tr>
						<td>
							<input type="radio" checked="checked" name="requir" id="addchildRd" value="addchild" onclick="javascript:$('#command').val('addchild');"/><label style="margin-left: 10px;">子目录</label>
							<input type="radio" name="requir" id="addBroRd" value="addBro" onclick="javascript:$('#command').val('addBro');" style="display:none"/><label id="addBroRdLabel" style="margin-left: 10px;display:none">同级目录</label><!-- checkIsParent(); -->
						</td>
					</tr>
				</table>
			</form>
			<footer style="padding:5px;text-align:right">
				<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="addCaseTypeNode('closed')">确定</a>
				<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" style="border: 1px solid #1e7cfb; color: #1e7cfb;" onclick="addCaseTypeNode('continue')">确定并继续</a>
				<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" style="border: 1px solid #1e7cfb; color: #1e7cfb;" onclick="javascript:$('#addCaseType').xwindow('close');">返回</a>
			</footer>
		</div>
	</div>
</div>

<!-- 增加用例库用例 -->
<div id="addCase" class="exui-window" style="display: none;" data-options="
	modal:true,
	width: 680,
	minimizable:false,
	maximizable:false,
	closed:true">
	<div class="tab-content" style="height: 480px;">
		<div class="addCaseFormDiv">
			<form id="addOrEditCaseForms" method="post">
				<table class="form-table">
					<tr class="hidden"><td>
						<input id="testCaseId" type="hidden" name="dto.testCase.testCaseId"/>
						<input id="categoryId" type="hidden" name="dto.testCase.categoryId"/>
						<input id="categoryNum" type="hidden" name="dto.testCase.categoryNum"/>
						<input id="createrId" type="hidden" name="dto.testCase.createrId"/>
						<input type="hidden" name="dto.testCase.creatDate"/>
						<input type="hidden" name="dto.testCase.recommendUserId"/>
						<input type="hidden" name="dto.testCase.recommendReason"/>
						<input type="hidden" name="dto.testCase.examineStatus"/>
						<input type="hidden" name="dto.testCase.examineUserId"/>
						<input type="hidden" name="dto.testCase.companyId"/>
					</td></tr>
					<tr >
						<td class="left_td"><sup>*</sup>类别:</td>
						<td style="width:130px">
							<input id="caseTypeId" class="exui-combobox caseTypeId" name="dto.testCase.caseType" data-options="
			    			required:true,
			    			valueField:'typeId',
		    				textField:'typeName',
			    			validateOnCreate:false,
			    			editable:false,
			    			prompt:'-请选择-'" style="width:120px"/>
						</td>
						<td class="left_td" style="padding-left: 0.5em;"><sup>*</sup>优先级:</td>
						<td style="width:130px">
							<input id="priId" class="exui-combobox priId" name="dto.testCase.casePri" data-options="
			    			required:true,
			    			validateOnCreate:false,
			    			valueField:'typeId',
		    				textField:'typeName',
			    			editable:false,
			    			prompt:'-请选择-'" style="width:120px"/>
						</td>
						<td class="left_td"> &nbsp;&nbsp;<sup>*</sup>成本: </td>
						<td style="width:130px">
							<input id="caseWeight" name="dto.testCase.weight"  class="exui-numberbox" value="2" data-options="min:1,max:10,required:true" style="width: 60px;">
							<font color="orange" style="font-size: 11px;">一单位5分钟</font>
						</td>
					</tr>
					<tr >
						<td class="left_td"><sup>*</sup>用例描述:</td>
						<td class="dataM_left" colspan="8">
							<input id="testCaseDes" name="dto.testCase.testCaseDes" class="exui-textbox" data-options="required:true" style="width: 100%;">
						</td>
					</tr>
					<tr >
						<td class="left_td"><sup>*</sup>过程及数据:</td>
						<td  colspan="8" style="background-color: #f7f7f7;">
		   			      <input name="dto.testCase.operData" class="exui-textbox" style="width: 100%;height: 160px;" data-options="multiline:true,required:true,prompt:'请填写过程及数据'" />
		  			      	</td>
					</tr>
					<tr >
						<td class="left_td"><sup>*</sup>期望结果:</td>
						<td class="dataM_left"  colspan="8">
							<input name="dto.testCase.expResult" class="exui-textbox" style="width: 100%;height: 120px;margin-top: 6px;" data-options="multiline:true,required:true,prompt:'请填写期望结果'" />
		  			      </td>
					</tr>
					<tr id="remarkTr">
						<td class="left_td">备注:</td>
						<td  colspan="8">
							<input name="dto.testCase.remark" class="exui-textbox" style="width: 100%;margin-top: 6px;" data-options="multiline:false,prompt:'备注'" />
		  			      </td>
					</tr>
				</table>
			</form>
			<footer style="padding:5px;text-align:right">
				<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="submitCase()">保存</a>
				<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="closeCaseWin()" style="border: 1px solid #1e7cfb;color: #1e7cfb;">取消</a>
			</footer>
		</div>
	</div>
</div>

<!-- 从excel导入测试用例模态窗 -->
<div id="uploadFromExcelWin" class="exui-window" style="display:none" data-options="
	modal:true,
	width: 450,
	minimizable:false,
	maximizable:false,
	resizable:false,
	closed:true">
	<form id="uploadForm" class="form-horizontal" method="post" enctype="multipart/form-data" role="form">
		<div class="file-box">
			<input id="importFile" accept=".xls,.xlsx" name="dto.importFile" type="file">
			<br/><span style="color:red">请按照模板要求导入数据，否则导入将会不成功！</span>
		</div>
	</form>
	<footer style="padding:5px;text-align:right">
		<a href="#" class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="sureToUpload()">确认导入</a>
		<a href="#" class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="closeUploadFromExcelWin()"  style="border: 1px solid #1e7cfb;color: #1e7cfb;">取消</a>
	</footer>
</div>

<script src="<%=request.getContextPath()%>/itest/js/testLibrary/caseLook.js" type="text/javascript" charset="utf-8"></script>