<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/itest/plug-in/chosen/chosen.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/itest/css/singleTestTaskManager/testTaskMagrList.css"/>
<style type="text/css">
th{
 	font-weight:400;
}
</style>
<div id="caseLayout" class="exui-layout" style="height: 530px;">
	<div id="caseTreeDiv" data-options="region:'west'" style="width:230px;padding:10px;">
		<%-- <input id="currTaksId" type="hidden" value="<%=currTaksId%>"> --%>
		<ul id="caseTypeTree" class="exui-tree"></ul>
	</div>
	<div data-options="region:'center'" style="overflow: scroll;" title="">
		<div class="tools" data-options="">
			<button style="padding:9.5px 18px;background: #1e7cfb none repeat scroll 0 0;border: 1px solid #1e7cfb;color: #ffffff;" type="button" class="btn btn-default" onclick="agreeLibrary();"><i class="glyphicon glyphicon-ok"></i>通过</button>
			<button style="padding:9.5px 18px;border: 1px solid #1e7cfb;color: #1e7cfb;" type="button" class="btn btn-default" onclick="disagreeLibrary();"><i class="glyphicon glyphicon-remove"></i>不通过</button>
		</div><!--/.top tools area-->
		
		<table id="caseList" data-options="
			fitColumns: true,
			singleSelect: false,
			pagination: true,
			pageNumber: 1,
			pageSize: 10,
			pageList:[10,30,50],
			layout:['list','first','prev','manual','next','last','refresh','info']">
		</table>
	</div>
</div>

<!-- 查看详情 -->
<div id="lookDetailWin" class="exui-window" style="display:none;width: 900px" data-options="
	modal:true,
	minimizable:false,
	maximizable:false,
	collapsible:false,
	closed:true">
	<form id="lookDetailForm" method="post">
		<table class="form-table" style="width:100%">
			<tr >
				<td class="left_td"><sup>*</sup>类别:</td>
				<td style="width:130px">
					<input class="exui-combobox caseTypeId" name="dto.testCase.caseType" data-options="
	    			required:true,
	    			valueField:'typeId',
    				textField:'typeName',
	    			validateOnCreate:false,
	    			editable:false,
	    			prompt:'-请选择-'" style="width:120px"/>
				</td>
				<td class="left_td" style="padding-left: 0.5em;"><sup>*</sup>优先级:</td>
				<td style="width:130px">
					<input class="exui-combobox priId" name="dto.testCase.casePri" data-options="
	    			required:true,
	    			validateOnCreate:false,
	    			valueField:'typeId',
    				textField:'typeName',
	    			editable:false,
	    			prompt:'-请选择-'" style="width:120px"/>
				</td>
				<td class="left_td"> &nbsp;&nbsp;<sup>*</sup>成本: </td>
				<td style="width:130px">
					<input id="caseWeight" name="dto.testCase.weight"  class="exui-numberbox" value="2" data-options="min:1,max:10,required:true" style="width: 60px;">
					<font color="orange" style="font-size: 11px;">一单位5分钟</font>
				</td>
			</tr>
			<tr >
				<td class="left_td"><sup>*</sup>用例描述:</td>
				<td class="dataM_left" colspan="8">
					<input id="testCaseDes" name="dto.testCase.testCaseDes" class="exui-textbox" data-options="required:true" style="width: 100%;">
				</td>
			</tr>
			<tr >
				<td class="left_td"><sup>*</sup>过程及数据:</td>
				<td  colspan="8" style="background-color: #f7f7f7;">
   			      <input name="dto.testCase.operData" class="exui-textbox" style="width: 100%;height: 160px;" data-options="multiline:true,required:true,prompt:'请填写过程及数据'" />
  			      	</td>
			</tr>
			<tr >
				<td class="left_td"><sup>*</sup>期望结果:</td>
				<td class="dataM_left"  colspan="8">
					<input name="dto.testCase.expResult" class="exui-textbox" style="width: 100%;height: 160px;margin-top: 6px;" data-options="multiline:true,required:true,prompt:'请填写期望结果'" />
  			      </td>
			</tr>
			<tr id="remarkTr">
				<td class="left_td">备注:</td>
				<td  colspan="8">
					<input name="dto.testCase.remark" class="exui-textbox" style="width: 100%;margin-top: 6px;" data-options="multiline:false,prompt:'备注'" />
  			      </td>
			</tr>
		</table>
	</form>
</div>
<script src="<%=request.getContextPath()%>/itest/js/testLibrary/caseExamine.js" type="text/javascript" charset="utf-8"></script>