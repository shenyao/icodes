<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/itest/css/testCaseManager/testCaseManager.css"/>
<style>
 
th{
 	font-weight:400;
}

.tools button,.tools button:hover{
  border: 1px solid #1E7CFB;
    color: #1E7CFB;
}

tr>th{
  font-size: 14px;
}
 
 #testCasePackageM .datagrid-header-check>input, #executeTestCaseDlg .datagrid-header-check>input{
    display:none
  } 

.indent-60{
   text-indent: 45px;
}
.datagrid-header {
	height: 40px;
}
.datagrid-htable{
	height: 40px;
}
.datagrid-body td{
    padding: 11px 0;
} 

.multPkgPeopleSel{
   display:inline-block;
   width:25px;
   height:25px;
   margin-left:5px;
   vertical-align:middle;
   background-image:url(<%=request.getContextPath()%>/itest/images/mSearch.png);
   background-repeat: no-repeat;
   background-size: contain;
   cursor: pointer;
   
}

.multPkgPeopleSel:hover{
 background-image:url(<%=request.getContextPath()%>/itest/images/mSearch1.png);
}

.searchable-select{
  min-width: 130px;
}
</style>

<%@include file="/itest/jsp/chooseMuiltPeople/chooseMuiltPeople.jsp" %>
<div class="newWrapper" style="padding:15px 15px 0 15px;height:auto" >     
<!--top tools area--> 
<div id="testCasePackageM">
<div class="tools" id="testcasepkgTool">
	<div class="input-field" style="width: 180px;">
	    <span style="width: 100px;color:#1E7CFB">快速查询：</span>
		<input class="form-control indent-4-5" id="queryParam" style="border: 1px solid #1E7CFB;" placeholder="包名+回车键"/>
	</div>
	<div class="input-field" style="width: 130px;" id="peopleNmList">
	    <select id="peopleNm" class="form-control chzn-select " >
	    	
	    </select>
	</div>
<!--  	<button type="button" class="btn btn-default" id="queryCasePkg"><i class="glyphicon glyphicon-search"></i>查询</button>
 -->	<button type="button" class="btn btn-default" id="showAddCasePkgWin"  schkUrl="testCasePackageAction!addTestCasePkg"><i class="glyphicon glyphicon-plus"></i>增加</button>
	<button type="button" class="btn btn-default" id="showEditCasePkgWin"  schkUrl="testCasePackageAction!updateTestCasePkg"><i class="glyphicon glyphicon-pencil"></i>修改</button>
	<button type="button" class="btn btn-default" id="delCasePkg"  schkUrl="testCasePackageAction!deleteTestCasePkg"><i class="glyphicon glyphicon-remove"></i>删除</button>
	<button type="button" class="btn btn-default" id ="uploadCase" onclick="upload();" title="导入用例/同步线下处理"><i class="glyphicon glyphicon-arrow-up"></i>导入/同步线下处理</button>
	<a style="padding:6.5px 15px;" href="#" type="button" class="btn btn-default bntcss hoverBu" onclick="exportCase(this);" title="导出用例/线下处理"><i class="glyphicon glyphicon-arrow-down"></i>导出/线下处理</a>
	<button type="button" class="btn btn-default"   id ="xmind2ExcelBtn" onclick="showXmind2ExcelWin();" title="xmind(转维导图)转Excel"><i class="glyphicon glyphicon-resize-vertical"></i>xmind转Excel</button>
	<a style="padding:6.5px 15px;display:none;" id="dwExcelFile" type="button" onclick="exeDwExcel(this);"  class="btn btn-default bntcss hoverBu"  title="xmind转Excel"></a>
</div><!--/.top tools area-->

<table id="testCasePkgTb" data-options="
	fitColumns: true,
	rownumbers: true,
	singleSelect: true,
	pagination: true,
	pageNumber: 1,
	pageSize: 10,
	layout:['list','first','prev','manual','next','last','refresh','info'],
	pageList: [10,30,50,100]
"></table>
</div>

<!-- 新增/修改单井模态窗开始 -->
<!-- <div id="addOrEditPkgWin" class="exui-window " style="display:none;" data-options="
	modal:true,
	width: 580,
	height:500,
	footer:'#addOrEditPkgFooter',
	minimizable:false,
	maximizable:false,
	cls:'pkg',
	closed:true">
	<table class="form-table" style="width:100%">
		<form id="addOrEditPkgForm" method="post">
			<tr class="hidden">
			  <td>
			    <input class="exui-textbox"  name="dto.testCasePackage.packageId" id="packageId"/>
			    <input class="exui-textbox"  name="dto.testCasePackage.createTime" id="createTime"/>
			    <input class="exui-textbox"  name="dto.testCasePackage.updateTime" id="updateTime"/>  
			    <input class="exui-textbox"  name="dto.testCasePackage.createrId" id="createrId"/>  
			  </td>
			</tr>
			<tr>
				<th style="width:26%"><sup>*</sup>测试包名称：</th>
	    		<td><input class="exui-textbox" name="dto.testCasePackage.packageName" id="packageName" style="width:75%"/></td>
	    	</tr>
	    	<tr>
	    	<th  style="width:25%"><sup>*</sup>执行人：</th>
	    		<td><select id="executor" class="exui-combobox executor"  data-options="
	    			valueField:'id',
	    			textField:'name',
	    			multiple:true,
	    			editable:false,
	    			prompt:'-请选择-'" style="width:75%"></select>	<span class="multPkgPeopleSel" onclick="showSeletctPeopleWindow('executor');" title="选择人员" ></span>
	    			
	    		</td>
	    	</tr>
	    	<tr>
	    		<th  style="width:25%">执行环境：</th>
	    		<td>
	    		  <textarea class="exui-textbox" name="dto.testCasePackage.execEnvironment" id="execEnvironment"  style="width:75%"></textarea>
	    		</td>
	    	</tr>
	    	<tr>
	    		<th  style="width:25%">备注：</th>
	    		<td>
	    		  <textarea class="exui-textbox" name="dto.testCasePackage.remark"  style="width:75%"></textarea>
	    		</td>
	    	</tr>
	    	<tr>
	    		<th  style="width:25%"><sup>*</sup>预计开始时间：</th>
	    		<td>
	    		  <input class="exui-datebox" name="dto.testCasePackage.expectedStartTime" id="expectedStartTime" style="width:75%" data-options="validateOnCreate:false,prompt:'-请选择-',editable:false"
></input>
	    		</td>
	    	</tr>
	    	<tr>
	    		<th  style="width:25%"><sup>*</sup>预计结束时间：</th>
	    		<td>
	    		  <input class="exui-datebox" name="dto.testCasePackage.expectedEndTime" id="expectedEndTime" style="width:75%" data-options="validateOnCreate:false,prompt:'-请选择-',editable:false"
></input>
	    		</td>
	    	</tr>
	    	<tr>
	    		<th  style="width:25%">实际开始时间：</th>
	    		<td>
	    		  <input class="exui-datebox" name="dto.testCasePackage.actualStartTime" id="actualStartTime" style="width:75%" data-options="validateOnCreate:false,prompt:'-请选择-',editable:false"
></input>
	    		</td>
	    	</tr>
	    </form>
	</table>
</div>
<div id="addOrEditPkgFooter" align="right" style="padding:5px;display:none">
	<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" id="submitPkgBtn">保存</a>
	<a class="exui-linkbutton"  data-options="btnCls:'default',size:'xs'" style=" border: 1px solid #1E7CFB;color: #1E7CFB;" id="closePkgWinBtn">取消</a>
</div> -->
<!-- 新增/修改单井模态窗结束 -->

<!-- 选择用例模态窗开始 -->
<!-- <div id="testCaseWin" class="exui-window" style="display:none;" data-options="
	modal:true,
	width: 580,
	footer:'#testCaseFooter',
	minimizable:false,
	maximizable:false,
	closed:true">
</div>
<div id="testCaseFooter" align="center" style="padding:5px;">
	<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" id="testCaseOkBtn">确认</a>
	<a class="exui-linkbutton" style=" border: 1px solid #1E7CFB;color: #1E7CFB;" data-options="btnCls:'default',size:'xs'" id="closeTestCaseBtn">取消</a>
</div> -->
<!-- 选择用例模态窗结束 -->
</div>

	<!-- 上传测试用例模态窗 -->
	<div id="uploadWin" class="exui-window" style="display:none;" data-options="
		modal:true,
		width: 600,
		footer:'#uploadFoot',
		minimizable:false,
		maximizable:false,
		resizable:false,
		closed:true">
		<button style="padding:9.5px 18px;" type="button" class="btn btn-default bntcss hoverBu" onclick="uploadFromExcel();"><i class="glyphicon glyphicon-arrow-up"></i>从Excel导入/同步线下处理</button>
		<button style="padding:9.5px 18px;margin-left:10px" type="button" class="btn btn-default bntcss hoverBu" onclick="uploadFromExcel();"><i class="glyphicon glyphicon-arrow-up"></i>从思维导图导入</button>
		<a style="padding:9.5px 18px;margin-left:10px" type="button" class="btn btn-default bntcss hoverBu" onclick="downloadExcelModel(this);"><i class="glyphicon glyphicon-arrow-down"></i>下载Excel导入摸板</a>
	</div>
	<div id="uploadFoot" align="right" style="display:none">
		<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="closeUploadWin()"  style="border: 1px solid #1e7cfb;color: #1e7cfb;">取消</a>
	</div>
	
	<!-- 从excel导入测试用例模态窗 -->
	<div id="uploadFromExcelWin" class="exui-window" style="display:none;" data-options="
		modal:true,
		width: 450,
		minimizable:false,
		maximizable:false,
		resizable:false,
		closed:true">
		<form id="uploadForm" class="form-horizontal" method="post" enctype="multipart/form-data" role="form">
			<div class="file-box">
				<!-- <input id="excelField" class="form-control pull-left" style="max-width: 380px;" readonly type="text">
				<button class="pull-left" type="button">浏览...</button>
				<input id="importFile" name="dto.importFile" style="width:5em;" size="15" onchange="fileChange(this)" type="file" accept=".xls,.xlsx"> -->
				<input id="importFile" accept=".xls,.xlsx,.xmind" name="dto.importFile" type="file">
				<br/><span style="color:red">&nbsp;&nbsp;&nbsp;excel导入时，请按照模板要求导入数据，否则导入将会不成功！</span>
			</div>
		</form>
		<footer style="padding:5px;text-align:right">
			<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="sureToUpload()">确认导入</a>
			<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="closeUploadFromExcelWin()"  style="border: 1px solid #1e7cfb;color: #1e7cfb;">取消</a>
		</footer>
	</div>
	
	<!-- 从xmind转EXCEL模态窗 -->
	<div id="xmind2ExcelWin" class="exui-window" style="display:none;" data-options="
		modal:true,
		width: 450,
		minimizable:false,
		maximizable:false,
		resizable:false,
		closed:true">
		<form id="xmind2ExceUploadForm" class="form-horizontal" method="post" enctype="multipart/form-data" role="form">
			<div class="file-box">

				<input id="2excelXmindFile" accept=".xmind" name="dto.importFile" type="file">
				
			</div>
		</form>
		<footer style="padding:5px;text-align:right">
			<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="uploadXmind2Excel()">转为Excel</a>
			<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="closeXmind2ExcelWin()"  style="border: 1px solid #1e7cfb;color: #1e7cfb;">取消</a>
		</footer>
	</div>
	<!-- <div id="uploadFromExcelFoot" align="right" style="display:none">
		<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="sureToUpload()">确认导入</a>
		<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="closeUploadFromExcelWin()"  style="border: 1px solid #1e7cfb;color: #1e7cfb;">取消</a>
	</div> -->
<script src="<%=request.getContextPath()%>/itest/js/testCasePackageMananger/testCasePackageMananger.js" type="text/javascript" charset="utf-8"></script>
