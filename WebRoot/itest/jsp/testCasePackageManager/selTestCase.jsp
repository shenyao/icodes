<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
  .panel-body {
    overflow: auto;
    }
</style>
	<div class="exui-layout" style="height: 100%;">
		<div id="caseTreeDiv" data-options="region:'west'" title="<div><span>测试需求 &nbsp;</span></div>" style="width:230px;padding:10px;">
			<ul id="selCaseTree" class="exui-tree"></ul>
		</div>
		<div data-options="region:'center'" >
			<div  class="tools" data-options="">
			    <div style="width: 300px;display: inline-block;">
					<span>切换用例类别：</span>
					<select class="exui-combobox" id="testCaseCategory"  style="width:170px"/></select>
				</div>
				<div style="width: 300px;display: inline-block;">
					<span>切换用例优先级：</span>
					<select class="exui-combobox" id="testCasePriority"  style="width:170px"/></select>
				</div>
				<div style="width: 230px;display: inline-block;">
					<span>基线：</span>
                      <input id="baseLine" class="exui-combobox" data-options="
		    			required:false,
		    			validateOnCreate:false,
		    			prompt:'-请选择-'" style="width:170px"/>			
	    		</div>
				<button type="button" class="btn btn-default" onclick="submitSelTestCase()"><i class="glyphicon glyphicon-ok"></i>确认</button>
			    <button type="button" class="btn btn-default" onclick="closeSelTestCaseWin()"><i class="glyphicon glyphicon-off"></i>关闭</button>
			</div><!--/.top tools area-->
		<!--/.top tools area-->
			<table id="selCaseList" class="exui-datagrid"  style="width:100%;height: auto;" ></table>
		</div>
	</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/itest/js/testCasePackageMananger/selTestCase.js"></script>