<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
   .testcaseResultWrapper{
	   display:inline-block;
	   border: 1px dashed #1e90fc;
	   height: 35px;
	   line-height: 32px;
	   vertical-align: bottom;
	   margin-left: 30px;
   }
   
   .testCaseResultCount{
      font-size: 1.5rem;
      font-weight: bold;
      margin-right: 20px;
   }
</style>
<div>
  <div id="viewTestcaseBtnTool" class="tools" >
  <div class="input-field" style="width: 180px;">
	    <select id="exeSta" class="form-control chzn-select" onchange="searchExeSta()">
	    	<option value="-1">全部</option>
<!-- 	    	<option value="0">待审核</option>
 -->	    	<option value="1">未测试</option>
	    	<option value="2">通过</option>
	    	<option value="3">不通过</option>
	    	<option value="4">不适用</option>
	    	<option value="5">阻塞</option>
<!-- 	    	<option value="6">待修正</option>
 -->	    </select>
	</div>
    <button type="button" class="btn btn-default" onclick="closeViewWin()"><i class="glyphicon glyphicon-off"></i>关闭</button>
    <div class="testcaseResultWrapper">    
    <ul style="display: flex;list-style: none;margin:0">
      <li class="testCaseResultCount" style="color:#a3d3fe;"><span >总用例数：</span><span id="viewAllCount">0</span></li>
      <li class="testCaseResultCount" style="color: #71CD71;"><span >通过数：</span><span id="viewPassCount">0</span></li>
      <li class="testCaseResultCount" style="color: #ED8282;"><span >未通过数：</span><span id="viewFailedCount">0</span></li>
      <li class="testCaseResultCount" style="color: #EEAA5A;"><span >阻塞数：</span><span id="viewBlockCount">0</span></li>
      <li class="testCaseResultCount" style="color: #B78EEA;"><span >不适用数：</span><span id="viewInvalidCount">0</span></li>
      <li class="testCaseResultCount" style="color: #B7AB9C;"><span >未测试数：</span><span id="viewNoTestCount">0</span></li>
    </ul>
    </div>
  </div><!--/.top tools area-->
  <div style="width:100%;display:inline-block">
    <table id="viewTestCaseList" class="exui-datagrid" title="" style="width:100%;height: auto;" data-options="fitColumns: true,
	rownumbers: false,
	singleSelect: true,
	pagination: true,
	pageNumber: 1,
	pageSize: 10,
	layout:['list','first','prev','manual','next','last','refresh','info']
	"></table>
  </div>
  <div style="border:1px solid #eee;margin:10px 0;clear:both"></div>
</div>

<script type="text/javascript" src="<%=request.getContextPath()%>/itest/js/testCasePackageMananger/viewTestCase.js" charset="utf-8"></script>
