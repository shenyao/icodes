<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/itest/plug-in/chosen/chosen.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/itest/css/singleTestTaskManager/testTaskMagrList.css"/>
<style type="text/css">
#detailTable tbody tr{
	height: 2em;
	border: 0.5px solid gray;
}
#detailTable tbody tr td{
	border-right: 0.5px solid gray;
}
th{
 	font-weight:400;
}
</style>
<div class="newWrapper">
<!--top tools area-->
<div class="tools">
	<div class="input-field" style="width: 160px;">
		<span>任务名称：</span>
		<input id="missionName" class="form-control indent-4-5" placeholder="名称"/>
	</div>
	<div class="input-field" style="width: 180px;">
	    <select id="projectNa" class="form-control chzn-select">
	    	
	    </select>
	</div>
	<div class="input-field" style="width: 180px;">
	    <select id="statu" class="form-control chzn-select">
	    	<option value="">-请选择状态-</option>
	    	<option value="0">未开始</option>
	    	<option value="1">进行中</option>
	    	<option value="2">完成</option>
	    	<option value="3">终止</option>
	    	<option value="4">暂停</option>
	    </select>
	</div>
	<button id="searchCon" type="button" class="btn btn-default" style="background: #1E7CFB;border: 1px solid #1E7CFB;color: #ffffff;" onclick="searchOtherMission()"><i class="glyphicon glyphicon-search"></i>查询</button>
	<!-- <button id="resetCon" type="button" class="btn btn-default" style="border: 1px solid #1E7CFB;color: #1E7CFB;" onclick="resetInfo()"><i class="glyphicon glyphicon-trash"></i>重置</button> -->
	<!-- <div style="float:right">
		<button id="editCon" type="button" schkUrl="singleTestTaskAction!add" class="btn btn-default" style="border: 1px solid #1E7CFB;color: #1E7CFB;" onclick="showEditWin()"><i class="glyphicon glyphicon-pencil"></i>修改</button>
	</div> -->
</div><!--/.top tools area-->
<!-- 其他任务显示列表 -->
<table id="missionDg" data-options="
	fitColumns: true,
	singleSelect: true,
	pagination: true,
	pageNumber: 1,
	pageSize: 10,
	pageList:[10,30,50],
	layout:['list','first','prev','manual','next','last','refresh','info']
"></table>

</div>


<!-- 查看详情模态窗 -->
<div id="detailWin" class="exui-window" data-options="
	modal:true,
	minimizable:false,
	maximizable:false,
	resizable:false,
	collapsible:false,
	closed:true">
	<ul class="nav nav-lattice" style="display:none">
		<li class="active"><a href="#xiangqing" data-toggle="tab">任务详情</a></li>
		<li><a href="#rizhi" data-toggle="tab">任务日志</a></li>
		<!-- <button onclick="closeDetailWin()" class="btn btn-default" style="margin-left:15px;border: 1px solid #1e7cfb;color: #1e7cfb;"><i class="glyphicon glyphicon-off"></i>关闭</button> -->
	</ul>
	<div class="tab-content" style="width:600px">
		<div id="xiangqing" class="tab-pane fade in active">
			<table id="detailTable">
				
			</table>
		</div>
		<div id="rizhi" class="tab-pane fade">
			<!-- <table id="logDg" data-options="
				fitColumns: true,
				singleSelect: true,
				pagination: true,
				pageNumber: 1,
				pageSize: 10,
				pageList:[10,30,50],
				layout:['list','first','prev','manual','next','last','refresh','info']
			"></table> -->
		</div>
	</div>
	<footer style="padding:5px;text-align:right">
		<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="closeDetailWin()" style="border: 1px solid #1e7cfb;color: #1e7cfb;"><i class="glyphicon glyphicon-off"></i>关闭</a>
	</footer>
</div>

<script type="text/javascript">
	$.parser.parse();
</script>
<script src="<%=request.getContextPath()%>/itest/js/otherMission/toMeConcern.js" type="text/javascript" charset="utf-8"></script>