<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/itest/plug-in/chosen/chosen.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/itest/css/singleTestTaskManager/testTaskMagrList.css"/>
<style type="text/css">
#detailTable tbody tr{
	height: 2em;
	border: 0.5px solid gray;
}
#detailTable tbody tr td{
	border-right: 0.5px solid gray;
}
#detailPkgTable tbody tr{
	height: 2em;
	border: 0.5px solid gray;
}
#detailPkgTable tbody tr td{
	border-right: 0.5px solid gray;
}
#detailIterationTable tbody tr{
	height: 2em;
	border: 0.5px solid gray;
}
#detailIterationTable tbody tr td{
	border-right: 0.5px solid gray;
}
th{
 	font-weight:400;
}
.result-wrapper{
      height:155px;
      border-top:1px solid #eee;
      color:#fff;
  }
  
  .result-content{
    display:inline-block;
    width:140px;
    height:140px;
    border:1px solid #eee;
  }
  
   .result-content div{
     width:100%;
     height:50%;
     text-align: center;
     line-height: 60px;
  }
  
  .result-content div:nth-child(odd){
    
  }
</style>
<div class="newWrapper">
<!--top tools area-->
<div class="tools">
	<div class="input-field" style="width: 180px;">
	    <select id="projectNa" class="form-control chzn-select">
	    	
	    </select>
	</div>
	<div class="input-field" style="width: 180px;display:none" id="peopleNameList">
	    <select id="peopleName" class="form-control chzn-select">
	    	
	    </select>
	</div>
	<button type="button" class="btn btn-default" style="border: 1px solid #1E7CFB;color: #1E7CFB;margin-top:-2px" onclick="showAddWin()"><i class="glyphicon glyphicon-plus"></i>创建任务</button>
	<!-- <button type="button" class="btn btn-default" style="background: #1E7CFB;border: 1px solid #1E7CFB;color: #ffffff;" onclick="loadOtherMission()"><i class="glyphicon glyphicon-search"></i>查询</button> -->
</div><!--/.top tools area-->
<!-- 面板展示div -->
<div class="panelHeight" style="width: 100%; background-color: rgb(255, 255, 255); padding-top: 15px; height: 485px; overflow-y: auto;">
	<div class="missionDiv" style="width:20%;float:left;text-align:center;padding:2px;border-right: 1px solid #eeeed1;">
		<div style="font-family: PingFangSC-Medium;font-size: 18px;color: white;border-bottom:4px solid #cdc1c5;margin-bottom:5px;height: 40px;background-color:#cdc1c5;border-radius: 4px;padding-top: 6px;font-weight: bold;">未开始(<span id="distributionCount">0</span>)</div>
		<div id="distribution" style="padding-left:7px;padding-right:7px">
			<!-- <div style="border:1px gray solid">
				<span>你好</span><br/>
				负责人：<span>我</span><a onclick="dianji()">填写进度</a>
			</div> -->
		</div>
	</div>
	<div class="missionDiv" style="width:20%;float:left;text-align:center;padding:2px;border-right: 1px solid #eeeed1;">
		<div style="font-family: PingFangSC-Medium;font-size: 18px;color: white;border-bottom:4px solid #AAAAF6;margin-bottom:5px;height: 40px;background-color:#AAAAF6;border-radius: 4px;padding-top: 6px;font-weight: bold;">进行中(<span id="acceptCount">0</span>)</div>
		<div id="accept" style="padding-left:7px;padding-right:7px">
			
		</div>
	</div>
	<div class="missionDiv" style="width:20%;float:left;text-align:center;padding:2px;border-right: 1px solid #eeeed1;">
		<div style="font-family: PingFangSC-Medium;font-size: 18px;color: white;border-bottom:4px solid #A7DAA7;margin-bottom:5px;height: 40px;background-color:#A7DAA7;border-radius: 4px;padding-top: 6px;font-weight: bold;">完成(<span id="finishCount">0</span>)</div>
		<div id="finish" style="padding-left:7px;padding-right:7px">
			
		</div>
	</div>
	<div class="missionDiv" style="width:20%;float:left;text-align:center;padding:2px;border-right: 1px solid #eeeed1;">
		<div style="font-family: PingFangSC-Medium;font-size: 18px;color: white;border-bottom:4px solid #F0BB7D;margin-bottom:5px;height: 40px;background-color:#F0BB7D;border-radius: 4px;padding-top: 6px;font-weight: bold;">终止(<span id="terminationCount">0</span>)</div>
		<div id="termination" style="padding-left:7px;padding-right:7px">
			
		</div>
	</div>
	<div class="missionDiv" style="width:20%;float:left;text-align:center;padding:2px;">
		<div style="font-family: PingFangSC-Medium;font-size: 18px;color: white;border-bottom:4px solid #ff82ab;margin-bottom:5px;height: 40px;background-color:#ff82ab;border-radius: 4px;padding-top: 6px;font-weight: bold;">暂停(<span id="pauseCount">0</span>)</div>
		<div id="pause" style="padding-left:7px;padding-right:7px">
			
		</div>
	</div>
</div>
</div>
<!-- 填写进度模态窗 -->
<div id="addOrEditWin" class="exui-window" style="display:none;width:420px;" data-options="
	modal:true,
	width: 580,
	minimizable:false,
	maximizable:false,
	collapsible:false,
	resizable:false,
	closed:true">
	<form id="addOrEditForm" method="post">
		<table class="form-table">
		    <!--  隐藏字段，新增，修改提交时传到后台 -->
			<tr class="hidden">
				<td>
					<input id="missionId" name="dto.otherMission.missionId"/>
				</td>
			</tr>
			<tr>
	    		<th><sup>*</sup>实际工作量(小时)：</th>
	    		<td><input class="exui-textbox" name="dto.otherMission.actualWorkload" data-options="required:true,validateOnCreate:false,validType:'zhengshu',prompt:'请填写非负整数'" style="width:180px"/><span style="margin-top:5px">(累计)</span></td>
	    	</tr>
	    	<tr>
	    		<th><sup>*</sup>实际开始时间：</th>
	    		<td><input class="exui-datebox" data-options="validateOnCreate:false,prompt:'-请选择-',editable:false" name="dto.otherMission.realStartTime" style="width:180px"/></td>
	    	</tr>
	    	<tr>
	    		<th><sup>*</sup>进度(%)：</th>
	    		<td><input class="exui-textbox" name="dto.otherMission.completionDegree" data-options="required:true,validateOnCreate:false,validType:'lessThan',prompt:'请填写0到100的数字',onChange:function(newValue,oldValue){
	    		changeMissionStatus(newValue)}" style="width:180px"/></td>
	    	</tr>
	    	<tr>
				<th><sup>*</sup>任务状态：</th>
	    		<td><input name="dto.otherMission.status" class="exui-combobox sss" data-options="
	    			required:true,
	    			validateOnCreate:false,
	    			valueField:'value',
	    			textField:'desc',
	    			editable:false,
	    			prompt:'-请选择-',
	    			data:[{desc:'未开始',value:'0'},{desc:'进行中',value:'1'},{desc:'完成',value:'2'},{desc:'终止',value:'3'},{desc:'暂停',value:'4'}]" style="width:180px"/></td>
	    	</tr>
	    	<tr class="stopZone" style="display:none;">
	    		<th><sup>*</sup>终止原因：</th>
	    		<td>
	    			<textarea class="stopReason" name="stopReason" style="width:180px;resize:none;"/></textarea>
	    			<br>
	    			<span style="color:red">设为终止，不得再修改状态。</span>
	    		</td>
	    	</tr>
	    	<tr class="zantingZone" style="display:none;">
	    		<th><sup>*</sup>暂停原因：</th>
	    		<td>
	    			<textarea class="zantingReason" name="zantingReason" style="width:180px;resize:none;"/></textarea>
	    		</td>
	    	</tr>
		</table>
	</form>
	<footer style="padding:5px;text-align:right">
		<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="submit11()">保存</a>
		<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="closeWin1()" style="border: 1px solid #1e7cfb;color: #1e7cfb;">取消</a>
	</footer>
</div>


<!-- 查看详情模态窗（任务） -->
<div id="detailWin" class="exui-window" style="display:none;" data-options="
	modal:true,
	minimizable:false,
	maximizable:false,
	resizable:false,
	collapsible:false,
	closed:true">
	<ul class="nav nav-lattice" style="display:none">
		<li class="active"><a href="#xiangqing" data-toggle="tab">任务详情</a></li>
		<li><a href="#rizhi" data-toggle="tab">任务日志</a></li>
		<!-- <button onclick="closeDetailWin()" class="btn btn-default" style="margin-left:15px;border: 1px solid #1e7cfb;color: #1e7cfb;"><i class="glyphicon glyphicon-off"></i>关闭</button> -->
	</ul>
	<div class="tab-content" style="width:600px">
		<div id="xiangqing" class="tab-pane fade in active">
			<table id="detailTable">
				
			</table>
		</div>
		<div id="rizhi" class="tab-pane fade">
			
		</div>
	</div>
	<footer style="padding:5px;text-align:right">
		<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="closeDetailWin()" style="border: 1px solid #1e7cfb;color: #1e7cfb;"><i class="glyphicon glyphicon-off"></i>关闭</a>
	</footer>
</div>

<!-- 查看详情模态窗（测试包） -->
<div id="detailPkgWin" class="exui-window" style="display:none;width:880px;" data-options="
	modal:true,
	minimizable:false,
	maximizable:false,
	resizable:false,
	collapsible:false,
	closed:true">
	<ul class="nav nav-lattice">
		<li class="active"><a href="#testPkgDetail" data-toggle="tab">测试包详情</a></li>
		<li><a href="#testPkgDetailResult" data-toggle="tab">测试结果</a></li>
	</ul>
	<div class="tab-content">
		<div id="testPkgDetail" class="tab-pane fade in active" style="padding-top:5px;">
			<table id="detailPkgTable">
			
			</table>
		</div>
		<div id="testPkgDetailResult" class="tab-pane fade" style="padding-top:5px;">
			<div class="result-wrapper" >
			  <div class="result-content" >
			    <div style=" background-color:#a3d3fe; font-size: 1.5em;">总用例数</div>
			     <div style="color:#a3d3fe;font-size: 2em;" id="allCount">0</div>
			  </div><div class="result-content" >
			     <div style="background-color:#71CD71;font-size: 1.5em;">通过数</div>
			     <div style="color:#71CD71;font-size: 2em;" id="passCount">0</div>
			  </div><div class="result-content" >
			     <div style="background-color:#ED8282;font-size: 1.5em;">未通过数</div>
			     <div style="color:#ED8282;font-size: 2em;" id="failedCount">0</div>
			  </div><div class="result-content" >
			     <div style="background-color:#EEAA5A;font-size: 1.5em;">阻塞数</div>
			     <div style="color:#EEAA5A;font-size: 2em; " id="blockCount">0</div>
			  </div><div class="result-content" >
			      <div style="background-color:#B78EEA;font-size: 1.5em;">不适用数</div>
			     <div style="color:#B78EEA;font-size: 2em;" id="invalidCount">0</div>
			  </div><div class="result-content" >
			     <div style="background-color:#B7AB9C;font-size: 1.5em;">未测试数</div>
			     <div style="color:#B7AB9C;font-size: 2em;" id="noTestCount">0</div>
			  </div>
			</div>
		</div>
	</div>
	<footer style="padding:5px;text-align:right">
		<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="closePkgDetailWin()" style="border: 1px solid #1e7cfb;color: #1e7cfb;"><i class="glyphicon glyphicon-off"></i>关闭</a>
	</footer>
</div>

<!-- 查看详情模态窗（迭代） -->
<div id="detailIterationWin" class="exui-window" style="display:none;width:700px" data-options="
	modal:true,
	minimizable:false,
	maximizable:false,
	resizable:false,
	collapsible:false,
	closed:true">
	<ul class="nav nav-lattice" style="display:none">
		<li class="active"><a href="#iterationDetail" data-toggle="tab">迭代详情</a></li>
		<li><a href="#iterationReport" data-toggle="tab">迭代报告</a></li>
	</ul>
	<div class="tab-content">
		<div id="iterationDetail" class="tab-pane fade in active" style="padding-top:5px;padding-bottom:5px">
			<table id="detailIterationTable">
			
			</table>
		</div>
		<div id="iterationReport" class="tab-pane fade" style="padding-top:5px;padding-bottom:5px">
			<div style="float:left;width:100%;padding:5px;border:#FFFFFF 1px solid;border-radius:2px;background-color:#FFFFFF;height:278px;">
			<div style="width:100%;padding:5px 10px">
				<p style="padding-left:10px;border-left:3px red solid;margin-bottom: 15px;font-weight: bold;">用例摘要</p>
				<div style="width:12.5%;float:left;text-align:center">
					<span id="11" style="color:#1C86EE;font-size:2em">0</span><br/>总用例数
				</div>
				<div style="width:12.5%;float:left;text-align:center">
					<span id="22" style="color:green;font-size:2em">0</span><br/>通过数
				</div>
				<div style="width:12.5%;float:left;text-align:center">
					<span id="33" style="color:red;font-size:2em">0</span><br/>未通过数
				</div>
				<div style="width:12.5%;float:left;text-align:center">
					<span id="44" style="color:#8B8378;font-size:2em">0</span><br/>阻塞数
				</div>
				<div style="width:12.5%;float:left;text-align:center">
					<span id="55" style="color:#8B8378;font-size:2em">0</span><br/>未测试数
				</div>
				<div style="width:12.5%;float:left;text-align:center">
					<span id="66" style="color:#8B8378;font-size:2em">0</span><br/>不适用数
				</div>
				<br/>
				<p style="width:100%;border-top:1px #EAEAEA solid;margin-top: 70px;"></p>
				<p style="padding-left:10px;border-left:3px red solid;margin-bottom: 15px;font-weight: bold;">缺陷摘要</p>
				<div style="width:12.5%;float:left;text-align:center">
					<span id="aa" style="color:#1C86EE;font-size:2em">0</span><br/>总BUG数
				</div>
				<div style="width:12.5%;float:left;text-align:center">
					<span id="bb" style="color:green;font-size:2em">0</span><br/>有效BUG数
				</div>
				<div style="width:18%;float:left;text-align:center">
					<span id="cc" style="color:#8B8378;font-size:2em">0</span><br/>已关闭BUG数
				</div>
				<div style="width:15%;float:left;text-align:center">
					<span id="dd" style="color:#8B8378;font-size:2em">0</span><br/>待处理BUG数
				</div>
				<div style="width:15%;float:left;text-align:center">
					<span id="fixCount" style="color:#8B8378;font-size:2em">0</span><br/>已改未确认
				</div>
				<div style="width:15%;float:left;text-align:center">
					<span id="noBugCount" style="color:#8B8378;font-size:2em">0</span><br/>非错未确认
				</div>
				
				<p style="width:100%;border-top:1px #EAEAEA solid;margin-top: 100px;"></p>
				<p style="padding-left:10px;border-left:3px red solid;margin-bottom: 15px;font-weight: bold;">任务摘要</p>
				<div style="width:12.5%;float:left;text-align:center">
					<span id="allMissions" style="color:#1C86EE;font-size:2em">0</span><br/>总任务数
				</div>
				<div style="width:12.5%;float:left;text-align:center">
					<span id="attributionMissions" style="color:#cdc1c5;font-size:2em">0</span><br/>未开始
				</div>
				<div style="width:18%;float:left;text-align:center">
					<span id="runingMissions" style="color:#aaaaf6;font-size:2em">0</span><br/>进行中
				</div>
				<div style="width:15%;float:left;text-align:center">
					<span id="finishMissions" style="color:#a7daa7;font-size:2em">0</span><br/>完成
				</div>
				<div style="width:15%;float:left;text-align:center">
					<span id="terminationMissions" style="color:#f0bb7d;font-size:2em">0</span><br/>终止
				</div>
				<div style="width:15%;float:left;text-align:center">
					<span id="stopMissions" style="color:#ff82ab;font-size:2em">0</span><br/>暂停
				</div>
			</div>
		</div>
		</div>
	</div>
	<footer style="padding:5px;text-align:right">
		<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="closeIterationDetailWin()" style="border: 1px solid #1e7cfb;color: #1e7cfb;"><i class="glyphicon glyphicon-off"></i>关闭</a>
	</footer>
</div>

<!-- 新增/修改单其他任务模态窗 -->
<div id="addOrEditWin2" class="exui-window" style="display:none;width:700px;" data-options="
	modal:true,
	width: 580,
	minimizable:false,
	maximizable:false,
	resizable:false,
	closed:true">
	<form id="addOrEditForm2" method="post">
		<table class="form-table">
		    <!--  隐藏字段，新增，修改提交时传到后台 -->
			<tr class="hidden">
				<td>
					<input id="missioId" name="dto.otherMission.missionId"/>
					<input id="actualWorkload" name="dto.otherMission.actualWorkload"/>
					<input id="completionDegree" name="dto.otherMission.completionDegree"/>
					<input id="projectType" name="dto.otherMission.projectType"/>
					<input id="createTime" name="dto.otherMission.createTime"/>
					<input id="updateTime" name="dto.otherMission.updateTime"/>
					<input id="missionStatus" name="dto.otherMission.status"/>
					<input id="createUserId" name="dto.otherMission.createUserId"/>
					<input id="missionNum" name="dto.otherMission.missionNum"/>
					<input id="realStartTime" name="dto.otherMission.realStartTime"/>
				</td>
			</tr>
			<tr>
	    		<th><sup>*</sup>任务名称：</th>
	    		<td><input class="exui-textbox missionNa" name="dto.otherMission.missionName" data-options="required:true,validateOnCreate:false,validType:'length[1,32]'" style="width:180px"/></td>
	    	</tr>
	    	<tr>
	    		<th><sup>*</sup>任务描述：</th>
	    		<td><span style="width: 180px; height: 80px;display: inline-block;position: relative;"><textarea name="dto.otherMission.description" style="resize:none;width:475px;height: 80px;"></textarea></span></td>
	    	</tr>
			<tr>
				<th><sup>*</sup>执行人员：</th>
	    		<td><input id="peopleList" class="exui-combobox peopleList" data-options="
	    			required:true,
	    			validateOnCreate:false,
	    			valueField:'id',
	    			textField:'name',
	    			multiple:true,
	    			editable:false,
	    			prompt:'-请选择-'" style="width:180px"/></td>
	    			<th style="text-align:left"><img class="searchLogo" src="<%=request.getContextPath()%>/itest/images/mSearch.png" onclick="showSeletctPeopleWindow('peopleList')" style="cursor:pointer;margin-left:5px" title="选择人员"><!-- <a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="showSeletctPeopleWindow('peopleList')">选择人员</a> --></th>
	    			<!-- <th><sup>*</sup>负责人：</th>
	    			<td><input id="inchargePeople" class="exui-combobox inchargePeople" name="dto.otherMission.chargePersonId" data-options="
		    			required:true,
		    			validateOnCreate:false,
		    			valueField:'id',
		    			textField:'name',
	    				editable:false,
		    			prompt:'-请选择-'" style="width:180px"/></td> -->
	    	</tr>
	    	<tr>
				<th><sup>*</sup>负责人：</th>
	    			<td><input id="inchargePeople" class="exui-combobox inchargePeople" name="dto.otherMission.chargePersonId" data-options="
		    			required:true,
		    			validateOnCreate:false,
		    			valueField:'id',
		    			textField:'name',
	    				editable:false,
		    			prompt:'-请选择-'" style="width:180px"/></td>
	    	</tr>
	    	<tr>
				<th>关注者：</th>
	    		<td><input id="concernList" class="exui-combobox concernList" data-options="
	    			validateOnCreate:false,
	    			valueField:'id',
	    			textField:'name',
	    			multiple:true,
	    			editable:false,
	    			prompt:'-请选择-'" style="width:180px"/></td>
	    		<th style="text-align:left"><img class="searchLogo" src="<%=request.getContextPath()%>/itest/images/mSearch.png" onclick="showSeletctPeopleWindow('concernList')" style="cursor:pointer;margin-left:5px" title="选择人员"><!-- <a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="showSeletctPeopleWindow('concernList')">选择人员</a> --></th>
	    	</tr>
	    	<tr>
				<th>任务类别：</th>
	    		<td><input id="missionCategory" class="exui-combobox missionCategory" name="dto.otherMission.missionCategory" data-options="
	    			validateOnCreate:false,
	    			valueField:'typeId',
	    			textField:'typeName',
	    			editable:false,
	    			prompt:'-请选择-'" style="width:180px"/>
	    		</td>
	    		<th></th>
	    		<td style="display:none">
					<input class="checkPro" type="checkbox">
					<span>同时创建测试项目</span>
				</td>
	    		<!-- <th>任务类型：</th>
	    		<td><input id="missionType" class="exui-combobox" name="dto.otherMission.missionType" data-options="
	    			required:true,
	    			validateOnCreate:false,
	    			valueField:'value',
	    			textField:'desc',
	    			prompt:'-请选择-',
	    			data:[{desc:'其他任务',value:'0'}]" style="width:180px"/>
	    		</td> -->
	    	</tr>
	    	<tr>
				<th>所属项目：</th>
	    		<td>
		    		<!-- <input id="projectIds" class="exui-combobox projectIds" name="dto.otherMission.projectId" data-options="
		    			validateOnCreate:false,
		    			valueField:'projectId',
		    			textField:'projectName',
		    			editable:false,
		    			prompt:'-请选择-'" style="width:180px"/> -->
		    		<select id="projectIds" class="form-control chzn-select projectIds" name="dto.otherMission.projectId" onchange="changeProject(this)">
	    	
	    			</select>
	    		</td>
	    		<th><a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="addProject()">新增项目</a></th>
	    	</tr>
	    	<tr>
				<th>紧急程度：</th>
	    		<td><input id="emergencyDegree" class="exui-combobox emergencyDegree" name="dto.otherMission.emergencyDegree" data-options="
	    			validateOnCreate:false,
	    			valueField:'typeId',
	    			textField:'typeName',
	    			editable:false,
	    			prompt:'-请选择-'" style="width:180px"/>
	    		</td>
	    		<th>难易程度：</th>
	    		<td><input id="difficultyDegree" class="exui-combobox difficultyDegree" name="dto.otherMission.difficultyDegree" data-options="
	    			validateOnCreate:false,
	    			valueField:'typeId',
	    			textField:'typeName',
	    			editable:false,
	    			prompt:'-请选择-'" style="width:180px"/>
	    		</td>
	    	</tr>
	    	<tr>
	    		<th>预计开始日期：</th>
	    		<td><input class="exui-datebox" data-options="validateOnCreate:false,prompt:'-请选择-',editable:false" name="dto.otherMission.predictStartTime" style="width:180px"/></td>
	    		<th>预计结束日期：</th>
	    		<td><input class="exui-datebox" data-options="validateOnCreate:false,prompt:'-请选择-',editable:false" name="dto.otherMission.predictEndTime" style="width:180px"/></td>
	    	</tr>
	    	<tr>
	    		<th>标准工作量(小时)：</th>
	    		<td><input class="exui-textbox" name="dto.otherMission.standardWorkload" data-options="validateOnCreate:false,validType:'zhengshu',prompt:'请填写非负整数'" style="width:180px"/></td>
	    	</tr>
		</table>
	</form>
	<footer style="padding:5px;text-align:right">
		<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="submitOtherMission()">保存</a>
		<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="closeWin()" style="border: 1px solid #1e7cfb;color: #1e7cfb;">取消</a>
	</footer>
</div>


<!-- 新增项目模态窗 -->
<!-- <div id="addProject" class="exui-window" style="display:none;width:300px;" data-options="
	modal:true,
	width: 180,
	minimizable:false,
	maximizable:false,
	closed:true">
	<form id="addProjectForm" method="post">
		<table class="form-table">
			<tr>
				<th><sup>*</sup>项目名称：</th>
	    		<td><input class="exui-textbox" name="dto.project.projectName" data-options="required:true,validateOnCreate:false" style="width:130px"/></td>
	    	</tr>
		</table>
	</form>
	<footer style="padding:5px;text-align:right">
		<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="submitProject()">保存</a>
		<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="closeProjectWin()" style="border: 1px solid #1e7cfb;color: #1e7cfb;">取消</a>
	</footer>
</div> -->
<!-- 新增/修改单井模态窗 -->
<div id="addProject" class="exui-window" style="display:none;" data-options="
	modal:true,
	width: 700,
	height:300,
	minimizable:false,
	maximizable:false,
	closed:true">
		<form id="addProjectForm" method="post">
		  <table class="form-table">
			<tr>
				<th><sup>*</sup>项目编号：</th>
	    		<td><input class="exui-textbox projectNums" name="dto.singleTest.proNum" data-options="required:true,validateOnCreate:false" style="width:180px"/></td>
	    		<th id="project_name" style="padding-left: 12px;"><sup>*</sup>项目名称：</th>
	    		<td>
	    			<input class="exui-textbox hadProject" name="dto.singleTest.proName" data-options="required:true,validateOnCreate:false" style="width:180px"/>
	    		</td>
	    	</tr>
	    
	    	<tr>
	    		<th><sup>*</sup>研发部门：</th>
	    		<td><input class="exui-textbox developmentDep" name="dto.singleTest.devDept" data-options="required:true,validateOnCreate:false" style="width:180px"/></td>
	    		
	    		<th><sup>*</sup>PM姓名：</th>
	    		<td>
	    			<input type="hidden" id="psmId" class="psmId" name="dto.singleTest.psmId"/>
	    			<input type="hidden" id="pmId" class="pmId" name="dto.singleTest.psmId"/>
	    			<select id="pm" class="proStName pm" onchange="searchPm()" style="width:180px"></select>
	    		</td>
	    	</tr>
	    	
	    	<tr>
	    		<th><sup>*</sup>开始日期：</th>
	    		<td><input class="exui-datebox startTime" data-options="required:true,editable:false,validateOnCreate:false,prompt:'-请选择-'" name="dto.singleTest.planStartDate" style="width:180px"/></td>
	    		<th><sup>*</sup>结束日期：</th>
	    		<td><input class="exui-datebox endTime" data-options="required:true,editable:false,validateOnCreate:false,prompt:'-请选择-'" name="dto.singleTest.planEndDate" style="width:180px"/></td>
	    	</tr>
	    	
	    	<tr id="editProStatus" style="display: none;">
	    		<th>项目状态：</th>
	    		<td><input class="exui-combobox editSta" name="dto.singleTest.status" style="width:180px"/></td>
	    	</tr>
	      </table>
	    </form>
	    <footer style="padding:5px;text-align:right">
			<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" style="background-color: #42a5f5;" onclick="submitProject()">保存</a>
			<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" style="border: 1px solid #1E7CFB;color: #1E7CFB;" onclick="closeProjectWin()">取消</a>
		</footer>
</div>
<!-- <div id="addOrEditSinFooter" align="right" style="padding:5px;">
	<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" style="background-color: #42a5f5;" onclick="submitProject()">保存</a>
	<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" style="border: 1px solid #1E7CFB;color: #1E7CFB;" onclick="closeProjectWin()">取消</a>
</div> -->
<!-- 引入多选页面 -->
<%@include file="/itest/jsp/chooseMuiltPeople/chooseMuiltPeople.jsp" %>

<!-- <script type="text/javascript">
	$.parser.parse();
</script> -->

<script src="<%=request.getContextPath()%>/itest/js/otherMission/overview.js" type="text/javascript" charset="utf-8"></script>