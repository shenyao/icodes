<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/itest/plug-in/chosen/chosen.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/itest/css/singleTestTaskManager/testTaskMagrList.css"/>
<style type="text/css">
#detailTable tbody tr{
	height: 2em;
	border: 0.5px solid gray;
}
#detailTable tbody tr td{
	border-right: 0.5px solid gray;
}
th{
 	font-weight:400;
}
</style>
<%@page import="cn.com.codes.common.util.StringUtils"%>
<%@page import="cn.com.codes.framework.security.filter.SecurityContextHolder"%>
<%@page import="cn.com.codes.framework.security.filter.SecurityContext"%>
<%@page import="cn.com.codes.framework.security.Visit"%>  
<%@page import="cn.com.codes.framework.security.VisitUser"%>
<%@page import="cn.com.codes.framework.common.JsonUtil"%>
<%@page import="java.util.Set" %>
<%@page import="java.util.Iterator" %>
<%@page import="java.lang.String" %>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<!--top tools area-->
<div class="newWrapper">
<div class="tools">
	<div class="input-field" style="width: 160px;">
		<span>任务名称：</span>
		<input id="missionName" class="form-control indent-4-5" placeholder="名称"/>
	</div>
	<div class="input-field" style="width: 180px;">
	    <select id="projectNa" class="form-control chzn-select">
	    	
	    </select>
	</div>
	<div class="input-field" style="width: 180px;">
	    <select id="statu" class="form-control chzn-select">
	    	<option value="">-请选择状态-</option>
	    	<option value="0">未开始</option>
	    	<option value="1">进行中</option>
	    	<option value="2">完成</option>
	    	<option value="3">终止</option>
	    	<option value="4">暂停</option>
	    </select>
	</div>
	<button id="searchCon" type="button" class="btn btn-default" style="background: #1E7CFB;border: 1px solid #1E7CFB;color: #ffffff;" onclick="searchOtherMission()"><i class="glyphicon glyphicon-search"></i>查询</button>
	<!-- <button id="resetCon" type="button" class="btn btn-default" style="border: 1px solid #1E7CFB;color: #1E7CFB;" onclick="resetInfo()"><i class="glyphicon glyphicon-trash"></i>重置</button> -->
	<!-- <div style="float:right"> -->
	<button type="button" class="btn btn-default concerButton" style="border: 1px solid #1E7CFB;color: #1E7CFB;display:none" onclick="concernMissions()"><i class="glyphicon glyphicon-pencil"></i>关注</button>
	<button type="button" class="btn btn-default" style="border: 1px solid #1E7CFB;color: #1E7CFB;" onclick="showAddWin1()"><i class="glyphicon glyphicon-plus"></i>创建任务</button>
	<!-- </div> -->
</div><!--/.top tools area-->
<!-- 其他任务显示列表 -->
<table id="missionDg" data-options="
	fitColumns: true,
	singleSelect: true,
	pagination: true,
	pageNumber: 1,
	pageSize: 10,
	pageList:[10,30,50],
	layout:['list','first','prev','manual','next','last','refresh','info']
"></table>

</div>
<!-- 新增/修改单其他任务模态窗 -->
<div id="addOrEditWin1" class="exui-window" style="display:none;width:700px;" data-options="
	modal:true,
	width: 580,
	minimizable:false,
	maximizable:false,
	resizable:false,
	closed:true">
	<form id="addOrEditForm1" method="post">
		<table class="form-table">
		    <!--  隐藏字段，新增，修改提交时传到后台 -->
			<tr class="hidden">
				<td>
					<input id="missioId" name="dto.otherMission.missionId"/>
					<input id="actualWorkload" name="dto.otherMission.actualWorkload"/>
					<input id="completionDegree" name="dto.otherMission.completionDegree"/>
					<input id="projectType" name="dto.otherMission.projectType"/>
					<input id="createTime" name="dto.otherMission.createTime"/>
					<input id="updateTime" name="dto.otherMission.updateTime"/>
					<input id="missionStatus" name="dto.otherMission.status"/>
					<input id="createUserId" name="dto.otherMission.createUserId"/>
					<input id="missionNum" name="dto.otherMission.missionNum"/>
					<input id="realStartTime" name="dto.otherMission.realStartTime"/>
				</td>
			</tr>
			<tr>
	    		<th><sup>*</sup>任务名称：</th>
	    		<td><input class="exui-textbox missionNa" name="dto.otherMission.missionName" data-options="required:true,validateOnCreate:false" style="width:180px"/></td>
	    	</tr>
	    	<tr>
	    		<th><sup>*</sup>任务描述：</th>
	    		<td><span style="width: 180px; height: 80px;display: inline-block;position: relative;"><textarea name="dto.otherMission.description" style="resize:none;width:475px;height: 80px;"></textarea></span></td>
	    	</tr>
			<tr>
				<th><sup>*</sup>执行人员：</th>
	    		<td><input id="peopleList" class="exui-combobox peopleList" data-options="
	    			required:true,
	    			validateOnCreate:false,
	    			valueField:'id',
	    			textField:'name',
	    			multiple:true,
	    			editable:false,
	    			prompt:'-请选择-'" style="width:180px"/></td>
	    			<th style="text-align:left"><img class="searchLogo" src="<%=request.getContextPath()%>/itest/images/mSearch.png" onclick="showSeletctPeopleWindow('peopleList')" style="cursor:pointer;margin-left:5px" title="选择人员"><!-- <a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="showSeletctPeopleWindow('peopleList')">选择人员</a> --></th>
	    			<!-- <th><sup>*</sup>负责人：</th>
	    			<td><input id="inchargePeople" class="exui-combobox inchargePeople" name="dto.otherMission.chargePersonId" data-options="
		    			required:true,
		    			validateOnCreate:false,
		    			valueField:'id',
		    			textField:'name',
	    				editable:false,
		    			prompt:'-请选择-'" style="width:180px"/></td> -->
	    	</tr>
	    	<tr>
				<th><sup>*</sup>负责人：</th>
	    			<td><input id="inchargePeople" class="exui-combobox inchargePeople" name="dto.otherMission.chargePersonId" data-options="
		    			required:true,
		    			validateOnCreate:false,
		    			valueField:'id',
		    			textField:'name',
	    				editable:false,
		    			prompt:'-请选择-'" style="width:180px"/></td>
	    	</tr>
	    	<tr>
				<th>关注者：</th>
	    		<td><input id="concernList" class="exui-combobox concernList" data-options="
	    			validateOnCreate:false,
	    			valueField:'id',
	    			textField:'name',
	    			multiple:true,
	    			editable:false,
	    			prompt:'-请选择-'" style="width:180px"/></td>
	    		<th style="text-align:left"><img class="searchLogo" src="<%=request.getContextPath()%>/itest/images/mSearch.png" onclick="showSeletctPeopleWindow('concernList')" style="cursor:pointer;margin-left:5px" title="选择人员"><!-- <a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="showSeletctPeopleWindow('concernList')">选择人员</a> --></th>
	    	</tr>
	    	<tr>
				<th>任务类别：</th>
	    		<td><input id="missionCategory" class="exui-combobox missionCategory" name="dto.otherMission.missionCategory" data-options="
	    			validateOnCreate:false,
	    			valueField:'typeId',
	    			textField:'typeName',
	    			editable:false,
	    			prompt:'-请选择-'" style="width:180px"/>
	    		</td>
	    		<th></th>
	    		<td style="display:none">
					<input class="checkPro" type="checkbox">
					<span>同时创建测试项目</span>
				</td>
	    		<!-- <th>任务类型：</th>
	    		<td><input id="missionType" class="exui-combobox" name="dto.otherMission.missionType" data-options="
	    			required:true,
	    			validateOnCreate:false,
	    			valueField:'value',
	    			textField:'desc',
	    			prompt:'-请选择-',
	    			data:[{desc:'其他任务',value:'0'}]" style="width:180px"/>
	    		</td> -->
	    	</tr>
	    	<tr>
				<th>所属项目：</th>
	    		<td>
		    		<!-- <input id="projectIds" class="exui-combobox projectIds" name="dto.otherMission.projectId" data-options="
		    			validateOnCreate:false,
		    			valueField:'projectId',
		    			textField:'projectName',
		    			editable:false,
		    			prompt:'-请选择-'" style="width:180px"/> -->
		    		<select id="projectIds" class="form-control chzn-select projectIds" name="dto.otherMission.projectId" onchange="changeProject(this)">
	    	
	    			</select>
	    		</td>
	    		<th><a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="addProject()">新增项目</a></th>
	    	</tr>
	    	<tr>
				<th>紧急程度：</th>
	    		<td><input id="emergencyDegree" class="exui-combobox emergencyDegree" name="dto.otherMission.emergencyDegree" data-options="
	    			validateOnCreate:false,
	    			valueField:'typeId',
	    			textField:'typeName',
	    			editable:false,
	    			prompt:'-请选择-'" style="width:180px"/>
	    		</td>
	    		<th>难易程度：</th>
	    		<td><input id="difficultyDegree" class="exui-combobox difficultyDegree" name="dto.otherMission.difficultyDegree" data-options="
	    			validateOnCreate:false,
	    			valueField:'typeId',
	    			textField:'typeName',
	    			editable:false,
	    			prompt:'-请选择-'" style="width:180px"/>
	    		</td>
	    	</tr>
	    	<tr>
	    		<th>预计开始日期：</th>
	    		<td><input class="exui-datebox" data-options="validateOnCreate:false,prompt:'-请选择-',editable:false" name="dto.otherMission.predictStartTime" style="width:180px"/></td>
	    		<th>预计结束日期：</th>
	    		<td><input class="exui-datebox" data-options="validateOnCreate:false,prompt:'-请选择-',editable:false" name="dto.otherMission.predictEndTime" style="width:180px"/></td>
	    	</tr>
	    	<tr>
	    		<th>标准工作量(小时)：</th>
	    		<td><input class="exui-textbox" name="dto.otherMission.standardWorkload" data-options="validateOnCreate:false,validType:'zhengshu',prompt:'请填写非负整数'" style="width:180px"/></td>
	    	</tr>
		</table>
	</form>
	<footer style="padding:5px;text-align:right">
		<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="submit()">保存</a>
		<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="closeWin()" style="border: 1px solid #1e7cfb;color: #1e7cfb;">取消</a>
	</footer>
</div>

<!-- 查看详情模态窗 -->
<div id="detailWin" class="exui-window" style="display:none;" data-options="
	modal:true,
	minimizable:false,
	maximizable:false,
	resizable:false,
	collapsible:false,
	closed:true">
	<form>
	<ul class="nav nav-lattice" style="display:none">
		<li class="active"><a href="#xiangqing" data-toggle="tab">任务详情</a></li>
		<li><a href="#rizhi" data-toggle="tab">任务日志</a></li>
		<!-- <button onclick="closeDetailWin()" class="btn btn-default" style="margin-left:15px;border: 1px solid #1e7cfb;color: #1e7cfb;"><i class="glyphicon glyphicon-off"></i>关闭</button> -->
	</ul>
	<div class="tab-content" style="width:600px">
		<div id="xiangqing" class="tab-pane fade in active">
			<table id="detailTable">
				
			</table>
		</div>
		<div id="rizhi" class="tab-pane fade">
			<!-- <table id="logDg" data-options="
				fitColumns: true,
				singleSelect: true,
				pagination: true,
				pageNumber: 1,
				pageSize: 10,
				pageList:[10,30,50],
				layout:['list','first','prev','manual','next','last','refresh','info']
			"></table> -->
		</div>
	</div>
	</form>
	<footer style="padding:5px;text-align:right">
		<!-- <a onclick="closeDetailWin()" class="btn btn-default" style="margin-left:15px;border: 1px solid #1e7cfb;color: #1e7cfb;"><i class="glyphicon glyphicon-off"></i>关闭</a> -->
		<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="closeDetailWin()" style="border: 1px solid #1e7cfb;color: #1e7cfb;"><i class="glyphicon glyphicon-off"></i>关闭</a>
	</footer>
</div>

<!-- 新增项目模态窗 -->
<!-- <div id="addProject" class="exui-window" style="display:none;width:300px;" data-options="
	modal:true,
	width: 180,
	minimizable:false,
	maximizable:false,
	closed:true">
	<form id="addProjectForm" method="post">
		<table class="form-table">
			<tr>
				<th><sup>*</sup>项目名称：</th>
	    		<td><input class="exui-textbox" name="dto.project.projectName" data-options="required:true,validateOnCreate:false" style="width:130px"/></td>
	    	</tr>
		</table>
	</form>
	<footer style="padding:5px;text-align:right">
		<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="submitProject()">保存</a>
		<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="closeProjectWin()" style="border: 1px solid #1e7cfb;color: #1e7cfb;">取消</a>
	</footer>
</div> -->
<!-- 新增/修改单井模态窗 -->
<div id="addProject" class="exui-window" style="display:none;" data-options="
	modal:true,
	width: 700,
	height:300,
	minimizable:false,
	maximizable:false,
	closed:true">
		<form id="addProjectForm" method="post">
		  <table class="form-table">
			<tr>
				<th><sup>*</sup>项目编号：</th>
	    		<td><input class="exui-textbox projectNums" name="dto.singleTest.proNum" data-options="required:true,validateOnCreate:false" style="width:180px"/></td>
	    		<th id="project_name" style="padding-left: 12px;"><sup>*</sup>项目名称：</th>
	    		<td>
	    			<input class="exui-textbox hadProject" name="dto.singleTest.proName" data-options="required:true,validateOnCreate:false" style="width:180px"/>
	    		</td>
	    	</tr>
	    
	    	<tr>
	    		<th><sup>*</sup>研发部门：</th>
	    		<td><input class="exui-textbox developmentDep" name="dto.singleTest.devDept" data-options="required:true,validateOnCreate:false" style="width:180px"/></td>
	    		
	    		<th><sup>*</sup>PM姓名：</th>
	    		<td>
	    			<input type="hidden" id="psmId" class="psmId" name="dto.singleTest.psmId"/>
	    			<input type="hidden" id="pmId" class="pmId" name="dto.singleTest.psmId"/>
	    			<select id="pm" class="proStName pm" onchange="searchPm()" style="width:180px"></select>
	    		</td>
	    	</tr>
	    	
	    	<tr>
	    		<th><sup>*</sup>开始日期：</th>
	    		<td><input class="exui-datebox startTime" data-options="required:true,editable:false,validateOnCreate:false,prompt:'-请选择-'" name="dto.singleTest.planStartDate" style="width:180px"/></td>
	    		<th><sup>*</sup>结束日期：</th>
	    		<td><input class="exui-datebox endTime" data-options="required:true,editable:false,validateOnCreate:false,prompt:'-请选择-'" name="dto.singleTest.planEndDate" style="width:180px"/></td>
	    	</tr>
	    	
	    	<tr id="editProStatus" style="display: none;">
	    		<th>项目状态：</th>
	    		<td><input class="exui-combobox editSta" name="dto.singleTest.status" style="width:180px"/></td>
	    	</tr>
	      </table>
	    </form>
	    <footer style="padding:5px;text-align:right">
			<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" style="background-color: #42a5f5;" onclick="submitProject()">保存</a>
			<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" style="border: 1px solid #1E7CFB;color: #1E7CFB;" onclick="closeProjectWin()">取消</a>
		</footer>
</div>
<!-- <div id="addOrEditSinFooter" align="right" style="padding:5px;">
	<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" style="background-color: #42a5f5;" onclick="submitProject()">保存</a>
	<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" style="border: 1px solid #1E7CFB;color: #1E7CFB;" onclick="closeProjectWin()">取消</a>
</div> -->
<!-- 引入多选页面 -->
<%@include file="/itest/jsp/chooseMuiltPeople/chooseMuiltPeople.jsp" %>

<!-- <script type="text/javascript">
	$.parser.parse();
</script> -->
<%
SecurityContext sc = SecurityContextHolder.getContext();
Visit visit = sc.getVisit();
VisitUser user =  null;
if(visit!=null){
	user = visit.getUserInfo(VisitUser.class);
}

String isAdmin = (null != user.getIsAdmin()) ? user.getIsAdmin().toString() : "";
String jsfileVar ="otherMissionAll2.js";
if(isAdmin.equals("1")||isAdmin.equals("2")){
	jsfileVar = "otherMissionAll.js";
}
%>
<script src="<%=request.getContextPath()%>/itest/js/otherMission/<%=jsfileVar%>" type="text/javascript" charset="utf-8"></script>