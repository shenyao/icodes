<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%-- <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/itest/plug-in/chosen/chosen.css"/> --%>
<%-- <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/itest/css/testCaseManager/testCaseManager.css"/> --%>
<style type="text/css">
/*******************************
	    自定义type=file文件上传样式
	*******************************/
.file-box {
	/* display: inline-block; */
	position: relative;
	/* cursor: pointer; */
}

.file-box input:first-child {
	width: calc(100% - 74px);
	min-width: 1em;
}

.file-box button {
	border: 0;
	background-color: #57a3f1;
	height: 34px;
	width: 68px;
	color: #ffffff;
	text-align: center;
	vertical-align: middle;
	margin-left: 6px;
	padding: 0 15px;
	white-space: nowrap;
}

.file-box input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	filter: alpha(opacity : 0);
	opacity: 0;
	height: 34px;
	width: -webkit-calc(100% - 6px);
	width: -moz-calc(100% - 6px);
	width: calc(100% - 6px);
	min-width: 68px;
	cursor: pointer;
}

.left_td {
	text-align: right;
	padding-right: 0.5em;
	width: 110px;
	word-spacing: normal;
	white-space: nowrap;
}
</style>
<div class="newWrapper">
<div class="tools">
	<button type="button" id="uploadTarImageBtn" class="btn btn-default"
		style="border: 1px solid #1E7CFB; color: #1E7CFB;"
		onclick="showUploadImageWin();">
		<i class="glyphicon glyphicon-plus"></i>上传tar镜像
	</button>
	<button type="button" id="uploadDockerfileBtn" class="btn btn-default"
		style="border: 1px solid #1E7CFB; color: #1E7CFB;"
		onclick="showUploadDockerfileWin();">
		<i class="glyphicon glyphicon-plus"></i>上传Dockerfile
	</button>
	<button type="button" id="certManagerBtn" class="btn btn-default"
		style="border: 1px solid #1E7CFB; color: #1E7CFB;"
		onclick="certManager();">
		<i class="glyphicon glyphicon-plus"></i>Docker容器证书管理
	</button>
	<button type="button" id="uploadCertBtn" class="btn btn-default"
		style="border: 1px solid #1E7CFB; color: #1E7CFB; display: none;"
		onclick="showUploadCertWin();">
		<i class="glyphicon glyphicon-plus"></i>上传证书
	</button>
	<button type="button" id="returnEnvBtn" class="btn btn-default"
		style="border: 1px solid #1E7CFB; color: #1E7CFB; display: none;"
		onclick="returnEnv();">
		<i class="glyphicon glyphicon-minus"></i>返回
	</button>
</div>

<table id="envListTab"
	data-options="
	fitColumns: true,
	singleSelect: true,
	pagination: true,
	pageNumber: 1,
	pageSize: 10,
	layout:['list','first','prev','manual','next','last','refresh','info']
"></table>

<table id="certManagerListTab"
	data-options="
	fitColumns: true,
	singleSelect: true,
	pagination: true,
	pageNumber: 1,
	pageSize: 10,
	layout:['list','first','prev','manual','next','last','refresh','info']
"></table>
</div>
<div id="uploadImageFooter" align="right" style="padding: 5px; display: none;">
	<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'"
		onclick="uploadTarImageFile();">上传</a> <a class="exui-linkbutton"
		data-options="btnCls:'default',size:'xs'"
		style="border: 1px solid #1e7cfb; color: #1e7cfb;" onclick="javascript:$('#uploadImageWin').xwindow('close');">取消</a>
</div>

<!-- 上传镜像 -->
<div id="uploadImageWin" class="exui-window" style="display: none;"
	data-options="
	modal:true,
	width: 680,
	footer:'#uploadImageFooter',
	minimizable:false,
	maximizable:false,
	closed:true">
	<form enctype="multipart/form-data">
		<div class="form-group">
			<div class="file-box">
				<input id="uploadImageFileId" type="file" class="file" accept=".tar">
			</div>
			<div>
				<label style="color: red;">镜像文件必须是tar文件类型</label>
			</div>
		</div>
	</form>
	<form id="uploadImageForm" method="post">
		<table class="form-table" style="width: 100%;">
			<tr>
				<td class="left_td"><sup>*</sup>镜像名称：</td>
				<td><input class="exui-textbox" id="imageName" name="imageName" data-options="required:true"
					style="width: 100%;" /></td>
			</tr>
			<!-- <tr>
				<td class="left_td">DOCKER服务器IP：</td>
				<td><input class="exui-textbox" id="serverIp" name="serverIp"
					style="width: 100%;" /></td>
			</tr> -->

			<tr>
				<td class="left_td">镜像描述：</td>
				<td><input class="exui-textbox" id="imageDesc" name="imageDesc"
					style="width: 100%;" /></td>
			</tr>

			<tr>
				<td class="left_td">备注：</td>
				<td><input class="exui-textbox" id="imageRemarks"
					name="imageRemarks" style="width: 100%;" /></td>
			</tr>
		</table>
	</form>
</div>

<div id="uploadDockerfileFooter" align="right" style="padding: 5px; display: none;">
	<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'"
		onclick="uploadBuildImageFile();">上传</a> <a class="exui-linkbutton"
		data-options="btnCls:'default',size:'xs'"
		style="border: 1px solid #1e7cfb; color: #1e7cfb;" onclick="javascript:$('#uploadDockerfileWin').xwindow('close');">取消</a>
</div>

<!-- Dockerfile上传镜像 -->
<div id="uploadDockerfileWin" class="exui-window" style="display: none;"
	data-options="
	modal:true,
	width: 680,
	footer:'#uploadDockerfileFooter',
	minimizable:false,
	maximizable:false,
	closed:true">
	<form enctype="multipart/form-data">
		<div class="form-group">
			<div class="file-box">
				<input id="buildImageFileId" type="file" class="file" accept=".tar.gz">
			</div>
			<div>
				<label style="color: red;">Dockerfile名称必须以：镜像名称_版本号命名并以tar.gz为文件类型！</label>
			</div>
		</div>
	</form>
	<form id="uploadDockerfileForm" method="post">
		<table class="form-table" style="width: 100%;">
			<tr>
				<td class="left_td"><sup>*</sup>镜像名称：</td>
				<td><input class="exui-textbox" id="imageName" name="imageName" data-options="required:true"
					style="width: 100%;" /></td>
			</tr>
			<!-- <tr>
				<td class="left_td">DOCKER服务器IP：</td>
				<td><input class="exui-textbox" id="serverIp" name="serverIp"
					style="width: 100%;" /></td>
			</tr> -->
			<tr>
				<td class="left_td">镜像描述：</td>
				<td><input class="exui-textbox" id="imageDesc" name="imageDesc"
					style="width: 100%;" /></td>
			</tr>
			<tr>
				<td class="left_td">备注：</td>
				<td><input class="exui-textbox" id="imageRemarks"
					name="imageRemarks" style="width: 100%;" /></td>
			</tr>
		</table>
	</form>
</div>

<div id="buildImageFooter" align="right" style="padding: 5px; display: none;">
	<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'"
		onclick="buildImage();">构建镜像</a> <a class="exui-linkbutton"
		data-options="btnCls:'default',size:'xs'"
		style="border: 1px solid #1e7cfb; color: #1e7cfb;" onclick="javascript:$('#buildImageWin').xwindow('close');">取消</a>
</div>

<!-- 构建镜像 -->
<div id="buildImageWin" class="exui-window" style="display: none;"
	data-options="
	modal:true,
	width: 680,
	footer:'#buildImageFooter',
	minimizable:false,
	maximizable:false,
	closed:true">
	<form id="buildImageForm" method="post">
		<input type="hidden" id="id" name="id"/>
		<input type="hidden" id="fileType" name="fileType"/>
		<input type="hidden" id="uploader" name="uploader"/>
		<input type="hidden" id="imageFileName" name="imageFileName">
		<input type="hidden" id="imageTag" name="imageTag">
		<table class="form-table" style="width: 100%;">
			<tr>
				<td class="left_td">镜像文件：</td>
				<td><input class="exui-textbox" id="imageFile" name="imageFile"
					style="width: 100%;" disabled="disabled"/></td>
			</tr>
			<tr>
				<td class="left_td"><sup>*</sup>镜像名称：</td>
				<td><input class="exui-textbox" id="imageName" name="imageName" data-options="required:true"
					style="width: 100%;" /></td>
			</tr>
			<tr>
				<td class="left_td"><sup>*</sup>docker 容器 API证书：</td>
				<td>
					<input type="radio" id="buildyApiCert" name="apiCert" value="1" onclick="buildYApiCert();"/> 证书认证
					<input type="radio" id="buildnoApiCert" name="apiCert" value="0" onclick="buildNoApiCert();"/> 无认证
				</td>
			</tr>
			<tr>
				<!-- <td class="left_td"><sup>*</sup>DOCKER服务器IP：</td>
				<td><input class="exui-textbox" id="serverIp" name="serverIp" data-options="required:true"
					style="width: 100%;" /></td> -->
				<td class="left_td"><sup>*</sup>docker容器IP：</td>
				<td>
				<input class="exui-textbox" id="bnServerIp" name="bnServerIp"
					style="width: 100%;"/>
				<input id="byServerIp" class="exui-combobox bCertServerIp" data-options="
		    		validateOnCreate:false,
		    		valueField:'serverIp',
		    		textField:'serverIp',
		    		multiple:false,
		    		editable:false" style="width:100%;"/>
		    	</td>
			</tr>
			<tr>
				<td class="left_td">镜像描述：</td>
				<td><input class="exui-textbox" id="imageDesc" name="imageDesc"
					style="width: 100%;" /></td>
			</tr>
			<tr>
				<td class="left_td">备注：</td>
				<td><input class="exui-textbox" id="imageRemarks"
					name="imageRemarks" style="width: 100%;" /></td>
			</tr>
		</table>
	</form>
</div>

<div id="deployAppFooter" align="right" style="padding: 5px; display: none;">
	<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'"
		onclick="deployApp();">安装</a> <a class="exui-linkbutton"
		data-options="btnCls:'default',size:'xs'"
		style="border: 1px solid #1e7cfb; color: #1e7cfb;" onclick="javascript:$('#deployAppWin').xwindow('close');">取消</a>
</div>

<!-- 部署应用 -->
<div id="deployAppWin" class="exui-window" style="display: none;"
	data-options="
	modal:true,
	width: 680,
	footer:'#deployAppFooter',
	minimizable:false,
	maximizable:false,
	closed:true">
	<form id="deployAppForm" method="post">
		<input type="hidden" id="tid" name="tid"/>
		<table class="form-table" style="width: 100%;">
			<tr>
				<td class="left_td"><sup>*</sup>镜像名称：</td>
				<td><input class="exui-textbox" id="imageName" name="imageName" data-options="required:true"
					style="width: 100%;" /></td>
			</tr>

			<tr>
				<td class="left_td"><sup>*</sup>应用名称：</td>
				<td><input class="exui-textbox" id="appName" name="appName" data-options="required:true"
					style="width: 100%;" /></td>
			</tr>

			<tr>
				<td class="left_td">访问IP：</td>
				<td><input class="exui-textbox" id="hostIp"
					name="hostIp" style="width: 100%;" /></td>
			</tr>
			<tr>
				<td class="left_td">容器端口：</td>
				<td><input class="exui-textbox" id="containersPort"
					name="containersPort" style="width: 100%;" /></td>
			</tr>
			<tr>
				<td class="left_td">访问端口：</td>
				<td><input class="exui-textbox" id="hostPort"
					name="hostPort" style="width: 100%;" /></td>
			</tr>
			<tr>
				<td class="left_td">环境变量：</td>
				<td><input class="exui-textbox" id="envVar"
					name="envVar" style="width: 100%;" /></td>
			</tr>
			<tr id="dApiTr">
				<td class="left_td"><sup>*</sup>docker容器 API证书：</td>
				<td>
					<input type="radio" id="deployyApiCert" name="dApiCert" value="1" onclick="deployYApiCert();"/> 证书认证
					<input type="radio" id="deploynoApiCert" checked="checked" name="dApiCert" value="0" onclick="deployNoApiCert();"/> 无认证
				</td>
			</tr>
			<tr>
				<!-- <td class="left_td"><sup>*</sup>DOCKER服务器IP：</td>
				<td><input class="exui-textbox" id="serverIp" name="serverIp" data-options="required:true"
					style="width: 100%;" disabled="disabled"/></td> -->
				<td class="left_td"><sup>*</sup>docker容器IP：</td>
				<td>
					<input class="exui-textbox" id="dnServerIp" name="dnServerIp" style="width: 100%;"/>
					<input id="dyServerIp" class="exui-combobox dCertServerIp" data-options="
		    			validateOnCreate:false,
		    			valueField:'serverIp',
		    			textField:'serverIp',
		    			multiple:false,
		    			editable:false" style="width:100%;"/></td><!-- required:true, -->
				</td>
			</tr>
			<tr>
				<td class="left_td">关联容器：</td>
				<td><input class="exui-textbox" id="link"
					name="link" style="width: 100%;" /></td>
			</tr>
		</table>
	</form>
</div>

<!-- 上传证书 -->
<div id="uploadCertWin" class="exui-window" style="display: none;"
	data-options="
	modal:true,
	width: 680,
	footer:'#uploadCertFooter',
	minimizable:false,
	maximizable:false,
	closed:true">
	<form enctype="multipart/form-data">
		<div class="form-group">
			<div class="file-box">
				<input id="uploadCertFileId" type="file" class="file">
			</div>
		</div>
	</form>
	<form id="uploadCertForm" method="post">
		<table class="form-table" style="width: 100%;">
			<tr>
				<td class="left_td">docker容器IP：</td>
				<td><input class="exui-textbox" id="serverIp" name="serverIp" data-options="required:true"
					style="width: 100%;" /></td>
			</tr>

			<tr>
				<td class="left_td">证书密码：</td>
				<td><input type="password" class="exui-textbox" id="certPwd" name="certPwd" data-options="required:true"
					style="width: 100%;" /></td>
			</tr>
		</table>
	</form>
</div>

<div id="uploadCertFooter" align="right" style="padding: 5px; display: none;">
	<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'"
		onclick="uploadCertFile();">上传</a> <a class="exui-linkbutton"
		data-options="btnCls:'default',size:'xs'"
		style="border: 1px solid #1e7cfb; color: #1e7cfb;" onclick="javascript:$('#uploadCertWin').xwindow('close');">取消</a>
</div>
<script src="<%=request.getContextPath()%>/itest/js/env/envManager.js"
	type="text/javascript" charset="utf-8"></script>
