﻿<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
	<HEAD>
		<TITLE>授权信息</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<link rel='STYLESHEET' type='text/css'href='<%=request.getContextPath()%>/css/page.css'>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>			
	</HEAD>
	<BODY  style="overflow-x:hidden;overflow-y:hidden;">
	<script type="text/javascript">
	</script>
		<div id="createDiv" class="cycleTask gridbox_light" style="border:0px;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="fdCreateForm"name="fdCreateForm" namespace="" action="">
				<table border="0" id="createTable" class="obj row20px" cellspacing="0" align="center" cellpadding="0" border="0" width="720">
					<tr class="ev_mypm"> 
						<td colspan="6" class="tdtxt" align="center" width="720" style="border-right:0">
							<div id="cUMTxt" align="center"
								style="color: Blue"></div>
						</td>
					</tr>
					<tr  class="odd_mypm">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0" nowrap>
							
						</td>
						<td class="tdtxt" colspan="5"style="background-color: #f7f7f7;  width: 640;border-right:0">
							<ww:textarea id="licenseInfo" readonly="true" name="licenseInfo" cssClass="text_c" cssStyle="width:640;padding:2 0 0 4;color=blue" rows="10" ></ww:textarea>
						</td>
					</tr>
					</ww:form>
					<tr  class="ev_mypm">
					<td class="tdtxt" align="center" width="720" colspan="6" style="border-right:0">
					<a class="bluebtn" href="javascript:void(0);"onclick="parent.licenseW_ch.setModal(false);parent.licenseW_ch.hide();try{parent.proW_ch.show();parent.proW_ch.bringToTop()}catch(err){}"style="margin-left: 6px"><span> 返回</span> </a>
					  <a class="bluebtn" href="javascript:void(0);" id="saveBtn" onclick="parent.parent.setActiveCodeInit();" style="margin-left: 6px;"><span>激活</span> </a>
					   <a class="bluebtn" href="javascript:void(0);" id="saveBtn" onclick="parent.parent.rlAppInit('reptLicenseApp');" style="margin-left: 6px;"><span>申请度量激活码</span> </a>
					 <a class="bluebtn" href="javascript:void(0);" id="saveBtn" onclick="parent.parent.ulAppInit('userLicenseApp');" style="margin-left: 6px;"><span>申请扩容激活码</span> </a>
					<td>
					</tr>
			</table>
		</div>
		</div>
		<script type="text/javascript">
			var licenseInfo = "    MYPM免费版最大支持50个在线用户,无时间和项目数限制，可永久使用。如需扩大在线用户数请联系我们购买扩容激活码，";
			licenseInfo = licenseInfo +"只需2000元即可获得100在线用户，3000元可获得200在线用户,4000元可获得400在线用户,10000元可获得1000在线用户。";
			licenseInfo = licenseInfo +"购买扩容激活码后，所获得的在线用户数，无时间和项目数限制，且在以后的免费版升级版本中，将一直保有己购买的在线用户数。";
			licenseInfo = licenseInfo +"如使用过程中，不管是否购买了扩容激活码，给我们积极的反馈或建议，将视情况奖励一定在线用户数，并在MYPM公告栏中公示所提建议或反馈。";
			licenseInfo = licenseInfo +"使用中，不得删除登录页上用于记录访问量的统计代码。";
			licenseInfo = licenseInfo +"  另外，MYPM免费版限制性开放测试度量功能，可免费查阅度量数据1000次，如想无限制查看测试度量，需3000元购买度量激活码，达到查看上限不激活测试度量，不影响其他功能的使用；";
			licenseInfo = licenseInfo +"只要激活了测试度量，后继升级时，无需再激活。";
			document.getElementById("licenseInfo").value=licenseInfo;
		</script>
	</BODY>

</HTML>
