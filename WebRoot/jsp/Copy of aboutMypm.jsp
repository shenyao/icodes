﻿<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>

<HTML>

	<HEAD>
		<TITLE>关于MYPM</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>

	</HEAD>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"
		bgcolor="#ffffff" style="text-align: center">
		<br>
		<center>
			<div
				style="margin: 0 auto; text-align: center; width: 685px; height: 260px; background: url(<%=request.getContextPath()%>/jsp/common/images/error_bj.gif );"></div>
			<div align="center" width="100%" style="margin-top: -200px;">
				<table width="100%" border="0" align="center" cellpadding="0"
					cellspacing="0">
					<tr>
						<td>
							<font color="blue">
								&nbsp;&nbsp;“MYPM”是一套专注于IT领域，专门为IT企业定制的集成化项目管理平台，她集成了计划、任务
							</font>
						</td>
					</tr>
					<tr>
						<td>
							<font color="blue">
								&nbsp;及进度管理，工时成本管理，知识管理，质量管理，文档管理，搜索和SVN。
							</font>
						</td>
					</tr>

					<tr height="10px">
					  <td></td>
					</tr>
					<tr>
						<td>
							<font color="red">
								&nbsp;&nbsp;&nbsp;当前版本为仅开放测试管理的永久免费版V2.3，禁止以任何形式作为商业用途使用。必须保留
							</font>
						</td>
					</tr>
					<tr>
						<td>
							<font color="red">
								&nbsp;所有本软件拷贝上的著作权标识。申请试用完整版，将获得三个月完整版的使用权。<a href="javascript:void(0);" onclick="parent.showProDetl('license')"  >授权信息</a>
							</font>
						</td>
					</tr>
					<tr height="10px">
					  <td></td>
					</tr>
					<tr>
						<td>
							<font color="blue">
								&nbsp;&nbsp;&nbsp;本软件产品版权属于北京嘉华通软科技有限公司所有，不得利用任何方法取得、使用本软件程
							</font>
						</td>
					</tr>
					<tr>
						<td>
							<font color="blue">
								&nbsp;序代码。其他一切未申明的权利北京嘉华通软科技有限公司均予保留。
							</font>
						</td>
					</tr>
					<tr height="10px">
					  <td></td>
					</tr>
					<tr>
						<td>
							<font color="blue">
								&nbsp;&nbsp;&nbsp;网址 www.mypm.cc ，QQ群 103076756，MYPM论坛bbs.mypm.cc，QQ 351482310 31931880
							</font>
						</td>
					</tr>
					<tr>
						<td>
							<font color="blue">
								&nbsp; 
							</font>
						</td>
					</tr>																	
				</table>
			</div>
			</div>
		</center>
	</body>
	<script type="text/javascript" language="javascript">
	<ww:if test="#request.EXP_INFO.message=='overdue'"> 
        }else{
			window.parent.location="<%=request.getContextPath()%>/jsp/userManager/login.jsp";
		}
	</ww:if>  
</script>
</HTML>
